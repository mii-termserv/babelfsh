package de.mii_termserv.babelfsh.fsh_rule_parsing

import ca.uhn.fhir.context.FhirContext
import de.mii_termserv.babelfsh.antlr.service.fsh.file.FshGrammarService
import de.mii_termserv.babelfsh.antlr.service.fsh.file.FshParseResult
import de.mii_termserv.babelfsh.antlr.service.fsh.file.GraphToJsonBuilder
import de.mii_termserv.babelfsh.antlr.service.fsh.rules.FshPathParserService
import de.mii_termserv.babelfsh.api.BabelfshContext
import io.kotest.core.spec.style.FunSpec
import io.kotest.matchers.collections.shouldHaveSize
import io.kotest.matchers.ints.shouldBeGreaterThan
import io.kotest.matchers.maps.shouldHaveKey
import io.kotest.matchers.should
import io.kotest.matchers.shouldBe
import io.kotest.matchers.shouldNotBe
import kotlinx.serialization.encodeToString
import org.antlr.v4.runtime.CharStreams

class FshDatatypesTest : FunSpec({

    val inputFshSpec = """
        RuleSet: DateTimes
        * ^date.full = 2021-01-01T12:00:00Z
        * ^date.year = 2021
        * ^date.month = 2021-01
        * ^date.day = 2021-01-01
        
        CodeSystem: TestCodeSystem
        Id: test-codesystem
        Title: "Test CodeSystem"
        Description: "This is a test code system"
        * ^stringValue = "Hello World"
        * ^numberValue = 42
        * insert DateTimes
        * ^time.full = 12:00:00.0Z
        * ^time.hour = 12
        * ^time.minute = 12:00
        * ^time.second = 12:00:00
        * ^time.millisecond = 12:00:00.0
        * ^time.microsecond = 12:00:00.000
        * ^time.offset.positive = 12:00:00.000+01:00
        * ^time.offset.negative = 12:00:00.000-01:00
        /*^babelfsh
          csv
        ^babelfsh*/
    """.trimIndent()

    fun getFshContent(): FshParseResult {
        val charStream = CharStreams.fromString(inputFshSpec)
        val grammarService = FshGrammarService(null, charStream, FshParseResult())
        return grammarService.parseForTerminologyContent()
    }

    test("Fsh file is parsed") {
        val content = getFshContent()
        content shouldNotBe null
        content.codeSystems shouldHaveSize 1
        content.codeSystems.first() should {
            it.nameFromFshItem shouldBe "TestCodeSystem"
            it.idFromFshItem shouldBe "test-codesystem"
            it.title shouldBe "Test CodeSystem"
            it.description shouldBe "This is a test code system"
            it.referencedRuleSets shouldHaveSize 1
            it.rules.size shouldBeGreaterThan 0
            it.pluginDefinedIn shouldNotBe null
        }
    }

    test("resource is parsed to JSON") {
        val content = getFshContent().resolveAliases().resolveInsertsAndParameters()
        val codeSystem = content.codeSystems.first()
        val graph = FshPathParserService(codeSystem.rules).resolvePaths()
        val json = GraphToJsonBuilder(graph, codeSystem).buildJson()
        json shouldNotBe null
        println(babelfshContext.json.encodeToString(json))
        json should {
            it.size shouldBeGreaterThan 0
            it shouldHaveKey "resourceType"
            it shouldHaveKey "date"
            it shouldHaveKey "time"
        }
    }

}) {
    companion object {
        private val babelfshContext = BabelfshContext(
            r4Context = FhirContext.forR4B(),
            r5Context = FhirContext.forR5(),
            releaseVersion = BabelfshContext.ReleaseVersion.R4B,
            jsonPrettyPrint = true
        )
    }
}