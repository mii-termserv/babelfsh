package de.mii_termserv.babelfsh.fsh_rule_parsing

import de.mii_termserv.babelfsh.antlr.service.fsh.file.TsResourceData
import de.mii_termserv.babelfsh.antlr.service.fsh.rules.FshPathParserService
import de.mii_termserv.babelfsh.antlr.service.fsh.rules.ParseTreeItem
import de.mii_termserv.babelfsh.api.ResourceType
import io.kotest.core.spec.style.FunSpec
import io.kotest.matchers.collections.shouldHaveSize
import io.kotest.matchers.should
import io.kotest.matchers.shouldBe
import io.kotest.matchers.shouldNotBe

class FshRuleParsingTest : FunSpec({

    val inputText = """
        down.the.rabbit.hole.went.arrayAlice[+].how.does.she.get.out
        rootObject.arrayA[0].arrayAobjectA
        rootObject.arrayA[=].arrayAobjectB
        rootObject.arrayA[+].arrayAarrayA[+].arrayAarrayAobjectA
        rootObject.arrayA[=].arrayAarrayA[=].arrayAarrayAobjectB
        rootObject.arrayA[=].arrayAarrayA[+].arrayAarrayAobjectB
        rootObject.arrayA[=].arrayAarrayA[1].arrayAarrayAobjectC
        rootObject.arrayA[=].arrayAarrayA[=].arrayAarrayAobject.subobjectA.subobjectB
        rootObject.directObjectA.arrayB[+].arrayBobjectA
        rootObject.directObjectB
        rootObject.arrayB[+]
        rootObject.arrayB[+]
    """.trimIndent()

    test("Test FshRuleParsing") {
        val lines = inputText.split("\n")
        val parser = FshPathParserService(lines.mapIndexed { index, path ->
            path to TsResourceData.Rule(
                definitionContext = TsResourceData.RuleDefinitionContext(
                    ResourceType.RuleSet, "test", null, index
                ), ruleType = TsResourceData.Rule.RuleType.STRING,
                value = "test"
            )
        })
        val resolved = parser.resolvePaths()
        resolved.resolvedPaths shouldHaveSize lines.size
        resolved.nodeGraph.vertexSet().find {
            it.pathPart == "arrayAlice"
        } should {
            it shouldNotBe null
            it!!.nodeType shouldBe ParseTreeItem.NodeType.ARRAY
            resolved.nodeGraph.outDegreeOf(it) shouldBe 1
        }

        resolved.nodeGraph.vertexSet().find {
            it.fullPath == "rootObject.arrayA"
        } should {
            it shouldNotBe null
            it!!.nodeType shouldBe ParseTreeItem.NodeType.ARRAY
            resolved.nodeGraph.outDegreeOf(it) shouldBe 2
        }
    }

})