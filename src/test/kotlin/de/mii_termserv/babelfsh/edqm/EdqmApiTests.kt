package de.mii_termserv.babelfsh.edqm

import de.mii_termserv.babelfsh.configuration_settings.ConfigurationSettings
import de.mii_termserv.babelfsh.configuration_settings.HttpSettings
import de.mii_termserv.babelfsh.plugins.specific.edqm.EdqmApi
import de.mii_termserv.babelfsh.plugins.specific.edqm.EdqmApi.HeaderBuilder.Companion.edqmHmac
import io.kotest.assertions.withClue
import io.kotest.core.spec.style.FunSpec
import io.kotest.matchers.shouldBe
import io.kotest.matchers.string.shouldNotBeBlank
import io.ktor.http.*
import org.slf4j.LoggerFactory
import java.nio.charset.StandardCharsets
import javax.crypto.spec.SecretKeySpec

const val EDQM_API_KEY_VARIABLE = "EDQM_API_KEY"
const val EDQM_USERNAME_VARIABLE = "EDQM_USERNAME"

class EdqmApiTests : FunSpec({

    test("EDQM HMAC follows the specification") {
        @Suppress("SpellCheckingInspection") val secretKey =
            SecretKeySpec("mysecret".toByteArray(charset = StandardCharsets.UTF_8), "HmacSHA512")
        val message = "GET&/standardterms/api/v1/languages&standardterms.edqm.eu&Mon, 07 Feb 2022 14:20:00 GMT"
        val hmac = edqmHmac(message, secretKey)
        val expected = "Z3DZ5tAtcmHGjeA4MUQw=="
        hmac shouldBe expected
    }

    test("EDQM should accept the authorization") {

        if (System.getenv(EDQM_API_KEY_VARIABLE) == null || System.getenv(EDQM_USERNAME_VARIABLE) == null) {
            println("Skipping test because environment variables are not set")
            return@test
        }

        withClue("Please set the environment variables $EDQM_API_KEY_VARIABLE and $EDQM_USERNAME_VARIABLE") {
            System.getenv(EDQM_API_KEY_VARIABLE).shouldNotBeBlank()
            System.getenv(EDQM_USERNAME_VARIABLE).shouldNotBeBlank()
        }

        val logger = LoggerFactory.getLogger(javaClass)
        val headerBuilder = EdqmApi.HeaderBuilder(
            EDQM_USERNAME_VARIABLE,
            EDQM_API_KEY_VARIABLE,
            logger
        )
        val edqmApi = EdqmApi(headerBuilder, ConfigurationSettings(http = HttpSettings()))
        edqmApi.executeRequest("/languages").status shouldBe HttpStatusCode.OK
    }

})