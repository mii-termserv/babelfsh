grammar FshRule;

doc: fshRule+ EOF;

fshRule: STAR? CARET? (simpleFshRule | arrayFshRule) NEWLINE;

simpleFshRule: pathPart+;

// syntax highlighting shows a warning for this rule
// it is primarily a performance problem: https://www.antlr.org/api/JavaTool/org/antlr/v4/tool/ErrorType.html#EPSILON_OPTIONAL
arrayFshRule: arrayPrefix indexedPathPart
            | arrayPrefix indexedPathPart (indexedPathPart | pathPart)+;

arrayPrefix: (RULE_TOKEN DOT)*;
indexedPathPart: DOT? RULE_TOKEN arrayIndex;
pathPart: DOT? RULE_TOKEN;
arrayIndex: '[' (HARD_INDEX | SOFT_INDEX) ']';

RULE_TOKEN: [A-Za-z]+;
HARD_INDEX: [0-9]+;
SOFT_INDEX: [+=];


DOT: '.';
NEWLINE: '\n';
STAR: '*' WS+;
CARET: '^';
WS: ' ';