grammar BabelFSHCommandLine;

start_rule: comment | data;

comment: comment_intro (SPACE | OTHER_WS)+ data;
data: comment_content EOF;
comment_intro: TERM_PLUGIN_DESIGNATOR;

comment_content: plugin_id (SPACE | OTHER_WS)+ command_line;
plugin_id: PRIMITIVE_LOWERCASE_STRING;

command_line: (arg_list_entry (SPACE | OTHER_WS)*)+;

arg_list_entry
    : flagging
    | storing;

arg_name
    : SHORT_ARG_NAME #shortArg
    | LONG_ARG_NAME #longArg;

flagging: arg_name;

storing
    : arg_name '=' arg_value
    | arg_name SPACE arg_value;


arg_value
    : json_argument // this does not accept spaces
    | NUMBER
    | DOUBLE_QUOTED_STRING
    | SINGLE_QUOTED_STRING
    | PRIMITIVE_LOWERCASE_STRING
    | PRIMITIVE_STRING;

json_argument
    : json_object
    | json_array;

json_value
    : DOUBLE_QUOTED_STRING
    | NUMBER
    | json_object
    | json_array
    | 'true'
    | 'false'
    | 'null';

json_object: '{' json_pair (',' (SPACE | OTHER_WS)*? json_pair)* '}';
json_pair: DOUBLE_QUOTED_STRING ':' json_value;
json_array
    : '['  (SPACE | OTHER_WS)*?json_value (',' (SPACE | OTHER_WS)*? json_value)*  (SPACE | OTHER_WS)*? ']'
    | '['  (SPACE | OTHER_WS)*? ']';



SPACE: ' ';
OTHER_WS: [\t\r\n\f\u00A0]; //whitespace is matched, since we want to enforce whitespace rules in the parsing
COMMENT_START: '//';
TERM_PLUGIN_DESIGNATOR: COMMENT_START (SPACE | OTHER_WS)*? '^babelfsh';

SHORT_ARG_NAME: '-' [a-z];
LONG_ARG_NAME: '--' PRIMITIVE_STRING_WITH_NUMBERS;
SINGLE_QUOTED_STRING: '\'' .*? '\'';
DOUBLE_QUOTED_STRING: '"' .*? '"';

NUMBER
    : '-'? INT '.' INT EXPONENT?
    | '-'? INT EXPONENT
    | '-'? INT;
fragment INT: '0' | [1-9] [0-9]*;
fragment EXPONENT: [Ee] [+\-]? INT;
// the NUMBER rule is above PRIMITIVE_STRING to resolve ambiguity

PRIMITIVE_LOWERCASE_STRING: [a-z\-]+; // an unquoted string, only lower-case and hyphens
PRIMITIVE_STRING_WITH_NUMBERS: [a-z\-0-9]+; // a slightly-less-primitive string that also allows numbers
PRIMITIVE_STRING: [a-zA-Z0-9\-/._]+; // a even-slightly-less-primitive string that allows upper-case characters and numbers

