/*
  Copyright CSIRO Australian e-Health Research Centre (http://aehrc.com). All rights reserved. Use is subject to
  license terms and conditions.
 */
package au.csiro.fhir.owl;

import au.csiro.fhir.owl.util.GraphUtils;

import java.io.*;
import java.nio.file.Path;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import de.mii_termserv.babelfsh.api.content_containers.cs.*;
import de.mii_termserv.babelfsh.plugins.core.owl.OwlPluginArgs;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.semanticweb.elk.owlapi.ElkReasonerFactory;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.*;
import org.semanticweb.owlapi.model.parameters.Imports;
import org.semanticweb.owlapi.reasoner.Node;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import org.semanticweb.owlapi.reasoner.OWLReasonerFactory;
import org.semanticweb.owlapi.search.EntitySearcher;
import org.semanticweb.owlapi.util.SimpleIRIMapper;
import uk.ac.manchester.cs.jfact.JFactFactory;

/**
 * Main service.
 *
 * @author Alejandro Metke Jimenez (heavily modified by Joshua Wiedekopf)
 */
@SuppressWarnings("HttpUrlsUsage")
public class FhirOwlService {

    private static final Log log = LogFactory.getLog(FhirOwlService.class);

    private final Map<IRI, IRI> iriMap = new HashMap<>();
    private final OwlPluginArgs args;

    public FhirOwlService(OwlPluginArgs args) {
        this.args = args;
        loadIriMappings();
    }

    private void loadIriMappings() {
        if (args.getIriMappings() == null) {
            log.info("No IRI mappings specified.");
            return;
        }
        if (args.getWorkingDirectory() == null) {
            throw new RuntimeException("Can't invoke loadIriMappings without working directory");
        }
        log.info("Loading IRI mappings from file " + args.getIriMappings());
        try (BufferedReader br = new BufferedReader(new FileReader(args.getIriMappings().toFile()))) {
            String line;
            int lineNumber = 0;
            while ((line = br.readLine()) != null) {
                lineNumber++;
                if (line.startsWith("#")) {
                    continue;
                }
                String[] parts = line.split(",");
                if (parts.length != 2) {
                    throw new RuntimeException("Invalid line in IRI mappings file (line# " + lineNumber + "): " + line);
                }
                String iri = parts[0];
                Path mappedPath = Path.of(parts[1]);
                if (!mappedPath.isAbsolute()) {
                    mappedPath = args.getWorkingDirectory().resolve(mappedPath);
                }
                if (!mappedPath.toFile().exists()) {
                    throw new RuntimeException(String.format("Mapped file %s for IRI '%s' does not exist.", mappedPath, iri));
                }
                iriMap.put(IRI.create(iri), IRI.create(mappedPath.toFile()));
                log.info(String.format("Adding mapping %s -> %s", iri, mappedPath));
            }
        } catch (IOException e) {
            log.warn("There was a problem loading IRI mappings.", e);
            throw new RuntimeException(e);
        }
    }

    /**
     * Transforms an OWL file into a FHIR code system.
     *
     * @param csc The code system container.
     * @throws OWLOntologyCreationException If there is a problem loading the ontology.
     */
    public List<CodeSystemConceptContainer> transform(CodeSystemContainer csc)
            throws OWLOntologyCreationException {
        log.info("Creating code systems");

        return createConceptList(csc);
    }

    private void addIriMappings(OWLOntologyManager manager) {
        for (IRI key : iriMap.keySet()) {
            manager.getIRIMappers().add(new SimpleIRIMapper(key, iriMap.get(key)));
        }
    }

    private Set<IRI> getIris(Set<OWLClass> classes) {
        final Set<IRI> res = new HashSet<>();
        for (OWLClass oc : classes) {
            res.add(oc.getIRI());
        }
        return res;
    }

    private List<CodeSystemConceptContainer> createConceptList(CodeSystemContainer csc)
            throws OWLOntologyCreationException {

        final File input = args.getOwlFile().toFile();

        log.info("Loading ontology from file " + input.getAbsolutePath());
        OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
        addIriMappings(manager);
        final OWLOntology rootOnt = manager.loadOntologyFromOntologyDocument(input);

        // We only need the preferred term property here
        final OWLDataFactory factory = manager.getOWLDataFactory();
        final OWLAnnotationProperty preferredTermProp = OwlProperties.loadProp(factory, args.getDisplayProperty(), null, null);

        // We implement the two supported mechanisms to determine which concepts belong in the
        // main ontology. If the main namespaces are provided then those are used. Otherwise,
        // we need to calculate which concepts are defined in the main file and are not
        // defined in the imported ontologies
        final Set<IRI> irisInMain = calculateIrisInMain(args.getMainNs(), rootOnt);

        // Extract labels for all classes
        final Map<IRI, String> iriDisplayMap = new HashMap<>();
        for (OWLClass owlClass : rootOnt.getClassesInSignature(Imports.INCLUDED)) {
            iriDisplayMap.put(owlClass.getIRI(), null);
        }

        final Set<OWLOntology> closure = manager.getImportsClosure(rootOnt);
        for (OWLOntology ont : closure) {
            for (OWLClass oc : ont.getClassesInSignature()) {
                if (iriDisplayMap.containsKey(oc.getIRI())) {
                    String pt = getPreferredTerm(oc, ont, preferredTermProp, Collections.emptyList());
                    if (pt != null) {
                        iriDisplayMap.put(oc.getIRI(), pt);
                    }
                }
            }
        }

        // Make sure there are no null labels
        final Set<IRI> unnamed = new HashSet<>();
        for (IRI key : iriDisplayMap.keySet()) {
            String display = iriDisplayMap.get(key);
            if (display == null) {
                log.warn("Could not find label for class " + key.toString());
                unnamed.add(key);
            }
        }

        for (IRI un : unnamed) {
            iriDisplayMap.put(un, un.toString());
        }

        // Classify root ontology
        OWLReasonerFactory reasonerFactory = switch (args.getReasoner()) {
            case "elk" -> new ElkReasonerFactory();
            case "jfact" -> new JFactFactory();
            default -> throw new RuntimeException("Invalid reasoner " + args.getReasoner());
        };

        log.info("Classifying ontology " + csc.getIdentification() + " with " + args.getReasoner());
        OWLReasoner reasoner = reasonerFactory.createReasoner(rootOnt);
        reasoner.precomputeInferences();

        // Create code system
        return createConceptList(rootOnt, manager.getOWLDataFactory(), reasoner, irisInMain, iriDisplayMap);
    }

    Set<IRI> calculateIrisInMain(Set<String> mainNamespaces, OWLOntology rootOnt) {
        final Set<IRI> irisInMain = new HashSet<>();
        if (mainNamespaces == null || mainNamespaces.isEmpty()) {
            // Get concepts in main ontology
            irisInMain.addAll(getIris(rootOnt.getClassesInSignature(Imports.EXCLUDED)));

            // Get all concepts in imported ontologies
            final Set<IRI> importedIris = new HashSet<>();
            for (OWLOntology io : rootOnt.getImports()) {
                importedIris.addAll(getIris(io.getClassesInSignature(Imports.INCLUDED)));
            }

            // Remove concepts in imported ontologies from the ones in the main ontology
            irisInMain.removeAll(importedIris);
        }
        return irisInMain;
    }

    /**
     * Creates a list of concepts from an ontology
     *
     * @param ont           The ontology.
     * @param factory       The OWL factory.
     * @param reasoner      The OWL reasoner.
     * @param irisInMain    The IRIs that belong in the main namespaces. Only populated if
     *                      mainNamespaces is empty.
     * @param iriDisplayMap A map of IRIs to their display.
     * @return The code system concepts.
     */
    List<CodeSystemConceptContainer> createConceptList(
            OWLOntology ont,
            final OWLDataFactory factory,
            OWLReasoner reasoner,
            Set<IRI> irisInMain,
            Map<IRI, String> iriDisplayMap) {

        // Determine if there are imports
        final boolean hasImports = !ont.getImportsDeclarations().isEmpty();

        final boolean includeDeprecated = args.getIncludeDeprecated();
        final OWLAnnotationProperty codeProp = OwlProperties.loadProp(factory, args.getCodeProperty(), null, null);
        final OWLAnnotationProperty preferredTermProp = OwlProperties.loadProp(factory, args.getDisplayProperty(), OwlProperties.RDFS_LABEL, null);
        final List<OWLAnnotationProperty> synonymProps = OwlProperties.loadProps(factory, args.getSynonymsProp(), List.of(OwlProperties.RDFS_LABEL), null);
        final String stringToReplaceInCodes = (args.getCodeReplace() == null) ? null : args.getCodeReplace().getFirst();
        final String replacementStringInCodes = (args.getCodeReplace() == null) ? null : args.getCodeReplace().getSecond();

        // Add classes
        Set<OWLClass> classes = ont.getClassesInSignature(Imports.INCLUDED);
        classes.add(factory.getOWLThing());
        Node<OWLClass> nothing = reasoner.getEquivalentClasses(factory.getOWLNothing());

        // Need to filter equivalents to OWLNothing
        classes = classes.stream()
                .filter(x -> !nothing.contains(x))
                .collect(Collectors.toSet());

        /* Shouldn't need to calculate transitive reduction because the reasoner should take care of it. However, JFact
         * doesn't seem to do this properly with object properties calculating for everything just in case.
         */
        Map<OWLClass, Set<OWLClass>> classParents = GraphUtils.transitiveReduction(
                classes, c -> reasoner.getSuperClasses(c, false).getFlattened());

        LinkedList<CodeSystemConceptContainer> conceptList = new LinkedList<>();

        for (OWLClass owlClass : classes) {
            processEntity(owlClass, conceptList, ont, args.getMainNs(), irisInMain, iriDisplayMap,
                    includeDeprecated, codeProp, preferredTermProp, synonymProps, hasImports,
                    stringToReplaceInCodes, replacementStringInCodes, args.getLabelsToExclude(), classParents);
        }

        if (args.getReasoner().equals("jfact")) {
            // Add object properties
            final Set<OWLObjectProperty> objectProps = ont.getObjectPropertiesInSignature(Imports.INCLUDED);
            if (!objectProps.isEmpty()) {
                objectProps.add(factory.getOWLTopObjectProperty());

                Map<OWLObjectProperty, Set<OWLObjectProperty>> opParents =
                        GraphUtils.transitiveReduction(objectProps, p -> reasoner.getSuperObjectProperties(p, false).getFlattened()
                                .stream()
                                .filter(o -> !o.isAnonymous())
                                .map(OWLObjectPropertyExpression::asOWLObjectProperty)
                                .collect(Collectors.toSet()));

                for (OWLObjectProperty prop : objectProps) {
                    processEntity(prop, conceptList, ont, args.getMainNs(), irisInMain, iriDisplayMap,
                            includeDeprecated, codeProp, preferredTermProp, synonymProps, hasImports,
                            stringToReplaceInCodes, replacementStringInCodes, args.getLabelsToExclude(), opParents);
                }
            }

            // Add data properties
            final Set<OWLDataProperty> dataProps = ont.getDataPropertiesInSignature(Imports.INCLUDED);
            if (!dataProps.isEmpty()) {
                dataProps.add(factory.getOWLTopDataProperty());

                Map<OWLDataProperty, Set<OWLDataProperty>> dpParents =
                        GraphUtils.transitiveReduction(dataProps, p -> reasoner.getSuperDataProperties(p, false).getFlattened()
                                .stream()
                                .filter(o -> !o.isAnonymous())
                                .map(OWLDataPropertyExpression::asOWLDataProperty)
                                .collect(Collectors.toSet()));

                for (OWLDataProperty prop : dataProps) {
                    processEntity(prop, conceptList, ont, args.getMainNs(), irisInMain, iriDisplayMap,
                            includeDeprecated, codeProp, preferredTermProp, synonymProps, hasImports,
                            stringToReplaceInCodes, replacementStringInCodes, args.getLabelsToExclude(), dpParents);
                }
            }
        }

        return conceptList;
    }

    private <T extends OWLEntity> boolean addHierarchyFields(OWLEntity owlEntity,
                                                             Set<T> parents,
                                                             OWLOntology rootOntology,
                                                             Set<String> mainNamespaces,
                                                             Set<IRI> irisInMain,
                                                             boolean includeDeprecated,
                                                             String stringToReplaceInCodes,
                                                             String replacementStringInCodes,
                                                             boolean hasImports,
                                                             LinkedList<ConceptPropertyContainer> properties) {
        if (owlEntity.isTopEntity()) {
            return true;
        }

        if (parents == null) {
            throw new RuntimeException("Got null parents. This should not happen!");
        }

        log.debug("Found " + parents.size() + " parents for concept " + owlEntity.getIRI());
        for (OWLEntity parent : parents.stream().sorted().toList()) {
            if (parent.isBottomEntity()) {
                continue;
            }

            // If excluding deprecated class then also exclude from parents. In some ontologies
            // deprecated classes are still in the hierarchy, e.g. MONDO.
            if (!includeDeprecated) {
                if (isDeprecated(parent, rootOntology)) {
                    continue;
                }
            }

            final IRI iri = parent.getIRI();

            final boolean imported = isImported(iri, mainNamespaces, irisInMain, hasImports);
            PropertyValueType.CodeContainer parentCode;

            if (!imported) {
                String code = iri.getShortForm();
                if (stringToReplaceInCodes != null && replacementStringInCodes != null) {
                    code = code.replace(stringToReplaceInCodes, replacementStringInCodes);
                }
                parentCode = new PropertyValueType.CodeContainer(code);
            } else {
                final String code = iri.toString();
                parentCode = new PropertyValueType.CodeContainer(code);
            }


            final ConceptPropertyContainer parentProp = new ConceptPropertyContainer("parent", parentCode);
            properties.add(parentProp);
        }

        // If we get here then this concept is not a root concept
        return false;
    }

    /**
     * Determines if an OWL class is deprecated based on annotations.
     *
     * @param owlEntity The OWL entity.
     * @param ont       The ontology it belongs to.
     * @return boolean True if deprecated, false otherwise.
     */
    private boolean isDeprecated(OWLEntity owlEntity, OWLOntology ont) {
        AtomicBoolean isDeprecated = new AtomicBoolean(false);
        EntitySearcher.getAnnotationObjects(owlEntity, ont).forEach(ann -> {
            OWLAnnotationProperty prop = ann.getProperty();
            if (prop.getIRI().getShortForm().equals("deprecated")) {
                OWLAnnotationValue val = ann.getValue();
                val.asLiteral();
                Optional<OWLLiteral> lit = val.asLiteral();
                if (lit.isPresent()) {
                    final OWLLiteral l = lit.get();
                    if (l.isBoolean()) {
                        isDeprecated.set(l.parseBoolean());
                    } else {
                        log.warn("Found deprecated attribute but it is not boolean: " + l);
                    }
                }
            }
        });
        return isDeprecated.get();
    }


    private String getCode(OWLEntity owlEntity, OWLOntology ont, OWLAnnotationProperty prop) {
        Stream<OWLAnnotation> annotations = EntitySearcher.getAnnotations(owlEntity, ont, prop);
        for (Object a : annotations.toArray()) {
            OWLAnnotationValue val = ((OWLAnnotation) a).getValue();
            if (val instanceof OWLLiteral) {
                return ((OWLLiteral) val).getLiteral();
            }
        }

        return null;
    }

    private String getPreferredTerm(OWLEntity owlEntity, OWLOntology ont,
                                    OWLAnnotationProperty preferredTermAnnotationProperty, List<String> labelsToExclude) {

        SortedSet<String> candidates = new TreeSet<>();
        EntitySearcher.getAnnotations(owlEntity, ont, preferredTermAnnotationProperty).forEach(a -> {
            OWLAnnotationValue val = a.getValue();
            if (val instanceof OWLLiteral) {
                String label = ((OWLLiteral) val).getLiteral();
                if (!labelsToExclude.contains(label)) {
                    candidates.add(label);
                }
            }
        });
        if (!candidates.isEmpty()) {
            return candidates.first();
        } else {
            return null;
        }
    }

    private Set<String> getSynonyms(OWLEntity owlEntity, OWLOntology ont, String preferredTerm,
                                    List<OWLAnnotationProperty> synonymAnnotationProperties, List<String> labelsToExclude) {
        final Set<String> synonyms = new HashSet<>();
        for (OWLAnnotationProperty prop : synonymAnnotationProperties) {
            EntitySearcher.getAnnotationObjects(owlEntity, ont, prop).forEach(a -> {
                        OWLAnnotationValue val = a.getValue();
                        if (val instanceof OWLLiteral) {
                            final String label = ((OWLLiteral) val).getLiteral();
                            if (!labelsToExclude.contains(label)) {
                                synonyms.add(label);
                            }
                        }
                    }
            );
        }
        synonyms.remove(preferredTerm);
        return synonyms;
    }


    private boolean isImported(IRI iri, Set<String> mainNamespaces, Set<IRI> irisInMain,
                               boolean hasImports) {
        if (mainNamespaces != null && !mainNamespaces.isEmpty()) {
            String s = iri.toString();
            for (String ns : mainNamespaces) {
                if (s.startsWith(ns)) {
                    return false;
                }
            }
            return true;
        } else {
            if (!hasImports) {
                return false;
            } else {
                return !irisInMain.contains(iri);
            }
        }
    }

    private <T extends OWLEntity> void processEntity(
            OWLEntity owlEntity,
            LinkedList<CodeSystemConceptContainer> concepts,
            OWLOntology ont,
            Set<String> mainNamespaces,
            Set<IRI> irisInMain,
            Map<IRI, String> iriDisplayMap,
            boolean includeDeprecated,
            OWLAnnotationProperty codeProp,
            OWLAnnotationProperty preferredTermProp,
            List<OWLAnnotationProperty> synonymProps,
            boolean hasImports,
            String stringToReplaceInCodes,
            String replacementStringInCodes,
            List<String> labelsToExclude,
            Map<T, Set<T>> parents) {

        if (owlEntity.isBottomEntity()) {
            return;
        }

        LinkedList<ConceptPropertyContainer> properties = new LinkedList<>();
        LinkedList<DesignationContainer> designations = new LinkedList<>();

        final boolean isDeprecated = isDeprecated(owlEntity, ont);
        if (!includeDeprecated && isDeprecated) {
            log.debug(String.format("Concept with IRI %s is deprecated, skipping", owlEntity.getIRI()));
            return;
        }

        final IRI iri = owlEntity.getIRI();

        // Determine if concept is imported or not
        boolean imported = isImported(iri, mainNamespaces, irisInMain, hasImports);

        // The code might come from an annotation property
        String code = null;
        if (codeProp != null) {
            code = getCode(owlEntity, ont, codeProp);
        }
        if (code == null) {
            code = imported ? iri.toString() : iri.getShortForm();
        }

        // We only do code replacements for local codes - not for imported
        if (!imported && stringToReplaceInCodes != null && replacementStringInCodes != null) {
            code = code.replace(stringToReplaceInCodes, replacementStringInCodes);
        }

        // Special cases: OWL:Thing, top object property and top data property
        String display = switch (code) {
            case "http://www.w3.org/2002/07/owl#Thing" -> "Thing";
            case "http://www.w3.org/2002/07/owl#topObjectProperty" -> "Top Object Property";
            case "http://www.w3.org/2002/07/owl#topDataProperty" -> "Top Data Property";
            default -> getPreferredTermAndSynonymsForClass(owlEntity, code, ont, preferredTermProp, labelsToExclude,
                    iriDisplayMap, iri, synonymProps, designations);
        };

        // This is hard to detect appropriately because the classes declared in an ontology
        // can be declared with an arbitrary namespace.
        final ConceptPropertyContainer importedProp = new ConceptPropertyContainer("imported",
                new PropertyValueType.BooleanContainer(imported));
        properties.add(importedProp);


        @SuppressWarnings("SuspiciousMethodCalls")
        boolean isRoot = addHierarchyFields(owlEntity, parents.get(owlEntity), ont, mainNamespaces, irisInMain,
                includeDeprecated, stringToReplaceInCodes, replacementStringInCodes, hasImports, properties);


        final ConceptPropertyContainer rootProp = new ConceptPropertyContainer("root",
                new PropertyValueType.BooleanContainer(isRoot));
        properties.add(rootProp);


        final ConceptPropertyContainer deprecatedProp = new ConceptPropertyContainer("deprecated",
                new PropertyValueType.BooleanContainer(isDeprecated));
        properties.add(deprecatedProp);

        final CodeSystemConceptContainer concept = new CodeSystemConceptContainer(code, display, null, designations, properties);
        concepts.add(concept);
    }

    private String getPreferredTermAndSynonymsForClass(
            OWLEntity owlEntity,
            String code,
            OWLOntology ont,
            OWLAnnotationProperty preferredTermProp,
            List<String> labelsToExclude,
            Map<IRI, String> iriDisplayMap,
            IRI iri,
            List<OWLAnnotationProperty> synonymProps,
            LinkedList<DesignationContainer> designations) {
        String preferredTerm = getPreferredTerm(owlEntity, ont, preferredTermProp, labelsToExclude);
        final Set<String> synonyms = getSynonyms(owlEntity, ont, preferredTerm, synonymProps, labelsToExclude);
        String display;

        if (preferredTerm == null && synonyms.isEmpty()) {
            String label = iriDisplayMap.get(iri);
            if (label != null) {
                display = label;
            } else {
                display = code;
            }
        } else if (preferredTerm == null) {
            // No preferred term but there are synonyms so pick any one as the display
            preferredTerm = synonyms.iterator().next();
            synonyms.remove(preferredTerm);
            display = preferredTerm;
            designations.addAll(generateSynonyms(synonyms));
        } else {
            display = preferredTerm;
            designations.addAll(generateSynonyms(synonyms));
        }

        return display;
    }

    private List<DesignationContainer> generateSynonyms(Set<String> synonyms) {
        return synonyms.stream().sorted().map(syn -> {
            PropertyValueType.CodingContainer use = new PropertyValueType.CodingContainer("http://snomed.info/sct", null, "900000000000013009", "Synonym (core metadata concept)", null);
            return new DesignationContainer(null, use, syn, List.of());
        }).toList();
    }
}
