/*
CSIRO Open Source Software Licence Agreement (variation of the BSD / MIT License)
Copyright (c) 2020, Commonwealth Scientific and Industrial Research Organisation (CSIRO) ABN 41 687 119 230.
All rights reserved. CSIRO is willing to grant you a licence to this software on the following terms, except where otherwise indicated for third party material.
Redistribution and use of this software in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
* Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
* Neither the name of CSIRO nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission of CSIRO.
EXCEPT AS EXPRESSLY STATED IN THIS AGREEMENT AND TO THE FULL EXTENT PERMITTED BY APPLICABLE LAW, THE SOFTWARE IS PROVIDED "AS-IS". CSIRO MAKES NO REPRESENTATIONS, WARRANTIES OR CONDITIONS OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO ANY REPRESENTATIONS, WARRANTIES OR CONDITIONS REGARDING THE CONTENTS OR ACCURACY OF THE SOFTWARE, OR OF TITLE, MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, NON-INFRINGEMENT, THE ABSENCE OF LATENT OR OTHER DEFECTS, OR THE PRESENCE OR ABSENCE OF ERRORS, WHETHER OR NOT DISCOVERABLE.
TO THE FULL EXTENT PERMITTED BY APPLICABLE LAW, IN NO EVENT SHALL CSIRO BE LIABLE ON ANY LEGAL THEORY (INCLUDING, WITHOUT LIMITATION, IN AN ACTION FOR BREACH OF CONTRACT, NEGLIGENCE OR OTHERWISE) FOR ANY CLAIM, LOSS, DAMAGES OR OTHER LIABILITY HOWSOEVER INCURRED.  WITHOUT LIMITING THE SCOPE OF THE PREVIOUS SENTENCE THE EXCLUSION OF LIABILITY SHALL INCLUDE: LOSS OF PRODUCTION OR OPERATION TIME, LOSS, DAMAGE OR CORRUPTION OF DATA OR RECORDS; OR LOSS OF ANTICIPATED SAVINGS, OPPORTUNITY, REVENUE, PROFIT OR GOODWILL, OR OTHER ECONOMIC LOSS; OR ANY SPECIAL, INCIDENTAL, INDIRECT, CONSEQUENTIAL, PUNITIVE OR EXEMPLARY DAMAGES, ARISING OUT OF OR IN CONNECTION WITH THIS AGREEMENT, ACCESS OF THE SOFTWARE OR ANY OTHER DEALINGS WITH THE SOFTWARE, EVEN IF CSIRO HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH CLAIM, LOSS, DAMAGES OR OTHER LIABILITY.
APPLICABLE LEGISLATION SUCH AS THE AUSTRALIAN CONSUMER LAW MAY APPLY REPRESENTATIONS, WARRANTIES, OR CONDITIONS, OR IMPOSES OBLIGATIONS OR LIABILITY ON CSIRO THAT CANNOT BE EXCLUDED, RESTRICTED OR MODIFIED TO THE FULL EXTENT SET OUT IN THE EXPRESS TERMS OF THIS CLAUSE ABOVE "CONSUMER GUARANTEES".  TO THE EXTENT THAT SUCH CONSUMER GUARANTEES CONTINUE TO APPLY, THEN TO THE FULL EXTENT PERMITTED BY THE APPLICABLE LEGISLATION, THE LIABILITY OF CSIRO UNDER THE RELEVANT CONSUMER GUARANTEE IS LIMITED (WHERE PERMITTED AT CSIRO'S OPTION) TO ONE OF FOLLOWING REMEDIES OR SUBSTANTIALLY EQUIVALENT REMEDIES:
(a)               THE REPLACEMENT OF THE SOFTWARE, THE SUPPLY OF EQUIVALENT SOFTWARE, OR SUPPLYING RELEVANT SERVICES AGAIN;
(b)               THE REPAIR OF THE SOFTWARE;
(c)               THE PAYMENT OF THE COST OF REPLACING THE SOFTWARE, OF ACQUIRING EQUIVALENT SOFTWARE, HAVING THE RELEVANT SERVICES SUPPLIED AGAIN, OR HAVING THE SOFTWARE REPAIRED.
IN THIS CLAUSE, CSIRO INCLUDES ANY THIRD PARTY AUTHOR OR OWNER OF ANY PART OF THE SOFTWARE OR MATERIAL DISTRIBUTED WITH IT.  CSIRO MAY ENFORCE ANY RIGHTS ON BEHALF OF THE RELEVANT THIRD PARTY.
Third Party Components
The following third party components are distributed with the Software.  You agree to comply with the licence terms for these components as part of accessing the Software.  Other third party software may also be identified in separate files distributed with the Software.
___________________________________________________________________
See dependencies in licenses.xml. Details about the licenses of the libraries are generated in target/generated-resources/licenses when building the application.
___________________________________________________________________
This file was modified from: https://github.com/aehrc/fhir-claml
*/

package au.csiro.fhir.claml;


import au.csiro.fhir.claml.Claml2FhirResult.Concept.ConceptProperty.PropertyValueType;
import au.csiro.fhir.claml.model.claml.Class;
import au.csiro.fhir.claml.model.claml.*;
import ca.uhn.fhir.parser.DataFormatException;
import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Unmarshaller;
import org.apache.commons.io.input.BOMInputStream;
import org.apache.commons.lang3.tuple.Pair;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.sax.SAXSource;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.*;
import java.util.stream.Stream;


@SuppressWarnings("HttpUrlsUsage")
public class FhirClamlConceptService {

    protected static final Logger log = LoggerFactory.getLogger(FhirClamlConceptService.class);

    protected final File clamlFile;
    protected final List<String> displayRubrics;
    protected final String definitionRubric;
    protected final List<String> excludeRubricProperties;
    protected final Map<String, String> metaToPropertyMap;
    private final Map<String, String> rubricToPropertyMap;
    private final boolean mapAllMetaToProperties;
    private final List<String> ignoreMetaCodes;
    protected List<String> designationRubrics;
    protected List<String> excludeClassKind;
    protected final Boolean excludeKindlessClasses;
    protected final Boolean applyModifiers;

    public FhirClamlConceptService(
            File clamlFile,
            List<String> displayRubrics,
            String definitionRubric,
            List<String> designationRubrics,
            List<String> excludeClassKind,
            List<String> excludeRubricProperties,
            boolean mapAllMetaToProperties,
            Map<String, String> metaToPropertyMap,
            Map<String, String> rubricToPropertyMap,
            List<String> ignoreMetaCodes,
            Boolean excludeKindlessClasses,
            Boolean applyModifiers
    ) {
        this.clamlFile = clamlFile;
        this.displayRubrics = displayRubrics;
        this.definitionRubric = definitionRubric;
        this.designationRubrics = designationRubrics;
        this.excludeClassKind = excludeClassKind;
        this.excludeRubricProperties = excludeRubricProperties;
        this.mapAllMetaToProperties = mapAllMetaToProperties;
        this.metaToPropertyMap = metaToPropertyMap;
        this.rubricToPropertyMap = rubricToPropertyMap;
        this.ignoreMetaCodes = ignoreMetaCodes;
        this.excludeKindlessClasses = excludeKindlessClasses;
        this.applyModifiers = applyModifiers;
    }

    protected ClaML parseFileAsClaml(File clamlFile) throws SAXException, ParserConfigurationException, JAXBException, IOException {

        JAXBContext jaxbContext = JAXBContext.newInstance(ClaML.class);

        SAXParserFactory spf = SAXParserFactory.newInstance();
        spf.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
        spf.setFeature("http://xml.org/sax/features/validation", false);

        XMLReader xmlReader = spf.newSAXParser().getXMLReader();

        try (FileInputStream fis = new FileInputStream(clamlFile)) {
            try (BOMInputStream bomInputStream = BOMInputStream.builder().setInputStream(fis).get()) {
                if (bomInputStream.hasBOM()) {
                    log.warn("The input file {} has a BOM, which will be ignored by BabelFSH.", clamlFile.getAbsolutePath());
                }
                InputSource inputSource = new InputSource(bomInputStream);
                SAXSource source = new SAXSource(xmlReader, inputSource);
                Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
                Object unmarshal = jaxbUnmarshaller.unmarshal(source);
                return (ClaML) unmarshal;
            }
        }
    }

    public Claml2FhirResult babelfshClaml2Fhir() throws DataFormatException, IOException, ParserConfigurationException, SAXException, JAXBException {

        if (designationRubrics == null) {
            designationRubrics = Collections.emptyList();
        }
        if (excludeClassKind == null) {
            excludeClassKind = Collections.emptyList();
        }

        log.debug("Parsing ClaML file {}", clamlFile.getAbsolutePath());
        ClaML claml = parseFileAsClaml(clamlFile);
        log.debug("Converting ClaML to FHIR");
        if (!claml.getModifier().isEmpty() && !applyModifiers) {
            log.warn("Modifiers are present in the ClaML file, but they will not be applied as applyModifiers is set to false");
        }
        Claml2FhirResult claml2FhirResult = claml2FhirObject(claml, displayRubrics, definitionRubric, designationRubrics, excludeClassKind,
                excludeKindlessClasses, applyModifiers);
        log.debug("Conversion complete");
        return claml2FhirResult;
    }

    protected Claml2FhirResult claml2FhirObject(ClaML claml,
                                                List<String> displayRubrics,
                                                String definitionRubric,
                                                List<String> designationRubrics,
                                                List<String> excludeClassKind,
                                                Boolean excludeKindlessClasses,
                                                Boolean applyModifiers) {

        // Default values
        if (displayRubrics == null || displayRubrics.isEmpty()) {
            displayRubrics = new ArrayList<>();
            displayRubrics.add("preferred");
        }
        if (definitionRubric == null) {
            definitionRubric = "definition";
        }

        LinkedList<Claml2FhirResult.CsProperty> csProperties = new LinkedList<>();
        HashMap<String, List<Rubric>> inheritedRubricKindMap = new HashMap<>();

        for (RubricKind rk : claml.getRubricKinds().getRubricKind()) {

            if (!definitionRubric.equals(rk.getName()) &&
                    !displayRubrics.contains(rk.getName()) &&
                    !designationRubrics.contains(rk.getName())) {


                if (this.excludeRubricProperties.contains(rk.getName())) {
                    log.info("Excluding rubric kind '%s' from properties".formatted(rk.getName()));
                    continue;
                }
                if (rk.getDisplay() != null && !rk.getDisplay().isEmpty()) {
                    if (rk.getDisplay().size() > 1) {
                        log.warn("Found more than one display for rubric kind {}: ignoring additional displays", rk.getName());
                    }
                    csProperties.add(new Claml2FhirResult.CsProperty(rk.getName(), rk.getDisplay().getFirst().getContent()));

                } else {
                    csProperties.add(new Claml2FhirResult.CsProperty(rk.getName(), null));
                }
            }
            if (rk.isInherited()) {
                log.warn("Inherited rubric kinds are not fully supported: {}", rk.getName());
            }
        }

        log.debug("Built rubrics: {}", csProperties);
        int count = 0;

        Map<String, Claml2FhirResult.Concept> concepts = new HashMap<>();
        Map<String, List<ModifiedBy>> modifiedBy = new HashMap<>();
        Map<String, Set<ExcludeModifier>> excludeModifiers = new HashMap<>();
        Map<String, Set<String>> descendents = new HashMap<>();

        log.debug("Processing {} classes", claml.getClazz().size());

        for (Class c : claml.getClazz()) {
            count += produceConceptFromClass(excludeClassKind, excludeKindlessClasses, c, concepts, modifiedBy, descendents, excludeModifiers, inheritedRubricKindMap);
        }

        log.info("Processed {} classes, generating displays", count);
        concepts.forEach((code, concept) -> setDisplayForConcept(concept, concepts));

        Map<String, Set<ModifierClass>> modifierClasses = new HashMap<>();
        for (ModifierClass modClass : claml.getModifierClass()) {
            if (!modifierClasses.containsKey(modClass.getModifier())) {
                modifierClasses.put(modClass.getModifier(), new HashSet<>());
            }
            modifierClasses.get(modClass.getModifier()).add(modClass);
        }

        if (applyModifiers) {
            for (String modifiedConcept : modifiedBy.keySet().stream().sorted().toList()) {
                //Don't add modifiers to non-leaf classes
                if (descendents.containsKey(modifiedConcept) && !modifiedBy.get(modifiedConcept).isEmpty() && !descendents.get(modifiedConcept).isEmpty()) {
                    if (log.isInfoEnabled()) {
                        log.info("Modifiers are only applied to leaf classes. Skipping {}", modifiedConcept);
                    }
                    for (String desc : descendents.get(modifiedConcept)) {
                        log.debug("Applying modifiers to descendent {} of code {}", desc, modifiedConcept);
                        applyModifiersToClass(desc, modifierClasses, modifiedBy.get(modifiedConcept), concepts, displayRubrics, excludeModifiers, inheritedRubricKindMap);
                    }
                } else {
                    count += applyModifiersToClass(modifiedConcept, modifierClasses, modifiedBy.get(modifiedConcept), concepts, displayRubrics, excludeModifiers, inheritedRubricKindMap);
                }
            }
        }

        log.info("After modifiers, processed {} classes", count);

        return new Claml2FhirResult(
                claml.getTitle().getContent(),
                csProperties,
                concepts.values().stream().toList()
        );
    }

    protected String generateDisplayForConceptFromLabel(Claml2FhirResult.Concept concept, Label label, Map<String, Claml2FhirResult.Concept> concepts) {
        return getLabelValue(label, true);
    }

    protected void setDisplayForConcept(Claml2FhirResult.Concept concept, Map<String, Claml2FhirResult.Concept> concepts) {
        Pair<List<Label>, String> preferredRubric = concept.getPreferredRubric(displayRubrics);
        if (preferredRubric != null && !preferredRubric.getLeft().isEmpty()) {
            if (preferredRubric.getLeft().size() > 1) {
                log.warn("Found more than one preferred rubric for code {}", concept.getCode());
            }
            String display = generateDisplayForConceptFromLabel(concept, preferredRubric.getLeft().getFirst(), concepts);
            concept.setDisplay(display);
        }
        Optional<ArrayList<Label>> definitionRubrics = concept.getSingleRubric(this.definitionRubric);
        if (definitionRubrics.isPresent() && !definitionRubrics.get().isEmpty()) {
            if (definitionRubrics.get().size() > 1) {
                log.warn("Found more than one definition rubric for code {}", concept.getCode());
            }
            String definition = getLabelValue(definitionRubrics.get().getFirst(), false);
            concept.setDefinition(definition);
        }
        for (String dr : designationRubrics) {
            Optional<ArrayList<Label>> designationRubrics = concept.getSingleRubric(dr);
            if (designationRubrics.isPresent() && !designationRubrics.get().isEmpty()) {
                for (Label l : designationRubrics.get()) {
                    addDesignationForLabel(concept, dr, l);
                }
            }
        }

        rubricToPropertyMap.keySet().forEach(rub -> concept.getSingleRubric(rub).ifPresent(sr -> sr.forEach(r -> {
            String value = getLabelValue(r, false).trim();
            if (!value.isEmpty()) {
                String propertyCode = rubricToPropertyMap.get(rub);
                concept.addProperty(propertyCode, value, PropertyValueType.STRING);
            }
        })));

        List<String> unexpectedRubricKinds = concept.getRubrics().keySet().stream().filter(
                rk -> !displayRubrics.contains(rk) && !rk.equals(definitionRubric) && !designationRubrics.contains(rk) && !rubricToPropertyMap.containsKey(rk)
        ).toList();
        unexpectedRubricKinds.forEach(rubric -> {
            // dump unexpected rubrics as designations
            Optional<ArrayList<Label>> singleRubric = concept.getSingleRubric(rubric);
            singleRubric.ifPresent(labels -> labels.forEach(s -> concept.addDesignation(getLabelValue(s, false), null, rubric)));
            log.warn("Found unexpected rubric kind {} for code {}", rubric, concept.getCode());
        });


        if (concept.noDisplay()) {
            log.warn("Concept {} has no display text. Using code as display text", concept.getCode());
            concept.setDisplay(concept.getCode());
            if (concept.noDefinition()) {
                concept.setDefinition(concept.getCode());
            }
        } else if (concept.noDefinition()) {
            concept.setDefinition(concept.getDisplay());
        }
    }

    @NotNull
    private Integer produceConceptFromClass(
            List<String> excludeClassKind,
            Boolean excludeKindlessClasses,
            Class c,
            Map<String, Claml2FhirResult.Concept> concepts,
            Map<String, List<ModifiedBy>> modifiedBy,
            Map<String, Set<String>> descendents,
            Map<String, Set<ExcludeModifier>> excludeModifiers,
            HashMap<String, List<Rubric>> inheritedRubricKindMap) {

        if (c.getKind() != null && excludeClassKind.contains(getClassKindName(c.getKind()))) {
            log.info("Concept {} has excluded kind {}: skipping", c.getCode(), getClassKindName(c.getKind()));
            return 0;
        }
        if (concepts.containsKey(c.getCode())) {
            log.error("A concept already exists with code {}", c);
        }

        String conceptCode = postProcessCode(c.getCode());
        Claml2FhirResult.Concept concept = new Claml2FhirResult.Concept(conceptCode);
        concepts.put(conceptCode, concept);
        modifiedBy.put(conceptCode, new ArrayList<>());
        if (!descendents.containsKey(conceptCode)) {
            descendents.put(conceptCode, new HashSet<>());
        }
        if (c.getKind() != null) {
            if (getClassKindName(c.getKind()) != null) {
                concept.addProperty("kind", getClassKindName(c.getKind()), PropertyValueType.CODE);
            } else {
                log.warn("Unrecognised class kind on class {}: {}", conceptCode, c.getKind());
            }
        } else if (excludeKindlessClasses) {
            log.info("Concept {} has excluded kind {}: skipping", conceptCode, getClassKindName(c.getKind()));
            return 0;
        } else {
            log.info("Concept {} has no kind.", conceptCode);
        }
        for (SubClass sub : c.getSubClass()) {
            String subCode = postProcessCode(sub.getCode());
            concept.addProperty("child", subCode, PropertyValueType.CODE);
            descendents.get(c.getCode()).add(subCode);
            if (descendents.containsKey(subCode) && !descendents.get(subCode).isEmpty()) {
                descendents.get(conceptCode).addAll(descendents.get(subCode));
            }
        }
        for (SuperClass sup : c.getSuperClass()) {
            String supCode = postProcessCode(sup.getCode());
            concept.addProperty("parent", supCode, PropertyValueType.CODE);
            if (!descendents.containsKey(supCode)) {
                descendents.put(supCode, new HashSet<>());
            }
            descendents.get(supCode).add(conceptCode);
            if (!descendents.get(conceptCode).isEmpty()) {
                descendents.get(supCode).addAll(descendents.get(conceptCode));
            }
        }
        List<Rubric> inheritedRubrics = c.getSuperClass().stream().flatMap(superClass -> {
            List<Rubric> rubrics = inheritedRubricKindMap.getOrDefault(postProcessCode(superClass.getCode()), Collections.emptyList());
            return rubrics.stream();
        }).toList();
        List<Rubric> allRubrics = Stream.concat(c.getRubric().stream(), inheritedRubrics.stream()).toList();
        for (Rubric rubric : allRubrics) {
            Object rubricKind = rubric.getKind();
            if (rubricKind instanceof RubricKind kind) {
                if (excludeRubricProperties.contains(kind.getName())) {
                    continue;
                }
                if (rubric.getLabel().size() > 1) {
                    log.warn("Found more than one label on rubric {} for code {}", kind.getName(), c.getCode());
                    continue;
                }
                concept.addRubric(kind.getName(), rubric.getLabel().getFirst());

                if (kind.isInherited() && c.getRubric().contains(rubric) && !displayRubrics.contains(((RubricKind) rubricKind).getName())) {
                    // only add rubrics defined on this concept to the transitive map, and don't consider display rubrics
                    inheritedRubricKindMap.computeIfAbsent(conceptCode, s -> new ArrayList<>()).add(rubric);
                }
            }
        }

        if (!c.getModifiedBy().isEmpty()) {
            if (!modifiedBy.containsKey(conceptCode)) {
                modifiedBy.put(conceptCode, new ArrayList<>());
            }
            if (log.isDebugEnabled()) {
                log.debug("Adding {} modifiers to class {}", c.getModifiedBy().size(), conceptCode);
            }
            modifiedBy.get(conceptCode).addAll(c.getModifiedBy());
        }
        if (!c.getExcludeModifier().isEmpty()) {
            if (!excludeModifiers.containsKey(conceptCode)) {
                excludeModifiers.put(conceptCode, new HashSet<>());
            }
            if (log.isDebugEnabled()) {
                log.debug("Adding {} modifier exclusions to class {}", c.getExcludeModifier().size(), conceptCode);
            }
            excludeModifiers.get(conceptCode).addAll(c.getExcludeModifier());
            // consider ExcludeModifier as transitive, if children are declared, each modifier should also be excluded for them
            if (!c.getSubClass().isEmpty()) {
                List<String> subclassCodes = c.getSubClass().stream().map(SubClass::getCode).toList();
                for (ExcludeModifier excludeModifier : c.getExcludeModifier()) {
                    subclassCodes.forEach(sc -> {
                        if (!excludeModifiers.containsKey(sc)) {
                            excludeModifiers.put(sc, new HashSet<>());
                        }
                        excludeModifiers.get(sc).add(excludeModifier);
                    });
                }
            }
        }

        if (!c.getMeta().isEmpty()) {
            for (Meta meta : c.getMeta()) {
                if (ignoreMetaCodes.contains(meta.getName())) {
                    continue;
                }
                if (mapAllMetaToProperties || metaToPropertyMap.containsKey(meta.getName())) {
                    String mappedCode = mapAllMetaToProperties ? meta.getName() : metaToPropertyMap.get(meta.getName());
                    if (mappedCode != null) {
                        concept.addProperty(mappedCode, meta.getValue(), PropertyValueType.STRING);
                    } else {
                        log.warn("No property mapping found for meta {} on class {}", meta.getName(), conceptCode);
                    }
                }
            }
        }

        addFurtherAttributesToConceptFromClass(c, concept);

        return 1;
    }

    protected void addFurtherAttributesToConceptFromClass(Class c, Claml2FhirResult.Concept concept) {
        //intentionally left blank in this implementation
    }

    void addDisplayRubric(Map<String, List<Rubric>> displayRubricValues,
                          Rubric rubric,
                          RubricKind rkind,
                          String code) {
        if (rubric.getLabel().size() > 1) {
            log.warn("Found more than one label on display rubric {} for code {} (adding display rubric)", rkind.getName(), code);
        }
        if (!displayRubricValues.containsKey(rkind.getName())) {
            displayRubricValues.put(rkind.getName(), new ArrayList<>());
        }
        displayRubricValues.get(rkind.getName()).add(rubric);
    }

    @SuppressWarnings("UnnecessaryLabelOnContinueStatement")
    private int applyModifiersToClass(String modifiedConcept, Map<String, Set<ModifierClass>> modifierClasses,
                                      List<ModifiedBy> modifiedBy, Map<String, Claml2FhirResult.Concept> concepts,
                                      List<String> displayRubrics, Map<String, Set<ExcludeModifier>> excludeModifiers, HashMap<String, List<Rubric>> inheritedRubricKindMap) {
        List<Claml2FhirResult.Concept> candidates = new ArrayList<>();
        int count = 0;
        candidates.add(concepts.get(modifiedConcept));
        //Apply the modifiers in order to the modified concept
        List<Claml2FhirResult.Concept> newCandidates;

        modifiers:
        for (ModifiedBy modBy : modifiedBy) {

            if (excludeModifiers.containsKey(modifiedConcept) && !excludeModifiers.get(modifiedConcept).isEmpty()) {
                for (ExcludeModifier excludeMod : excludeModifiers.get(modifiedConcept)) {
                    if (modBy.getCode().equals(excludeMod.getCode())) {
                        log.info("Modifier {} is excluded for class {} : Skipping", modBy.getCode(), modifiedConcept);
                        continue modifiers;
                    }
                }
            }

            // for each candidate
            newCandidates = new ArrayList<>();
            for (Claml2FhirResult.Concept cand : candidates) {
                Set<ModifierClass> affectedModifierClasses = modifierClasses.get(modBy.getCode());
                log.debug("Applying modifier {} with {} modifierClasses to {}", modBy.getCode(), affectedModifierClasses.size(), cand.getCode());

                // for each applicable ModifierClass
                modifierClasses:
                for (ModifierClass modClass : affectedModifierClasses) {
                    log.debug("Applying modifierClass {} to {}", modClass.getCode(), cand.getCode());

                    // If ModifiedBy.all == false and there is no ModifiedBy.ValidModifierClass for this modifier class, then skip it
                    if (!modBy.isAll() && modBy.getValidModifierClass().stream().noneMatch(vmc -> vmc.getCode().equals(modClass.getCode()))) {
                        log.debug("Skipping modifierClass {} due to missing ValidModifierClass on class {}", modClass.getCode(), cand.getCode());

                        continue modifierClasses;
                    }
                    for (Meta excl : modClass.getMeta().stream().filter(met -> met.getName().equals("excludeOnPrecedingModifier")).toList()) {
                        String[] substrings = excl.getValue().split(" ");
                        if (substrings.length == 2 && cand.getCode().endsWith(substrings[1])) {
                            log.debug("Skipping modifierClass {} due to excludeOnPrecedingModifier on class {}", modClass.getCode(), cand.getCode());
                            continue modifierClasses;
                        }
                    }
                    String newCode = createCodeForModifiedClass(cand, modClass);
                    if (concepts.containsKey(newCode)) {
                        log.warn("Code {} already exists as a declared Class - skipping application of modifierClass {}::{} to code {}", newCode, modBy.getCode(), modClass.getCode(), cand.getCode());
                        continue modifierClasses;
                    }

                    //Set code to append modifierClass code
                    Claml2FhirResult.Concept concept = new Claml2FhirResult.Concept(postProcessCode(newCode));
                    count++;
                    newCandidates.add(concept);
                    log.debug("Creating code {}", concept.getCode());
                    Map<String, List<Rubric>> displayRubricValues = new HashMap<>();
                    List<Rubric> inheritedRubrics = inheritedRubricKindMap.getOrDefault(cand.getCode(), Collections.emptyList());
                    List<Rubric> allRubrics = Stream.concat(inheritedRubrics.stream(), modClass.getRubric().stream()).toList();
                    //Fix display to append modifierClass display
                    for (Rubric rubric : allRubrics) {
                        Object kind = rubric.getKind();
                        if (kind instanceof RubricKind rkind) {
                            if (displayRubrics.contains(rkind.getName())) {
                                String superClassCode = modClass.getSuperClass().getCode();
                                ensureSuperClassRubricsInConcept(modClass, superClassCode, affectedModifierClasses, displayRubricValues, concept);
                                addDisplayRubric(displayRubricValues, rubric, rkind, modClass.getCode());
                                concept.addRubric(rkind.getName(), rubric.getLabel().getFirst());
                            }
                        }
                    }
                    for (String dr : displayRubrics) {
                        if (!displayRubricValues.containsKey(dr)) {
                            continue;
                        }
                        List<Rubric> values = displayRubricValues.get(dr);
                        String value = getLabelValueForMultipleRubrics(values, dr, modClass, modifiedConcept);
                        String newDisplay = generateDisplayForModifiedConcept(cand, concepts.get(modifiedConcept), value, modClass, concepts, dr);
                        concept.setDisplay(newDisplay);
                        break;
                    }
                    // we consider the property 'kind' as transitive, since it is required on the Class element in ClaML
                    Optional<Claml2FhirResult.Concept.ConceptProperty> kindProperty = cand.getProperties().stream().filter(p -> p.code().equals("kind")).findFirst();
                    kindProperty.ifPresentOrElse(p -> concept.addProperty("kind", p.valueAsString(), p.valueType()), () -> log.warn("No kind property found on {}", cand.getCode()));

                    //Remove old parent/child links
                    //concept.getProperty().removeIf(p -> p.getCode().equals("parent") || p.getCode().equals("child"));
                    concept.addProperty("parent", cand.getCode(), PropertyValueType.CODE);
                }
            }
            candidates = newCandidates;
            if (!candidates.isEmpty()) {
                for (Claml2FhirResult.Concept c : candidates) {
                    concepts.putIfAbsent(c.getCode(), c);
                }
            }
        }
        return count;
    }

    protected @NotNull String createCodeForModifiedClass(Claml2FhirResult.Concept cand, ModifierClass modClass) {
        return cand.getCode() + modClass.getCode();
    }

    protected void ensureSuperClassRubricsInConcept(ModifierClass modClass, String superClassCode, Set<ModifierClass> affectedModifierClasses, Map<String, List<Rubric>> displayRubricValues, Claml2FhirResult.Concept concept) {
        //intentionally left blank in this implementation
    }

    protected String getLabelValueForMultipleRubrics(List<Rubric> values, String dr, ModifierClass modClass, String modifiedConcept) {
        if (values.size() > 1) {
            log.warn("Found multiple display rubrics {} for modifierClass {}on code {}", dr, modClass.getCode(), modifiedConcept);
        }
        Rubric rubric = values.getFirst();
        if (rubric.getLabel().size() > 1) {
            log.warn("Found more than one label on display rubric {} for code {}", dr, modClass.getCode());
        }
        return getLabelValue(rubric.getLabel().getFirst(), true).trim();
    }

    protected String generateDisplayForModifiedConcept(
            Claml2FhirResult.Concept candidate,
            Claml2FhirResult.Concept modifiedConcept,
            String displayRubricValue,
            ModifierClass modClass,
            Map<String, Claml2FhirResult.Concept> concepts,
            String displayRubric) {
        return String.format("%s : %s", candidate.getDisplay(), displayRubricValue);
    }

    private void addDesignationForLabel(Claml2FhirResult.Concept concept, String designationUse, Label l) {
        if (getLabelValue(l, false) == null) {
            log.warn("Skipping null label for designation {} for concept {}", getLabelValue(l, false), concept.getCode());
            return;
        }
        String v = getLabelValue(l, false).trim();
        if (!v.isEmpty()) {
            if (!concept.noDisplay() && concept.getDisplay().equals(v)) {
                return;
            }
            concept.addDesignation(v, l.getLang(), designationUse);
        } else {
            log.warn("Skipping empty label for rubric {} for concept {}", getLabelValue(l, false), concept.getCode());
        }
    }

    private String getClassKindName(Object kind) {
        if (kind instanceof String) {
            return (String) kind;
        } else if (kind instanceof ClassKind) {
            return ((ClassKind) kind).getName();
        } else {
            log.warn("Unrecognized class kind:{}", kind);
            return null;
        }
    }

    protected String getLabelValue(Label label, boolean forDisplay) {
        StringBuilder result = new StringBuilder();
        for (Object cont : label.getContent()) {
            if (!result.isEmpty()) {
                result.append("\n");
            }
            result.append(getLabelValue(cont, forDisplay));
        }
        return result.toString();
    }

    /**
     * Post-process the code as needed (e.g. for replacing the colon in ICD-O with a slash)
     *
     * @param code the code to process
     * @return the code unmodified in this implementation
     */
    public String postProcessCode(String code) {
        return code;
    }

    protected String getLabelValue(Reference reference, boolean forDisplay) {
        return "[" + reference.getContent() + "]";
    }

    protected String getLabelValue(Fragment fragment, boolean forDisplay) {
        StringBuilder result = new StringBuilder();
        for (Object cont : fragment.getContent()) {
            if (fragment.getType() != null && fragment.getType().equals("list")) {
                result.append(" - ");
            }
            result.append(getLabelValue(cont, forDisplay));
        }
        return result.toString();
    }

    protected String getLabelValue(Term term, boolean forDisplay) {
        switch (term.getClazz()) {
            case "tab" -> {
                return "\t";
            }
            case "subscript" -> {
                return "_" + term.getContent();
            }
            case "italics", "bold" -> {
                return term.getContent();
            }
            default -> {
                log.warn("Unrecognized Term class:{}", term.getClazz());
                return term.getContent();
            }
        }
    }

    protected String getLabelValue(Object l, boolean forDisplay) {
        switch (l) {
            case Label label -> {
                return getLabelValue(label, forDisplay);
            }
            case String s -> {
                return s;  // This is a hack, check all contents?
            }
            case Reference reference -> {
                return getLabelValue(reference, forDisplay);
            }
            case Para para -> {
                StringBuilder result = new StringBuilder();
                for (Object cont : para.getContent()) {
                    result.append(getLabelValue(cont, forDisplay));
                }
                return result.toString();
            }
            case Fragment frg -> {
                return getLabelValue(frg, forDisplay);
            }
            case Term term -> {
                return getLabelValue(term, forDisplay);
            }
            case au.csiro.fhir.claml.model.claml.List list -> {
                StringBuilder result = new StringBuilder();
                for (ListItem item : list.getListItem()) {
                    result.append(" - ");
                    for (Object cont : item.getContent()) {
                        result.append(getLabelValue(cont, forDisplay));
                    }
                    result.append("\n");
                }
                return result.toString();
            }
            case null, default -> {
                assert l != null;
                log.warn("Ignoring non-String label contents on Label ({})", l.getClass().getSimpleName());
                return l.getClass().getSimpleName().toUpperCase();
            }
        }
    }

}
