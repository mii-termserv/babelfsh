package au.csiro.fhir.claml;

public enum BfarmClamlMode {
    ICD_10_GM, ICD_10_WHO, OPS, ICD_O_3, ICF
}
