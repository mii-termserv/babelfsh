/*
CSIRO Open Source Software Licence Agreement (variation of the BSD / MIT License)
Copyright (c) 2020, Commonwealth Scientific and Industrial Research Organisation (CSIRO) ABN 41 687 119 230.
All rights reserved. CSIRO is willing to grant you a licence to this software on the following terms, except where otherwise indicated for third party material.
Redistribution and use of this software in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
* Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
* Neither the name of CSIRO nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission of CSIRO.
EXCEPT AS EXPRESSLY STATED IN THIS AGREEMENT AND TO THE FULL EXTENT PERMITTED BY APPLICABLE LAW, THE SOFTWARE IS PROVIDED "AS-IS". CSIRO MAKES NO REPRESENTATIONS, WARRANTIES OR CONDITIONS OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO ANY REPRESENTATIONS, WARRANTIES OR CONDITIONS REGARDING THE CONTENTS OR ACCURACY OF THE SOFTWARE, OR OF TITLE, MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, NON-INFRINGEMENT, THE ABSENCE OF LATENT OR OTHER DEFECTS, OR THE PRESENCE OR ABSENCE OF ERRORS, WHETHER OR NOT DISCOVERABLE.
TO THE FULL EXTENT PERMITTED BY APPLICABLE LAW, IN NO EVENT SHALL CSIRO BE LIABLE ON ANY LEGAL THEORY (INCLUDING, WITHOUT LIMITATION, IN AN ACTION FOR BREACH OF CONTRACT, NEGLIGENCE OR OTHERWISE) FOR ANY CLAIM, LOSS, DAMAGES OR OTHER LIABILITY HOWSOEVER INCURRED.  WITHOUT LIMITING THE SCOPE OF THE PREVIOUS SENTENCE THE EXCLUSION OF LIABILITY SHALL INCLUDE: LOSS OF PRODUCTION OR OPERATION TIME, LOSS, DAMAGE OR CORRUPTION OF DATA OR RECORDS; OR LOSS OF ANTICIPATED SAVINGS, OPPORTUNITY, REVENUE, PROFIT OR GOODWILL, OR OTHER ECONOMIC LOSS; OR ANY SPECIAL, INCIDENTAL, INDIRECT, CONSEQUENTIAL, PUNITIVE OR EXEMPLARY DAMAGES, ARISING OUT OF OR IN CONNECTION WITH THIS AGREEMENT, ACCESS OF THE SOFTWARE OR ANY OTHER DEALINGS WITH THE SOFTWARE, EVEN IF CSIRO HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH CLAIM, LOSS, DAMAGES OR OTHER LIABILITY.
APPLICABLE LEGISLATION SUCH AS THE AUSTRALIAN CONSUMER LAW MAY APPLY REPRESENTATIONS, WARRANTIES, OR CONDITIONS, OR IMPOSES OBLIGATIONS OR LIABILITY ON CSIRO THAT CANNOT BE EXCLUDED, RESTRICTED OR MODIFIED TO THE FULL EXTENT SET OUT IN THE EXPRESS TERMS OF THIS CLAUSE ABOVE "CONSUMER GUARANTEES".  TO THE EXTENT THAT SUCH CONSUMER GUARANTEES CONTINUE TO APPLY, THEN TO THE FULL EXTENT PERMITTED BY THE APPLICABLE LEGISLATION, THE LIABILITY OF CSIRO UNDER THE RELEVANT CONSUMER GUARANTEE IS LIMITED (WHERE PERMITTED AT CSIRO'S OPTION) TO ONE OF FOLLOWING REMEDIES OR SUBSTANTIALLY EQUIVALENT REMEDIES:
(a)               THE REPLACEMENT OF THE SOFTWARE, THE SUPPLY OF EQUIVALENT SOFTWARE, OR SUPPLYING RELEVANT SERVICES AGAIN;
(b)               THE REPAIR OF THE SOFTWARE;
(c)               THE PAYMENT OF THE COST OF REPLACING THE SOFTWARE, OF ACQUIRING EQUIVALENT SOFTWARE, HAVING THE RELEVANT SERVICES SUPPLIED AGAIN, OR HAVING THE SOFTWARE REPAIRED.
IN THIS CLAUSE, CSIRO INCLUDES ANY THIRD PARTY AUTHOR OR OWNER OF ANY PART OF THE SOFTWARE OR MATERIAL DISTRIBUTED WITH IT.  CSIRO MAY ENFORCE ANY RIGHTS ON BEHALF OF THE RELEVANT THIRD PARTY.
Third Party Components
The following third party components are distributed with the Software.  You agree to comply with the licence terms for these components as part of accessing the Software.  Other third party software may also be identified in separate files distributed with the Software.
___________________________________________________________________
See dependencies in licenses.xml. Details about the licenses of the libraries are generated in target/generated-resources/licenses when building the application.
___________________________________________________________________
This file was adapted from: https://github.com/aehrc/fhir-claml
*/

package au.csiro.fhir.claml;

import au.csiro.fhir.claml.model.claml.*;
import au.csiro.fhir.claml.model.claml.Class;
import org.apache.commons.lang3.tuple.Pair;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.List;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toMap;


@SuppressWarnings("SwitchStatementWithTooFewBranches")
public class FhirClamlBfarmConceptService extends FhirClamlConceptService {

    final static List<String> DISPLAY_RUBRICS = List.of("preferredLong", "preferred");
    final static String DEFINITION_RUBRIC = "definition";
    final static List<String> DESIGNATION_RUBRICS = List.of("preferredLong");
    final static List<String> EXCLUDE_CLASS_KINDS = List.of();
    final static List<String> EXCLUDE_RUBRIC_PROPERTIES = List.of("modifierlink");
    final static boolean EXCLUDE_KINDLESS_CLASSES = false;

    final static List<String> PROPERTY_RUBRICS = List.of(
            "inclusion",
            "exclusion",
            "note",
            "text",
            "coding-hint",
            "introduction"
    );

    /**
     * Maps rubrics to properties using the same name
     */
    final static Map<String, String> RUBRIC_TO_PROPERTY_MAP = PROPERTY_RUBRICS
            .stream()
            .map(s -> Pair.of(s, s))
            .collect(toMap(Pair::getKey, Pair::getValue));

    private final BfarmClamlMode clamlMode;

    /**
     * Post-processes the code to match the desired format, according to the BfArM mode
     * @param code the code to process
     * @return the processed code for ICD-O and ICF, or the unprocessed code for NORMAL and OPS
     */
    @Override
    public String postProcessCode(String code) {
        switch (clamlMode) {
            case ICD_O_3 -> {
                return code.replace(':', '/');
            }
            case ICF -> {
                return code.replace(':', '+');
            }
            default -> {
                return code;
            }
        }
    }

    /**
     * Ignore meta that aren't documented in the meta files for the given mode
     * @param mode the claml mode
     * @return the list of meta to ignore
     */
    private static List<String> ignoreMetaForMode(BfarmClamlMode mode) {
        return switch (mode) {
            case ICD_10_GM -> List.of("TextEditor", "UC", "merged");
            default -> List.of();
        };
    }

    /**
     * "hard-code" whether modifier should be used for the given mode
     * @param mode the claml mode
     * @return true if meta should be applied
     */
    private static boolean shouldApplyModifiers(BfarmClamlMode mode) {
        return switch (mode) {
            case ICD_10_GM, ICD_10_WHO, OPS -> true;
            default -> false;
        };
    }

    public FhirClamlBfarmConceptService(File clamlFile, boolean mapAllMetaToProperties, Map<String, String> metaToPropertyMap, BfarmClamlMode mode) {
        super(clamlFile,
                DISPLAY_RUBRICS,
                DEFINITION_RUBRIC,
                DESIGNATION_RUBRICS,
                EXCLUDE_CLASS_KINDS,
                EXCLUDE_RUBRIC_PROPERTIES,
                mapAllMetaToProperties,
                metaToPropertyMap,
                RUBRIC_TO_PROPERTY_MAP,
                ignoreMetaForMode(mode),
                EXCLUDE_KINDLESS_CLASSES,
                shouldApplyModifiers(mode));
        this.clamlMode = mode;
    }

    @Override
    protected String getLabelValue(Reference reference, boolean forDisplay) {
        if (forDisplay) {
            return "";
        }
        String referenceValue = String.format("(%s)", reference.getContent());
        return String.format(" %s", referenceValue);
    }

    @Override
    protected String getLabelValue(Fragment fragment, boolean forDisplay) {
        List<String> parts = fragment.getContent().stream().map(c -> {
            switch (c) {
                case null -> throw new IllegalArgumentException("Fragment contains null content");
                case Reference reference -> {
                    return getLabelValue(reference, forDisplay);
                }
                case String s -> {
                    return s;
                }
                default -> {
                    return "- %s".formatted(getLabelValue(c, forDisplay));
                }
            }
        }).toList();
        return String.join(" ", parts);
    }

    /**
     * override to remove extraneous whitespace that may be present in some BfArM rubrics due to the XML markup
     * @param label the label to process
     * @param forDisplay whether the labelValue is used for the display, not used here
     * @return the string value of the label
     */
    @Override
    protected String getLabelValue(Label label, boolean forDisplay) {
        Pattern extraneousWhitespace = Pattern.compile("\\s{2,}");
        List<String> parts = label.getContent().stream()
                .map(l -> getLabelValue(l, forDisplay)).toList();
        List<String> cleanedParts = parts.stream()
                .map(s -> s.replaceAll(extraneousWhitespace.pattern(), " "))
                .toList();
        return String.join("", cleanedParts)
                .replaceAll("^-\\s*", "") // remove leading hyphens for Fragment rubrics
                .replaceAll(extraneousWhitespace.pattern(), " ") // do it again in case two spaces were joined together above
                .trim();
    }

    /**
     * override to handle special cases for BfArM rubrics
     * @param term the term to process
     * @param forDisplay not used here
     * @return the string value of the term
     */
    @Override
    protected String getLabelValue(Term term, boolean forDisplay) {
        switch (term.getClazz()) {
            case "tab" -> {
                return "\t";
            }
            case "subscript", "superscript", "italics", "italic", "bold" -> {
                return term.getContent();
            }
            default -> {
                log.warn("Unrecognized Term class:" + term.getClazz());
                return term.getContent();
            }
        }
    }

    /**
     * generate a label for a concept based on a label and previously-generated concepts
     * @param concept the concept to generate the label for
     * @param label the label to use
     * @param concepts the list of already-generated concepts
     * @return the String value for the display
     */
    private String generateDisplay(Claml2FhirResult.Concept concept, Label label, Map<String, Claml2FhirResult.Concept> concepts) {
        switch (clamlMode) {
            case OPS -> {
                return generateLabelForOps(concept, label, concepts);
            }
            default -> {
                return getLabelValue(label, true).trim();
            }
        }
    }

    /**
     * special-case label generation for OPS
     * @param concept the concept to generate the label for
     * @param label the label to use
     * @param concepts the list of already-generated concepts
     * @return the String value for the display
     */
    @NotNull
    private String generateLabelForOps(Claml2FhirResult.Concept concept, Label label, Map<String, Claml2FhirResult.Concept> concepts) {
        if (concept.getSingleRubric("preferredLong").isPresent() && !concept.getSingleRubric("preferredLong").get().isEmpty()) {
            return getLabelValue(concept.getSingleRubric("preferredLong").get().getFirst(), true).trim();
        }
        ArrayList<Claml2FhirResult.Concept> ancestors = new ArrayList<>();
        ancestors.add(concept); // add the concept itself (since it is not in the list of parent codes)
        Optional<Claml2FhirResult.Concept> ancestor = getClassByAncestorCodeOps(concept, concepts);
        while (ancestor.isPresent()) {
            ancestors.addFirst(ancestor.get());
            ancestor = getClassByAncestorCodeOps(ancestor.get(), concepts);
        }
        if (ancestors.isEmpty()) {
            return getLabelValue(label, true).trim();
        }
        List<Pair<String, Label>> labelStream = ancestors.stream()
                .map(this::getPreferredDisplayFromConcept)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .toList();
        List<Pair<String, Label>> preferredLong = labelStream.stream().filter(p -> p.getKey().equals("preferredLong")).toList();
        if (preferredLong.isEmpty()) {
            return String.join(": ", labelStream.stream().map(p -> getLabelValue(p.getValue(), true)).toList());
        } else {
            int i = labelStream.indexOf(preferredLong.getLast());
            List<Pair<String, Label>> pairs = labelStream.subList(i, labelStream.size());
            return String.join(": ", pairs.stream().map(p -> getLabelValue(p.getValue(), true)).toList());
        }
    }

    /**
     * generate a display for a concept based on a label and previously-generated concepts
     * @param concept the concept to generate the display for
     * @param label the label to use
     * @param concepts the list of already-generated concepts
     * @return the String value for the display
     */
    @Override
    protected String generateDisplayForConceptFromLabel(Claml2FhirResult.Concept concept, Label label, Map<String, Claml2FhirResult.Concept> concepts) {
        switch (clamlMode) {
            case OPS -> {
                Optional<Claml2FhirResult.Concept> classByAncestorCode = getClassByAncestorCodeOps(concept, concepts);
                return classByAncestorCode.map(value -> generateDisplay(concept, label, concepts)).orElseGet(() -> super.generateDisplayForConceptFromLabel(concept, label, concepts));
            }
            default -> {
                return super.generateDisplayForConceptFromLabel(concept, label, concepts);
            }
        }
    }

    /**
     * get the class by ancestor code for OPS mode
     * @param concept the concept to get the ancestor code for
     * @param concepts the list of already-generated concepts
     * @return the concept with the ancestor code, or empty if not found
     */
    private Optional<Claml2FhirResult.Concept> getClassByAncestorCodeOps(Claml2FhirResult.Concept concept, Map<String, Claml2FhirResult.Concept> concepts) {
        Optional<String> ancestorCode = getAncestorCodeOps(concept);
        if (ancestorCode.isEmpty()) {
            return Optional.empty();
        }
        Claml2FhirResult.Concept parentConcept = concepts.get(ancestorCode.get());
        return Optional.ofNullable(parentConcept);
    }

    /**
     * get an ancestor code from a given code using code semantics from OPS
     * @param concept the concept to get the ancestor code for
     * @return the ancestor code, or empty if none applicable
     */
    private Optional<String> getAncestorCodeOps(Claml2FhirResult.Concept concept) {
        String ancestorCode = concept.getCode();
        if (ancestorCode.contains("...")) { //groups in OPS
            return Optional.empty();
        }
        if (ancestorCode.length() < 5) {
            return Optional.empty();
        }
        while (ancestorCode.length() > 5) {
            ancestorCode = ancestorCode.substring(0, ancestorCode.length() - 1);
            char lastChar = ancestorCode.charAt(ancestorCode.length() - 1);
            if (lastChar == '.') {
                continue;
            }
            return Optional.of(ancestorCode);
        }
        return Optional.empty();
    }

    /**
     * if multiple rubrics are present on the concept, join them with a colon
     * @param rubrics the list of rubrics to join
     * @param displayRubric the display rubric to use, unused here
     * @param modClass the modifier class to use, unused here
     * @param modifiedConcept the modified concept to use, unused here
     * @return the joined string value of the rubrics
     */
    @Override
    protected String getLabelValueForMultipleRubrics(List<Rubric> rubrics, String displayRubric, ModifierClass modClass, String modifiedConcept) {
        List<String> rubricLabels = rubrics.stream().map(r -> getLabelValue(r.getLabel().getFirst(), true)).toList();
        return String.join(": ", rubricLabels);
    }

    /**
     * generates a display for a modified concept based on the modifier class and the modified concept
     * @param candidate the candidate concept that is being modified
     * @param modifiedConcept the modified concept derived from the candidate and the modifier class
     * @param displayRubricValue the display rubric value to use
     * @param modClass the modifier class to use
     * @param concepts the list of already-generated concepts
     * @param displayRubric the display rubric to use
     * @return the string value for the display
     */
    @Override
    protected String generateDisplayForModifiedConcept(
            Claml2FhirResult.Concept candidate,
            Claml2FhirResult.Concept modifiedConcept,
            String displayRubricValue,
            ModifierClass modClass,
            Map<String, Claml2FhirResult.Concept> concepts,
            String displayRubric) {

        Optional<Rubric> modDisplay = getPreferredDisplayFromRubrics(modClass.getRubric());
        if (modDisplay.isEmpty()) {
            log.warn("No display rubric found for modifier class: " + modClass.getCode());
            return displayRubricValue;
        }

        return switch (clamlMode) {
            case OPS -> {
                if (displayRubric.equals("preferredLong")) {
                    Optional<String> preferred = candidate.getSingleRubric("preferred").map(r -> String.format("%s: %s", getLabelValue(r.getFirst(), true), displayRubricValue));
                    if (preferred.isPresent())
                        yield preferred.get();
                }
                String generated = generateDisplay(candidate, modDisplay.get().getLabel().getFirst(), concepts);
                yield "%s: %s".formatted(generated, displayRubricValue);
            }
            default -> {
                String modifiedConceptDisplay = candidate.getDisplay();
                // we do not use a super call here since the default implementation joins like this: "a : b" (with a space before the colon)
                // bfarm doesn't do that, so we need to adjust the format
                String modDisplayValue = getLabelValue(modDisplay.get().getLabel().getFirst(), true);
                yield "%s: %s".formatted(modifiedConceptDisplay, modDisplayValue);
            }
        };

    }

    /**
     * get the preferred display rubric from a list of rubrics
     * @param rubrics the list of rubrics to search
     * @return the preferred display rubric, or empty if none found
     */
    @NotNull
    private Optional<Rubric> getPreferredDisplayFromRubrics(List<Rubric> rubrics) {
        Stream<Rubric> optionalStream = displayRubrics.stream()
                .map(dr -> rubrics.stream().filter(r -> Objects.equals(((RubricKind) r.getKind()).getName(), dr))
                        .findFirst()).filter(Optional::isPresent).map(Optional::get);
        return optionalStream.findFirst();
    }

    /**
     * get the preferred display rubric from a concept
     * @param concept the concept to search
     * @return the the displayRubric name and the associated label (second), or empty if none found
     */
    @NotNull
    private Optional<Pair<String, Label>> getPreferredDisplayFromConcept(Claml2FhirResult.Concept concept) {
        return concept.getRubrics().entrySet().stream()
                .filter(c -> displayRubrics.contains(c.getKey()))
                .map(e -> Pair.of(e.getKey(), e.getValue().getFirst())).findFirst();
    }

    /**
     * ensure that the super class rubrics are present in the concept for OPS mode
     * @param modClass the modifier class to use
     * @param superClassCode the super class code to use
     * @param affectedModifierClasses the list of affected modifier classes
     * @param displayRubricValues the display rubric values to use
     * @param concept the concept to use
     */
    @Override
    protected void ensureSuperClassRubricsInConcept(ModifierClass modClass, String superClassCode, Set<ModifierClass> affectedModifierClasses, Map<String, List<Rubric>> displayRubricValues, Claml2FhirResult.Concept concept) {
        if (clamlMode != BfarmClamlMode.OPS) return;
        if (superClassCode != null && !superClassCode.startsWith("ST")) {
            List<ModifierClass> list = affectedModifierClasses.stream().filter(c -> c.getModifier().equals(modClass.getModifier()) && c.getCode().equals(superClassCode)).toList();
            list.stream().findFirst().ifPresent(
                    superClass -> {
                        for (Rubric superClassRubric : superClass.getRubric()) {
                            RubricKind superClassRubricKind = (RubricKind) superClassRubric.getKind();
                            addDisplayRubric(displayRubricValues, superClassRubric, superClassRubricKind, superClassCode);
                            concept.addRubric(superClassRubricKind.getName(), superClassRubric.getLabel().getFirst());
                        }
                    }
            );
        }
    }

    @NotNull
    @Override
    protected String createCodeForModifiedClass(Claml2FhirResult.Concept cand, ModifierClass modClass) {
        return switch (clamlMode) {
            case ICD_10_WHO, ICD_10_GM: yield createCodeForModifiedClassIcd10(cand, modClass);
            default: yield super.createCodeForModifiedClass(cand, modClass);
        };
    }

    @NotNull
    private String createCodeForModifiedClassIcd10(Claml2FhirResult.Concept cand, ModifierClass modClass) {
        return switch (cand.getCode().length()) {
            case 3: yield cand.getCode() + "." + modClass.getCode().replaceFirst("^\\.", "");
            default: yield super.createCodeForModifiedClass(cand, modClass);
        };
    }

    /**
     * add further attributes to a concept from a class as a side effect
     * @param c the class to use
     * @param concept the concept to use
     */
    @Override
    protected void addFurtherAttributesToConceptFromClass(Class c, Claml2FhirResult.Concept concept) {
        if (c.getUsage() != null) {
            concept.addProperty("usage", ((UsageKind) c.getUsage()).getMark(), Claml2FhirResult.Concept.ConceptProperty.PropertyValueType.STRING);
        }
    }
}

