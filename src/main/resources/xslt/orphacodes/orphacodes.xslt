<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet
        xmlns="urn:mii-termserv:xmlns:babelfsh:codesystem"
        xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
        version="3.0">

    <xsl:output method="xml" indent="yes" encoding="UTF-8"/>

    <!-- declaration from XSLT 3.0, ignore warning from IDE! -->
    <xsl:mode on-no-match="shallow-skip"/>

    <!-- required attribute also not supported by IntelliJ -->
    <xsl:param name="SelfCodeSystemUri" required="true" static="true"/>
    <!-- TODO: further attributes for URIs of properties?-->


    <xsl:template match="//DisorderList">
        <CodeSystem name="ORPHAcodes">
            <concepts>
                <xsl:apply-templates select="Disorder" mode="root"/>
            </concepts>
        </CodeSystem>
    </xsl:template>

    <xsl:template match="Disorder" mode="root">
        <concept>
            <code>
                <xsl:value-of select="OrphaCode"/>
            </code>
            <display>
                <xsl:value-of select="Name"/>
            </display>

            <xsl:apply-templates select="SummaryInformationList/SummaryInformation" mode="definition"/>

            <xsl:call-template name="IdProperty"/>
            <xsl:call-template name="FlagValueProperty"/>
            <xsl:apply-templates select="ExpertLink" mode="property"/>
            <xsl:apply-templates select="AggregationLevelSection/AggregationLevelList" mode="property"/>
            <xsl:apply-templates select="DisorderType" mode="property"/>
            <xsl:apply-templates select="ClassificationLevel" mode="property"/>
            <xsl:apply-templates select="DisorderDisorderAssociationList" mode="property"/>

            <xsl:apply-templates select="SynonymList/Synonym" mode="designation"/>

        </concept>
    </xsl:template>


    <xsl:template
            match="Disorder/SummaryInformationList/SummaryInformation/TextSectionList/TextSection[TextSectionType/@id = '16907']"
            mode="definition">
        <definition>
            <xsl:value-of select="./Contents"/>
        </definition>
    </xsl:template>


    <xsl:template name="IdProperty">
        <property code="Id">
            <valueInteger>
                <xsl:value-of select="@id"/>
            </valueInteger>
        </property>
    </xsl:template>

    <xsl:template name="FlagValueProperty">
        <property code="FlagValue">
            <valueCoding>
                <system>http://orpha.net/fhir/CodeSystem/FlagValue</system>
                <code>
                    <xsl:value-of select="FlagValue"/>
                </code>
                <display>
                    <xsl:value-of select="Totalstatus"/>
                </display>
            </valueCoding>
        </property>
    </xsl:template>

    <xsl:template
            match="AggregationLevelList/AggregationLevel"
            mode="property">
        <property code="AggregationLevel">
            <valueCoding>
                <code>
                    <xsl:value-of select="OrphaCode"/>
                </code>
                <system>
                    <xsl:value-of select="$SelfCodeSystemUri"/>
                </system>
                <display>
                    <xsl:value-of select="PreferredTerm"/>
                </display>
            </valueCoding>
        </property>
    </xsl:template>

    <xsl:template
            match="ClassificationLevel"
            mode="property">
        <property code="ClassificationLevel">
            <valueCoding>
                <code>
                    <xsl:value-of select="@id"/>
                </code>
                <system>http://orpha.net/fhir/CodeSystem/ClassificationLevel</system>
                <display>
                    <xsl:value-of select="Name"/>
                </display>
            </valueCoding>
        </property>
    </xsl:template>

    <xsl:template
            match="ExpertLink"
            mode="property">
        <property code="ExpertLink">
            <valueString>
                <xsl:value-of select="."/>
            </valueString>
        </property>
    </xsl:template>
    <xsl:template match="DisorderType" mode="property">
        <property code="DisorderType">
            <valueCoding>
                <system>http://orpha.net/fhir/CodeSystem/DisorderType</system>
                <code>
                    <xsl:value-of select="./@id"/>
                </code>
                <display>
                    <xsl:value-of select="./Name"/>
                </display>
            </valueCoding>
        </property>
    </xsl:template>

    <xsl:template
            match="DisorderDisorderAssociationList/DisorderDisorderAssociation[DisorderDisorderAssociationType/@id = '27341']"
            mode="property">
        <property code="ReferredTo">
            <valueCode>
                <xsl:value-of select="RootDisorder/OrphaCode"/>
            </valueCode>
        </property>
    </xsl:template>

    <xsl:template
            match="DisorderDisorderAssociationList/DisorderDisorderAssociation[DisorderDisorderAssociationType/@id = '21471']"
            mode="property">
        <property code="MovedTo">
            <valueCode>
                <xsl:value-of select="RootDisorder/OrphaCode"/>
            </valueCode>
        </property>
    </xsl:template>


    <xsl:template
            match="Disorder/SynonymList/Synonym"
            mode="designation">
        <designation>
            <value>
                <xsl:value-of select="."/>
            </value>
            <language>
                <xsl:value-of select="@lang"/>
            </language>
            <use>
                <code>900000000000013009</code>
                <system>http://snomed.info/sct</system>
                <display>Synonym</display>
            </use>
        </designation>
    </xsl:template>

</xsl:stylesheet>
