package de.mii_termserv.babelfsh.companionapps

import com.xenomachina.argparser.ArgParser
import com.xenomachina.argparser.mainBody
import de.mii_termserv.babelfsh.utils.logging.info
import net.sf.saxon.s9api.Processor
import net.sf.saxon.s9api.Serializer
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.io.File
import java.nio.file.Path
import javax.xml.transform.stream.StreamSource

private val logger: Logger = LoggerFactory.getLogger("ClamlXsltTransformer")



class ClamlXsltTransformerArguments(parser: ArgParser) {
    val inputFiles: List<File> by parser.positionalList("input files") {
        Path.of(this).toFile()
    }

    fun resolveOutputFile(inputFile: File): File = inputFile.parentFile.resolve(inputFile.nameWithoutExtension + "_metadata.html")
}

class ClamlXsltTransformer(private val clamlXsltTransformerArguments: ClamlXsltTransformerArguments) {

    private val logger = LoggerFactory.getLogger(javaClass)

    companion object : CompanionApp<ClamlXsltTransformerArguments> {
        override fun parseArgs(argParser: ArgParser): ClamlXsltTransformerArguments = ClamlXsltTransformerArguments(argParser)

        @JvmStatic
        fun main(args: Array<String>) {
            mainBody {
                val clamlXsltTransformerArguments = parseArguments(args, logger)
                val transformer = ClamlXsltTransformer(clamlXsltTransformerArguments)
                transformer.transform()
            }
        }

        override fun app(args: ClamlXsltTransformerArguments) {
            ClamlXsltTransformer(args).transform()
        }
    }

    fun transform() {
        val processor = Processor(false)
        val compiler = processor.newXsltCompiler()
        val stream = this::class.java.classLoader.getResourceAsStream("xslt/claml/claml-metadata.xslt")
        val stylesheet = compiler.compile(StreamSource(stream))
        clamlXsltTransformerArguments.inputFiles.forEach { inputFile ->
            val outputFile = clamlXsltTransformerArguments.resolveOutputFile(inputFile)
            logger.info { "Transforming $inputFile to $outputFile" }

            val serializer = processor.newSerializer(outputFile).apply {
                setOutputProperty(Serializer.Property.INDENT, "yes")
                setOutputProperty(Serializer.Property.METHOD, "html")
            }
            val transformer = stylesheet.load30()
            transformer.transform(StreamSource(inputFile), serializer)
        }
    }
}