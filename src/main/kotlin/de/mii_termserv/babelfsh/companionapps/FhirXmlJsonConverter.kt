package de.mii_termserv.babelfsh.companionapps

import ca.uhn.fhir.context.FhirContext
import com.xenomachina.argparser.ArgParser
import com.xenomachina.argparser.default
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.nio.file.Path
import kotlin.io.path.*

private val logger: Logger = LoggerFactory.getLogger("FhirXmlJsonConverter")

class FhirXmlJsonConverterArgs(parser: ArgParser) {
    val fhirMode: FhirMode by parser.mapping(
    "--r4b" to FhirMode.R4B,
    "--r5" to FhirMode.R5,
    help = "FHIR version to use, default R4B"
    ).default {
        FhirMode.R4B
    }

    val inputFolder: Path by parser.storing(
        "--input-folder",
        help = "Input folder"
    ) {
        Path.of(this).absolute()
    }

    val outputFolder: Path by parser.storing(
        "--output-folder",
        help = "Output folder"
    ) {
        Path.of(this).absolute().also {
            it.createDirectories()
        }
    }

    val inputGlob: String by parser.storing(
        "--input-glob",
        help = "Glob pattern for input files"
    )

    val from: Format by parser.mapping(
        "--from-xml" to Format.XML,
        "--from-json" to Format.JSON,
        help = "Input format, default XML"
    ).default {
        Format.XML
    }

    val to: Format by parser.mapping(
        "--to-xml" to Format.XML,
        "--to-json" to Format.JSON,
        help = "Output format, default XML"
    ).default {
        Format.JSON
    }

    val prettyPrint: Boolean by parser.mapping(
        "--pretty-print" to true,
        "--no-pretty-print" to false,
        help = "Pretty print output, default true"
    ).default {
        true
    }
}

enum class Format(val extension: String) {
    XML("xml"),
    JSON("json")
}

class FhirXmlJsonConverter(private val args: FhirXmlJsonConverterArgs) {

    companion object : CompanionApp<FhirXmlJsonConverterArgs> {
        @JvmStatic
        fun main(args: Array<String>) {
            run(args, logger)
        }

        override fun parseArgs(argParser: ArgParser): FhirXmlJsonConverterArgs = FhirXmlJsonConverterArgs(argParser)

        override fun app(args: FhirXmlJsonConverterArgs) {
            FhirXmlJsonConverter(args).convert()
        }
    }

    fun convert() {
        if (args.from == args.to) {
            logger.error("Input and output format are the same, nothing to do")
            return
        }
        val fhirContext = when (args.fhirMode) {
            FhirMode.R4B -> FhirContext.forR4B()
            else -> FhirContext.forR5()
        }
        val fromParser = when (args.from) {
            Format.XML -> fhirContext.newXmlParser()
            Format.JSON -> fhirContext.newJsonParser()
        }
        val toParser = when(args.to) {
            Format.XML -> fhirContext.newXmlParser()
            Format.JSON -> fhirContext.newJsonParser()
        }.apply {
            setPrettyPrint(args.prettyPrint)
        }

        val inputFiles = args.inputFolder.listDirectoryEntries(glob = args.inputGlob).toList()

        inputFiles.forEach { input ->
            val resource = fromParser.parseResource(input.readText())
            val output = toParser.encodeResourceToString(resource)
            val outputFilename = args.outputFolder.resolve(input.nameWithoutExtension + "." + args.to.extension)
            outputFilename.writeText(output)
            logger.info("Converted $input to ${args.to} format, wrote to $outputFilename")
        }
    }
}