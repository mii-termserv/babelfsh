package de.mii_termserv.babelfsh.companionapps

import ca.uhn.fhir.context.FhirContext
import com.github.doyaaaaaken.kotlincsv.dsl.csvWriter
import com.xenomachina.argparser.ArgParser
import com.xenomachina.argparser.default
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.io.File
import java.nio.file.Path
import org.hl7.fhir.r4b.model.CodeSystem as R4BCs
import org.hl7.fhir.r5.model.CodeSystem as R5Cs


private val logger: Logger = LoggerFactory.getLogger("FhirToCsv")

class FhirToCsvArguments(parser: ArgParser) {
    val inputFile: List<File> by parser.positionalList("input file") {
        Path.of(this).toFile()
    }

    val fhirMode: FhirMode by parser.mapping(
        "--r4b" to FhirMode.R4B,
        "--r5" to FhirMode.R5,
        help = "FHIR version to use, default R4B"
    ).default {
        FhirMode.R4B
    }

    fun resolveOutputFile(inputFile: File): File = inputFile.parentFile.resolve(inputFile.nameWithoutExtension + "_concepts.csv")
}


@Suppress("DuplicatedCode")
class FhirToCsv(private val args: FhirToCsvArguments) {

    companion object : CompanionApp<FhirToCsvArguments> {
        @JvmStatic
        fun main(args: Array<String>) {
            run(args, logger)
        }

        override fun parseArgs(argParser: ArgParser): FhirToCsvArguments = FhirToCsvArguments(argParser)

        override fun app(args: FhirToCsvArguments) {
            FhirToCsv(args).transform()
        }
    }

    fun transform() {
        args.inputFile.forEach { input ->
            val data = when (args.fhirMode) {
                FhirMode.R4B -> transformR4B(input)
                FhirMode.R5 -> transformR5(input)
            }
            csvWriter {
                charset = "UTF-8"
            }.open(args.resolveOutputFile(input)) {
                writeRow(data.first().keys.toList())
                data.forEach { writeRow(it.values.toList()) }
            }
            logger.info("Wrote ${data.size} rows to ${args.resolveOutputFile(input)}")
        }

    }

    private fun transformR5(input: File): List<Map<String,String?>> {
        val cs = FhirContext.forR5().newJsonParser().parseResource(R5Cs::class.java, input.readText())
        val propKeys = cs.concept.flatMap { it.property.map { prop -> prop.code } }.distinct()

        return cs.concept.map { concept ->
            mapOf(
                Headers.code.name to concept.code,
                Headers.display.name to concept.display,
                Headers.definition.name to concept.definition
            ).plus(
                propKeys.map { propKey ->
                    "property_$propKey" to concept.property.find { it.code == propKey }?.value?.primitiveValue()
                }
            )
        }
    }

    private fun transformR4B(input: File): List<Map<String, String?>> {
        val cs = FhirContext.forR4B().newJsonParser().parseResource(R4BCs::class.java, input.readText())
        val propKeys = cs.concept.flatMap { it.property.map { prop -> prop.code } }.distinct()
        return cs.concept.map { concept ->
            mapOf(
                Headers.code.name to concept.code,
                Headers.display.name to concept.display,
                Headers.definition.name to concept.definition
            ).plus(
                concept.designation.map {
                    "designation_${it.language}" to it.value
                }
            ).plus(
                propKeys.map { propKey ->
                    "property_$propKey" to concept.property.find { it.code == propKey }?.value?.primitiveValue()
                }
            )
        }
    }

    @Suppress("EnumEntryName")
    private enum class Headers {
        code, display, definition
    }

}