package de.mii_termserv.babelfsh.companionapps

import com.xenomachina.argparser.ArgParser
import com.xenomachina.argparser.mainBody
import org.slf4j.Logger

interface CompanionApp<T> {
    fun parseArguments(args: Array<String>, logger: Logger): T {
        try {
            return ArgParser(args).parseInto(::parseArgs)
        } catch (e: Exception) {
            logger.error("Error parsing arguments: ${e.message}")
            ArgParser(arrayOf("--help")).parseInto(::parseArgs)
            throw e
        }
    }

    fun parseArgs(argParser: ArgParser): T

    fun app(args: T)

    fun run(args: Array<String>, logger: Logger) {
        mainBody {
            val parsedArgs = parseArguments(args, logger)
            app(parsedArgs)
        }
    }
}

enum class FhirMode {
    R4B, R5
}