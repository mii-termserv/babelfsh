package de.mii_termserv.babelfsh.readers

import com.github.doyaaaaaken.kotlincsv.dsl.context.CsvReaderContext
import com.github.doyaaaaaken.kotlincsv.dsl.csvReader
import de.mii_termserv.babelfsh.antlr.service.babelfsh.comments.CommandLineParseException
import de.mii_termserv.babelfsh.api.plugins.ConversionFailedException
import de.mii_termserv.babelfsh.api.propertymapping.PropertyMapper
import de.mii_termserv.babelfsh.api.propertymapping.PropertyMapperFactory
import kotlinx.serialization.Serializable
import org.slf4j.LoggerFactory
import java.nio.charset.Charset
import java.nio.charset.StandardCharsets
import java.nio.file.Path

class CsvTerminologyReader(
    private val path: Path
) {

    private val logger = LoggerFactory.getLogger(CsvTerminologyReader::class.java)

    fun <T> use(
        headers: List<String>? = null,
        csvConfig: CsvReaderContext.() -> Unit,
        conceptFilter: List<CsvFilter>,
        ensureHeadersExists: List<String>,
        mapData: (Map<String, String>, Int) -> List<T>?
    ): List<T> {
        val reader = csvReader(init = csvConfig)
        val presentHeaders = when (headers) {
            // peek at the first row to get the headers
            null -> {
                reader.open(path.toFile()) {
                    return@open this.readAllWithHeaderAsSequence().first().keys
                }
            }
            else -> headers
        }

        return reader.open(path.toFile()) {
            val data = when (headers) {
                null -> {
                    logger.info("Reading CSV file from $path, using headers from file")
                    this.readAllWithHeaderAsSequence()
                }

                else -> {
                    logger.info("Reading CSV file from $path, using provided headers")
                    this.readAllAsSequence(headers.size).map { row ->
                        headers.zip(row).toMap()
                    }
                }
            }
            ensureHeadersExists.forEach { header ->
                if (header !in presentHeaders) {
                    throw ConversionFailedException("Header '$header' not found in CSV file, but referenced in the command line arguments")
                }
            }
            val filtered = filterData(data, conceptFilter)
            val resultStream = filtered.flatMapIndexed { rowNumber: Int, row: Map<String, String> ->
                mapData(row, rowNumber) ?: emptyList()
            }
            return@open resultStream.toList()
        }
    }

    private fun filterData(data: Sequence<Map<String, String>>, conceptFilter: List<CsvFilter>): Sequence<Map<String, String>> {
        return data.filter { row ->
            conceptFilter.all { filter ->
                val columnValue = row[filter.column]
                when (filter.operatorValue) {
                    CsvFilter.ComparisonOperator.EQUALS -> columnValue == filter.comparison
                    CsvFilter.ComparisonOperator.NOT_EQUALS -> columnValue != filter.comparison
                    CsvFilter.ComparisonOperator.NOT_BLANK -> !columnValue.isNullOrBlank()
                    CsvFilter.ComparisonOperator.BLANK -> columnValue.isNullOrBlank()
                }
            }
        }
    }

    companion object {

        fun <T> readCsv(
            path: Path,
            csvParameters: CsvReaderParameters,
            ensureHeadersExist: List<String> = emptyList(),
            mapData: (Map<String, String>, Int) -> List<T>?
        ): List<T> {
            return CsvTerminologyReader(path).use(
                headers = when (csvParameters.headers.isEmpty()) {
                    true -> null
                    false -> csvParameters.headers
                },
                ensureHeadersExists = ensureHeadersExist,
                csvConfig = getCsvConfig(csvParameters),
                mapData = mapData,
                conceptFilter = csvParameters.conceptFilter
            )
        }

        private fun getCsvConfig(parameters: CsvReaderParameters): (CsvReaderContext.() -> Unit) {
            return {
                delimiter = parameters.delimiter
                charset = parameters.charset.name()
                quoteChar = parameters.quote
                escapeChar = parameters.escape
                skipEmptyLine = parameters.skipEmptyLines
            }
        }
    }

}

data class CsvReaderParameters(
    val delimiter: Char = ',',
    val quote: Char = '"',
    val escape: Char = '"',
    val skipEmptyLines: Boolean = false,
    val charset: Charset = StandardCharsets.UTF_8,
    val lineEnding: String = "\n",
    val headers: List<String> = emptyList(),
    val conceptFilter: List<CsvFilter> = emptyList(),
)

@Serializable
data class CsvFilter(
    val column: String,
    val operator: String,
    val comparison: String? = null
) {

    enum class ComparisonOperator(val tokens: List<String>, val needsValue: Boolean = true) {
        EQUALS(listOf("=", "==", "equals")),
        NOT_EQUALS(listOf("!=", "<>", "not-equals", "not_equals")),
        NOT_BLANK(listOf("not-blank", "not_blank"), false),
        BLANK(listOf("blank"), false)
    }

    val operatorValue: ComparisonOperator
        get() = ComparisonOperator.entries.find { operator.lowercase() in it.tokens }
            ?: throw CommandLineParseException("Unknown operator $operator", null)

    companion object {
        fun operatorHelp(): String {
            val operators = ComparisonOperator.entries.associate { entry -> entry.name to entry.tokens.joinToString { "'$it'" } }
            return "Supported operators with supported case-insensitive keywords in parentheses: ${operators.entries.joinToString { "${it.key} (${it.value})" }}"
        }
    }
}


@Serializable
data class PropertyMappingEntry(
    val column: String,
    val property: String,
    val required: Boolean = false,
    val mapper: PropertyMapper? = null
) {
    companion object {
        fun help() =
            buildString {
                append("The property mapping is a JSON array of objects, where each object has the following properties: ")
                append("<column>: the name of the column in the CSV file; ")
                append("<code>: the code of the property in the FHIR CodeSystem; ")
                append("<required>: whether the property is required (true or false, default false); ")
                append("<mapper>: a mapper specification (see below)")
                append(
                    "Example: {\"column\": \"my_column\", " +
                            "\"required\": true, " +
                            "\"property\": \"my_property\", " +
                            "\"mapper\": {\"id\": \"boolean\", \"arguments\": {\"1\": \"true\", \"0\": \"false\"}}}"
                )
                append(PropertyMapperFactory.mapperHelp())
            }
    }
}