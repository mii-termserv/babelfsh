@file:Suppress("unused")

package de.mii_termserv.babelfsh.plugins.core.csv


import com.ibm.icu.impl.Assert.fail
import com.xenomachina.argparser.ArgParser
import com.xenomachina.argparser.default
import de.mii_termserv.babelfsh.antlr.service.babelfsh.comments.CommandLineParseException
import de.mii_termserv.babelfsh.api.plugins.PluginArguments
import de.mii_termserv.babelfsh.readers.CsvFilter
import de.mii_termserv.babelfsh.readers.CsvReaderParameters
import java.net.URI
import java.net.URL
import java.nio.charset.Charset
import java.nio.charset.StandardCharsets
import java.nio.file.Path


@Suppress("MemberVisibilityCanBePrivate")
abstract class CsvArguments(parser: ArgParser, workingDirectory: Path?) : PluginArguments(parser, workingDirectory) {

    abstract val referencedColumns: Set<String>

    override fun validateDependentArguments() {
        if (path == null && url == null) {
            failValidation("Either 'path' or 'url' must be provided")
        }
    }

    fun toCsvParams() = CsvReaderParameters(
        delimiter = delimiter,
        quote = quote,
        escape = escape,
        skipEmptyLines = skipEmptyLines,
        charset = charset,
        lineEnding = lineEnding,
        headers = headers,
        conceptFilter = conceptFilter
    )

    val path: Path? by parser.storing(
        "--path",
        "-p",
        "--file",
        "-f",
        help = "Path to the CSV file"
    ) {
        convertToAbsolutePath()
    }.default(null)

    val url: URL? by parser.storing(
        "--url",
        "-u",
        help = "URL to the CSV file"
    ) {
        URI.create(this.preprocess()).toURL()
    }.default(null)

    val delimiter by parser.storing(
        "--delimiter",
        "-d",
        help = "Delimiter used in the CSV file. Default is comma (,)"
    ) {
        when (val p = this.preprocess()) {
            "\\t" -> '\t'
            else -> p.single()
        }
    }.default(',')

    val quote by parser.storing(
        "--csv-quote",
        help = "Quote character used in the CSV file. Default is double quote (\")"
    ) {
        this.preprocess().single()
    }.default('"')

    val escape by parser.storing(
        "--csv-escape",
        help = "Escape character used in the CSV file. Default is double quote (\")"
    ) {
        this.preprocess().single()
    }.default('"')

    val skipEmptyLines by parser.flagging(
        "--skip-empty-lines",
        "-s",
        help = "Skip empty lines in the CSV file"
    )

    val charset: Charset by parser.storing(
        "--charset",
        "-c",
        help = "The charset of the file, with the name from java.nio.charset.Charset: ${
            Charset.availableCharsets().keys.joinToString(
                ", ",
                limit = 5,
                truncated = "..."
            )
        } See https://docs.oracle.com/javase/8/docs/technotes/guides/intl/encoding.doc.html"
    ) {
        if (!Charset.isSupported(this.preprocess())) fail("Charset $this is not supported")
        Charset.forName(this.preprocess())
    }.default { StandardCharsets.UTF_8 }


    val lineEnding: String by parser.storing(
        "--line-ending",
        "--le",
        help = "Line ending used in the CSV file, default \\n"
    ) { this.preprocess() }.default("\n")

    val headers: List<String> by parser.storing(
        "--headers",
        "--hd",
        help = "List of headers to use, provided as a JSON list. " +
                "If not provided, headers will be read from the file. " +
                "Example: [\"header1\", \"header2\"]"
    ) {
        try {
            return@storing jsonArrayToStringList(this.trim('\''))
        } catch (e: Exception) {
            throw CommandLineParseException("Could not parse 'headers' parameter for the CSV plugin due to: ${e.message}", null)
        }
    }.default(emptyList())

    val conceptFilter: List<CsvFilter> by parser.storing(
        "--concept-filter",
        "--cf",
        help = "Filter concepts based on the values in the CSV file. ${CsvFilter.operatorHelp()}"
    ) {
        return@storing decodeJsonArray<CsvFilter>(this, "concept-filter", "CSV") {
            if (it.column.isBlank() || it.operator.isBlank()) {
                failValidation("Concept filter needs to have a non-empty column and operator. Found: $it")
            }
            try {
                val operatorValue = it.operatorValue
                if (operatorValue.needsValue && it.comparison.isNullOrBlank()) {
                    failValidation("Operator '${it.operatorValue.name}' requires a comparison value")
                }
            } catch (e: Exception) {
                failValidation(e.message ?: "Unknown error")
            }
        }
    }.default(emptyList())
}