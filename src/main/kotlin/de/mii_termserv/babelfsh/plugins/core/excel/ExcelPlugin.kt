package de.mii_termserv.babelfsh.plugins.core.excel

import com.xenomachina.argparser.ArgParser
import com.xenomachina.argparser.default
import de.mii_termserv.babelfsh.api.BabelfshContext
import de.mii_termserv.babelfsh.api.content_containers.cs.CodeSystemConceptContainer
import de.mii_termserv.babelfsh.api.content_containers.cs.CodeSystemContainer
import de.mii_termserv.babelfsh.api.content_containers.cs.ConceptPropertyContainer
import de.mii_termserv.babelfsh.api.plugins.PluginArguments
import de.mii_termserv.babelfsh.api.plugins.cs.CodeSystemPlugin
import de.mii_termserv.babelfsh.utils.logging.info
import org.apache.poi.ss.usermodel.Row
import org.apache.poi.ss.usermodel.WorkbookFactory
import org.slf4j.LoggerFactory
import java.nio.file.Path

abstract class ExcelCodeSystemArguments(parser: ArgParser, workingDirectory: Path?) : PluginArguments(parser, workingDirectory) {
    val excelFile: Path by parser.storing("-f", "--file", help = "The Excel file to be processed") {
        convertToAbsolutePath()
    }
    val sheet: String by parser.storing("-s", "--sheet", help = "The sheet name to be processed") {
        preprocess()
    }

    abstract val codeColumnIndex: Int
    abstract val displayColumnIndex: Int
    abstract val definitionColumnIndex: Int?
    abstract val skipRows: Int
}


class GenericExcelCodeSystemPluginArgs(parser: ArgParser, workingDirectory: Path?) :
    ExcelCodeSystemArguments(parser = parser, workingDirectory = workingDirectory) {

    override val codeColumnIndex: Int by parser.storing("-c", "--code-column", help = "The column index of the code (zero-based!)") {
        preprocess().toInt()
    }

    override val displayColumnIndex: Int by parser.storing(
        "-d",
        "--display-column",
        help = "The column index of the display (zero-based!)"
    ) {
        preprocess().toInt()
    }

    override val definitionColumnIndex: Int? by parser.storing(
        "--definition-column",
        help = "The column index of the definition (zero-based!)"
    ) {
        preprocess().toInt()
    }.default(null)

    override val skipRows: Int by parser.storing(
        "-r",
        "--skip-rows",
        help = "The number of rows to skip at the head, default is 0, i.e. no rows are skipped"
    ) {
        preprocess().toInt()
    }.default {
        0
    }
}


abstract class ExcelCodeSystemPlugin<T : ExcelCodeSystemArguments>(babelfshContext: BabelfshContext, pluginId: String, shortHelp: String) :
    CodeSystemPlugin<ExcelCodeSystemArguments>(
        pluginId = pluginId,
        babelfshContext = babelfshContext,
        shortHelp = shortHelp
    ) {


    private val logger = LoggerFactory.getLogger(javaClass)

    override fun produceContent(args: PluginArguments, resource: CodeSystemContainer): List<CodeSystemConceptContainer> {
        args as ExcelCodeSystemArguments
        return readExcel(args)
    }

    private fun readExcel(args: ExcelCodeSystemArguments): List<CodeSystemConceptContainer> {
        logger.info { "Reading ${args.excelFile}" }
        val workbook = WorkbookFactory.create(args.excelFile.toFile())
        val sheetNames = workbook.sheetIterator().asSequence().map { it.sheetName.trim() }.toList()
        if (sheetNames.isEmpty()) throw IllegalArgumentException("No sheets found in ${args.excelFile}")
        if (!sheetNames.contains(args.sheet)) throw IllegalArgumentException("Sheet ${args.sheet} not found in ${args.excelFile}, these sheets are available: ${sheetNames.joinToString { "'$it'" }}")
        val sheetIndex = sheetNames.indexOf(args.sheet)
        val sheet = workbook.getSheetAt(sheetIndex)
            ?: throw IllegalArgumentException("Sheet ${args.sheet} not found in ${args.excelFile}")

        logger.info { "Reading the row range ${sheet.firstRowNum + args.skipRows} to ${sheet.lastRowNum}" }

        val rowRange = (sheet.firstRowNum + args.skipRows)..sheet.lastRowNum

        val terminologyContentContainers = rowRange.mapNotNull { rowIndex ->
            val row = sheet.getRow(rowIndex) ?: return@mapNotNull null // if the row is empty, the lib conveniently returns null
            val code = row.getCell(args.codeColumnIndex, Row.MissingCellPolicy.RETURN_BLANK_AS_NULL)?.stringCellValue?.let {
                postProcessCode(it)
            } ?: return@mapNotNull null
            val display =
                row.getCell(args.displayColumnIndex, Row.MissingCellPolicy.RETURN_BLANK_AS_NULL)?.stringCellValue ?: return@mapNotNull null
            val definition = args.definitionColumnIndex?.let {
                row.getCell(it, Row.MissingCellPolicy.RETURN_BLANK_AS_NULL)?.stringCellValue
            }
            val properties = propertiesFromRow(row, rowIndex, args, code, display)
            CodeSystemConceptContainer(code = code, display = display, definition = definition, property = properties)
        }

        workbook.close()

        return terminologyContentContainers
    }

    open fun propertiesFromRow(
        row: Row,
        rowIndex: Int,
        args: ExcelCodeSystemArguments,
        code: String,
        display: String
    ): List<ConceptPropertyContainer> {
        return emptyList()
    }

    open fun postProcessCode(code: String): String {
        return code
    }
}

class GenericExcelCodeSystemPlugin(babelfshContext: BabelfshContext) :
    ExcelCodeSystemPlugin<GenericExcelCodeSystemPluginArgs>(
        babelfshContext = babelfshContext,
        pluginId = "excel",
        shortHelp = "Convert from MS Excel files (.xlsx)",
    ) {
    override fun parseInto(parser: ArgParser, workingDirectory: Path?): GenericExcelCodeSystemPluginArgs {
        return parser.parseInto { GenericExcelCodeSystemPluginArgs(it, workingDirectory) }
    }
}