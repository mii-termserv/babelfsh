package de.mii_termserv.babelfsh.plugins.core.csv

import com.xenomachina.argparser.ArgParser
import com.xenomachina.argparser.default
import de.mii_termserv.babelfsh.api.BabelfshContext
import de.mii_termserv.babelfsh.api.content_containers.cm.Canonical
import de.mii_termserv.babelfsh.api.content_containers.cm.ConceptMapContainer
import de.mii_termserv.babelfsh.api.content_containers.cm.ConceptMapEntryContainer
import de.mii_termserv.babelfsh.api.http_client.FileDownloadHelper.Companion.downloadFileIfNecessary
import de.mii_termserv.babelfsh.api.plugins.ConversionFailedException
import de.mii_termserv.babelfsh.api.plugins.PluginArguments
import de.mii_termserv.babelfsh.api.plugins.cm.CmFhirCompatibility
import de.mii_termserv.babelfsh.api.plugins.cm.ConceptMapPlugin
import de.mii_termserv.babelfsh.api.plugins.cm.fhirCompatibilityArg
import de.mii_termserv.babelfsh.readers.CsvTerminologyReader
import de.mii_termserv.babelfsh.utils.logging.warn
import kotlinx.coroutines.runBlocking
import org.hl7.fhir.r4b.model.ConceptMap.ConceptMapEquivalence
import org.hl7.fhir.r5.model.Enumerations.ConceptMapRelationship
import org.slf4j.LoggerFactory
import java.nio.file.Path

class CsvConceptMapPlugin(babelfshContext: BabelfshContext) : ConceptMapPlugin<CsvConceptMapPlugin.CsvCmArguments>(
    pluginId = "csv",
    babelfshContext = babelfshContext,
    shortHelp = "Create ConceptMap resources from a CSV mapping file",
) {

    private val logger = LoggerFactory.getLogger(javaClass)

    override fun parseInto(parser: ArgParser, workingDirectory: Path?): CsvCmArguments =
        parser.parseInto { CsvCmArguments(it, workingDirectory) }

    override fun produceContent(args: PluginArguments, resource: ConceptMapContainer): List<ConceptMapEntryContainer> {
        val csvArgs = args as CsvCmArguments
        val pathToReadFrom = runBlocking {
            downloadFileIfNecessary(csvArgs.path, csvArgs.url, logger)
        }
        return CsvTerminologyReader.readCsv(
            path = pathToReadFrom,
            csvParameters = csvArgs.toCsvParams(),
            ensureHeadersExist = csvArgs.referencedColumns.toList()
        ) { conceptRow, rowNumber ->
            return@readCsv listOf(generateConceptMapEntry(conceptRow, csvArgs, rowNumber))
        }.sortedBy { "${it.sourceCode}|${it.targetCode}" }
    }

    private fun generateConceptMapEntry(
        conceptRow: Map<String, String>,
        csvArgs: CsvCmArguments,
        rowNumber: Int
    ): ConceptMapEntryContainer {
        val sourceCode = conceptRow[csvArgs.sourceCodeColumn]
            ?: throw ConversionFailedException("Source code (from column ${csvArgs.sourceCodeColumn}) }is missing in row $rowNumber")
        val sourceDisplay = csvArgs.sourceDisplayColumn?.let { conceptRow[it]!! }
        val targetCode = conceptRow[csvArgs.targetCodeColumn]
            ?: throw ConversionFailedException("Target code (from column ${csvArgs.targetCodeColumn}) is missing in row $rowNumber")
        val targetDisplay = csvArgs.targetDisplayColumn?.let { conceptRow[it]!! }
        val sourceCanonical = getUriOrDefault(
            uriColumnName = csvArgs.sourceSystemUriColumn,
            conceptRow = conceptRow,
            fixedSystem = csvArgs.fixedSourceSystem,
            errorMessageIfMissing = "Source system URI (with optional version) is missing in row $rowNumber and no default is provided"
        )
        val targetCanonical = getUriOrDefault(
            uriColumnName = csvArgs.targetSystemUriColumn,
            conceptRow = conceptRow,
            fixedSystem = csvArgs.fixedTargetSystem,
            errorMessageIfMissing = "Target system URI (with optional version) is missing in row $rowNumber and no default is provided"
        )

        val equivalenceR4 = csvArgs.equivalenceColumnR4?.let { conceptRow[it] }?.let {
            ConceptMapEquivalence.valueOf(it.uppercase())
        }
        val relationshipR5 = csvArgs.relationshipColumnR5?.let { conceptRow[it] }?.let {
            ConceptMapRelationship.valueOf(it.uppercase())
        }

        when {
            (csvArgs.fhirCompatibility == CmFhirCompatibility.R4 || csvArgs.fhirCompatibility == CmFhirCompatibility.BOTH) && equivalenceR4 == null -> {
                throw ConversionFailedException("No equivalence provided for FHIR R4 in row $rowNumber")
            }

            //TODO: if the row is not mapped in R5, this means that it should be unmapped
            (csvArgs.fhirCompatibility == CmFhirCompatibility.R5 || csvArgs.fhirCompatibility == CmFhirCompatibility.BOTH) && relationshipR5 == null -> {
                throw ConversionFailedException("No relationship provided for FHIR R5 in row $rowNumber")
            }

            (equivalenceR4 == null && relationshipR5 == null) -> {
                throw ConversionFailedException("No equivalence/relationship provided for FHIR R4 and R5 in row $rowNumber")
            }
        }

        val comment = csvArgs.commentColumn?.let { conceptRow[it] }

        val targetDisplayIfEquivalence = when {
            equivalenceR4 != ConceptMapEquivalence.UNMATCHED -> targetDisplay
            else -> null
            // TODO this doesn't work for R5, obviously
        }

        return ConceptMapEntryContainer(
            sourceCanonical = sourceCanonical,
            targetCanonical = targetCanonical,
            sourceCode = sourceCode,
            sourceDisplay = sourceDisplay,
            targetCode = targetCode,
            targetDisplay = targetDisplayIfEquivalence,
            equivalenceR4 = equivalenceR4,
            relationshipR5 = relationshipR5,
            //r5NoMap = args.fhirCompatibility != relationshipR5 == null,
            // TODO: this should be set explicitly in the CSV plugin, not in the serialization to FHIR R5 as it is done currently
            targetComment = comment
        )
    }

    private fun splitUriStringToCanonicalAndVersion(value: String?): Canonical? = when {
        value == null -> null
        "|" in value -> {
            val (canonical, version) = value.split("|")
            Canonical(canonical, version)
        }

        else -> Canonical(value)
    }

    private fun getUriOrDefault(
        uriColumnName: String?,
        conceptRow: Map<String, String>,
        fixedSystem: String?,
        errorMessageIfMissing: String
    ): Canonical {
        when {
            uriColumnName != null -> {
                return splitUriStringToCanonicalAndVersion(conceptRow[uriColumnName]) ?: run {
                    when (fixedSystem) {
                        null -> throw ConversionFailedException(errorMessageIfMissing)
                        else -> splitUriStringToCanonicalAndVersion(fixedSystem) ?: throw ConversionFailedException(
                            errorMessageIfMissing
                        )
                    }
                }
            }

            else -> {
                return splitUriStringToCanonicalAndVersion(fixedSystem) ?: run {
                    throw ConversionFailedException(errorMessageIfMissing)
                }
            }
        }
    }

    class CsvCmArguments(parser: ArgParser, workingDirectory: Path?) :
        CsvArguments(parser = parser, workingDirectory = workingDirectory) {

        private val logger = LoggerFactory.getLogger(javaClass)

        override val referencedColumns: Set<String>
            get() = setOfNotNull(
                sourceCodeColumn,
                sourceDisplayColumn,
                targetCodeColumn,
                targetDisplayColumn,
                sourceSystemUriColumn,
                targetSystemUriColumn,
                equivalenceColumnR4,
                relationshipColumnR5,
                commentColumn
            )

        val fhirCompatibility by fhirCompatibilityArg()

        val sourceCodeColumn by parser.storing(
            "--source-code-column",
            help = "Name of the column containing the source codes"
        ) {
            preprocess()
        }

        val targetCodeColumn by parser.storing(
            "--target-code-column",
            help = "Name of the column containing the target codes"
        ) {
            preprocess()
        }

        val sourceDisplayColumn: String? by parser.storing(
            "--source-display-column",
            help = "Name of the column containing the source display names"
        ) {
            preprocess()
        }.default(null).addValidator {
            if (this.value == null) {
                logger.warn { "No source display column specified" }
            }
        }

        val targetDisplayColumn: String? by parser.storing(
            "--target-display-column",
            help = "Name of the column containing the target display names"
        ) {
            preprocess()
        }.default(null).addValidator {
            if (this.value == null) {
                logger.warn { "No target display column specified" }
            }
        }

        val sourceSystemUriColumn: String? by parser.storing(
            "--source-system-uri-column",
            help = "Name of the column containing the source system URIs"
        ) {
            preprocess()
        }.default(null)

        val targetSystemUriColumn: String? by parser.storing(
            "--target-system-uri-column",
            help = "Name of the column containing the target system URIs"
        ) {
            preprocess()
        }.default(null)

        val fixedSourceSystem: String? by parser.storing(
            "--fixed-source-system",
            help = "Fixed source system URI for all mappings"
        ) {
            preprocess()
        }.default(null)

        val fixedTargetSystem: String? by parser.storing(
            "--fixed-target-system",
            help = "Fixed target system URI for all mappings"
        ) {
            preprocess()
        }.default(null)

        val equivalenceColumnR4: String? by parser.storing(
            "--equivalence-column-r4",
            help = "Name of the column containing the equivalence values for FHIR R4/R4B"
        ) {
            preprocess()
        }.default(null)

        val relationshipColumnR5: String? by parser.storing(
            "--relationship-column-r5",
            help = "Name of the column containing the relationship values for FHIR R5"
        ) {
            preprocess()
        }.default(null)

        val commentColumn: String? by parser.storing(
            "--comment-column",
            help = "Name of the column containing comments for the target"
        ) {
            preprocess()
        }.default(null)

        override fun validateDependentArguments() {
            if (sourceSystemUriColumn != null && fixedSourceSystem != null) {
                failValidation("Cannot specify both --source-system-uri-column and --fixed-source-system")
            }
            if (targetSystemUriColumn != null && fixedTargetSystem != null) {
                failValidation("Cannot specify both --target-system-uri-column and --fixed-target-system")
            }

            if (equivalenceColumnR4 != null && relationshipColumnR5 != null) {
                failValidation("Cannot specify both --equivalence-column-r4 and --equivalence-column-r5")
            }
            if (fhirCompatibility == CmFhirCompatibility.R4 && relationshipColumnR5 != null) {
                failValidation("Cannot specify --equivalence-column-r5 if only R4 is supported")
            }
            if (fhirCompatibility == CmFhirCompatibility.R4 && equivalenceColumnR4 == null) {
                failValidation("Must specify --equivalence-column-r4 for FHIR R4 since R4 is specified to be supported")
            }
            if (fhirCompatibility == CmFhirCompatibility.R5 && equivalenceColumnR4 != null) {
                failValidation("Cannot specify --equivalence-column-r4 if only R5 is supported")
            }
            if (fhirCompatibility == CmFhirCompatibility.R5 && relationshipColumnR5 == null) {
                failValidation("Must specify --equivalence-column-r5 for FHIR R5 since R5 is specified to be supported")
            }
            if (fhirCompatibility == CmFhirCompatibility.BOTH && (equivalenceColumnR4 == null || relationshipColumnR5 == null)) {
                failValidation("Must specify --equivalence-column-r4 and --equivalence-column-r5 for FHIR R4 and R5")
            }
        }
    }
}

