package de.mii_termserv.babelfsh.plugins.core.claml

import au.csiro.fhir.claml.BfarmClamlMode
import au.csiro.fhir.claml.Claml2FhirResult
import au.csiro.fhir.claml.Claml2FhirResult.Concept.ConceptProperty
import au.csiro.fhir.claml.FhirClamlBfarmConceptService
import au.csiro.fhir.claml.FhirClamlConceptService
import com.xenomachina.argparser.ArgParser
import com.xenomachina.argparser.default
import de.mii_termserv.babelfsh.api.BabelfshContext
import de.mii_termserv.babelfsh.api.content_containers.PropertyContainer
import de.mii_termserv.babelfsh.api.content_containers.cs.*
import de.mii_termserv.babelfsh.api.plugins.PluginArguments
import de.mii_termserv.babelfsh.api.plugins.cs.CodeSystemPlugin
import java.nio.file.Path
import java.util.*


abstract class ClamlCodeSystemPlugin<ArgType : ClamlArguments>(
    pluginId: String,
    babelfshContext: BabelfshContext,
    shortHelp: String,
    extraHelp: String? = null
) :
    CodeSystemPlugin<ClamlArguments>(
        pluginId = pluginId,
        babelfshContext = babelfshContext,
        shortHelp = shortHelp,
        extraHelp = extraHelp,
        attribution = "This plugin is based on 'fhir-claml' by the Australian e-Health Research Center (AEHRC) at CSIRO, Australia. See https://github.com/aehrc/fhir-claml for more information."
    ) {
    abstract fun createClamlConverter(args: ClamlArguments): FhirClamlConceptService

    private fun generateFromClaml(args: PluginArguments): Claml2FhirResult {
        val clamlConverter = createClamlConverter(args as ClamlArguments)
        return clamlConverter.babelfshClaml2Fhir() ?: failConversion("Failed to generate FHIR CodeSystem from ClaML")
    }

    abstract fun clamlPropToPropertyContainer(resource: CodeSystemContainer, clamlProp: ConceptProperty): ConceptPropertyContainer

    override fun produceContent(args: PluginArguments, resource: CodeSystemContainer): List<CodeSystemConceptContainer> {
        val clamlResult = generateFromClaml(args as ClamlArguments)
        val data = clamlResult.concepts.filter {
            true
        }.map { clamlConcept ->
            val designations = clamlConcept.designations.map { des ->
                DesignationContainer(
                    language = des.language,
                    use = PropertyValueType.CodingContainer(system = null, code = des.useAsString, display = null, userSelected = null, version = null),
                    value = des.value
                )
            }
            val properties = clamlConcept.properties.map { clamlProp ->
                clamlPropToPropertyContainer(resource, clamlProp)
            }
            CodeSystemConceptContainer(
                code = clamlConcept.code,
                display = clamlConcept.display,
                definition = clamlConcept.definition,
                designation = designations,
                property = properties
            )
        }.sortedBy { it.code }

        return data
    }
}

class StandardClamlCodeSystemPlugin(babelfshContext: BabelfshContext) : ClamlCodeSystemPlugin<StandardClamlCodeSystemPlugin.StandardClamlArguments>(
    pluginId = "claml-standard",
    babelfshContext = babelfshContext,
    shortHelp = "Convert a ClaML file to a FHIR CodeSystem",
    extraHelp = buildString {
        append("The arguments --display-rubric, --designation-rubric, --exclude-class-kind can include a ")
        append("comma-separated list to provide multiple arguments. Example: --display-rubric 'foo,bar'")
    },
) {

    class StandardClamlArguments(parser: ArgParser, workingDirectory: Path?) : ClamlArguments(parser, workingDirectory) {

        val displayRubrics: List<String> by parser.storing(
            "--display-rubric",
            "--dis-rub",
            help = buildString {
                append("A comma-separated list of ClaML rubric/s that might contain the ")
                append("concepts' displays. The first populated rubric in the list that is present on a class will be ")
                append("used as the concept's display text. Subsequent rubrics in the list will be used as designations. ")
                append("Default is 'preferred'")
            }
        ) {
            makeList()
        }.default {
            listOf("preferred")
        }

        val definitionRubric: String by parser.storing(
            "--definition-rubric",
            "--def-rub",
            help = buildString {
                append("The ClaML rubric that contains the concepts' definitions. ")
                append("Default is 'definition'")
            }
        ) {
            preprocess()
        }.default("definition")

        val designationRubrics: List<String> by parser.storing(
            "--designation-rubric",
            "--des-rub",
            help = buildString {
                append("A list of ClaML rubrics that contain the concepts' synonyms. Default is empty.")
            }
        ) {
            makeList()
        }.default {
            emptyList()
        }

        val excludeClassKind: List<String> by parser.storing("--exclude-class-kind", help = "The class kind to exclude") {
            makeList()
        }.default {
            emptyList()
        }

        val excludeKindlessClasses: Boolean by parser.mapping(
            "--include-kindless-classes" to false,
            "--exclude-kindless-classes" to true,
            help = "Whether to include kindless classes. Default is to include them."
        ).default(false)

        val excludeRubricProperties: List<String> by parser.storing(
            "--exclude-rubric-properties",
            help = "The rubrics to exclude in the creation of concept properties"
        ) {
            makeList()
        }.default {
            emptyList()
        }

        val ignoreMetaCodes: List<String> by parser.storing(
            "--ignore-meta-codes",
            help = "A list of meta codes to ignore"
        ) {
            makeList()
        }.default { listOf() }

        val applyModifiers: Boolean by parser.mapping(
            "--apply-modifiers" to true,
            "--dont-apply-modifiers" to false,
            help = "Whether to apply modifiers (default is not to apply them)"
        ).default(false)

        override fun toString(): String {
            return "ClamlArguments(path=$path, displayRubrics=$displayRubrics, definitionRubric='$definitionRubric', designationRubrics=$designationRubrics, excludeClassKind=$excludeClassKind, excludeKindlessClasses=$excludeKindlessClasses, applyModifiers=$applyModifiers)"
        }

        override fun validateDependentArguments() {
            if (mapAllMetaToProperties && metaToPropertyMap.isNotEmpty()) {
                failValidation("Combining the flag '--map-all-meta-to-properties' with '--meta-to-property-passthrough' or '--meta-to-property-renamed' is not allowed")
            }
        }
    }

    override fun parseInto(parser: ArgParser, workingDirectory: Path?): ClamlArguments {
        return parser.parseInto { StandardClamlArguments(it, workingDirectory) }
    }

    override fun createClamlConverter(args: ClamlArguments): FhirClamlConceptService {
        if (args !is StandardClamlArguments) failConversion("Invalid arguments")
        return FhirClamlConceptService(
            args.path.toFile(),
            args.displayRubrics,
            args.definitionRubric,
            args.designationRubrics,
            args.excludeClassKind,
            args.excludeRubricProperties,
            args.mapAllMetaToProperties,
            args.metaToPropertyMap,
            args.rubricToPropertyMap,
            args.ignoreMetaCodes,
            args.excludeKindlessClasses,
            args.applyModifiers
        )
    }

    override fun clamlPropToPropertyContainer(resource: CodeSystemContainer, clamlProp: ConceptProperty) =
        ConceptPropertyContainer(
            code = clamlProp.code,
            valueType = when (clamlProp.valueType!!) {
                ConceptProperty.PropertyValueType.CODE -> PropertyValueType.CodeContainer(clamlProp.valueAsString)
                ConceptProperty.PropertyValueType.STRING -> PropertyValueType.StringContainer(clamlProp.valueAsString)
            }
        )
}

class ClamlBfarmCodeSystemPlugin(babelfshContext: BabelfshContext) : ClamlCodeSystemPlugin<ClamlBfarmCodeSystemPlugin.BfarmClamlArguments>(
    pluginId = "claml-bfarm",
    babelfshContext = babelfshContext,
    shortHelp = "A version of the ClaML plugin fine-tuned for BfArM ClaML files",
    extraHelp = buildString {
        append("This plugin is fine-tuned to provide adequate concepts for CodeSystems provided by the German BfArM ")
        append("(Bundesinstitut für Arzneimittel und Medizinprodukte). It may produce results that are not suitable ")
        append("for other ClaML files produced by other agencies. Use the 'claml-standard' plugin for general use cases.")
    }
) {

    class BfarmClamlArguments(parser: ArgParser, workingDirectory: Path?) : ClamlArguments(parser, workingDirectory) {

        val mode: BfarmClamlMode by parser.storing(
            "--mode",
            help = "The mode of the ClaML-BfArM plugin. Choices: ${BfarmClamlMode.entries.joinToString(", ")}"
        ) {
            BfarmClamlMode.valueOf(this.preprocess().uppercase(Locale.getDefault()))
        }

        override fun toString(): String {
            return super.toString().trimEnd(')') + ", mode=$mode)"
        }
    }

    override fun parseInto(parser: ArgParser, workingDirectory: Path?): ClamlArguments =
        parser.parseInto { BfarmClamlArguments(it, workingDirectory) }

    override fun createClamlConverter(args: ClamlArguments): FhirClamlConceptService {
        args as BfarmClamlArguments
        return FhirClamlBfarmConceptService(
            /* clamlFile = */ args.path.toFile(),
            /* mapAllMetaToProperties = */ args.mapAllMetaToProperties,
            /* metaToPropertyMap = */ args.metaToPropertyMap,
            /* mode = */ args.mode
        )
    }

    override fun clamlPropToPropertyContainer(resource: CodeSystemContainer, clamlProp: ConceptProperty): ConceptPropertyContainer {
        val matchingProperty = resource.getProperties().find { it.code == clamlProp.code }
        return ConceptPropertyContainer(
            code = clamlProp.code,
            valueType = when (val matchingPropType = matchingProperty?.type) {
                null -> when (clamlProp.valueType!!) {
                    ConceptProperty.PropertyValueType.CODE -> PropertyValueType.CodeContainer(clamlProp.valueAsString)
                    ConceptProperty.PropertyValueType.STRING -> PropertyValueType.StringContainer(clamlProp.valueAsString)
                }
                else -> when (matchingPropType) {
                    PropertyContainer.PropertyTypeEnum.boolean -> when (clamlProp.valueAsString.lowercase()) {
                        "j", "y" -> PropertyValueType.BooleanContainer(true)
                        "n" -> PropertyValueType.BooleanContainer(false)
                        else -> failConversion("Invalid boolean value: ${clamlProp.valueAsString}")
                    }
                    PropertyContainer.PropertyTypeEnum.code -> PropertyValueType.CodeContainer(clamlProp.valueAsString)
                    PropertyContainer.PropertyTypeEnum.string -> PropertyValueType.StringContainer(clamlProp.valueAsString)
                    else -> failConversion("Invalid property type: $matchingPropType")

                }
            }
        )
    }

}

abstract class ClamlArguments(parser: ArgParser, workingDirectory: Path?) : PluginArguments(parser, workingDirectory) {

    val path: Path by parser.storing(
        "--path",
        "-p",
        help = "Path to the CLAML file"
    ) {
        convertToAbsolutePath()
    }

    val mapAllMetaToProperties by parser.flagging(
        "--map-all-meta-to-properties",
        help = "Map all meta to properties with the same name"
    ).default(false)

    private val metaToProvideUnmapped by parser.storing(
        "--meta-to-property-passthrough",
    ) {
        jsonArrayToStringList(this)
    }.default(emptyList())

    private val metaToProvideRenamed by parser.storing(
        "--meta-to-property-renamed",
    ) {
        jsonStringToStringMap(this)
    }.default(emptyMap())

    val metaToPropertyMap get() = metaToProvideRenamed + metaToProvideUnmapped.associateWith { it }

    private val rubricToPropertyUnmapped by parser.storing(
        "--rubric-to-property-passthrough",
    ) {
        jsonArrayToStringList(this)
    }.default(emptyList())

    private val rubricToPropertyRenamed by parser.storing(
        "--rubric-to-property-renamed",
    ) {
        jsonStringToStringMap(this)
    }.default(emptyMap())

    val rubricToPropertyMap get() = rubricToPropertyRenamed + rubricToPropertyUnmapped.associateWith { it }
}