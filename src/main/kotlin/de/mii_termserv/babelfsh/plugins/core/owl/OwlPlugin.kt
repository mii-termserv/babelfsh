package de.mii_termserv.babelfsh.plugins.core.owl

import au.csiro.fhir.owl.FhirOwlService
import au.csiro.fhir.owl.OwlProperties
import com.xenomachina.argparser.ArgParser
import com.xenomachina.argparser.InvalidArgumentException
import com.xenomachina.argparser.default
import de.mii_termserv.babelfsh.api.BabelfshContext
import de.mii_termserv.babelfsh.api.content_containers.cs.CodeSystemConceptContainer
import de.mii_termserv.babelfsh.api.content_containers.cs.CodeSystemContainer
import de.mii_termserv.babelfsh.api.plugins.PluginArguments
import de.mii_termserv.babelfsh.api.plugins.cs.CodeSystemPlugin
import java.nio.file.Path

class OwlPlugin(babelfshContext: BabelfshContext) : CodeSystemPlugin<OwlPluginArgs>(
    pluginId = "owl",
    babelfshContext = babelfshContext,
    shortHelp = "Convert OWL files using fhir-owl (https://github.com/aehrc/fhir-owl)",
    attribution = "This plugin is based on 'fhir-owl' by the Australian e-Health Research Center (AEHRC) as CSIRO, Australia. See https://github.com/aehrc/fhir-owl for more information."
) {

    override fun parseInto(parser: ArgParser, workingDirectory: Path?): OwlPluginArgs =
        parser.parseInto { OwlPluginArgs(it, workingDirectory) }

    override fun produceContent(args: PluginArguments, resource: CodeSystemContainer): List<CodeSystemConceptContainer> {
        args as OwlPluginArgs
        val service = FhirOwlService(args)
        return service.transform(resource)
    }
}

class OwlPluginArgs(parser: ArgParser, workingDirectory: Path?) : PluginArguments(parser, workingDirectory) {

    private val reasonerChoices = listOf("elk", "jfact")

    val owlFile by parser.storing(
        "--file",
        help = "The OWL file to parse"
    ) {
        convertToAbsolutePath()
    }

    val codeProperty: String? by parser.storing(
        "--code-property", "--code-prop", "--code",
        help = "The code property to use"
    ) {
        preprocess()
    }.default(null)

    val codeReplace: Pair<String, String>? by parser.storing(
        "--code-replace",
        help = "Two strings separated by a comma. Replaces the first string with the second string in all local codes."
    ) {
        val processed = this.preprocess()
        processed.split(",").let { it[0] to it[1] }
    }.default(null)

    val displayProperty: String? by parser.storing(
        "--display-property", "--display-prop", "--display",
        help = "Indicates which annotation property contains the concepts' displays. Default is '${OwlProperties.RDFS_LABEL}'."
    ) {
        preprocess()
    }.default(null)

    val definitionProperty: String? by parser.storing(
        "--definition-property", "--definition-prop", "--definition",
        help = "Indicates which annotation property contains the concepts' definitions. Default is None."
    ) {
        preprocess()
    }.default(null)

    val includeDeprecated: Boolean by parser.mapping(
        "--include-deprecated" to true,
        "--no-include-deprecated" to false,
        help = "Include deprecated concepts in the code system. Default is false."
    ).default(false)

    val labelsToExclude: List<String> by parser.storing(
        "--exclude-labels",
        help = "Comma-separated list of class labels to exclude from the code system."
    ) {
        makeList()
    }.default(emptyList())

    val mainNs: Set<String> by parser.storing(
        "--main-namespaces", "--main-ns",
        help = "Comma-separated list of namespace prefixes that determine which classes are part of the main ontology."
    ) {
        makeList().toSet()
    }.default(emptySet())

    val synonymsProp: List<String> by parser.storing(
        "--synonyms-property", "--synonyms-prop", "--synonyms",
        help = "Comma-separated list of OWL annotation properties that contain the concepts' synonyms."
    ) {
        makeList()
    }.default(emptyList())

    val reasoner: String by parser.storing(
        "--reasoner",
        help = "The reasoner to use. Choices are ${reasonerChoices.joinToString()}. Default is 'elk'"
    ) {
        preprocess().lowercase()
    }.default("elk").addValidator {
        if (this.value !in reasonerChoices) {
            throw InvalidArgumentException("Invalid reasoner. Choices are: ${reasonerChoices.joinToString()}")
        }
    }

    val iriMappings: Path? by parser.storing(
        "--iri-mappings",
        help = "A file containing IRI mappings. The file should be a comma-separated file associating an ontology IRI with a path relative to the BabelFSH file. Default is null."
    ) {
        convertToAbsolutePath()
    }.default(null)
}