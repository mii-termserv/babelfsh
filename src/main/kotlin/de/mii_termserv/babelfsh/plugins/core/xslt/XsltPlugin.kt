package de.mii_termserv.babelfsh.plugins.core.xslt

import com.xenomachina.argparser.ArgParser
import com.xenomachina.argparser.default
import de.mii_termserv.babelfsh.api.BabelfshContext
import de.mii_termserv.babelfsh.api.content_containers.cs.*
import de.mii_termserv.babelfsh.api.plugins.ConversionFailedException
import de.mii_termserv.babelfsh.api.plugins.PluginArguments
import de.mii_termserv.babelfsh.api.plugins.cs.CodeSystemPlugin
import de.mii_termserv.babelfsh.plugins.core.xslt.GenericXsltCodeSystemPlugin.XsltCsArgs
import de.mii_termserv.babelfsh.utils.logging.debug
import de.mii_termserv.babelfsh.utils.logging.info
import de.mii_termserv.babelfsh.utils.logging.warn
import io.github.furstenheim.CopyDown
import net.sf.saxon.s9api.*
import net.sf.saxon.s9api.streams.Steps
import org.apache.commons.lang3.StringUtils
import org.apache.commons.text.StringEscapeUtils
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.core.io.ClassPathResource
import java.io.File
import java.nio.file.Path
import javax.xml.XMLConstants
import javax.xml.transform.stream.StreamSource
import javax.xml.validation.SchemaFactory
import kotlin.io.path.createTempFile
import kotlin.jvm.optionals.getOrNull

private const val NAMESPACE_CODESYSTEM = "urn:mii-termserv:xmlns:babelfsh:codesystem"

abstract class XsltCodeSystemPlugin<ArgType : PluginArguments>(
    pluginId: String,
    babelfshContext: BabelfshContext,
    shortHelp: String,
    extraHelp: String? = null,
    attribution: String? = null,
) : CodeSystemPlugin<ArgType>(
    pluginId = pluginId,
    babelfshContext = babelfshContext,
    shortHelp = shortHelp,
    extraHelp = extraHelp,
    attribution = attribution
) {
    private val processor by lazy {
        Processor(false)
    }

    private val copyDown by lazy {
        CopyDown()
    }

    private fun XdmNode.stringValueWithoutEscapes(rewriteToMarkdown: Boolean): String {
        val unescaped = StringEscapeUtils.unescapeHtml4(stringValue)
        return when (rewriteToMarkdown) {
            true -> copyDown.convert(unescaped)
            else -> unescaped
        }
    }

    private val validator by lazy {
        val schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI)
        val schema = schemaFactory.newSchema(StreamSource(ClassPathResource("xsd/babelfsh-target.xsd").inputStream))
        schema.newValidator()
    }

    protected fun validateTransformedXmlFile(tempFile: File, logger: Logger) {
        try {
            validator.validate(StreamSource(tempFile.inputStream()))
        } catch (e: Exception) {
            logger.error("Could not validate output file from XSLT transformation")
            failConversion("XML validation error; ${e.message}")
        }
    }

    protected fun produceContentFromTempFile(
        tempFile: File,
        args: XsltCsArgs,
        logger: Logger
    ): List<CodeSystemConceptContainer> {
        val inputFile = processor.newDocumentBuilder().build(tempFile)
        val xpathCompiler = processor.newXPathCompiler().apply {
            declareNamespace("", NAMESPACE_CODESYSTEM)
        }
        val conceptPath = xpathCompiler.compile("//concepts/concept").load().apply {
            contextItem = inputFile
        }
        val conceptResults = conceptPath.stream().map { cItem ->
            val code = cItem["code"]!!.stringValueWithoutEscapes(args.rewriteHtmlToMarkdown)
            val display = cItem["display"]?.stringValueWithoutEscapes(args.rewriteHtmlToMarkdown)
            val definition = cItem["definition"]?.stringValueWithoutEscapes(args.rewriteHtmlToMarkdown)
            val designation = cItem.select(Steps.child("designation")).asList().mapNotNull {
                parseDesignation(it!!, args)
            }
            if (args.filterConceptWithoutDisplayAndDesignation && display == null && designation.isEmpty()) {
                logger.warn { "Skipping concept $code because it has neither display nor designation" }
                return@map null
            }
            val property = cItem.select(Steps.child("property")).asList().mapNotNull {
                parseProperty(code, it!!, args)
            }
            return@map CodeSystemConceptContainer(
                code = code,
                display = display,
                definition = definition,
                designation = designation,
                property = property
            )
        }.toList().filterNotNull()
        logger.info { "Generated ${conceptResults.size} concepts using the XSLT" }

        val duplicates = conceptResults.groupBy { it.code }.filter { it.value.size > 1 }
        val finalConceptList = when (duplicates.any()) {
            true -> handleDuplicates(conceptResults, duplicates, args, logger)
            else -> conceptResults
        }

        return finalConceptList
    }

    private fun handleDuplicates(
        conceptResults: List<CodeSystemConceptContainer>,
        duplicates: Map<String, List<CodeSystemConceptContainer>>,
        args: XsltCsArgs,
        logger: Logger
    ): List<CodeSystemConceptContainer> {
        val withoutDuplicates = conceptResults.filter { it.code !in duplicates.keys }
        duplicates.onEach { (code, concepts) ->
            val displays =
                concepts.map { it.display }.toSet().joinToString("; ") { if (it != null) "'$it'" else "null" }
            val designations = concepts.flatMap { c -> c.designation.map { it.value } }.toSet().joinToString("; ") {
                "'${StringUtils.abbreviate(it, 50)}'"
            }
            logger.warn { "Duplicate code $code (displays: $displays; designations $designations)" }
        }
        when {
            args.mergeDuplicateCodes -> {
                logger.warn {
                    "Found ${duplicates.size} duplicate codes (${
                        duplicates.map { it.value.size }.sum()
                    } entries) in ${conceptResults.size} concepts, merging"
                }
                val resolvedDuplicates = duplicates.mapNotNull { entry ->
                    val displays = entry.value.map { it.display }.toSet()
                    if (displays.size > 1) {
                        throw ConversionFailedException(
                            "At least for the concept ${entry.key}, there are multiple displays defined: ${displays.joinToString()}. Can't handle this case =(",
                            recommendPluginImplementation = true
                        )
                    }
                    val resultConcept = CodeSystemConceptContainer(
                        code = entry.key,
                        display = displays.firstOrNull(),
                        definition = entry.value.mapNotNull { it.definition }.joinToString { "; " }.let {
                            it.ifBlank { null }
                        },
                        designation = entry.value.flatMap { it.designation }.toSet().toList(),
                        property = entry.value.flatMap { it.property }.toSet().toList()
                    )
                    resultConcept
                }
                return withoutDuplicates + resolvedDuplicates
            }

            args.filterForDuplicateCodes -> {
                return withoutDuplicates
            }

            else -> {
                throw ConversionFailedException(
                    "Duplicate codes are present, but not strategy was chosen to handle them =(",
                    recommendPluginImplementation = true
                )
            }
        }
    }

    protected fun convertUsingStylesheet(
        inputFile: File,
        xsltFile: File,
        logger: Logger,
        xsltArguments: Map<String, String>? = null
    ): File {
        logger.debug { "Compiling XSLT at: $xsltFile" }

        val xsltCompiler = processor.newXsltCompiler()
        xsltArguments?.forEach { (name, value) ->
            xsltCompiler.setParameter(
                QName(name), XdmAtomicValue(value)
            )
        }
        val executable = xsltCompiler.compile(xsltFile)
        val transformer = executable.load30()

        val tempOutputFile = createTempFile("babelfsh", ".xml").toFile().also {
            logger.debug {
                "Writing XSLT output to temporary file: ${it.absolutePath}"
            }
        }

        val serializer = processor.newSerializer(tempOutputFile)
        transformer.transform(
            StreamSource(inputFile),
            serializer
        )
        logger.debug {
            "Wrote XSLT result to $tempOutputFile"
        }
        return tempOutputFile
    }

    private fun parseProperty(conceptCode: String, node: XdmNode, args: XsltCsArgs): ConceptPropertyContainer {
        val propCode = node["code", ChildKind.ATTRIBUTE]!!.stringValueWithoutEscapes(args.rewriteHtmlToMarkdown)
        val valueNodes = node.select(Steps.child { child ->
            return@child child.nodeName?.localName?.startsWith("value") == true
        }).asList()
        if (valueNodes.size != 1) {
            throw IllegalStateException("Expected exactly one value node in property $propCode in concept $conceptCode")
        }
        val theNode = valueNodes.first()
        val propValue = when (theNode.nodeName.localName) {
            "valueCode" -> PropertyValueType.CodeContainer(theNode.stringValueWithoutEscapes(args.rewriteHtmlToMarkdown))
            "valueString" -> PropertyValueType.StringContainer(theNode.stringValueWithoutEscapes(args.rewriteHtmlToMarkdown))
            "valueInteger" -> PropertyValueType.IntegerContainer(theNode.intValue)
            "valueBoolean" -> PropertyValueType.BooleanContainer(theNode.booleanValue)
            "valueDateTime" -> PropertyValueType.DateTimeContainer(theNode.stringValue)
            "valueDecimal" -> PropertyValueType.DecimalContainer(theNode.decimalValue)
            "valueCoding" -> parseCodingProperty(theNode, args)
            else -> throw IllegalStateException("Unknown property value type ${theNode.nodeName.localName} in concept code=$conceptCode")
        }
        return ConceptPropertyContainer(
            code = propCode,
            valueType = propValue as PropertyValueType<*, *>
        )
    }

    private fun parseCodingProperty(theNode: XdmNode, args: XsltCsArgs): PropertyValueType.CodingContainer {
        val system = theNode["system"]?.stringValueWithoutEscapes(args.rewriteHtmlToMarkdown)
        val version = theNode["version"]?.stringValueWithoutEscapes(args.rewriteHtmlToMarkdown)
        val code = theNode["code"]?.stringValueWithoutEscapes(args.rewriteHtmlToMarkdown)
        val display = theNode["display"]?.stringValueWithoutEscapes(args.rewriteHtmlToMarkdown)
        val userSelected = theNode["userSelected"]?.booleanValue
        return PropertyValueType.CodingContainer(
            system = system,
            version = version,
            code = code,
            display = display,
            userSelected = userSelected
        )
    }

    private fun parseDesignation(node: XdmNode, args: XsltCsArgs): DesignationContainer {
        val language = node["language"]?.stringValueWithoutEscapes(args.rewriteHtmlToMarkdown)
        val use = node["use"]?.let { parseCodingProperty(it, args) }
        val value = node["value"]!!.stringValueWithoutEscapes(args.rewriteHtmlToMarkdown)
        val additionalUse = node.select(Steps.child("additionalUse")).asList().map {
            parseCodingProperty(it, args)
        }
        return DesignationContainer(
            language = language,
            use = use,
            value = value,
            additionalUse = additionalUse
        )
    }
}

class GenericXsltCodeSystemPlugin(
    babelfshContext: BabelfshContext
) : XsltCodeSystemPlugin<GenericXsltCodeSystemPlugin.XsltCsArgs>(
    babelfshContext = babelfshContext, pluginId = "xslt",
    shortHelp = "Convert XML files using XML Stylesheets (XSLT) transformations to a specified schema",
) {

    class XsltCsArgs(parser: ArgParser, workingDirectory: Path?) : PluginArguments(parser, workingDirectory) {

        val xsltFile by parser.storing(
            "--xslt",
            help = "The relative path to the XSLT file for converting to the target schema",
        ) {
            convertToAbsolutePath()
        }

        val xmlFile by parser.storing(
            "--xml",
            help = "The relative path to the XML file to be converted",
        ) {
            convertToAbsolutePath()
        }

        override fun validateDependentArguments() {
            if (this.mergeDuplicateCodes && this.filterForDuplicateCodes) {
                failValidation("The arguments `merge-duplicate-codes-by-designation` and `filter-duplicate-codes` are incompatible.")
            }
        }

        val filterConceptWithoutDisplayAndDesignation by parser.mapping(
            mapOf(
                "--filter-concepts-without-display-and-designations" to true,
                "--dont-filter-concepts" to false
            ),
            help = "Filter out concepts that have neither display nor designation, default false"
        ).default(false)

        val filterForDuplicateCodes by parser.mapping(
            mapOf(
                "--filter-duplicate-codes" to true,
                "--dont-filter-duplicate-codes" to false
            ),
            help = "Filter out codes with duplicate codes"
        ).default(false)

        val rewriteHtmlToMarkdown by parser.mapping(
            mapOf(
                "--rewrite-html-to-markdown" to true,
                "--dont-rewrite-html-to-markdown" to false
            ), help = "Rewrite HTMl in string fields to Markdown, default false"
        ).default(false)

        val mergeDuplicateCodes by parser.mapping(
            mapOf(
                "--merge-duplicate-codes-by-designation" to true,
                "--dont-merge-duplicate-codes-by-designation" to false,
            ),
            help = "Merge duplicate codes (their designations)"
        ).default(false)
    }

    private val logger = LoggerFactory.getLogger(javaClass)

    override fun parseInto(parser: ArgParser, workingDirectory: Path?): XsltCsArgs =
        parser.parseInto { XsltCsArgs(it, workingDirectory) }

    override fun produceContent(
        args: PluginArguments,
        resource: CodeSystemContainer
    ): List<CodeSystemConceptContainer> {
        args as XsltCsArgs
        val tempFile = convertUsingStylesheet(
            inputFile = args.xmlFile.toFile(),
            xsltFile = args.xsltFile.toFile(),
            logger = logger
        )
        validateTransformedXmlFile(tempFile, logger)
        return produceContentFromTempFile(tempFile, args, logger)
    }

}

private operator fun XdmItem.get(childName: String, what: ChildKind = ChildKind.ELEMENT): XdmNode? = when (what) {
    ChildKind.ELEMENT -> select(Steps.child(childName)).findFirst().getOrNull()
    ChildKind.ATTRIBUTE -> select(Steps.attribute(childName)).findFirst().getOrNull()
}

private val XdmNode.booleanValue get() = (typedValue as XdmAtomicValue).booleanValue
private val XdmNode.intValue get() = (typedValue as XdmAtomicValue).longValue.toInt()
private val XdmNode.decimalValue get() = (typedValue as XdmAtomicValue).decimalValue.toDouble()

private enum class ChildKind {
    ELEMENT, ATTRIBUTE
}