package de.mii_termserv.babelfsh.plugins.core.csv

import com.xenomachina.argparser.ArgParser
import com.xenomachina.argparser.default
import de.mii_termserv.babelfsh.api.BabelfshContext
import de.mii_termserv.babelfsh.api.content_containers.PropertyContainer
import de.mii_termserv.babelfsh.api.content_containers.cs.CodeSystemConceptContainer
import de.mii_termserv.babelfsh.api.content_containers.cs.CodeSystemContainer
import de.mii_termserv.babelfsh.api.content_containers.cs.ConceptPropertyContainer
import de.mii_termserv.babelfsh.api.content_containers.cs.PropertyValueType
import de.mii_termserv.babelfsh.api.http_client.FileDownloadHelper.Companion.downloadFileIfNecessary
import de.mii_termserv.babelfsh.api.plugins.ConversionFailedException
import de.mii_termserv.babelfsh.api.plugins.PluginArguments
import de.mii_termserv.babelfsh.api.plugins.cs.CodeSystemPlugin
import de.mii_termserv.babelfsh.readers.CsvTerminologyReader
import de.mii_termserv.babelfsh.readers.PropertyMappingEntry
import kotlinx.coroutines.runBlocking
import org.slf4j.LoggerFactory
import java.nio.file.Path

class CsvCodeSystemPlugin(babelfshContext: BabelfshContext) :
    CodeSystemPlugin<CsvCodeSystemPlugin.CsvCsArguments>(
        pluginId = "csv",
        babelfshContext = babelfshContext,
        shortHelp = "Convert CSV files with custom column allocations",
    ) {

    private val logger = LoggerFactory.getLogger(javaClass)

    override fun parseInto(parser: ArgParser, workingDirectory: Path?): CsvCsArguments =
        parser.parseInto { CsvCsArguments(it, workingDirectory) }

    override fun produceContent(
        args: PluginArguments,
        resource: CodeSystemContainer
    ): List<CodeSystemConceptContainer> {
        val csvArgs = args as CsvCsArguments
        val pathToReadFrom = runBlocking {
            downloadFileIfNecessary(csvArgs.path, csvArgs.url, logger)
        }
        val conceptList = CsvTerminologyReader.readCsv(
            path = pathToReadFrom,
            csvParameters = args.toCsvParams(),
            ensureHeadersExist = csvArgs.referencedColumns.toList()
        ) { conceptRow, rowNumber ->
            return@readCsv generateConcepts(conceptRow, csvArgs, args, rowNumber, resource)
        }
        return filterDuplicates(conceptList, args).sortedWith(
            compareBy(comparator = alphanumericComparator, selector = { it.code })
        )
    }

    private fun filterDuplicates(
        conceptList: List<CodeSystemConceptContainer>,
        args: CsvCsArguments
    ): List<CodeSystemConceptContainer> {
        val duplicates = conceptList.groupingBy { it.code }.eachCount().filterValues { it > 1 }
        return when {
            duplicates.isNotEmpty() && !args.filterDuplicates -> {
                failConversion("Duplicate concepts found: $duplicates")
            }

            duplicates.isNotEmpty() && args.filterDuplicates -> {
                val duplicateConcepts = duplicates.mapValues { (code, _) -> conceptList.filter { it.code == code } }
                for (duplicateGroup in duplicateConcepts) {
                    if (duplicateGroup.value.all { it == duplicateGroup.value.first() }) {
                        continue
                    }
                    failConversion("Duplicate concepts found for code ${duplicateGroup.key}: ${duplicateGroup.value}")
                }
                val nonDuplicates = conceptList.filter { it.code !in duplicates.keys }
                val uniqueConcepts = nonDuplicates + duplicateConcepts.map { it.value.first() }
                uniqueConcepts
            }

            else -> conceptList
        }
    }

    private fun generateConcepts(
        conceptRow: Map<String, String>,
        csvArgs: CsvCsArguments,
        args: CsvCsArguments,
        rowNumber: Int,
        resource: CodeSystemContainer
    ): List<CodeSystemConceptContainer>? {
        val code = conceptRow[csvArgs.codeColumnName]!!.ifBlank {
            if (args.allowMissingCodeColumn) {
                return null
            }
            throw ConversionFailedException("Code column ${args.codeColumnName} is empty in row $rowNumber, but missing codes aren't filtered")
        }
        val display = conceptRow[csvArgs.displayColumnName]
        val definition = csvArgs.definitionColumn?.let { conceptRow[it] }
        return when (args.splitCodeAndDisplayUsing) {
            null -> {
                val properties = generatePropertiesForConceptRow(csvArgs.propertyMapping, conceptRow, resource, code)
                listOf(
                    CodeSystemConceptContainer(
                        code = code,
                        display = display,
                        definition = definition,
                        property = properties
                    )
                )
            }

            else -> {
                val splitCode = code.split(args.splitCodeAndDisplayUsing!!)
                val splitDisplay = display?.split(args.splitCodeAndDisplayUsing!!)
                val splitDefinition = definition?.split(args.splitCodeAndDisplayUsing!!)
                if (display != null && splitDisplay?.size != splitCode.size) {
                    failConversion("Splitting code and display using ${args.splitCodeAndDisplayUsing} resulted in different number of parts for code and display in row $rowNumber")
                }
                if (definition != null && splitDefinition?.size != splitCode.size) {
                    failConversion("Splitting code and display using ${args.splitCodeAndDisplayUsing} resulted in different number of parts for code and definition in row $rowNumber")
                }
                splitCode.mapIndexed { index, splitCodePart ->
                    CodeSystemConceptContainer(
                        code = splitCodePart,
                        display = splitDisplay?.getOrNull(index),
                        definition = splitDefinition?.getOrNull(index),
                        property = listOf()
                    )
                }
            }
        }
    }

    class CsvCsArguments(parser: ArgParser, workingDirectory: Path?) : CsvArguments(parser, workingDirectory) {

        override val referencedColumns: Set<String>
            get() = setOfNotNull(
                codeColumnName,
                displayColumnName,
                definitionColumn,
                *propertyMapping.map { it.column }.toTypedArray()
            )

        val codeColumnName: String by parser.storing(
            "--code-column",
            "--cc",
            help = "Name of the column containing the codes",
        ) { this.preprocess() }

        val displayColumnName: String by parser.storing(
            "--display-column",
            "--dc",
            help = "Name of the column containing the display names",
        ) { this.preprocess() }

        val definitionColumn: String? by parser.storing(
            "--definition-column",
            help = "Name of the column containing the definitions",
        ) { this.preprocess() }.default { null }

        val allowMissingCodeColumn: Boolean by parser.flagging(
            "--allow-missing-code-column",
            help = "Allow missing code column in CSV file (these rows are filtered out, useful for mapping source files that mainly define a ConceptMap). This is opt-in, i.e. default=false"
        ).default(false)

        val propertyMapping: List<PropertyMappingEntry> by parser.storing(
            "--property-mapping",
            "--pm",
            help = "Mapping of additional properties to columns in the CSV file. " + PropertyMappingEntry.help(),
        ) {
            propertyMappingFromString(stringValue = this, argumentName = "property-mapping", pluginName = "csv")
        }.default(listOf())

        val filterDuplicates by parser.mapping(
            "--filter-duplicates" to true,
            "--no-filter-duplicates" to false,
            help = "Filter duplicate concepts based on code and display name. If collisions occur, this will result in an error. Default false (all duplicates are treated as errors)."
        ).default(false)

        val splitCodeAndDisplayUsing: Char? by parser.storing(
            "--split-code-and-display-using",
            help = "If specified, split code and display columns using this separator. Default is not to split."
        )
        {
            preprocess().single()
        }.default(null)

        override fun validateDependentArguments() {
            super.validateDependentArguments()

            if (headers.isNotEmpty() && codeColumnName !in headers) {
                failValidation("Code column $codeColumnName not found in headers")
            }
            if (headers.isNotEmpty() && displayColumnName !in headers) {
                failValidation("Display column $displayColumnName not found in headers")
            }

            propertyMapping.forEach {
                if (it.column !in headers && headers.isNotEmpty()) {
                    failValidation("Column ${it.column} not found in headers")
                }
            }

            if (propertyMapping.isNotEmpty() && splitCodeAndDisplayUsing != null) {
                failValidation(
                    "Won't split code and display columns when using property mapping. " +
                            "This is to finicky to get right in the general-purpose CSV plugin, so implementing a purpose-built plugin is required."
                )
            }
        }
    }
}

private fun mapPropertyValueToContainer(
    propertyValueType: PropertyContainer.PropertyTypeEnum,
    columnValue: String
): PropertyValueType<*, *> = when (propertyValueType) {
    PropertyContainer.PropertyTypeEnum.code -> PropertyValueType.CodeContainer(columnValue)
    PropertyContainer.PropertyTypeEnum.string -> PropertyValueType.StringContainer(columnValue)
    PropertyContainer.PropertyTypeEnum.integer -> PropertyValueType.IntegerContainer(columnValue.toInt())
    PropertyContainer.PropertyTypeEnum.boolean -> PropertyValueType.BooleanContainer(columnValue.toBoolean())
    PropertyContainer.PropertyTypeEnum.dateTime -> PropertyValueType.DateTimeContainer(columnValue)
    PropertyContainer.PropertyTypeEnum.decimal -> PropertyValueType.DecimalContainer(columnValue.toDouble())
    PropertyContainer.PropertyTypeEnum.Coding -> throw NotImplementedError("Generating CODING properties from CSV is not implemented. Implement a mapper for this that gives the URL of the system from the command line arguments.")
}

fun CodeSystemPlugin<*>.generatePropertiesForConceptRow(
    propertyMapping: List<PropertyMappingEntry>,
    row: Map<String, String>,
    resource: CodeSystemContainer,
    code: String
): List<ConceptPropertyContainer> {
    return propertyMapping.mapNotNull { propertyMappingEntry ->
        if (propertyMappingEntry.column !in row) {
            failConversion("Column ${propertyMappingEntry.column} not found in CSV file")
        }
        val columnValue = row[propertyMappingEntry.column]
        when {
            columnValue.isNullOrBlank() && propertyMappingEntry.required -> {
                failConversion("Required column ${propertyMappingEntry.column} is missing for concept with code $code")
            }

            !columnValue.isNullOrBlank() -> {
                val matchingProperty = resource.getProperties().find { it.code == propertyMappingEntry.property }
                    ?: run {
                        getImplicitPropertyForCode(propertyMappingEntry.property)
                            ?: throw ConversionFailedException("Property ${propertyMappingEntry.property} not found in CodeSystem")
                    }
                val propCode = propertyMappingEntry.property
                val propertyValueType = matchingProperty.type
                val propValue = when (propertyMappingEntry.mapper) {
                    null -> mapPropertyValueToContainer(propertyValueType, columnValue)
                    else -> propertyMappingEntry.mapper.mapper.mapPropertyValue(columnValue)
                }
                ConceptPropertyContainer(propCode, propValue)
            }

            else -> null
        }
    }
}