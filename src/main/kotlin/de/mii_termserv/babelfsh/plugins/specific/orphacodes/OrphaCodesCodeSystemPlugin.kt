package de.mii_termserv.babelfsh.plugins.specific.orphacodes

import com.xenomachina.argparser.ArgParser
import com.xenomachina.argparser.default
import de.mii_termserv.babelfsh.api.BabelfshContext
import de.mii_termserv.babelfsh.api.content_containers.cs.CodeSystemConceptContainer
import de.mii_termserv.babelfsh.api.content_containers.cs.CodeSystemContainer
import de.mii_termserv.babelfsh.api.plugins.PluginArguments
import de.mii_termserv.babelfsh.plugins.core.xslt.XsltCodeSystemPlugin
import de.mii_termserv.babelfsh.utils.getStrictParser
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json
import org.slf4j.LoggerFactory
import org.springframework.core.io.ClassPathResource
import java.io.File
import java.nio.file.Path

enum class SupportedOrphaLanguages(val languageName: String) {
    EN("English"), CZ("Czech"), NL("Dutch"), FR("French"), DE("German"), IT("Italian"), ES("Spanish"), PL("Polish"), PT(
        "Portuguese"
    ),
}

private val logger = LoggerFactory.getLogger(OrphaCodesCodeSystemPlugin::class.java)

class OrphaCodesCodeSystemPlugin(babelfshContext: BabelfshContext) :
    XsltCodeSystemPlugin<OrphaCodesCodeSystemPlugin.OrphaCodeSystemPluginArgs>(
        pluginId = "orphacodes",
        babelfshContext = babelfshContext,
        shortHelp = "Generate ORPHAcodes"
    ) {

    class OrphaCodeSystemPluginArgs(parser: ArgParser, workingDirectory: Path?) : PluginArguments(
        parser, workingDirectory
    ) {
        val inputFiles by parser.storing(
            "-i",
            "--input",
            help = """ORPHAcodes input files from the nomenclature packs. 
                |By repeating this argument, you can provide translations. 
                |JSON Format: [{"path": "./input-file_EN.xml", "language": "EN"}]
                |""".trimMargin()
        ) {
            Json.decodeFromString<List<InputFileInput>>(this.preprocess().trimStart('=', '"')).map { input ->
                InputFile(
                    path = input.path.convertToAbsolutePath(),
                    language = SupportedOrphaLanguages.valueOf(input.language.uppercase())
                )
            }.also {
                logger.info("Input file: $it")
            }
        }

        val xsltFile: File by parser.storing(
            "--xslt-file",
            help = "The path to the XSLT file for conversion, uses classpath resource by default.",
        ) {
            convertToAbsolutePath().toFile()
        }.default(ClassPathResource("xslt/orphacodes/orphacodes.xslt").file)

        val primaryLanguage by parser.storing(
            "--primary-language",
            "-l",
            help = "Primary language to convert"
        ) {
            SupportedOrphaLanguages.valueOf(this.preprocess().uppercase())
        }.default(SupportedOrphaLanguages.EN)

        @Serializable
        data class InputFileInput(
            val path: String, val language: String
        )

        data class InputFile(
            val path: Path, val language: SupportedOrphaLanguages
        )

        override fun validateDependentArguments() {
            val languageCodes = inputFiles.map { it.language }.toSet()
            when {
                primaryLanguage in inputFiles.map { it.language }.toSet() -> {
                    logger.info("Primary language: $primaryLanguage. Generating designations for ${languageCodes - primaryLanguage}.")
                }

                else -> failValidation("You requested generation using the primary language ${primaryLanguage}, but no input file for that language was provided.")
            }
        }

    }

    override fun parseInto(parser: ArgParser, workingDirectory: Path?) =
        parser.parseInto { OrphaCodeSystemPluginArgs(it, workingDirectory) }

    override fun produceContent(
        args: PluginArguments, resource: CodeSystemContainer
    ): List<CodeSystemConceptContainer> {
        args as OrphaCodeSystemPluginArgs
        val canonical = resource.toR4B(
            babelfshContext,
            babelfshContext.r4Context.getStrictParser()
        ).url ?: failConversion("Did not find a canonical URI for the resource ${resource.identification}")
        val transformationResults = args.inputFiles.associateWith { input ->
            convertUsingStylesheet(
                inputFile = input.path.toFile(),
                xsltFile = args.xsltFile,
                logger = logger,
                xsltArguments = mapOf("SelfCodeSystemUri" to canonical)
            )
        }
        val primaryLanguageResult = transformationResults.toList().find { (input, _) ->
            input.language == args.primaryLanguage
        }
        TODO("Merge the resulting concepts lists and write out")
        @Suppress("UNREACHABLE_CODE")
        TODO("Post-processing: optionally re-write the Aggregation Level to Parent-Child Relationship")
    }

}