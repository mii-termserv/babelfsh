package de.mii_termserv.babelfsh.plugins.specific.atc_ddd

import com.xenomachina.argparser.ArgParser
import com.xenomachina.argparser.default
import de.mii_termserv.babelfsh.api.BabelfshContext
import de.mii_termserv.babelfsh.api.content_containers.cs.ConceptPropertyContainer
import de.mii_termserv.babelfsh.api.content_containers.cs.PropertyValueType
import de.mii_termserv.babelfsh.plugins.core.excel.ExcelCodeSystemArguments
import de.mii_termserv.babelfsh.plugins.core.excel.ExcelCodeSystemPlugin
import org.apache.poi.ss.usermodel.Row
import java.nio.file.Path


class AtcDddPluginArgs(parser: ArgParser, workingDirectory: Path?) :
    ExcelCodeSystemArguments(parser, workingDirectory) {
    override val codeColumnIndex: Int = 0
    override val displayColumnIndex: Int = 2
    override val definitionColumnIndex: Int? = null
    override val skipRows: Int = 1

    val dddPropertyCode: String? by parser.storing("--ddd-property-code", help = "The code of the DDD property") {
        preprocess()
    }.default { null }

    val dddPropertyColumnIndex: Int? by parser.storing(
        "--ddd-property-column",
        help = "The column index of the DDD property (zero-based!)"
    ) {
        preprocess().toInt()
    }.default { null }

    override fun validateDependentArguments() {
        if (dddPropertyCode != null || dddPropertyColumnIndex != null) {
            if (null in listOf(dddPropertyCode, dddPropertyColumnIndex)) {
                failValidation("If --ddd-property-code is set, --ddd-property-column must be set as well")
            }
        }
    }
}

class AtcDddCodeSystemPlugin(babelfshContext: BabelfshContext) :
    ExcelCodeSystemPlugin<AtcDddPluginArgs>(
        babelfshContext = babelfshContext,
        pluginId = "atc-ddd",
        shortHelp = "Convert ATC/DDD Excel files from the WIdo (www.wido.de)"
    ) {

    override fun postProcessCode(code: String): String {
        return code.trimEnd()
    }

    override fun parseInto(parser: ArgParser, workingDirectory: Path?): AtcDddPluginArgs {
        return parser.parseInto { AtcDddPluginArgs(it, workingDirectory) }
    }

    override fun propertiesFromRow(
        row: Row,
        rowIndex: Int,
        args: ExcelCodeSystemArguments,
        code: String,
        display: String
    ): List<ConceptPropertyContainer> {
        args as AtcDddPluginArgs
        val dddProperty = when (args.dddPropertyCode != null && args.dddPropertyColumnIndex != null) {
            true -> {
                val dddProperty =
                    row.getCell(
                        args.dddPropertyColumnIndex!!,
                        Row.MissingCellPolicy.RETURN_BLANK_AS_NULL
                    )?.stringCellValue?.trim()
                if (dddProperty?.isNotBlank() == true) {
                    ConceptPropertyContainer(args.dddPropertyCode!!, PropertyValueType.StringContainer(dddProperty))
                } else {
                    null
                }
            }

            else -> null
        }
        val parentProperty = when (code.length) {
            7 -> code.substring(0, 5)
            5 -> code.substring(0, 4)
            4 -> code.substring(0, 3)
            3 -> code.substring(0, 1)
            else -> null
        }?.let {
            ConceptPropertyContainer("parent", PropertyValueType.CodeContainer(it))
        }
        return listOfNotNull(dddProperty, parentProperty)
    }

}