package de.mii_termserv.babelfsh.plugins.specific.edqm

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
class EdqmApiModel<T : EdqmContentModel>(val content: List<T>)

interface EdqmContentModel

@Serializable
data class EdqmCodeNamePairModel(
    val code: String,
    val name: String
) : EdqmContentModel

@Serializable
data class EdqmConceptModel(
    @SerialName("class") val conceptClass: String,
    val code: String,
    val english: String,
    val comment: String? = null,
    val status: String,
    val domain: String,
    val version: Int,
    @SerialName("creation_date") val creationDate: String,
    @SerialName("modification_date") val modificationDate: String,
    val definition: String? = null,
    val translations: List<Translation>,
    val links: Map<String, List<EdqmLinkModel>>? = null
) : EdqmContentModel {
    @Serializable
    data class Translation(
        val language: String,
        val region: String,
        val term: String
    )

    @Serializable
    data class EdqmLinkModel(
        val code: String,
        val term: String
    )
}