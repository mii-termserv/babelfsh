package de.mii_termserv.babelfsh.plugins.specific.iso_country_codes

import com.xenomachina.argparser.ArgParser
import com.xenomachina.argparser.default
import de.mii_termserv.babelfsh.api.BabelfshContext
import de.mii_termserv.babelfsh.api.content_containers.cs.CodeSystemConceptContainer
import de.mii_termserv.babelfsh.api.content_containers.cs.CodeSystemContainer
import de.mii_termserv.babelfsh.api.http_client.FileDownloadHelper.Companion.downloadFileIfNecessary
import de.mii_termserv.babelfsh.api.plugins.PluginArguments
import de.mii_termserv.babelfsh.api.plugins.cs.CodeSystemPlugin
import de.mii_termserv.babelfsh.plugins.core.csv.generatePropertiesForConceptRow
import de.mii_termserv.babelfsh.readers.CsvReaderParameters
import de.mii_termserv.babelfsh.readers.CsvTerminologyReader
import de.mii_termserv.babelfsh.readers.PropertyMappingEntry
import kotlinx.coroutines.runBlocking
import org.slf4j.LoggerFactory
import java.net.URI
import java.net.URL
import java.nio.file.Path

enum class IsoCsvColumns(val columnName: String) {
    NAME("name"),
    ALPHA2("alpha-2"),
    ALPHA3("alpha-3"),
    NUMERIC("country-code")
}

class IsoCountryCodeCodeSystemPlugin(babelfshContext: BabelfshContext) :
    CodeSystemPlugin<IsoCountryCodeCodeSystemPlugin.IsoCodeSystemPluginArgs>(
        pluginId = "iso-country-codes",
        babelfshContext = babelfshContext,
        shortHelp = "Generate ISO-3166 Part 1 CodeSystem"
    ) {

    private val logger = LoggerFactory.getLogger(javaClass)

    override fun parseInto(parser: ArgParser, workingDirectory: Path?): IsoCodeSystemPluginArgs =
        parser.parseInto { IsoCodeSystemPluginArgs(it, workingDirectory) }

    override fun produceContent(
        args: PluginArguments,
        resource: CodeSystemContainer
    ): List<CodeSystemConceptContainer> {
        args as IsoCodeSystemPluginArgs
        val pathToReadFrom = runBlocking {
            downloadFileIfNecessary(null, args.url, logger)
        }

        val referencedColumns = (IsoCsvColumns.entries.map { it.columnName }.plus(
            args.supplementalPropertyMapping.map { it.column }
        )).toSet()

        val conceptList = CsvTerminologyReader.readCsv(
            path = pathToReadFrom,
            csvParameters = CsvReaderParameters(),
            ensureHeadersExist = referencedColumns.toList()
        ) { conceptRow, _ ->
            generateIsoConceptsFromRow(
                conceptRow = conceptRow,
                isoArgs = args,
                codeSystem = resource
            )
        }

        return conceptList
    }

    private fun generateSingleConceptFromRow(
        display: String,
        code: String,
        args: IsoCodeSystemPluginArgs,
        conceptRow: Map<String, String>,
        resource: CodeSystemContainer,
    ) = CodeSystemConceptContainer(
        code = code,
        display = display,
        definition = null,
        designation = listOf(),
        property = generatePropertiesForConceptRow(
            propertyMapping = args.supplementalPropertyMapping,
            row = conceptRow,
            resource = resource,
            code = code
        )
    )

    private fun generateIsoConceptsFromRow(
        conceptRow: Map<String, String>,
        isoArgs: IsoCodeSystemPluginArgs,
        codeSystem: CodeSystemContainer
    ): List<CodeSystemConceptContainer> {
        val display = conceptRow[IsoCsvColumns.NAME.columnName]!!
        val alpha2 = conceptRow[IsoCsvColumns.ALPHA2.columnName]!!
        val alpha3 = conceptRow[IsoCsvColumns.ALPHA3.columnName]!!
        val numeric = conceptRow[IsoCsvColumns.NUMERIC.columnName]!!


        val returnList = mutableListOf<CodeSystemConceptContainer>()
        if (isoArgs.includeTwoLetterCodes) {
            returnList += generateSingleConceptFromRow(
                display = display, code = alpha2, args = isoArgs, conceptRow = conceptRow, resource = codeSystem
            )
        }
        if (isoArgs.includeThreeLetterCodes) {
            returnList += generateSingleConceptFromRow(
                display = display,
                code = alpha3,
                args = isoArgs,
                conceptRow = conceptRow,
                resource = codeSystem
            )
        }
        if (isoArgs.includeNumericCodes) {
            returnList += generateSingleConceptFromRow(
                display = display,
                code = numeric,
                args = isoArgs,
                conceptRow = conceptRow,
                resource = codeSystem
            )
        }

        return returnList
    }

    class IsoCodeSystemPluginArgs(
        argParser: ArgParser,
        workingDirectory: Path?
    ) : PluginArguments(argParser, workingDirectory) {

        override fun validateDependentArguments() {
            if (!(includeNumericCodes || includeThreeLetterCodes || includeTwoLetterCodes)) {
                failValidation("At least one option for code generation must be selected.")
            }
        }

        val url: URL by parser.storing(
            "--url",
            "-u",
            help = "URL to the CSV file"
        ) {
            URI.create(this.preprocess()).toURL()
        }

        val includeTwoLetterCodes by parser.mapping(
            "--include-two-letter-codes" to true,
            "--dont-include-two-letter-codes" to false,
            help = "Include two letter country codes, default true"
        ).default(true)

        val includeThreeLetterCodes by parser.mapping(
            "--include-three-letter-codes" to true,
            "--dont-include-three-letter-codes" to false,
            help = "Include three letter country codes, default true"
        ).default(true)

        val includeNumericCodes by parser.mapping(
            "--include-numeric-codes" to true,
            "--dont-include-numeric-codes" to false,
            help = "Include numeric country codes, default true"
        ).default(true)

        val supplementalPropertyMapping: List<PropertyMappingEntry> by parser.storing(
            "--supplemental-property-mapping",
            "--spm",
            help = "Mapping of additional properties to columns in the CSV file. " + PropertyMappingEntry.help(),
        ) {
            propertyMappingFromString(
                stringValue = this,
                argumentName = "supplemental-property-mapping",
                pluginName = "iso-country-codes"
            )
        }.default(listOf())

    }

}