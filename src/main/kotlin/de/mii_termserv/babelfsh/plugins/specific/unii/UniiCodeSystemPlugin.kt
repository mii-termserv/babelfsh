package de.mii_termserv.babelfsh.plugins.specific.unii

import com.xenomachina.argparser.ArgParser
import com.xenomachina.argparser.default
import de.mii_termserv.babelfsh.api.BabelfshContext
import de.mii_termserv.babelfsh.api.content_containers.cs.CodeSystemConceptContainer
import de.mii_termserv.babelfsh.api.content_containers.cs.CodeSystemContainer
import de.mii_termserv.babelfsh.api.content_containers.cs.DesignationContainer
import de.mii_termserv.babelfsh.api.content_containers.cs.PropertyValueType
import de.mii_termserv.babelfsh.api.plugins.PluginArguments
import de.mii_termserv.babelfsh.api.plugins.cs.CodeSystemPlugin
import de.mii_termserv.babelfsh.readers.CsvReaderParameters
import de.mii_termserv.babelfsh.readers.CsvTerminologyReader
import de.mii_termserv.babelfsh.utils.logging.warn
import org.slf4j.LoggerFactory
import java.nio.file.Path

class UniiCodeSystemPlugin(babelfshContext: BabelfshContext) : CodeSystemPlugin<UniiCodeSystemPlugin.UniiPluginArgs>(
    pluginId = "unii",
    babelfshContext = babelfshContext,
    shortHelp = "Convert Unique Ingredient Identifier (UNII) master files (https://precision.fda.gov/uniisearch)",
    extraHelp = """
        This plugin converts UNII master files from the FDA to FHIR. 
        It requires a special plugin since multiple designations are available in the UNII files that need to be joined into a single concept. 
        It uses the 'UNII List' format available from https://precision.fda.gov/uniisearch/archive""".trimIndent()
) {

    private val logger = LoggerFactory.getLogger(javaClass)

    class UniiPluginArgs(argParser: ArgParser, workingDirectory: Path?) : PluginArguments(argParser, workingDirectory) {
        val inputPath by argParser.storing("-p", "--path", "-f", "--file", help = "Input file path") {
            convertToAbsolutePath()
        }

        private fun processDesignationMap(stringValue: String): Map<String, String> {
            val regex = Regex("""'(?<code>\w+)':'(?<display>[^']+)',?\w*""")
            if (regex.find(stringValue) == null) {
                throw IllegalArgumentException("Designation map must be in the format `'code':'description','code':'description',...`")
            }
            return regex.findAll(stringValue).map {
                it.groups["code"]!!.value to it.groups["display"]!!.value
            }.toMap()
        }

        private val defaultDesignationMapString = mapOf(
            "of" to "Name identified as having official status",
            "sys" to "Systematic Name (chemical and taxonomic names)",
            "cn" to "Common Name",
            "cd" to "Code",
            "bn" to "Brand/Trade Name"
        ).entries.joinToString(separator = ",") { "'${it.key}':'${it.value}'" }

        val nameColumn by argParser.storing(
            "-n",
            "--name-column",
            help = "Name column. Default: 'Name'"
        ) { preprocess() }.default("Name")
        val typeColumn by argParser.storing(
            "-t",
            "--type-column",
            help = "Type column. Default: 'TYPE'"
        ) { preprocess() }.default("TYPE")
        val uniiColumn by argParser.storing(
            "-u",
            "--unii-column",
            help = "UNII column. Default: 'UNII'"
        ) { preprocess() }.default("UNII")
        val displayColumn by argParser.storing("-d", "--display-column", help = "Display column. Default: 'Display'") {
            preprocess()
        }.default("Display Name")
        val designationMap: Map<String, String> by parser.storing(
            "--designation-map",
            help = "Map designation codes to human-readable codes. Default: `${defaultDesignationMapString}`"
        ) {
            processDesignationMap(this)
        }.default(processDesignationMap(defaultDesignationMapString))

        @Suppress("HttpUrlsUsage")
        val designationUseCodeSystem by argParser.storing(
            "--designation-use-code-system", help = "Code system for designation use"
        ) { preprocess() }.default("http://fdasis.nlm.nih.gov/designations")
    }

    override fun parseInto(parser: ArgParser, workingDirectory: Path?): UniiPluginArgs {
        return UniiPluginArgs(parser, workingDirectory)
    }

    private data class UniiRow(val name: String, val type: String, val unii: String, val display: String)

    override fun produceContent(
        args: PluginArguments,
        resource: CodeSystemContainer
    ): List<CodeSystemConceptContainer> {
        val uniiArgs = args as UniiPluginArgs
        val csvData = CsvTerminologyReader.readCsv(
            path = uniiArgs.inputPath,
            csvParameters = CsvReaderParameters(
                delimiter = '\t'
            ),
            ensureHeadersExist = listOf(
                uniiArgs.nameColumn,
                uniiArgs.typeColumn,
                uniiArgs.uniiColumn,
                uniiArgs.displayColumn
            ),
        ) { row, _ ->
            listOf(
                UniiRow(
                    name = row[uniiArgs.nameColumn]!!,
                    type = row[uniiArgs.typeColumn]!!,
                    unii = row[uniiArgs.uniiColumn]!!,
                    display = row[uniiArgs.displayColumn]!!
                )
            )
        }
        val joined = csvData.groupBy { it.unii }
        return joined.mapNotNull { (code, rowList) ->
            if (rowList.map { it.display }.toSet().size > 1) {
                logger.warn { "Multiple display names for UNII code $code: ${rowList.map { it.display }}" }
                return@mapNotNull null
            }
            return@mapNotNull CodeSystemConceptContainer(
                code = code,
                display = rowList.first().display,
                definition = null,
                property = listOf(),
                designation = mapDesignations(rowList, args.designationUseCodeSystem, args.designationMap)
            )
        }
    }

    private fun mapDesignations(
        rowList: List<UniiRow>,
        system: String,
        designationMap: Map<String, String>
    ): List<DesignationContainer> {
        val designationSet = rowList.map { it.display to it.type }.toSet()
        return designationSet.map { (display, type) ->
            DesignationContainer(
                value = display, use = PropertyValueType.CodingContainer(
                    system = system,
                    code = type,
                    display = designationMap[type] ?: type,
                    version = null,
                    userSelected = null
                ), additionalUse = listOf(), language = null
            )
        }
    }
}