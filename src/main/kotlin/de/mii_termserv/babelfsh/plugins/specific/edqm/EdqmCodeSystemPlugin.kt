@file:Suppress("HttpUrlsUsage")

package de.mii_termserv.babelfsh.plugins.specific.edqm

import com.xenomachina.argparser.ArgParser
import com.xenomachina.argparser.default
import de.mii_termserv.babelfsh.api.BabelfshContext
import de.mii_termserv.babelfsh.api.content_containers.cs.*
import de.mii_termserv.babelfsh.api.plugins.PluginArguments
import de.mii_termserv.babelfsh.api.plugins.cs.CodeSystemPlugin
import de.mii_termserv.babelfsh.utils.logging.info
import io.ktor.client.call.*
import kotlinx.coroutines.runBlocking
import org.slf4j.LoggerFactory
import java.nio.file.Path
import java.time.format.DateTimeFormatter

class EdqmCodeSystemPlugin(babelfshContext: BabelfshContext) :
    CodeSystemPlugin<EdqmCodeSystemPlugin.EdqmCsPluginArgs>(
        pluginId = "edqm", babelfshContext = babelfshContext,
        shortHelp = "Convert EDQM standard terms (https://standardterms.edqm.eu)",
    ) {

    private val logger = LoggerFactory.getLogger(javaClass)

    private val dateTimeFormatterEdqm = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")

    override fun parseInto(parser: ArgParser, workingDirectory: Path?): EdqmCsPluginArgs {
        return parser.parseInto { EdqmCsPluginArgs(it, workingDirectory) }
    }

    override fun produceContent(args: PluginArguments, resource: CodeSystemContainer): List<CodeSystemConceptContainer> {
        args as EdqmCsPluginArgs
        val edqmApi = EdqmApi(
            EdqmApi.HeaderBuilder(
                args.usernameEnvironmentVariable,
                args.apiKeyEnvironmentVariable,
                logger
            ),
            babelfshContext.configurationSettings
        )
        return runBlocking {
            validateLanguageSelections(args, edqmApi)
            when (args.mode) {
                EdqmCsPluginMode.CLASS_CODES -> generateClassCodeConcepts(edqmApi)
                EdqmCsPluginMode.STANDARD_TERMS -> generateStandardTermConcepts(args, edqmApi)
            }
        }
    }

    private suspend fun validateLanguageSelections(args: EdqmCsPluginArgs, edqmApi: EdqmApi) {
        val languagesAvailable: EdqmApiModel<EdqmCodeNamePairModel> = edqmApi.executeRequest("/languages").body()
        if (args.designationLanguages.contains("all")) {

            logger.info {
                "Generating designations in all available languages: ${
                    languagesAvailable.content.sortedBy { it.code }.joinToString { "'${it.code}' (${it.name})" }
                }\""
            }
            return
        }
        val allValid = args.designationLanguages.fold(true) { _, requestedLanguage ->
            if (languagesAvailable.content.none { it.code == requestedLanguage }) {
                logger.error("Language $requestedLanguage is not available for designations.")
                return@fold false
            }
            return@fold true
        }
        when {
            !allValid -> throw IllegalArgumentException("Invalid language selection for designations.")
            else -> {
                val languagesRequested = args.designationLanguages.associateWith { req ->
                    languagesAvailable.content.first { it.code == req }.name
                }
                logger.info {
                    "Generating designations for languages: ${languagesRequested.toList().joinToString { "'${it.first}' (${it.second})" }}"
                }
            }
        }
    }

    private suspend fun generateClassCodeConcepts(
        edqmApi: EdqmApi
    ): List<CodeSystemConceptContainer> {
        val classes: EdqmApiModel<EdqmCodeNamePairModel> = edqmApi.executeRequest("/classes").body()
        return classes.content.sortedBy { it.code }.map {
            CodeSystemConceptContainer(
                code = it.code,
                display = it.name,
                definition = null
            )
        }
    }

    private fun EdqmConceptModel.filterTranslations(args: EdqmCsPluginArgs): List<EdqmConceptModel.Translation> =
        when (args.designationLanguages == listOf("all")) {
            true -> translations
            else -> translations.filter { it.language in args.designationLanguages }
        }.filter { it.term.isNotBlank() }.sortedBy { it.language }

    private suspend fun generateStandardTermConcepts(
        args: EdqmCsPluginArgs,
        edqmApi: EdqmApi
    ): List<CodeSystemConceptContainer> {
        val allConcepts: EdqmApiModel<EdqmConceptModel> = edqmApi.executeRequest("/full_data_by_class/1/1/1").body()
        logger.info { "Received ${allConcepts.content.size} concepts from the EDQM API." }
        val concepts = allConcepts.content.sortedBy { it.code }.map { edqmConcept ->
            val designations = edqmConcept.filterTranslations(args).map { translation ->
                DesignationContainer(
                    language = translation.language,
                    use = null,
                    value = translation.term
                )
            }
            val properties = buildProperties(
                edqmConcept,
                args
            )
            CodeSystemConceptContainer(
                code = edqmConcept.code,
                display = edqmConcept.english,
                definition = edqmConcept.definition,
                designation = designations,
                property = properties
            )
        }

        return concepts
    }

    private fun propertyForPropCodeArgument(
        propCodeArgument: String?,
        buildValue: () -> PropertyValueType<*, *>
    ): ConceptPropertyContainer? {
        return propCodeArgument?.let { propCode ->
            ConceptPropertyContainer(
                code = propCode,
                valueType = buildValue()
            )
        }
    }

    private fun buildProperties(
        edqmConcept: EdqmConceptModel,
        args: EdqmCsPluginArgs
    ): List<ConceptPropertyContainer> {
        val classProperty = when (args.classAsString) {
            true -> ConceptPropertyContainer(
                code = args.classPropertyCode,
                valueType = PropertyValueType.StringContainer(edqmConcept.conceptClass)
            )

            else -> ConceptPropertyContainer(
                code = args.classPropertyCode,
                valueType = PropertyValueType.CodingContainer(
                    system = args.classPropertySystem,
                    code = edqmConcept.conceptClass,
                    display = null,
                    version = null,
                    userSelected = false
                )
            )
        }
        val domainProperty = ConceptPropertyContainer(
            code = args.domainPropertyCode,
            valueType = PropertyValueType.StringContainer(edqmConcept.domain)
        )
        val statusProperty = ConceptPropertyContainer(
            code = args.statusProperty,
            valueType = PropertyValueType.StringContainer(edqmConcept.status)
        )
        val creationDateProperty = propertyForPropCodeArgument(args.creationDateProperty) {
            val parsedCreationDate = dateTimeFormatterEdqm.parse(edqmConcept.creationDate)
            PropertyValueType.DateTimeContainer(
                PropertyValueType.DateTimeContainer.formatterWithSecondsAtZulu.format(
                    parsedCreationDate
                )
            )
        }
        val modificationDateProperty = propertyForPropCodeArgument(args.modificationDateProperty) {
            val parsedModificationDate = dateTimeFormatterEdqm.parse(edqmConcept.modificationDate)
            PropertyValueType.DateTimeContainer(
                PropertyValueType.DateTimeContainer.formatterWithSecondsAtZulu.format(
                    parsedModificationDate
                )
            )
        }
        val linkProperty = edqmConcept.links?.flatMap { (_, linkList) ->
            linkList.sortedBy { it.code }.map { link ->
                ConceptPropertyContainer(
                    code = args.linkProperty,
                    valueType = PropertyValueType.CodeContainer(value = link.code)
                )
            }
        }.orEmpty()
        val versionProperty = propertyForPropCodeArgument(args.versionProperty) {
            PropertyValueType.IntegerContainer(edqmConcept.version)
        }
        return (listOfNotNull(
            classProperty,
            domainProperty,
            statusProperty,
            creationDateProperty,
            modificationDateProperty,
            versionProperty
        ) + linkProperty).sortedBy { it.code }
    }

    class EdqmCsPluginArgs(parser: ArgParser, workingDirectory: Path?) : PluginArguments(parser, workingDirectory) {
        val usernameEnvironmentVariable: String by parser.storing(
            "-u", "--username-env",
            help = "The environment variable containing the username for the EDQM API. " +
                    "It is intentionally not possible to provide the username in the FSH source file directly to avoid leaking your username!"
        ) {
            preprocess()
        }

        val apiKeyEnvironmentVariable: String by parser.storing(
            "-k",
            "--api-key-env",
            help = "The environment variable containing the API key for the EDQM API. " +
                    "It is intentionally not possible to provide the API key in the FSH source file directly to avoid leaking your key!"
        ) {
            preprocess()
        }

        val designationLanguages: List<String> by parser.storing(
            "-l",
            "--language",
            help = "The language(s) to be used for designations. The special code 'all' can be used to generate designations in all languages."
        ) {
            makeList().map { it.lowercase() }
        }.default(listOf("de", "en"))

        val mode: EdqmCsPluginMode by parser.mapping(
            "--mode-class-codes" to EdqmCsPluginMode.CLASS_CODES,
            "--mode-standard-terms" to EdqmCsPluginMode.STANDARD_TERMS,
            help = "The mode for the CodeSystem plugin, either generate the class code system, or the code system with the actual terms."
        )

        val classPropertyCode: String by parser.storing(
            "--class-property",
            help = "The code for the property that contains the class of the concept; default 'class'; required property"
        ) {
            preprocess()
        }.default("class")

        val statusProperty: String by parser.storing(
            "--status-property",
            help = "The code for the property that contains the status of the concept; default 'status'; required property"
        ) {
            preprocess()
        }.default("status")

        val classAsString: Boolean by parser.mapping(
            mapOf(
                "--class-as-string" to true,
                "--class-as-coding" to false
            ),
            help = "Whether to use the class as a code or as a coding. Default is 'true' (code)."
        ).default(true)

        val classPropertySystem: String by parser.storing(
            "--class-system",
            help = "The system for the property that contains the class of the concept, needed if --class-as-coding is provided; default 'http://standardterms.edqm.eu/classes'"
        ) {
            preprocess()
        }.default("http://standardterms.edqm.eu/classes")

        val domainPropertyCode: String by parser.storing(
            "--domain-property",
            help = "The code for the property that contains the domain of the concept; default 'domain'"
        ) {
            preprocess()
        }.default("domain")

        val creationDateProperty: String? by parser.storing(
            "--creation-date-property",
            help = "The code for the property that contains the creation date of the concept; optional property"
        ) {
            preprocess()
        }.default(null)

        val modificationDateProperty: String? by parser.storing(
            "--modification-date-property",
            help = "The code for the property that contains the modification date of the concept; optional property"
        ) {
            preprocess()
        }.default(null)

        val linkProperty: String by parser.storing(
            "--link-property",
            help = "The code for the property that contains the links of the concept; default 'link'; required property"
        ) {
            preprocess()
        }.default("link")

        val versionProperty: String? by parser.storing(
            "--version-property", help = "The code for the property that contains the version of the concept; optional property"
        ) {
            preprocess()
        }.default(null)

        override fun validateDependentArguments() {
            if (!classAsString && classPropertySystem.isBlank()) {
                failValidation("If the class is used as a coding, the system must be provided.")
            }
            if (designationLanguages.contains("all") && designationLanguages.size > 1) {
                failValidation("The special code 'all' must be the only language selected for designations.")
            }
        }
    }

    enum class EdqmCsPluginMode {
        CLASS_CODES,
        STANDARD_TERMS
    }
}
