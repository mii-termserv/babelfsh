package de.mii_termserv.babelfsh.plugins.specific.edqm

import de.mii_termserv.babelfsh.api.http_client.HttpClientBuilder
import de.mii_termserv.babelfsh.configuration_settings.ConfigurationSettings
import de.mii_termserv.babelfsh.utils.logging.debug
import de.mii_termserv.babelfsh.utils.logging.info
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.http.*
import io.ktor.serialization.kotlinx.json.*
import kotlinx.coroutines.runBlocking
import kotlinx.serialization.json.Json
import org.apache.commons.validator.routines.EmailValidator
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.nio.charset.StandardCharsets
import java.time.OffsetDateTime
import java.time.ZoneId
import java.time.ZoneOffset
import java.time.format.DateTimeFormatter
import java.util.*
import javax.crypto.Mac
import javax.crypto.spec.SecretKeySpec
import kotlin.time.DurationUnit
import kotlin.time.measureTimedValue

class EdqmApi(
    private val headerBuilder: HeaderBuilder, private val configurationSettings: ConfigurationSettings
) {

    val logger: Logger = LoggerFactory.getLogger(javaClass)

    private val ktorClient by lazy {
        HttpClientBuilder(configurationSettings.http).buildClient(configurationSettings.http) {
            install(ContentNegotiation) {
                json(Json {
                    ignoreUnknownKeys = true
                })
            }
        }
    }

    class HeaderBuilder(usernameEnvironmentVariable: String, apiKeyEnvironmentVariable: String, private val logger: Logger) {

        private val username: String = System.getenv(usernameEnvironmentVariable)?.also {
            if (it.isBlank()) {
                throw IllegalArgumentException("Username environment variable '$usernameEnvironmentVariable' is empty")
            }
            val isEmail = EmailValidator.getInstance().isValid(it)
            if (!isEmail) {
                throw IllegalArgumentException("Username environment variable '$usernameEnvironmentVariable' is not a valid email address")
            }
            logger.info("Using username: ${it.replaceRange(2, it.indexOf('@'), "***").dropLast(8) + "***"}")
        } ?: throw IllegalArgumentException("Username environment variable '$usernameEnvironmentVariable' not set")

        private val apiKey: String = System.getenv(apiKeyEnvironmentVariable)?.also {
            if (it.isBlank()) {
                throw IllegalArgumentException("API key environment variable '$apiKeyEnvironmentVariable' is empty")
            }
            logger.info("Using API Key with length: ${it.length}, starting ${it.take(2)}***")
        } ?: throw IllegalArgumentException("API key environment variable '$apiKeyEnvironmentVariable' not set")

        private val dateTimeFormatter =
            DateTimeFormatter.ofPattern("EEE, dd MMM yyyy HH:mm:ss z", Locale.ENGLISH).withZone(ZoneId.of("GMT"))

        private val secretKey = SecretKeySpec(apiKey.toByteArray(charset = StandardCharsets.UTF_8), "HmacSHA512")

        private fun generateDateHeader(): String {
            val now = OffsetDateTime.now(ZoneOffset.UTC)
            val formatted = now.format(dateTimeFormatter)
            return formatted
        }

        private fun generateXStApiKey(requestUri: String, verb: String, date: String, host: String): String {
            val stringToSign = "$verb&$requestUri&$host&$date"
            logger.debug { "Signing: \"$stringToSign\"" }
            val signed = edqmHmac(stringToSign, secretKey)
            val header = "$username|$signed"
            logger.debug { "Generated header: \"$header\"" }
            return header
        }


        fun generateHeaders(headersBuilder: HeadersBuilder, relativeUrl: String, requestMethod: HttpMethod, host: String) {
            headersBuilder.apply {
                append("Date", generateDateHeader())
                append("Accept", "application/json")
                append("Host", host)
                @Suppress("SpellCheckingInspection") append(
                    "X-STAPI-KEY",
                    generateXStApiKey(relativeUrl, requestMethod.value, headersBuilder["Date"]!!, host)
                )
            }
        }


        companion object {
            fun edqmHmac(stringToSign: String, secretKey: SecretKeySpec): String {
                val mac = Mac.getInstance("HmacSHA512")
                val messageBytes = stringToSign.toByteArray(charset = StandardCharsets.UTF_8)
                mac.init(secretKey)
                val digest = mac.doFinal(messageBytes)
                val base64 = Base64.getEncoder().encodeToString(digest)
                val truncatedHash = base64.takeLast(22)
                return truncatedHash
            }
        }
    }

    suspend fun executeRequest(
        url: String,
        requestMethod: HttpMethod = HttpMethod.Get,
        host: String = "standardterms.edqm.eu",
        protocol: String = "https",
        urlPrefix: String = "/standardterms/api/v1"
    ): HttpResponse {
        val relativeUrl = "/${urlPrefix.trim('/')}/${url.trimStart('/')}"
        val requestUrl = "$protocol://$host$relativeUrl"
        logger.info { "Requesting from EDQM API: ${requestMethod.value} $requestUrl" }
        val (request, timeTaken) = measureTimedValue {
            ktorClient.request(urlString = requestUrl) {
                method = requestMethod
                headers {
                    headerBuilder.generateHeaders(this, relativeUrl, requestMethod, host)
                }
            }
        }
        logger.debug { "Request ${requestMethod.value} $requestUrl took ${timeTaken.toString(DurationUnit.SECONDS, decimals = 1)}" }
        return request
    }
}

private fun testEdqmApiAccess() {
    val edqmApi = EdqmApi(
        headerBuilder = EdqmApi.HeaderBuilder(
            usernameEnvironmentVariable = "username", apiKeyEnvironmentVariable = "api-key", logger = LoggerFactory.getLogger("EdqmApi")
        ), configurationSettings = ConfigurationSettings()
    )
    val requestUri = "/classes"
    runBlocking {
        val response = edqmApi.executeRequest(requestUri, HttpMethod.Get)
        println(response.bodyAsText())
    }
}

fun main() {
    testEdqmApiAccess()
}