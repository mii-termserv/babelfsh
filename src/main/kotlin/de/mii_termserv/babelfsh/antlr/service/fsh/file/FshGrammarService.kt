package de.mii_termserv.babelfsh.antlr.service.fsh.file

import de.mii_termserv.babelfsh.antlr.FSHBaseListener
import de.mii_termserv.babelfsh.antlr.FSHLexer
import de.mii_termserv.babelfsh.antlr.FSHParser
import de.mii_termserv.babelfsh.antlr.service.babelfsh.comments.CommentDefinedIn
import de.mii_termserv.babelfsh.antlr.service.fsh.file.TsResourceData.Rule.RuleType
import de.mii_termserv.babelfsh.api.ResourceType
import de.mii_termserv.babelfsh.utils.logging.debug
import de.mii_termserv.babelfsh.utils.logging.trace
import de.mii_termserv.babelfsh.utils.logging.warn
import de.mii_termserv.babelfsh.utils.stripCode
import de.mii_termserv.babelfsh.utils.stripQuotes
import org.antlr.v4.runtime.CharStream
import org.antlr.v4.runtime.CommonTokenStream
import org.antlr.v4.runtime.ParserRuleContext
import org.antlr.v4.runtime.tree.ParseTreeWalker
import org.slf4j.LoggerFactory
import java.io.File
import java.nio.file.Path

private const val BABELFSH_COMMENT_PREFIX = "/*^babelfsh"
private const val BABELFSH_COMMENT_SUFFIX = "^babelfsh*/"

class FshGrammarService(
    private val inputFile: File?,
    charStream: CharStream,
    private val previousParseResult: FshParseResult
) {

    private val logger = LoggerFactory.getLogger(javaClass)

    private val lexer = FSHLexer(charStream)
    private val tokens = CommonTokenStream(lexer)
    private val parser = FSHParser(tokens)
    private val parseTree: FSHParser.DocContext = parser.doc()
    private val walker = ParseTreeWalker()
    private val workingDirectory = inputFile?.parentFile?.toPath()?.toAbsolutePath()

    fun parseForTerminologyContent(): FshParseResult {
        logger.trace { "Parse tree: ${parseTree.toStringTree(parser)}" }
        val listener = FshListener(inputFile, workingDirectory, previousParseResult)
        walker.walk(listener, parseTree)
        return FshParseResult(
            aliases = listener.aliasList,
            codeSystems = listener.codeSystemList,
            valueSets = listener.valueSetList,
            conceptMaps = listener.conceptMapList,
            ruleSets = listener.ruleSets
        )
    }

}

private class FshListener(
    val inputFile: File?,
    val workingDirectory: Path?,
    previousParseResult: FshParseResult
) : FSHBaseListener() {

    private val logger = LoggerFactory.getLogger(javaClass)

    val aliasList = mutableListOf<Alias>().also {
        it.addAll(previousParseResult.aliases)
    }

    val codeSystemList = mutableListOf<TsResourceData>().also {
        it.addAll(previousParseResult.codeSystems)
    }
    val valueSetList = mutableListOf<TsResourceData>().also {
        it.addAll(previousParseResult.valueSets)
    }

    val ruleSets = mutableListOf<RuleSet>().also {
        it.addAll(previousParseResult.ruleSets)
    }

    val conceptMapList = mutableListOf<ConceptMapInstance>().also {
        it.addAll(previousParseResult.conceptMaps)
    }

    var currentResource: TsResourceData? = null
    val currentMode: ResourceType? get() = currentResource?.resourceType

    override fun enterAlias(ctx: FSHParser.AliasContext) {
        logger.trace { "Got alias: ${ctx.text}" }
        val aliasName = ctx.SEQUENCE(0).text.trim()
        if (!aliasName.startsWith("$")) {
            throw ParserStateUnsupportedException(
                buildString {
                    append("Alias name '$aliasName' does not start with '$'. ")
                    append("While this is optional per the FSH specification (section 3.5.2), ")
                    append("BabelFSH only supports alias declarations with a leading '$' to avoid unexpected results.")
                }, ctx
            )
        }
        val aliasValue = when {
            ctx.getTokens(FSHParser.SEQUENCE).size == 2 -> ctx.SEQUENCE(1)
            ctx.CODE() != null -> ctx.CODE()
            else -> throw ParserStateUnsupportedException(
                "Alias value is not set. This may happen if you quote the alias value, which is not allowed in the FSH grammar!",
                ctx
            )
        }.text.trim()
        aliasList.add(Alias(aliasName, aliasValue))
    }

    private fun validateName(name: String) {
        if (NAME_REGEX.matches(name).not()) {
            logger.warn("Name $name does not match the FHIR name regex, this is not recommended.")
        }
        if (name.length > 255) {
            throw FshSemanticException("Name $name is longer than 255 characters", file = inputFile)
        }
    }

    override fun enterCodeSystem(ctx: FSHParser.CodeSystemContext) {
        logger.trace { "Entering CodeSystem" }
        if (currentMode != null) {
            throw ParserStateUnsupportedException("Mode is already set to $currentMode", ctx)
        }
        val name = ctx.name()?.text?.trim()?.stripQuotes() ?: throw ParserStateUnsupportedException("Name is not set", ctx)
        validateName(name)
        currentResource = CodeSystemValueSetDefinition(
            workingDirectory = workingDirectory,
            file = inputFile,
            nameFromFshItem = name
        )
    }

    override fun enterValueSet(ctx: FSHParser.ValueSetContext) {
        logger.trace { "Entering ValueSet" }
        if (currentMode != null) {
            throw ParserStateUnsupportedException("Mode is already set to $currentMode", ctx)
        }
        val name = ctx.name()?.text?.trim() ?: throw ParserStateUnsupportedException("Name is not set", ctx)
        validateName(name)
        currentResource = CodeSystemValueSetDefinition(
            workingDirectory = workingDirectory,
            file = inputFile,
            nameFromFshItem = name
        )
    }

    private fun collisionDeclaredInFile(firstFile: File?, secondFile: File?): String = when {
        firstFile == null && secondFile == null -> "in unknown files"
        firstFile == null -> "in file $secondFile"
        secondFile == null -> "in file $firstFile"
        else -> when {
            firstFile.absolutePath == secondFile.absolutePath -> "in file $firstFile"
            else -> "in files $firstFile and $secondFile"
        }
    }

    override fun enterRuleSet(ctx: FSHParser.RuleSetContext) {
        logger.trace { "Entering RuleSet" }
        if (currentMode != null) {
            throw ParserStateUnsupportedException("Mode is already set to $currentMode", ctx)
        }
        val rulesetName = ctx.RULESET_REFERENCE().text.trim()
        when (val existingRuleSet = ruleSets.find { it.nameFromFshItem == rulesetName }) {
            null -> currentResource =
                RuleSet(workingDirectory = workingDirectory, file = inputFile, name = rulesetName, pluginDefinedIn = null)

            else -> {
                throw ParserStateUnsupportedException(
                    "RuleSet with name '$rulesetName' already exists (${collisionDeclaredInFile(inputFile, existingRuleSet.file)})",
                    ctx
                )
            }
        }
    }

    private fun currentDefinitionContext(ctx: ParserRuleContext): TsResourceData.RuleDefinitionContext {
        return TsResourceData.RuleDefinitionContext(
            resourceType = currentMode ?: throw ParserStateUnsupportedException("ResourceType is not set", ctx),
            resourceName = currentResource?.identification ?: throw ParserStateUnsupportedException("Resource is not set", ctx),
            file = inputFile,
            lineNumber = ctx.start.line
        )
    }

    private fun extractParameters(
        parameters: List<FSHParser.ParameterContext>,
        lastParameter: FSHParser.LastParameterContext
    ): List<String> {
        return parameters.map { it.text.trim().removeSuffix(",").trim() } + lastParameter.text.trim().removeSuffix(")").trim()
    }

    override fun enterParamRuleSet(ctx: FSHParser.ParamRuleSetContext) {
        if (currentMode != null) {
            throw ParserStateUnsupportedException("Mode is already set to $currentMode", ctx)
        }
        val parameters = extractParameters(ctx.paramRuleSetRef().parameter(), ctx.paramRuleSetRef().lastParameter())
        val rulesetName = ctx.paramRuleSetRef().PARAM_RULESET_REFERENCE().text.trim().removeSuffix("(").trim()
        when (val existingRuleSet = ruleSets.find { it.nameFromFshItem == rulesetName }) {
            null -> {
                currentResource = RuleSet(
                    workingDirectory = workingDirectory,
                    file = inputFile,
                    name = rulesetName,
                    parameters = parameters.toMutableList(),
                    pluginDefinedIn = null
                )
            }

            else -> {
                throw ParserStateUnsupportedException(
                    "RuleSet with name '$rulesetName' already exists (${collisionDeclaredInFile(inputFile, existingRuleSet.file)})",
                    ctx
                )
            }
        }
    }

    override fun exitCodeSystem(ctx: FSHParser.CodeSystemContext) {
        if (currentMode != ResourceType.CodeSystem) {
            throw ParserStateUnsupportedException("Mode is not set to CodeSystem", ctx)
        }
        if (currentResource == null) {
            throw ParserStateUnsupportedException("Resource is null", ctx)
        }
        if (currentResource!!.terminologyPluginComment == null && currentResource!!.referencedRuleSets.none() && currentResource!!.directConcepts.isEmpty()) {
            throw FshSemanticException(
                message = "CodeSystem ${currentResource?.nameFromFshItem} does not have a plugin comment, direct concepts, or referenced RuleSets",
                resourceType = ResourceType.CodeSystem,
                resourceName = currentResource?.nameFromFshItem,
                file = inputFile,
                line = (ctx.start?.line)?.inc()
            )
        }
        if (currentResource!!.terminologyPluginComment != null && currentResource!!.directConcepts.isNotEmpty()) {
            throw FshSemanticException(
                message = "CodeSystem ${currentResource?.nameFromFshItem} has both a plugin comment and direct concepts. This is not allowed.",
                resourceType = ResourceType.CodeSystem,
                resourceName = currentResource?.nameFromFshItem,
                file = inputFile,
                line = (ctx.start?.line)?.inc()
            )
        }
        codeSystemList.add(currentResource ?: throw ResourceIsNotSetException(ctx))
        currentResource = null
    }

    override fun exitValueSet(ctx: FSHParser.ValueSetContext) {
        if (currentMode != ResourceType.ValueSet) {
            throw ParserStateUnsupportedException("Mode is not set to ValueSet", ctx)
        }
        if (currentResource?.terminologyPluginComment == null && currentResource?.referencedRuleSets?.none() == true) {
            throw FshSemanticException(
                "ValueSet ${currentResource?.nameFromFshItem} does not have a plugin comment or referenced RuleSets",
                ResourceType.ValueSet,
                currentResource?.nameFromFshItem,
                inputFile,
                line = (ctx.start?.line)?.inc()
            )
        }
        valueSetList.add(currentResource ?: throw ResourceIsNotSetException(ctx))
        currentResource = null
    }

    private fun onExitRuleset(ctx: ParserRuleContext) {
        if (currentMode != ResourceType.RuleSet) {
            throw ParserStateUnsupportedException("Mode is not set to RuleSet", ctx)
        }
        ruleSets.add((currentResource as RuleSet?) ?: throw ResourceIsNotSetException(ctx))
        currentResource = null
    }

    override fun exitRuleSet(ctx: FSHParser.RuleSetContext) = onExitRuleset(ctx)

    override fun exitParamRuleSet(ctx: FSHParser.ParamRuleSetContext) = onExitRuleset(ctx)

    override fun enterDescription(ctx: FSHParser.DescriptionContext) {
        currentResource?.apply {
            description = when {
                ctx.STRING() != null -> ctx.STRING().text
                ctx.MULTILINE_STRING() != null -> ctx.MULTILINE_STRING().text
                else -> throw ParserStateUnsupportedException("Description is not set", ctx)
            }.stripQuotes()
        } ?: throw ResourceIsNotSetException(ctx)
    }

    override fun enterTitle(ctx: FSHParser.TitleContext) {
        currentResource?.apply {
            title = ctx.STRING().text.stripQuotes()
        } ?: throw ResourceIsNotSetException(ctx)
    }

    override fun enterId(ctx: FSHParser.IdContext) {
        if (!ID_REGEX.matches(ctx.name().text)) {
            throw FshSemanticException(
                "Id '${ctx.name().text} 'does not match the FHIR id regex (max 64 characters, [A-Za-z.-] allowed, no exceptions.",
                file = inputFile,
                line = ctx.start.line
            )
        }
        val id = ctx.name().text.stripQuotes()
        val idCollision = when (currentResource!!.resourceType) {
            ResourceType.CodeSystem -> codeSystemList
            ResourceType.ValueSet -> valueSetList
            else -> throw ParserStateUnsupportedException("Id outside of CodeSystem or ValueSet", ctx)
        }.find { it.idFromFshItem == id }
        if (idCollision != null) {
            throw FshSemanticException(
                "Id $id already exists in ${idCollision.resourceType} ${idCollision.nameFromFshItem} (${
                    collisionDeclaredInFile(
                        inputFile,
                        idCollision.file
                    )
                })",
            )
        }
        currentResource?.apply {
            this.idFromFshItem = id
        } ?: throw ResourceIsNotSetException(ctx)
    }

    override fun enterConcept(ctx: FSHParser.ConceptContext) {
        if (currentMode != ResourceType.CodeSystem) {
            throw FshSemanticException("Concepts are only allowed in CodeSystems", file = inputFile, line = ctx.start.line)
        }
        val code = ctx.CODE().first().text.trim('#')
        val childCodes = ctx.CODE().drop(1).map { it.text.trim('#') }
        val displayString = ctx.STRING(0)?.text?.trim('"', '\'')
        val definitionString = (ctx.STRING(1)?.text ?: ctx.MULTILINE_STRING()?.text)?.trim('"', '\'')
        currentResource!!.addDirectConcept(
            code = code,
            childCodes = childCodes,
            display = displayString,
            definitionString = definitionString
        )
    }

    override fun enterInsertRule(ctx: FSHParser.InsertRuleContext) {
        if (currentResource == null) throw throw ResourceIsNotSetException(ctx)
        val definitionContext = currentDefinitionContext(ctx)
        val rule: TsResourceData.Rule = when {
            ctx.RULESET_REFERENCE() != null -> {
                val ruleKey = ctx.RULESET_REFERENCE().text.trim()
                TsResourceData.Rule(definitionContext = definitionContext, ruleType = RuleType.INSERT, value = ruleKey)
            }

            ctx.paramRuleSetRef() != null -> {
                val ruleKey =
                    ctx.paramRuleSetRef().PARAM_RULESET_REFERENCE().text.trim().removeSuffix("(").trim()
                val parameters = extractParameters(
                    ctx.paramRuleSetRef().parameter(),
                    ctx.paramRuleSetRef().lastParameter()
                ).map { it.trim('"', '\'') }
                val rulesetInsertsWithParameters = (currentResource as? RuleSet?)?.parameters?.any { outerParam ->
                    parameters.any { it.contains(outerParam) }
                } ?: false
                if (rulesetInsertsWithParameters) {
                    throw FshSemanticException(
                        "InsertRule parameters must not reference parameters from the ruleset",
                        file = inputFile,
                        line = (ctx.start?.line)?.inc()
                    )
                }
                TsResourceData.Rule(
                    definitionContext = definitionContext,
                    ruleType = RuleType.INSERT,
                    value = ruleKey,
                    parameterValues = parameters
                )
            }

            else -> throw ParserStateUnsupportedException("InsertRule is not parseable", ctx)
        }
        currentResource!!.rules.add(rule.value!! to rule)
        currentResource!!.referencedRuleSets.add(rule.value)
    }

    override fun enterFixedValueRule(ctx: FSHParser.FixedValueRuleContext) {
        logger.trace { "FixedValueRule in ${currentResource!!.resourceType} ${currentResource!!.nameFromFshItem}" }
        val rule = interpretRule(ctx.value(), ctx)
        val ruleKey = ctx.path().text
        currentResource?.rules?.add(ruleKey to rule) ?: throw ParserStateUnsupportedException("Resource is not set", ctx)
    }

    override fun enterCaretValueRule(ctx: FSHParser.CaretValueRuleContext) {
        currentResource?.rules?.add(ctx.caretPath().text to interpretRule(ctx.value(), ctx))
            ?: throw ResourceIsNotSetException(ctx)
    }

    /**
     * within CodeSystem FSH items, the insert rules are parsed as codeInsertRules (i.e. they can't have a path)
     */
    override fun enterCodeInsertRule(ctx: FSHParser.CodeInsertRuleContext) {
        logger.debug { "Entering CodeInsertRule: '${ctx.text.trim()}'" }
        if (ctx.CODE().isNotEmpty()) {
            throw FshDialectNotSupportedException("The explicit definition of concepts in FSH is not supported by BabelFSH", ctx)
        }
        when {
            ctx.paramRuleSetRef() != null -> {
                val ruleSetName = ctx.paramRuleSetRef().PARAM_RULESET_REFERENCE().text.trim().removeSuffix("(").trim()
                val parameterValues = extractParameters(ctx.paramRuleSetRef().parameter(), ctx.paramRuleSetRef().lastParameter()).map {
                    it.trim('"', '\'')
                }
                currentResource?.apply {
                    rules += ruleSetName to TsResourceData.Rule(
                        definitionContext = currentDefinitionContext(ctx),
                        ruleType = RuleType.INSERT,
                        value = ruleSetName,
                        parameterValues = parameterValues
                    )
                    referencedRuleSets.add(ruleSetName)
                }
                logger.trace { "Insert Param with ruleId $ruleSetName and parameters: $parameterValues" }
            }

            else -> {
                val ruleId = ctx.RULESET_REFERENCE().text.trim()
                currentResource?.apply {
                    rules.add(
                        ruleId to TsResourceData.Rule(
                            definitionContext = currentDefinitionContext(ctx),
                            ruleType = RuleType.INSERT,
                            value = ruleId
                        )
                    )
                    referencedRuleSets.add(ruleId)
                } ?: throw ResourceIsNotSetException(ctx)
            }
        }
    }

    override fun enterCodeCaretValueRule(ctx: FSHParser.CodeCaretValueRuleContext) {
        logger.trace { "Entering CodeCaretValueRule: '${ctx.text.trim()}'" }
        if (ctx.CODE() != null && ctx.CODE().size >= 1) {
            throw FshDialectNotSupportedException(
                what = "Generating CodeCaretValueRules with CODE is not supported by BabelFSH (example rule: '* #foo ^bar = \"baz\"')", ctx
            )
        }
        val star = ctx.STAR().text
        if (star.contains("  ")) {
            throw FshDialectNotSupportedException(
                what = "Generating CodeCaretValueRules with multiple spaces (i.e. reusing parent elements) is not supported by BabelFSH",
                ctx
            )
        }
        val code = ctx.caretPath().text
        val value = ctx.value() ?: throw ParserStateUnsupportedException("Value is not set", ctx)
        val rule = interpretRule(value, ctx)
        currentResource?.apply {
            rules.add(code to rule)
            logger.trace { "Added $currentMode rule for $code: $rule" }
        } ?: throw ResourceIsNotSetException(ctx)
    }

    private fun interpretRule(value: FSHParser.ValueContext, ctx: ParserRuleContext): TsResourceData.Rule {
        val unsupported = mapOf(
            "reference" to value.reference(),
            "canonical" to value.canonical(),
            "quantity" to value.quantity(),
            "ratio" to value.ratio(),
            "name" to value.name()
        )
        val definitionContext = currentDefinitionContext(ctx)
        val rule = when {
            value.code() != null -> TsResourceData.Rule(
                definitionContext = definitionContext,
                ruleType = RuleType.CODE,
                value = value.code().text
            )

            value.STRING() != null -> TsResourceData.Rule(definitionContext, RuleType.STRING, value.text)
            value.MULTILINE_STRING() != null -> TsResourceData.Rule(definitionContext, RuleType.MULTILINE_STRING, value.text)
            value.NUMBER() != null -> TsResourceData.Rule(definitionContext, RuleType.NUMBER, value.text)
            value.DATETIME() != null -> TsResourceData.Rule(definitionContext, RuleType.DATETIME, value.text)
            value.TIME() != null -> TsResourceData.Rule(definitionContext, RuleType.TIME, value.text)
            value.bool() != null -> TsResourceData.Rule(definitionContext, RuleType.BOOL, value.text)
            unsupported.any { it.value != null } -> {
                throw FshDialectNotSupportedException(
                    "Generating rules of type ${unsupported.filter { it.value != null }.keys} is not supported by BabelFSH",
                    value
                )
            }

            else -> throw ParserStateUnsupportedException("Value of rule is not set", value)
        }
        return rule
    }

    override fun enterTerminologyPluginComment(ctx: FSHParser.TerminologyPluginCommentContext) {
        logger.trace { "Entering TerminologyPluginComment: ${ctx.text}" }
        val commentText = ctx.TERM_PLUGIN_MULTILINE_COMMENT().text.replace("\n", " ")
            .trim()
            .removePrefix(BABELFSH_COMMENT_PREFIX)
            .removeSuffix(BABELFSH_COMMENT_SUFFIX)
            .trim()
        currentResource?.apply {
            terminologyPluginComment = commentText
            pluginDefinedIn = CommentDefinedIn(
                inputFile,
                ctx.start.line,
                resourceType,
                resourceIdentification = this.identification
            )
        } ?: throw ResourceIsNotSetException(ctx)
    }

    override fun enterInstance(ctx: FSHParser.InstanceContext) {
        val instanceOf = try {
            val instanceOfStatement = ctx.instanceMetadata().firstNotNullOf {
                it.instanceOf()
            }
            instanceOfStatement.name().text
        } catch (e: NoSuchElementException) {
            throw ParserStateUnsupportedException(
                "The instance ${ctx.name().text} does not have an InstanceOf statement, this is required by FSH",
                ctx
            )
        }
        if (instanceOf != "ConceptMap") {
            throw FshDialectNotSupportedException(what = "Instances other than ConceptMap (provided: $instanceOf)", ctx)
        }
        try {
            val usage = ctx.instanceMetadata().firstNotNullOf {
                it.usage()
            }
            val usageText = usage.CODE().text.lowercase().stripCode()
            if (usageText != "definition") {
                throw FshSemanticException(
                    "The instance ${ctx.name().text} does not have an Usage element set to #definition (value: #$usageText. This is a BabelFSH requirement, not a base FSH requirement.",
                    file = inputFile,
                    line = ctx.start.line
                )
            }
        } catch (e: NoSuchElementException) {
            throw ParserStateUnsupportedException(
                "The instance ${ctx.name().text} does not have an Usage element. This is not required by FSH, but BabelFSH requires that Usage is set to #definition",
                ctx
            )
        }
        if (currentMode != null) {
            throw ParserStateUnsupportedException("Mode is already set to $currentMode", ctx)
        }
        val id = ctx.name().text.stripQuotes()
        val title = ctx.instanceMetadata().firstNotNullOfOrNull {
            it.title()?.STRING()?.text?.stripQuotes()?.trim()
        } ?: run {
            logger.warn { "The ConceptMap instance $id does not have a metadata item Title. This is recommended over setting the title in the rules." }
            null
        }
        val description = ctx.instanceMetadata().firstNotNullOfOrNull {
            it.description()?.STRING()?.text?.stripQuotes()?.trim()
        } ?: run {
            logger.warn { "The ConceptMap instance $id does not have a metadata item Description. This is recommended over setting the title in the rules." }
            null
        }
        currentResource = ConceptMapInstance(
            workingDirectory = workingDirectory,
            file = inputFile,
            id = id,
            title = title,
            description = description
        )
    }

    override fun exitInstance(ctx: FSHParser.InstanceContext) {
        if (currentMode != ResourceType.ConceptMap) {
            throw ParserStateUnsupportedException("Mode is not set to ConceptMapInstance", ctx)
        }

        conceptMapList.add(currentResource as? ConceptMapInstance? ?: throw ResourceIsNotSetException(ctx))

    }

    override fun enterProfile(ctx: FSHParser.ProfileContext) {
        throw FshDialectNotSupportedException(what = "Profiles", ctx)
    }

    override fun enterExtension(ctx: FSHParser.ExtensionContext) {
        throw FshDialectNotSupportedException(what = "Extensions", ctx)
    }

    override fun enterInvariant(ctx: FSHParser.InvariantContext) {
        throw FshDialectNotSupportedException(what = "Invariants", ctx)
    }

    override fun enterMapping(ctx: FSHParser.MappingContext) {
        throw FshDialectNotSupportedException(what = "Mappings", ctx)
    }

    override fun enterLogical(ctx: FSHParser.LogicalContext) {
        throw FshDialectNotSupportedException(what = "Logical models", ctx)
    }

    override fun enterResource(ctx: FSHParser.ResourceContext) {
        throw FshDialectNotSupportedException(what = "Resources", ctx)
    }

    override fun exitDoc(ctx: FSHParser.DocContext?) {
        logger.debug { "Finished parsing file" }
    }

    companion object {
        private val ID_REGEX = Regex("^[A-Za-z0-9\\-.]{1,64}$")
        private val NAME_REGEX = Regex("^[A-Z]([A-Za-z0-9_]){1,254}$")
    }
}

class FshDialectNotSupportedException(
    unsupportedOperation: String, context: ParserRuleContext
) : ParserException(unsupportedOperation, context) {
    constructor(
        what: String,
        context: ParserRuleContext,
        verb: String = "Generating",
        suffix: String = "is not supported by BabelFSH"
    ) : this(
        "${verb.trim()} _${what.trim()}_ ${suffix.trim()}", context
    )
}

open class ParserStateUnsupportedException(message: String, context: ParserRuleContext) : ParserException(message, context)

class ResourceIsNotSetException(context: ParserRuleContext) :
    ParserStateUnsupportedException("Resource is not set", context)

sealed class ParserException(message: String, context: ParserRuleContext) : Exception(message) {
    val line = (context.start?.line)?.inc()
    val offendingText = context.text.trim()
    val inRule = context::class.simpleName
}

class FshSemanticException(
    message: String,
    val resourceType: ResourceType? = null,
    val resourceName: String? = null,
    val file: File? = null,
    val line: Int? = null
) : Exception(message)
