package de.mii_termserv.babelfsh.antlr.service.fsh.file

import de.mii_termserv.babelfsh.antlr.service.fsh.rules.ParseTreeItem
import de.mii_termserv.babelfsh.antlr.service.fsh.rules.RuleParseTree
import de.mii_termserv.babelfsh.utils.stripCode
import de.mii_termserv.babelfsh.utils.stripQuotes
import kotlinx.serialization.json.*

class GraphToJsonBuilder(
    private val parseTree: RuleParseTree,
    private val resourceData: TsResourceData,
) {

    fun buildJson(): JsonObject {
        val rootNode = parseTree.nodeGraph.vertexSet().find { it.nodeType == ParseTreeItem.NodeType.RESOURCE }
            ?: throw IllegalStateException("No root node found")
        return buildJson(rootNode) as JsonObject
    }

    private fun getChildren(node: ParseTreeItem): List<ParseTreeItem> {
        val edges = parseTree.nodeGraph.outgoingEdgesOf(node)
        return edges.map { parseTree.nodeGraph.getEdgeTarget(it) }
    }

    fun buildJson(node: ParseTreeItem): JsonElement {
        return when (node.nodeType) {
            ParseTreeItem.NodeType.RESOURCE -> {
                buildJsonObject {
                    put("resourceType", resourceData.resourceType.name)
                    resourceData.idFromFshItem?.let {
                        put("id", it)
                    }
                    resourceData.nameFromFshItem?.let {
                        put("name", it)
                    }
                    resourceData.title?.let {
                        put("title", it)
                    }
                    resourceData.description?.let {
                        put("description", it)
                    }
                    getChildren(node).forEach { child ->
                        put(child.pathPart!!, buildJson(child))
                    }
                }
            }

            ParseTreeItem.NodeType.ARRAY -> {
                buildJsonArrayFromNode(node)
            }

            ParseTreeItem.NodeType.OBJECT, ParseTreeItem.NodeType.ARRAY_ELEMENT ->
                when (val nodeValue = node.getValue()) {
                    null -> {
                        buildJsonObject {
                            getChildren(node).forEach { child ->
                                put(child.pathPart!!, buildJson(child))
                            }
                        }
                    }

                    else -> {
                        val primitive = ruleEvaluator(node.fullPath, nodeValue)
                        primitive
                    }
                }
        }
    }

    private fun buildJsonArrayFromNode(node: ParseTreeItem): JsonArray = buildJsonArray {
        getChildren(node).forEach {
            add(buildJson(it))
        }
    }


    private fun ParseTreeItem.getValue(): TsResourceData.Rule? {
        if (nodeType !in listOf(ParseTreeItem.NodeType.OBJECT, ParseTreeItem.NodeType.ARRAY_ELEMENT)) {
            throw IllegalStateException("Cannot get value for node type $nodeType")
        }
        return this.ruleIndex?.let {
            resourceData.rules[it].second
        }
    }

    private fun ruleEvaluator(fullPath: String, ruleValue: TsResourceData.Rule): JsonPrimitive {
        if (ruleValue.value == null) throw IllegalStateException("Rule value is null at rule $fullPath")
        return when (ruleValue.ruleType) {
            TsResourceData.Rule.RuleType.STRING -> JsonPrimitive(ruleValue.value.stripQuotes())
            TsResourceData.Rule.RuleType.BOOL -> JsonPrimitive(ruleValue.value.toBoolean())
            TsResourceData.Rule.RuleType.CODE -> JsonPrimitive(ruleValue.value.stripCode())
            TsResourceData.Rule.RuleType.NUMBER -> {
                when {
                    ruleValue.value.contains(".") -> JsonPrimitive(ruleValue.value.toDouble())
                    else -> JsonPrimitive(ruleValue.value.toLong())
                }
            }

            TsResourceData.Rule.RuleType.DATETIME, TsResourceData.Rule.RuleType.TIME -> JsonPrimitive(ruleValue.value)
            TsResourceData.Rule.RuleType.CANONICAL -> JsonPrimitive(ruleValue.value)
            TsResourceData.Rule.RuleType.NAME -> throw UnsupportedOperationException("Rules with lexical value type ${ruleValue.ruleType} are not supported by BabelFSH")

            else -> TODO("Rule type evaluation: ${ruleValue.ruleType}")
        }
    }
}