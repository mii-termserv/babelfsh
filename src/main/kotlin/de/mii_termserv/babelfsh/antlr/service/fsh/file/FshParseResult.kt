package de.mii_termserv.babelfsh.antlr.service.fsh.file

import de.mii_termserv.babelfsh.antlr.service.babelfsh.comments.CommentDefinedIn
import de.mii_termserv.babelfsh.api.ResourceType
import de.mii_termserv.babelfsh.utils.logging.trace
import org.slf4j.LoggerFactory

data class FshParseResult(
    val aliases: List<Alias> = listOf(),
    val codeSystems: List<TsResourceData> = listOf(),
    val valueSets: List<TsResourceData> = listOf(),
    val conceptMaps: List<ConceptMapInstance> = listOf(),
    val ruleSets: List<RuleSet> = listOf()
) {

    private val logger = LoggerFactory.getLogger(javaClass)

    operator fun plus(parseResult: FshParseResult): FshParseResult {
        return this.copy(
            aliases = this.aliases + parseResult.aliases,
            codeSystems = this.codeSystems + parseResult.codeSystems,
            valueSets = this.valueSets + parseResult.valueSets,
            ruleSets = this.ruleSets + parseResult.ruleSets
        )
    }

    /**
     * apply all aliases in turn to the provided resource and replace instances of the alias in the rules
     * This does only support aliases in the rules, not in the resource metadata (id, title, description)!
     */
    private fun resolveAliasesInResource(data: TsResourceData): TsResourceData {
        return aliases.fold(data) { acc, alias ->
            val newRules = acc.rules.map { (ruleKey, ruleValue) ->
                if (ruleValue.value?.contains(alias.name) != true) {
                    return@map ruleKey to ruleValue
                }
                logger.trace { "Replacing alias ${alias.name} with ${alias.value} in rule ${ruleValue.value}" }
                ruleKey to ruleValue.copy(value = ruleValue.value.replace(alias.name, alias.value))
            }
            acc.copyWithRules(newRules.toMutableList())
        }
    }

    fun resolveAliases(): FshParseResult {
        val resolvedRuleSets = ruleSets.map(::resolveAliasesInResource).map {
            it as RuleSet
        }
        val resolvedCodeSystems = codeSystems.map { resolveAliasesInResource(it) }
        val resolvedValueSets = valueSets.map { resolveAliasesInResource(it) }
        val resolvedConceptMaps = conceptMaps.map { resolveAliasesInResource(it) as ConceptMapInstance }

        return FshParseResult(
            aliases = this.aliases,
            codeSystems = resolvedCodeSystems,
            valueSets = resolvedValueSets,
            conceptMaps = resolvedConceptMaps,
            ruleSets = resolvedRuleSets
        )
    }

    private fun resolveInsertRule(
        resolvedRuleSets: List<RuleSet>,
        rule: TsResourceData.Rule,
        data: TsResourceData
    ): List<Pair<String, TsResourceData.Rule>> {
        val referencedRuleSet = resolvedRuleSets.find { rs -> rs.nameFromFshItem == rule.value }
            ?: throw FshSemanticException(
                "Referenced RuleSet ${rule.value} not found",
                file = data.file,
                resourceName = data.nameFromFshItem,
                resourceType = data.resourceType
            )
        if (referencedRuleSet.parameters.size != (rule.parameterValues?.size ?: 0)) {
            throw FshSemanticException(
                "Referenced RuleSet ${rule.value} (in ${data.resourceType} '${data.nameFromFshItem}' in file ${data.file}) has ${referencedRuleSet.parameters.size} parameters, but ${rule.parameterValues?.size ?: 0} were provided",
                file = data.file,
                resourceName = data.nameFromFshItem,
                resourceType = data.resourceType
            )
        }
        return when (rule.parameterValues?.size) {
            0, null -> resolveInsertRules(referencedRuleSet, resolvedRuleSets).rules
            else -> {
                val parameterMap = referencedRuleSet.parameters.zip(rule.parameterValues).toMap()
                referencedRuleSet.rules.map { (refRuleName, refRule) ->
                    val newRule = refRule.copy(value = refRule.value?.let { value ->
                        parameterMap.entries.fold(value) { acc, (paramName, paramValue) ->
                            acc.replace("{$paramName}", paramValue)
                        }
                    })
                    refRuleName to newRule
                }
            }
        }
    }

    private fun resolveCommentInsert(data: TsResourceData, ruleSetsWithComment: List<RuleSet>): Pair<String, CommentDefinedIn>? {
        return when {
            data.terminologyPluginComment == null -> {
                when (ruleSetsWithComment.size) {
                    0 -> null
                    1 -> {
                        val theComment = ruleSetsWithComment.first()
                        val theInsert =
                            data.rules.find { it.second.ruleType == TsResourceData.Rule.RuleType.INSERT && (it.second.value?.equals(theComment.nameFromFshItem) ?: false) }
                                ?: throw FshSemanticException(
                                    "${data.resourceType} ${data.nameFromFshItem} references RuleSet ${theComment.nameFromFshItem} with plugin comment, but no insert rule found (this should be impossible!)",
                                    file = data.file,
                                    resourceName = data.nameFromFshItem,
                                    resourceType = data.resourceType
                                )
                        return when (theComment.parameters.isNotEmpty()) {
                            true -> {
                                val kvPairs = theComment.parameters.zip(theInsert.second.parameterValues!!)
                                val newComment = theComment.terminologyPluginComment!!.let { comment ->
                                    kvPairs.fold(comment) { acc, (key, value) ->
                                        acc.replace("{$key}", value)
                                    }
                                }
                                newComment to theComment.pluginDefinedIn!!
                            }
                            else -> {
                               theComment.terminologyPluginComment!! to theComment.pluginDefinedIn!!
                            }
                        }

                    }

                    else -> throw FshSemanticException(
                        "${data.resourceType} ${data.nameFromFshItem} references multiple RuleSets with plugin comments. Only one is allowed.",
                        file = data.file,
                        resourceName = data.nameFromFshItem,
                        resourceType = data.resourceType
                    )
                }
            }

            ruleSetsWithComment.isNotEmpty() -> {
                throw FshSemanticException(
                    "${data.resourceType} ${data.nameFromFshItem} has a plugin comment and references RuleSet(-s) ${ruleSetsWithComment.joinToString { it.identification }}} with plugin comments. Only one is allowed.",
                    file = data.file,
                    resourceName = data.nameFromFshItem,
                    resourceType = data.resourceType
                )
            }

            else -> data.terminologyPluginComment!! to data.pluginDefinedIn!!
        }
    }

    private fun <T : TsResourceData> resolveInsertRules(data: T, resolvedRuleSets: List<RuleSet> = ruleSets): T {
        val newRules = data.rules.map { (code, rule) ->
            when (rule.ruleType) {
                TsResourceData.Rule.RuleType.INSERT -> resolveInsertRule(resolvedRuleSets, rule, data)
                else -> listOf(code to rule)
            }
        }.flatten().toMutableList()
        val allReferencedRuleSets = ruleSets.filter { it.nameFromFshItem in data.referencedRuleSets }
        val ruleSetsWithComment = allReferencedRuleSets.filter { it.terminologyPluginComment != null }
        @Suppress("UNCHECKED_CAST")
        return when (val newComment = resolveCommentInsert(data, ruleSetsWithComment)) {
            null -> data.copyWithRules(newRules) as T
            else -> data.copyWithRules(newRules, newTerminologyComment = newComment.first, newPluginDefinedIn = newComment.second) as T
        }
    }

    fun resolveInsertsAndParameters(): FshParseResult {
        val insertGraph = RuleInsertGraph()
        ruleSets.forEach { ruleSet ->
            ruleSet.referencedRuleSets.forEach { referencedRule ->
                insertGraph.addEdge(ruleSet.identification, referencedRule)
            }
        }
        val cyclicDescription = insertGraph.isCyclicDescription()
        if (cyclicDescription != null) {
            throw FshSemanticException(cyclicDescription, resourceType = ResourceType.RuleSet)
        }
        val resolvedRuleSets = ruleSets.map(::resolveInsertRules).map {
            it
        }
        val resolvedCodeSystems = codeSystems.map { resolveInsertRules(it, resolvedRuleSets) }
        val resolvedValueSets = valueSets.map { resolveInsertRules(it, resolvedRuleSets) }
        val resolvedConceptMaps = conceptMaps.map { resolveInsertRules(it, resolvedRuleSets) }

        return FshParseResult(
            aliases = this.aliases,
            codeSystems = resolvedCodeSystems,
            valueSets = resolvedValueSets,
            conceptMaps = resolvedConceptMaps,
            ruleSets = resolvedRuleSets
        )
    }
}

/**
 * helper to ensure that the insert rules do not form a cycle that would lead to an infinite loop
 */
private class RuleInsertGraph {
    private val adjList: MutableMap<String, MutableList<String>> = mutableMapOf()

    private val vertices get() = adjList.keys

    fun addEdge(u: String, v: String) {
        adjList.computeIfAbsent(u) { mutableListOf() }.add(v)
    }

    fun isCyclicUtil(v: String, visited: MutableMap<String, Boolean>, stack: MutableMap<String, Boolean>): String? {

        visited[v] = true
        stack[v] = true

        for (u in adjList[v]!!) {
            when {
                visited[u] == false -> {
                    val recursion = isCyclicUtil(u, visited, stack)
                    if (recursion != null) {
                        return recursion
                    }
                }

                stack[u] == true -> {
                    return "$v -[references]-> $u -[references]-> $v"
                }
            }
        }
        stack[v] = false
        return null
    }

    fun isCyclicDescription(): String? {
        val visited = vertices.associateWith { false }.toMutableMap()
        val stack = vertices.associateWith { false }.toMutableMap()

        return vertices.map { vertex ->
            when {
                visited[vertex] == false -> isCyclicUtil(vertex, visited, stack)
                else -> null
            }
        }.firstOrNull()?.let {
            "Cyclic insert rules detected in RuleSets: $it"
        } ?: return null
    }
}