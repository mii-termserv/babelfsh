package de.mii_termserv.babelfsh.antlr.service.fsh.file

import de.mii_termserv.babelfsh.antlr.service.babelfsh.comments.CommentDefinedIn
import de.mii_termserv.babelfsh.api.ResourceType
import java.io.File
import java.nio.file.Path


data class Alias(
    val name: String, val value: String
)

data class DirectConcept(
    val code: String,
    val childCodes: List<String>,
    val display: String?,
    val definitionString: String?
)

abstract class TsResourceData(
    val workingDirectory: Path?,
    val file: File?,
    val resourceType: ResourceType,
    val nameFromFshItem: String? = null,
    var idFromFshItem: String? = null,
    var title: String? = null,
    var description: String? = null,
    /**
     * not a map, because keys can be duplicated!
     */
    val rules: MutableList<Pair<String, Rule>> = mutableListOf(),
    val directConcepts: MutableList<DirectConcept> = mutableListOf(),
    var terminologyPluginComment: String? = null,
    var pluginDefinedIn: CommentDefinedIn? = null,
    val referencedRuleSets: MutableList<String> = mutableListOf()
) {

    abstract val identification: String

    abstract fun copyWithRules(
        newRules: MutableList<Pair<String, Rule>>,
        newTerminologyComment: String? = terminologyPluginComment,
        newPluginDefinedIn: CommentDefinedIn? = pluginDefinedIn
    ): TsResourceData

    fun addDirectConcept(code: String, childCodes: List<String>, display: String?, definitionString: String?) {
        if (resourceType != ResourceType.CodeSystem) {
            throw IllegalStateException("Direct concepts can only be added to CodeSystems")
        }
        directConcepts.add(DirectConcept(code, childCodes, display, definitionString))
    }

    data class Rule(
        val definitionContext: RuleDefinitionContext,
        val ruleType: RuleType,
        val value: String?,
        val parameterValues: List<String>? = null
    ) {
        enum class RuleType {
            STRING, MULTILINE_STRING, NUMBER, DATETIME, TIME, REFERENCE, CANONICAL, CODE, BOOL, NAME, INSERT;
        }
    }

    data class RuleDefinitionContext(
        val resourceType: ResourceType,
        val resourceName: String,
        val file: File?,
        val lineNumber: Int
    ) {
        data class Key(val resourceType: String, val resourceName: String, val file: File?)

        val key = Key(resourceType.name, resourceName, file)
    }
}

class CodeSystemValueSetDefinition(
    workingDirectory: Path?,
    file: File?,
    nameFromFshItem: String,
    idFromFshItem: String? = null,
    title: String? = null,
    description: String? = null,
    rules: MutableList<Pair<String, Rule>> = mutableListOf(),
    directConcepts: MutableList<DirectConcept> = mutableListOf(),
    terminologyPluginComment: String? = null,
    pluginDefinedIn: CommentDefinedIn? = null,
    referencedRuleSets: MutableList<String> = mutableListOf()
) : TsResourceData(
    workingDirectory = workingDirectory,
    file = file,
    resourceType = ResourceType.CodeSystem,
    nameFromFshItem = nameFromFshItem,
    idFromFshItem = idFromFshItem,
    title = title,
    description = description,
    rules = rules,
    directConcepts = directConcepts,
    terminologyPluginComment = terminologyPluginComment,
    pluginDefinedIn = pluginDefinedIn,
    referencedRuleSets = referencedRuleSets
) {

    override val identification: String
        get() = nameFromFshItem!!

    override fun copyWithRules(
        newRules: MutableList<Pair<String, Rule>>,
        newTerminologyComment: String?,
        newPluginDefinedIn: CommentDefinedIn?
    ): CodeSystemValueSetDefinition {
        return CodeSystemValueSetDefinition(
            workingDirectory = this.workingDirectory,
            file = file,
            nameFromFshItem = this.nameFromFshItem!!,
            idFromFshItem = this.idFromFshItem,
            title = this.title,
            description = this.description,
            rules = newRules,
            directConcepts = directConcepts,
            terminologyPluginComment = newTerminologyComment,
            pluginDefinedIn = newPluginDefinedIn,
            referencedRuleSets = referencedRuleSets
        )
    }
}

class ConceptMapInstance(
    workingDirectory: Path?,
    file: File?,
    id: String,
    name: String? = null,
    title: String? = null,
    description: String? = null,
    rules: MutableList<Pair<String, Rule>> = mutableListOf(),
    terminologyPluginComment: String? = null,
    pluginDefinedIn: CommentDefinedIn? = null,
    referencedRuleSets: MutableList<String> = mutableListOf()
) : TsResourceData(
    workingDirectory = workingDirectory,
    file = file,
    idFromFshItem = id,
    nameFromFshItem = name,
    title = title,
    description = description,
    rules = rules,
    terminologyPluginComment = terminologyPluginComment,
    pluginDefinedIn = pluginDefinedIn,
    resourceType = ResourceType.ConceptMap,
    referencedRuleSets = referencedRuleSets
) {

    override val identification: String get() = this.idFromFshItem!!

    override fun copyWithRules(
        newRules: MutableList<Pair<String, Rule>>,
        newTerminologyComment: String?,
        newPluginDefinedIn: CommentDefinedIn?
    ) = ConceptMapInstance(
        workingDirectory = this.workingDirectory,
        file = this.file,
        id = this.idFromFshItem!!,
        name = this.nameFromFshItem,
        title = this.title,
        description = this.description,
        rules = newRules,
        terminologyPluginComment = newTerminologyComment,
        pluginDefinedIn = newPluginDefinedIn,
        referencedRuleSets = referencedRuleSets
    )
}

class RuleSet(
    workingDirectory: Path?,
    file: File?,
    name: String? = null,
    id: String? = null,
    title: String? = null,
    description: String? = null,
    rules: MutableList<Pair<String, Rule>> = mutableListOf(),
    terminologyPluginComment: String? = null,
    referencedRuleSets: MutableList<String> = mutableListOf(),
    pluginDefinedIn: CommentDefinedIn? = null,
    val parameters: MutableList<String> = mutableListOf()
) : TsResourceData(
    workingDirectory = workingDirectory,
    file = file,
    resourceType = ResourceType.RuleSet,
    nameFromFshItem = name,
    idFromFshItem = id,
    title = title,
    description = description,
    rules = rules,
    terminologyPluginComment = terminologyPluginComment,
    pluginDefinedIn = pluginDefinedIn,
    referencedRuleSets = referencedRuleSets
) {

    override val identification: String get() = nameFromFshItem!!

    override fun copyWithRules(
        newRules: MutableList<Pair<String, Rule>>, newTerminologyComment: String?,
        newPluginDefinedIn: CommentDefinedIn?
    ): TsResourceData {
        return RuleSet(
            workingDirectory = this.workingDirectory,
            file = this.file,
            name = this.nameFromFshItem,
            id = this.idFromFshItem,
            title = this.title,
            description = this.description,
            rules = newRules,
            terminologyPluginComment = this.terminologyPluginComment,
            referencedRuleSets = this.referencedRuleSets,
            pluginDefinedIn = this.pluginDefinedIn,
            parameters = this.parameters
        )
    }
}