package de.mii_termserv.babelfsh.antlr.service.fsh.rules

import de.mii_termserv.babelfsh.antlr.FshRuleBaseListener
import de.mii_termserv.babelfsh.antlr.FshRuleLexer
import de.mii_termserv.babelfsh.antlr.FshRuleParser
import de.mii_termserv.babelfsh.antlr.FshRuleParser.*
import de.mii_termserv.babelfsh.antlr.service.fsh.file.TsResourceData
import de.mii_termserv.babelfsh.utils.logging.trace
import org.antlr.v4.runtime.CharStreams
import org.antlr.v4.runtime.CommonTokenStream
import org.antlr.v4.runtime.tree.ParseTreeWalker
import org.jgrapht.graph.DefaultDirectedGraph
import org.jgrapht.graph.DefaultEdge
import org.slf4j.LoggerFactory


class FshPathParserService(private val rules: List<Pair<String, TsResourceData.Rule>>) {

    private val logger = LoggerFactory.getLogger(javaClass)

    private val paths = rules.map { it.first }

    private val texts = paths.joinToString("\n").plus("\n")
    private val inputStream = CharStreams.fromString(texts)
    private val lexer = FshRuleLexer(inputStream)
    private val tokenStream = CommonTokenStream(lexer)
    private val parser = FshRuleParser(tokenStream)
    private val tree = parser.doc()
    private val walker = ParseTreeWalker()

    fun resolvePaths(): RuleParseTree {
        val ruleParseTree = RuleParseTree()
        val listener = FshPathListener(ruleParseTree, rules)
        logger.trace { "Parse tree for FSH rule set: ${tree.toStringTree(parser)}" }
        walker.walk(listener, tree)
        return ruleParseTree
    }
}

private class FshPathListener(val ruleParseTree: RuleParseTree, val rules: List<Pair<String, TsResourceData.Rule>>) : FshRuleBaseListener() {

    private val logger = LoggerFactory.getLogger(javaClass)

    private var currentRuleIndex = 0

    private var currentRuleText = ""

    private var currentRuleContext: TsResourceData.RuleDefinitionContext? = null

    override fun enterFshRule(ctx: FshRuleContext) {
        currentRuleText = "rule@$currentRuleIndex : '${ctx.text.trim()}'"
        logger.trace { "Entering FSH $currentRuleText" }
        currentRuleContext = rules[currentRuleIndex].second.definitionContext
    }

    override fun exitFshRule(ctx: FshRuleContext) {
        logger.trace { "Exited $currentRuleText" }
        currentRuleIndex += 1
    }

    override fun enterSimpleFshRule(ctx: SimpleFshRuleContext) {
        logger.trace { "Entering simple FSH rule: $currentRuleText" }
        val tokens = ctx.pathPart().map { it.RULE_TOKEN() }
        val rootRuleIndex = when (tokens.size == 1) {
            true -> currentRuleIndex
            else -> null
        }
        val initialRoot = ruleParseTree.getOrCreateVertex(
            pathToken = tokens.first().text,
            nodeType = ParseTreeItem.NodeType.OBJECT,
            parent = null,
            arrayIndex = null,
            ruleIndex = rootRuleIndex,
            definitionContext = currentRuleContext!!
        )
        val fullRulePath = tokens.drop(1).fold(initialRoot) { parent, token ->
            val ruleIndex = when (token == tokens.last()) {
                true -> currentRuleIndex
                else -> null
            }
            ruleParseTree.getOrCreateVertex(
                pathToken = token.text,
                nodeType = ParseTreeItem.NodeType.OBJECT,
                parent = parent,
                arrayIndex = null,
                ruleIndex = ruleIndex,
                definitionContext = currentRuleContext!!
            )

        }
        logger.trace { "Parse tree after simple rule ${fullRulePath.fullPath}: \n${ruleParseTree.print().prependIndent("  ")}" }
    }

    override fun enterArrayFshRule(ctx: ArrayFshRuleContext) {
        logger.trace { "Entering array FSH rule: $currentRuleText" }
        val initial: ParseTreeItem? = null
        val leafNode = ctx.children.fold(initial) { parent, child ->
            val thisRuleIndex = when (child == ctx.children.last()) {
                true -> currentRuleIndex
                else -> null
            }
            val newNode =
                when (child) {
                    is ArrayPrefixContext -> {
                        //prefix of the rule that doesn't contain array indices and is not terminal
                        child.RULE_TOKEN().fold(parent) { prefixParent, prefixToken ->
                            ruleParseTree.getOrCreateVertex(
                                pathToken = prefixToken.text,
                                nodeType = ParseTreeItem.NodeType.OBJECT,
                                parent = prefixParent,
                                arrayIndex = null,
                                ruleIndex = null,
                                definitionContext = currentRuleContext!!
                            )
                        }
                    }

                    is IndexedPathPartContext -> {
                        val pathPart = child.RULE_TOKEN().text
                        val arrayContainer = ruleParseTree.getArrayContainer(parent, pathPart, currentRuleContext!!)
                        val newPath = buildString {
                            parent?.fullPath?.let { append(it).append(".") }
                            append(pathPart)
                        }
                        val currentIndex = ruleParseTree.currentIndex(newPath)
                        when {
                            child.arrayIndex().HARD_INDEX() != null -> {
                                val intIndex = child.arrayIndex().HARD_INDEX().text.toInt()
                                when {
                                    currentIndex == null -> {
                                        if (intIndex == 0) {
                                            ruleParseTree.getOrCreateVertex(
                                                pathToken = pathPart,
                                                nodeType = ParseTreeItem.NodeType.ARRAY_ELEMENT,
                                                parent = arrayContainer,
                                                arrayIndex = intIndex,
                                                ruleIndex = thisRuleIndex,
                                                definitionContext = currentRuleContext!!
                                            )
                                        } else {
                                            throw UnsupportedOperationException(
                                                "No index found for $newPath in $currentRuleText. " +
                                                        "This likely means that you started an array with an index of '0' without a preceding index. " +
                                                        "Use '+' instead."
                                            )
                                        }
                                    }

                                    intIndex in currentIndex..(currentIndex + 1) -> {
                                        ruleParseTree.getOrCreateVertex(
                                            pathToken = pathPart,
                                            nodeType = ParseTreeItem.NodeType.ARRAY_ELEMENT,
                                            parent = arrayContainer,
                                            arrayIndex = intIndex,
                                            ruleIndex = thisRuleIndex,
                                            definitionContext = currentRuleContext!!
                                        )
                                    }

                                    else -> throw UnsupportedOperationException(
                                        "Index mismatch for $newPath in $currentRuleText. " +
                                                "Expected $currentIndex or ${currentIndex + 1}, got $intIndex"
                                    )
                                }
                            }

                            child.arrayIndex().SOFT_INDEX()?.text?.contains("+") == true -> {
                                ruleParseTree.getOrCreateVertex(
                                    pathToken = pathPart,
                                    nodeType = ParseTreeItem.NodeType.ARRAY_ELEMENT,
                                    parent = arrayContainer,
                                    arrayIndex = currentIndex?.inc() ?: run {
                                        logger.trace { "No index found for $newPath in $currentRuleText. Starting at 0" }
                                        0
                                    },
                                    ruleIndex = thisRuleIndex,
                                    definitionContext = currentRuleContext!!
                                )
                            }

                            child.arrayIndex().SOFT_INDEX()?.text?.contains("=") == true -> {
                                currentIndex?.let {
                                    ruleParseTree.getByPathAndIndex(newPath, it)
                                } ?: throw UnsupportedOperationException(
                                    "No index found for $newPath in $currentRuleText. " +
                                            "This likely means that you started an array with an index of '=' without a preceding index. " +
                                            "Use '+' instead."
                                )
                            }

                            else -> throw IllegalStateException("Unparsable index: '${child.arrayIndex().text}' in $currentRuleText")
                        }
                    }

                    is PathPartContext -> {
                        ruleParseTree.getOrCreateVertex(
                            pathToken = child.RULE_TOKEN().text,
                            nodeType = ParseTreeItem.NodeType.OBJECT,
                            parent = parent,
                            arrayIndex = null,
                            ruleIndex = thisRuleIndex,
                            definitionContext = currentRuleContext!!
                        )
                    }

                    else -> throw IllegalStateException("Unexpected child type: ${child::class.java.name}")
                }

            logger.trace { "Fold result: $newNode" }
            newNode
        }

        logger.trace { "Parse tree after resolving array $currentRuleText to ${leafNode!!.fullPath}: ${ruleParseTree.print()}" }
    }

    override fun exitDoc(ctx: DocContext?) {
        logger.trace { "Resolved paths: ${ruleParseTree.resolvedPaths}" }
    }

}

class RuleParseTree {

    val resourceRoot = ParseTreeItem(
        pathPart = null,
        nodeType = ParseTreeItem.NodeType.RESOURCE,
        parent = null,
        arrayIndex = null,
        ruleIndex = null
    )

    val nodeGraph = DefaultDirectedGraph<ParseTreeItem, DefaultEdge>(DefaultEdge::class.java).also {
        it.addVertex(resourceRoot)
    }

    private val knownPaths = mutableListOf<ParseTreeItem>()

    private val _resolvedPaths = mutableListOf<ParseTreeItem>()
    val resolvedPaths: List<ParseTreeItem> get() = _resolvedPaths.sortedBy { it.ruleIndex }

    fun getOrCreateVertex(
        pathToken: String?,
        nodeType: ParseTreeItem.NodeType,
        parent: ParseTreeItem?,
        arrayIndex: Int?,
        ruleIndex: Int?,
        definitionContext: TsResourceData.RuleDefinitionContext
    ): ParseTreeItem {
        val found = knownPaths.find {
            (it.nodeType == nodeType) && it.parent == (parent ?: resourceRoot) && it.pathPart == pathToken && it.arrayIndex == arrayIndex
        }
        if (found != null) {
            return found.apply {
                this.definitionContext.add(definitionContext)
            }
        }
        val new = ParseTreeItem(
            pathPart = pathToken,
            nodeType = nodeType,
            parent = parent ?: resourceRoot,
            arrayIndex = arrayIndex,
            ruleIndex = ruleIndex
        ).also {
            it.definitionContext.add(definitionContext)
            addEdge(parent = (parent ?: resourceRoot), item = it)
            if (ruleIndex != null) {
                _resolvedPaths.add(it)
            }
        }

        return new
    }

    fun getByPathAndIndex(path: String, index: Int): ParseTreeItem? {
        val matching = knownPaths.find {
            it.fullPath == "$path[$index]"
        }
        return matching
    }

    private fun buildArrayRegex(pathPrefix: String): Regex {
        val escapedBrackets = pathPrefix.replace("[", "\\[").replace("]", "\\]")
        return Regex("""$escapedBrackets\[(\d+|[+=])]""")
    }

    private fun addEdge(parent: ParseTreeItem, item: ParseTreeItem) {
        nodeGraph.addVertex(item)
        nodeGraph.addEdge(parent, item, DefaultEdge())
        if (knownPaths.none { it.fullPath == item.fullPath }) {
            knownPaths.add(item)
        }
    }

    fun print(): String = nodeGraph.edgeSet().joinToString("\n") { edge ->
        buildString {
            append(nodeGraph.getEdgeSource(edge))
            append(" -> ")
            append(nodeGraph.getEdgeTarget(edge))
        }
    }

    fun currentIndex(pathPrefix: String): Int? {
        return knownPaths
            .filter { it.nodeType == ParseTreeItem.NodeType.ARRAY_ELEMENT }
            .filter { it.fullPath.matches(buildArrayRegex(pathPrefix)) }.maxByOrNull { it.arrayIndex!! }?.arrayIndex
    }

    fun getArrayContainer(parent: ParseTreeItem?, pathToken: String?, definitionContext: TsResourceData.RuleDefinitionContext): ParseTreeItem {
        return nodeGraph.vertexSet().find { it.parent == parent && it.pathPart == pathToken } ?: getOrCreateVertex(
            pathToken = pathToken,
            nodeType = ParseTreeItem.NodeType.ARRAY,
            parent = parent,
            arrayIndex = null,
            ruleIndex = null,
            definitionContext = definitionContext
        )
    }
}

class ParseTreeItem(
    val pathPart: String?,
    val nodeType: NodeType,
    val parent: ParseTreeItem? = null,
    var arrayIndex: Int? = null,
    val ruleIndex: Int? = null,
    val definitionContext: MutableList<TsResourceData.RuleDefinitionContext> = mutableListOf()
) {

    val fullPath: String = buildString {
        if (nodeType == NodeType.RESOURCE) {
            return@buildString
        }
        parent?.let {
            append(it.fullPath)
        }
        pathPart?.let {
            if (nodeType != NodeType.ARRAY_ELEMENT) {
                if (parent != null && parent.nodeType != NodeType.RESOURCE) append(".")
                append(it)
            }
        }
        arrayIndex?.let {
            append("[$it]")
        }
    }

    override fun toString(): String {
        return buildString {
            append(nodeType.name)
            append("{")
            append(fullPath)
            append("}")
            if (ruleIndex != null) {
                append(" rule@$ruleIndex")
            }
            append(" parent: ")
            append(parent?.fullPath)
            append(")")
        }
    }

    enum class NodeType {
        OBJECT, ARRAY, ARRAY_ELEMENT, RESOURCE
    }
}
