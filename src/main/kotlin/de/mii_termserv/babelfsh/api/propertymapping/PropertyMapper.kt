package de.mii_termserv.babelfsh.api.propertymapping

import de.mii_termserv.babelfsh.api.content_containers.cs.PropertyValueType
import kotlinx.serialization.Serializable

@Serializable
data class PropertyMapper(
    val id: String, val arguments: Map<String, String>
) {
    val mapper by lazy { PropertyMapperFactory.createPropertyMapper(id, arguments) }
}

abstract class PropertyMapperImpl(val id: String, val arguments: Map<String, String>) {
    abstract fun mapPropertyValue(value: String): PropertyValueType<*, *>

    abstract fun validateArguments(): String?
}

object PropertyMapperFactory {

    private val propertyMappers: Map<String, (Map<String, String>) -> PropertyMapperImpl> = mapOf("boolean" to { BooleanMapper(it) })
    fun createPropertyMapper(id: String, arguments: Map<String, String>): PropertyMapperImpl =
        propertyMappers[id]?.invoke(arguments)?.let { mapper ->
            mapper.validateArguments()?.let { errorMessage ->
                throw PropertyMappingException(
                    propertyMapper = mapper, message = errorMessage
                )
            }
            mapper
        } ?: throw PropertyMappingException(
            propertyMapper = null, message = "No property mapper found for id $id (supported: ${listPropertyMapperIds()})"
        )

    private fun listPropertyMapperIds() = propertyMappers.keys

    fun mapperHelp() = buildString {
        appendLine("Supported property mappers: ${listPropertyMapperIds().joinToString()}")
    }
}

class BooleanMapper(arguments: Map<String, String>) : PropertyMapperImpl("boolean", arguments) {

    private enum class Keys(val jsonKey: String, val booleanValue: Boolean) {
        TRUE("true", true), FALSE("false", false)
    }

    override fun mapPropertyValue(value: String): PropertyValueType<*, *> {
        return PropertyValueType.BooleanContainer(toBoolean(value))
    }

    override fun validateArguments(): String? {
        Keys.entries.forEach { key ->
            if (!arguments.containsKey(key.jsonKey.lowercase())) {
                return "Missing argument for Boolean mapper: ${key.jsonKey}"
            }
            if (arguments.values.groupingBy { it }.eachCount().any { it.value > 1 }) {
                return "Duplicate argument for Boolean mapper: ${key.jsonKey}"
            }
        }
        return null
    }

    private fun toBoolean(value: String): Boolean {
        val mappedValue = arguments.entries.find { it.value == value } ?: throw PropertyMappingException(
            propertyMapper = this, message = "Value $value not found in arguments"
        )
        val booleanValue = Keys.entries.find { it.jsonKey == mappedValue.key.lowercase() }?.booleanValue ?: throw PropertyMappingException(
            propertyMapper = this, message = "Value $value not found in arguments"
        )
        return booleanValue
    }
}

class PropertyMappingException(val propertyMapper: PropertyMapperImpl?, message: String) : Exception(message)