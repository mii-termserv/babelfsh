package de.mii_termserv.babelfsh.api.plugins.vs

import de.mii_termserv.babelfsh.api.BabelfshContext
import de.mii_termserv.babelfsh.api.ResourceType
import de.mii_termserv.babelfsh.api.content_containers.vs.ValueSetContainer
import de.mii_termserv.babelfsh.api.plugins.BabelfshConversionResult
import de.mii_termserv.babelfsh.api.plugins.BabelfshTerminologyPlugin
import de.mii_termserv.babelfsh.api.plugins.PluginArguments


@Suppress("unused")
abstract class ValueSetPlugin<ArgType : PluginArguments>(
    pluginId: String,
    babelfshContext: BabelfshContext,
    shortHelp: String,
    extraHelp: String? = null,
    attribution: String? = null
) : BabelfshTerminologyPlugin<ValueSetContainer, BabelfshConversionResult, ArgType>(
    // TODO generic type?
    pluginId = pluginId,
    babelfshContext = babelfshContext,
    resourceType = ResourceType.ValueSet,
    shortHelp = shortHelp,
    extraHelp = extraHelp,
    attribution = attribution
)