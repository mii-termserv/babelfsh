package de.mii_termserv.babelfsh.api.plugins.cs

import de.mii_termserv.babelfsh.api.BabelfshContext
import de.mii_termserv.babelfsh.api.ResourceType
import de.mii_termserv.babelfsh.api.content_containers.PropertyContainer
import de.mii_termserv.babelfsh.api.content_containers.cs.CodeSystemConceptContainer
import de.mii_termserv.babelfsh.api.content_containers.cs.CodeSystemContainer
import de.mii_termserv.babelfsh.api.plugins.BabelfshTerminologyPlugin
import de.mii_termserv.babelfsh.api.plugins.PluginArguments
import se.sawano.java.text.AlphanumericComparator
import java.util.*

abstract class CodeSystemPlugin<ArgType : PluginArguments>(
    pluginId: String,
    babelfshContext: BabelfshContext,
    shortHelp: String,
    extraHelp: String? = null,
    attribution: String? = null
) : BabelfshTerminologyPlugin<CodeSystemContainer, CodeSystemConceptContainer, ArgType>(
    pluginId = pluginId,
    babelfshContext = babelfshContext,
    resourceType = ResourceType.CodeSystem,
    shortHelp = shortHelp,
    extraHelp = extraHelp,
    attribution = attribution
) {

    val alphanumericComparator by lazy { AlphanumericComparator(Locale.getDefault()) }

    /**
     * This function is used to get implicit properties as defined in the CodeSystem:
     * http://www.hl7.org/fhir/codesystem.html#defined-props
     */
    fun getImplicitPropertyForCode(property: String): PropertyContainer? {
        return when (property) {
            "status" -> PropertyContainer(
                "status",
                "http://hl7.org/fhir/concept-properties#status",
                null,
                type = PropertyContainer.PropertyTypeEnum.code
            )

            "inactive" -> PropertyContainer(
                "inactive",
                "http://hl7.org/fhir/concept-properties#inactive",
                null,
                type = PropertyContainer.PropertyTypeEnum.boolean
            )

            "effectiveDate" -> PropertyContainer(
                "effectiveDate",
                "http://hl7.org/fhir/concept-properties#effectiveDate",
                null,
                type = PropertyContainer.PropertyTypeEnum.dateTime
            )

            "deprecationDate" -> PropertyContainer(
                "deprecationDate",
                "http://hl7.org/fhir/concept-properties#deprecationDate",
                null,
                type = PropertyContainer.PropertyTypeEnum.dateTime
            )

            "retirementDate" -> PropertyContainer(
                "retirementDate",
                "http://hl7.org/fhir/concept-properties#retirementDate",
                null,
                type = PropertyContainer.PropertyTypeEnum.dateTime
            )

            "notSelectable" -> PropertyContainer(
                "notSelectable",
                "http://hl7.org/fhir/concept-properties#notSelectable",
                null,
                type = PropertyContainer.PropertyTypeEnum.boolean
            )

            "parent" -> PropertyContainer(
                "parent",
                "http://hl7.org/fhir/concept-properties#parent",
                null,
                type = PropertyContainer.PropertyTypeEnum.code
            )

            "child" -> PropertyContainer(
                "child",
                "http://hl7.org/fhir/concept-properties#child",
                null,
                type = PropertyContainer.PropertyTypeEnum.code
            )

            "partOf" -> PropertyContainer(
                "partOf",
                "http://hl7.org/fhir/concept-properties#partOf",
                null,
                type = PropertyContainer.PropertyTypeEnum.code
            )

            "synonym" -> PropertyContainer(
                "synonym",
                "http://hl7.org/fhir/concept-properties#synonym",
                null,
                type = PropertyContainer.PropertyTypeEnum.code
            )

            "comment" -> PropertyContainer(
                "comment",
                "http://hl7.org/fhir/concept-properties#comment",
                null,
                type = PropertyContainer.PropertyTypeEnum.code
            )

            "itemWeight" -> PropertyContainer(
                "itemWeight",
                "http://hl7.org/fhir/concept-properties#itemWeight",
                null,
                type = PropertyContainer.PropertyTypeEnum.decimal
            )

            else -> null
        }
    }
}