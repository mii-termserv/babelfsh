package de.mii_termserv.babelfsh.api.plugins

import com.xenomachina.argparser.ArgParser
import com.xenomachina.argparser.mainBody
import de.mii_termserv.babelfsh.antlr.service.babelfsh.comments.CommandLineParseException
import de.mii_termserv.babelfsh.antlr.service.babelfsh.comments.PluginCommentData
import de.mii_termserv.babelfsh.antlr.service.fsh.file.TsResourceData
import de.mii_termserv.babelfsh.api.BabelfshContext
import de.mii_termserv.babelfsh.api.BabelfshHelpFormatter
import de.mii_termserv.babelfsh.api.ResourceType
import de.mii_termserv.babelfsh.api.content_containers.ResourceContainer
import de.mii_termserv.babelfsh.api.propertymapping.PropertyMapperFactory
import de.mii_termserv.babelfsh.api.propertymapping.PropertyMappingException
import de.mii_termserv.babelfsh.readers.PropertyMappingEntry
import de.mii_termserv.babelfsh.utils.getTerminalWidth
import de.mii_termserv.babelfsh.utils.logging.debug
import de.mii_termserv.babelfsh.utils.logging.error
import kotlinx.serialization.SerializationException
import kotlinx.serialization.json.Json
import org.slf4j.LoggerFactory
import java.io.File
import java.nio.file.FileSystemException
import java.nio.file.Path

abstract class BabelfshTerminologyPlugin<ResourceContainerClass : ResourceContainer<*, *>, ResourceContentContainerClass : BabelfshConversionResult, ArgType : PluginArguments>(
    val pluginId: String,
    val babelfshContext: BabelfshContext,
    val resourceType: ResourceType,
    val attribution: String?,
    val extraHelp: String?,
    val shortHelp: String
) {

    private val logger = LoggerFactory.getLogger(javaClass)

    fun parseArgs(pluginCommentData: PluginCommentData, resource: TsResourceData): ArgType {
        return mainBody(programName = "BabelFSH $resourceType plugin '$pluginId'", columns = getTerminalWidth()) {
            val parser = ArgParser(pluginCommentData.pluginCommandLine.toTypedArray())
            try {
                logger.debug {
                    "Parsing plugin arguments of ${resource.resourceType} ${resource.identification} (defined: ${resource.pluginDefinedIn} with command line: ${pluginCommentData.pluginCommandLine}"
                }
                val args = parseInto(parser, resource.workingDirectory)
                args.validateDependentArguments()
                args
            } catch (e: Exception) {
                logger.error { "Error parsing plugin arguments of ${resource.resourceType} ${resource.identification}: ${e.message}" }
                resource.pluginDefinedIn?.let {
                    logger.error { "This plugin comment was originally defined within the FSH item '${it.resourceType}: ${it.resourceIdentification}' in ${it.filename} with the comment starting in line ${it.line}" }
                }
                logger.error { "The command line received was:\n    ${pluginCommentData.pluginCommandLine}" }
                val helpParser = ArgParser(args = arrayOf("--help"), helpFormatter = BabelfshHelpFormatter(this))
                parseInto(helpParser, resource.workingDirectory)
                throw e
            }
        }
    }

    abstract fun parseInto(parser: ArgParser, workingDirectory: Path?): ArgType

    abstract fun produceContent(
        args: PluginArguments, resource: ResourceContainerClass
    ): List<ResourceContentContainerClass>

    fun failConversion(s: String): Nothing {
        throw ConversionFailedException(s)
    }

    fun getHelp() {
        mainBody(columns = getTerminalWidth()) {
            val helpParser = ArgParser(
                args = arrayOf("--help"), helpFormatter = BabelfshHelpFormatter(this)
            )
            parseInto(helpParser, null)
        }

    }

}

class ConversionFailedException(s: String, recommendPluginImplementation: Boolean = false) : IllegalStateException(
    when (recommendPluginImplementation) {
        true -> "$s -- Your problem may require a specific plugin, don't hesitate to implement one!"
        else -> s
    }
)


abstract class PluginArguments(val parser: ArgParser, val workingDirectory: Path?) {

    private val logger = LoggerFactory.getLogger(javaClass)

    fun String.preprocess(): String = preprocessString(this)
    private fun preprocessString(string: String): String {
        return string.replace("'", "").trim('"').trim()
    }

    fun String.convertToAbsolutePath(): Path {
        val path = Path.of(this.preprocess())
        return validatePathDelegate(path).toAbsolutePath()
    }

    open fun validateDependentArguments() {
        // intentionally blank
    }

    fun failValidation(s: String): Nothing {
        throw CommandLineExpectationFailed(s)
    }

    protected fun jsonArrayToStringList(
        string: String, validation: ((String) -> Unit)? = {
            if (it.isBlank()) {
                failValidation("Data is blank")
            }
        }
    ): List<String> {
        val data = Json.decodeFromString<List<String>>(string).map {
            it.preprocess().trim()
        }
        data.forEach {
            validation?.invoke(it)
        }
        return data
    }

    fun String.makeList() = split(",").map { it.trim().trim('"') }.filter { it.isNotBlank() }.toList()

    fun jsonStringToStringMap(s: String, validation: ((Map<String, String>) -> Unit)? = null): Map<String, String> =
        Json.decodeFromString<Map<String, String>>(s).also {
            validation?.invoke(it)
        }

    private fun validatePathDelegate(path: Path): Path {
        val file = path.toFile()
        try {
            validateFileExistsAndIsReadable(file)
            return file.toPath().toAbsolutePath()
        } catch (e: Exception) {
            if (!file.isAbsolute) {
                val resolvedToWorkingDirectory = workingDirectory?.resolve(file.toPath()) ?: run {
                    failValidation("Could not resolve path $path as no working directory is set")
                }
                try {
                    validateFileExistsAndIsReadable(resolvedToWorkingDirectory.toFile())
                    logger.debug { "Resolved path $path to ${resolvedToWorkingDirectory.toAbsolutePath()}" }
                    return resolvedToWorkingDirectory.toAbsolutePath()
                } catch (e: Exception) {
                    failValidation(e.message ?: "Unknown error")
                }
            }
            failValidation(e.message ?: "Unknown error")
        }
    }

    private fun validateFileExistsAndIsReadable(file: File) {
        if (!file.exists()) {
            throw FileSystemException("File $file does not exist")
        }
        if (!file.isFile) {
            throw FileSystemException("File $file is not a file")
        }
        if (!file.canRead()) {
            throw FileSystemException("File $file is not readable")
        }
    }

    inline fun <reified T> decodeJsonArray(
        stringValue: String, argumentName: String, pluginName: String, validator: (T) -> Unit
    ): List<T> {
        val data = try {
            Json.decodeFromString<List<T>>(stringValue)
        } catch (e: SerializationException) {
            throw CommandLineParseException(
                "Could not parse '$argumentName' parameter for the $pluginName plugin due to: ${e.message}", null
            )
        }
        data.forEach { validator.invoke(it) }
        return data
    }

    fun propertyMappingFromString(stringValue: String, argumentName: String, pluginName: String) =
        decodeJsonArray<PropertyMappingEntry>(
            stringValue = stringValue,
            argumentName = argumentName,
            pluginName = pluginName
        ) { entry ->
            if (entry.column.isBlank() || entry.property.isBlank()) {
                failValidation("Property mappings need to have a non-empty column and property code. Found: $entry")
            }
            entry.mapper?.let {
                try {
                    PropertyMapperFactory.createPropertyMapper(it.id, it.arguments)
                } catch (e: PropertyMappingException) {
                    failValidation("Property mapper ${e.propertyMapper?.id}  was not configured correctly: ${e.message}")
                }
            }
        }

}


class CommandLineExpectationFailed(s: String) : IllegalArgumentException(s)

interface BabelfshConversionResult