package de.mii_termserv.babelfsh.api.plugins.cm

import com.xenomachina.argparser.ArgParser
import de.mii_termserv.babelfsh.api.BabelfshContext
import de.mii_termserv.babelfsh.api.ResourceType
import de.mii_termserv.babelfsh.api.content_containers.cm.ConceptMapContainer
import de.mii_termserv.babelfsh.api.content_containers.cm.ConceptMapEntryContainer
import de.mii_termserv.babelfsh.api.plugins.BabelfshTerminologyPlugin
import de.mii_termserv.babelfsh.api.plugins.CommandLineExpectationFailed
import de.mii_termserv.babelfsh.api.plugins.PluginArguments
import de.mii_termserv.babelfsh.utils.stripQuotes


abstract class ConceptMapPlugin<ArgType : PluginArguments>(
    pluginId: String,
    babelfshContext: BabelfshContext,
    shortHelp: String,
    extraHelp: String? = null,
    attribution: String? = null
) : BabelfshTerminologyPlugin<ConceptMapContainer, ConceptMapEntryContainer, ArgType>(
    pluginId = pluginId,
    babelfshContext = babelfshContext,
    resourceType = ResourceType.ConceptMap,
    shortHelp = shortHelp,
    extraHelp = extraHelp,
    attribution = attribution
)

enum class CmFhirCompatibility {
    R4, R5, BOTH
}

fun PluginArguments.fhirCompatibilityArg(): ArgParser.DelegateProvider<CmFhirCompatibility> = parser.storing(
    "--fhir-compatibility"
) {
    try {
        if (this.uppercase().stripQuotes() == "R4B") {
            return@storing CmFhirCompatibility.R4
        }
        return@storing CmFhirCompatibility.valueOf(this.uppercase())
    } catch (e: IllegalArgumentException) {
        throw CommandLineExpectationFailed("Invalid FHIR compatibility value: $this")
    }
}