package de.mii_termserv.babelfsh.api

@Target(AnnotationTarget.FIELD)
@Retention(AnnotationRetention.RUNTIME)
@MustBeDocumented
annotation class FhirVersion(vararg val value: BabelfshContext.ReleaseVersion)