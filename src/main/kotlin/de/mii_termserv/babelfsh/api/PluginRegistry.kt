package de.mii_termserv.babelfsh.api

import de.mii_termserv.babelfsh.api.plugins.BabelfshTerminologyPlugin
import de.mii_termserv.babelfsh.api.plugins.cm.ConceptMapPlugin
import de.mii_termserv.babelfsh.api.plugins.cs.CodeSystemPlugin
import de.mii_termserv.babelfsh.api.plugins.vs.ValueSetPlugin
import de.mii_termserv.babelfsh.plugins.core.claml.ClamlBfarmCodeSystemPlugin
import de.mii_termserv.babelfsh.plugins.core.claml.StandardClamlCodeSystemPlugin
import de.mii_termserv.babelfsh.plugins.core.csv.CsvCodeSystemPlugin
import de.mii_termserv.babelfsh.plugins.core.csv.CsvConceptMapPlugin
import de.mii_termserv.babelfsh.plugins.core.excel.GenericExcelCodeSystemPlugin
import de.mii_termserv.babelfsh.plugins.core.owl.OwlPlugin
import de.mii_termserv.babelfsh.plugins.core.xslt.GenericXsltCodeSystemPlugin
import de.mii_termserv.babelfsh.plugins.specific.atc_ddd.AtcDddCodeSystemPlugin
import de.mii_termserv.babelfsh.plugins.specific.edqm.EdqmCodeSystemPlugin
import de.mii_termserv.babelfsh.plugins.specific.iso_country_codes.IsoCountryCodeCodeSystemPlugin
import de.mii_termserv.babelfsh.plugins.specific.orphacodes.OrphaCodesCodeSystemPlugin
import de.mii_termserv.babelfsh.plugins.specific.unii.UniiCodeSystemPlugin
import de.mii_termserv.babelfsh.utils.getTerminalWidth
import de.vandermeer.asciitable.AsciiTable
import de.vandermeer.asciitable.CWC_FixedWidth
import de.vandermeer.skb.interfaces.transformers.textformat.TextAlignment

abstract class PluginRegistry<T> {
    fun hasPluginId(id: String) = plugins.containsKey(id)

    private val plugins = mutableMapOf<String, () -> T>()

    fun register(pluginId: String, plugin: () -> T) {
        plugins[pluginId] = plugin
    }

    fun getPluginConstructor(id: String): (() -> T)? {
        return plugins[id]
    }

    fun printAllPluginIds(): String {
        return plugins.keys.joinToString(", ") { "'$it'" }
    }

    fun generateHelpTable(): String {
        val terminalWidth = getTerminalWidth()
        val pluginIdColumnLength = plugins.keys.maxOf { it.length } + 2 // +2 for padding
        val descriptionColumnLength = terminalWidth - pluginIdColumnLength - 3 // -3 for the rules

        val table = AsciiTable().apply {
            context.apply {
                renderer.apply {
                    cwc = CWC_FixedWidth().add(pluginIdColumnLength).add(descriptionColumnLength)
                }
            }
            addRule()
            addRow("Plugin ID".uppercase(), "Description".uppercase()).apply {
                cells[0].context.apply {
                    textAlignment = TextAlignment.CENTER
                }
                cells.onEach { it.context.setPaddingLeftRight(1) }
            }
            addRule()
        }
        plugins.toSortedMap().forEach { (id, plugin) ->
            table.addRow(id, (plugin() as BabelfshTerminologyPlugin<*, *, *>).shortHelp).apply {
                cells[0].context.apply {
                    textAlignment = TextAlignment.CENTER
                }
                cells[1].context.apply {
                    textAlignment = TextAlignment.LEFT
                }
                cells.onEach { it.context.setPaddingLeftRight(1) }
            }
            table.addRule()
        }
        return table.render()
    }
}

object CodeSystemPluginRegistry : PluginRegistry<CodeSystemPlugin<*>>() {
    fun registerAll(babelfshContext: BabelfshContext) {
        register("csv") { CsvCodeSystemPlugin(babelfshContext) }
        register("excel") { GenericExcelCodeSystemPlugin(babelfshContext) }
        register("xslt") { GenericXsltCodeSystemPlugin(babelfshContext) }
        register("owl") { OwlPlugin(babelfshContext) }
        register("claml-standard") { StandardClamlCodeSystemPlugin(babelfshContext) }
        register("claml-bfarm") { ClamlBfarmCodeSystemPlugin(babelfshContext) }
        register("atc-ddd") { AtcDddCodeSystemPlugin(babelfshContext) }
        register("edqm") { EdqmCodeSystemPlugin(babelfshContext) }
        register("unii") { UniiCodeSystemPlugin(babelfshContext) }
        register("iso-country-codes") { IsoCountryCodeCodeSystemPlugin(babelfshContext) }
        register("orphacodes") { OrphaCodesCodeSystemPlugin(babelfshContext) }
    }
}

@Suppress("unused", "UNUSED_PARAMETER")
object ValueSetPluginRegistry : PluginRegistry<ValueSetPlugin<*>>() {
    fun registerAll(babelfshContext: BabelfshContext) {
        //TODO()
    }
}

object ConceptMapPluginRegistry : PluginRegistry<ConceptMapPlugin<*>>() {
    fun registerAll(babelfshContext: BabelfshContext) {
        register("csv") { CsvConceptMapPlugin(babelfshContext) }
    }
}