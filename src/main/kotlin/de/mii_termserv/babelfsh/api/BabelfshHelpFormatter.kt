// Copyright © 2016 Laurence Gonsalves
//
// This file is adapted from kotlin-argparser, a library which can be found at
// http://github.com/xenomachina/kotlin-argparser
//
// This library is free software; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as published by the
// Free Software Foundation; either version 2.1 of the License, or (at your
// option) any later version.
//
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
// for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, see http://www.gnu.org/licenses/

// The original file is located at: https://github.com/xenomachina/kotlin-argparser/blob/master/src/main/kotlin/com/xenomachina/argparser/DefaultHelpFormatter.kt
// Relevant changes:
//   - the long argument is preferred if provided (accomplished by sorting the usages by length and taking the first one)
//   - positional arguments are forbidden
//   - minor code cleanup


package de.mii_termserv.babelfsh.api

import com.xenomachina.argparser.HelpFormatter
import com.xenomachina.text.NBSP_CODEPOINT
import com.xenomachina.text.term.codePointWidth
import com.xenomachina.text.term.columnize
import com.xenomachina.text.term.wrapText
import de.mii_termserv.babelfsh.antlr.service.babelfsh.comments.CommandLineParseException
import de.mii_termserv.babelfsh.api.plugins.BabelfshTerminologyPlugin

class BabelfshHelpFormatter(
    private val plugin: BabelfshTerminologyPlugin<*, *, *>
) : HelpFormatter {
    private val indent = "  "
    private val indentWidth = indent.codePointWidth()
    private val usagePrefix = "Usage:"
    override fun format(
        programName: String?,
        columns: Int,
        values: List<HelpFormatter.Value>
    ): String {
        val effectiveColumns = when {
            columns < 0 -> throw IllegalArgumentException("columns must be non-negative")
            columns == 0 -> Int.MAX_VALUE
            else -> columns
        }

        val prologue = buildString {
            appendLine("BabelFSH Plugin help for ${plugin.resourceType} plugin '${plugin.pluginId}':".wrapText(effectiveColumns))
            append("\n")
            appendLine("**${plugin.shortHelp}**".wrapText(effectiveColumns))

            plugin.attribution?.let { attribution ->
                append("\n")
                append(attribution.wrapText(effectiveColumns))
                append("\n\n")
            } ?: run {
                append("\n")
            }
            appendUsage(this, effectiveColumns, programName, values)
            append("\n")
            plugin.extraHelp?.let { pluginHelp ->
                append("\n")
                append(pluginHelp.wrapText(effectiveColumns))
                append("\n\n")
            }

        }
        return buildString {

            append(prologue)

            val required = values.filter { it.isRequired }
            val optional = values.filterNot { it.isRequired or it.isPositional }
            values.filter { it.isPositional }.let {
                if (it.isNotEmpty()) {
                    throw CommandLineParseException("Positional arguments are not supported by BabelFSH plugins.", null)
                }
            }

            val usageColumns = 2 * indentWidth - 1 + when (columns) {
                0 -> {
                    values.maxOfOrNull { usageText(it).length } ?: 0
                }

                else -> {
                    // Make left column as narrow as possible without wrapping any of the individual usages, though no wider than
                    // half the screen.
                    values.maxOfOrNull { value -> usageText(value).split(" ").maxOfOrNull { it.length } ?: 0 } ?: 0
                        .coerceAtMost(effectiveColumns / 2)
                }
            }

            appendSection(this, usageColumns, effectiveColumns, "required", required)
            appendSection(this, usageColumns, effectiveColumns, "optional", optional)
        }

    }

    private fun appendSection(
        sb: StringBuilder,
        usageColumns: Int,
        columns: Int,
        name: String,
        values: List<HelpFormatter.Value>
    ) {
        if (values.isNotEmpty()) {
            sb.append("$name arguments:\n")
            for (value in values) {
                val left = usageText(value).wrapText(usageColumns - indentWidth).prependIndent(indent)
                val right = value.help.wrapText(columns - usageColumns - 2 * indentWidth).prependIndent(indent)
                sb.append(columnize(left, right, minWidths = intArrayOf(usageColumns)))
                sb.append("\n\n")
            }
        }
    }

    private fun usageText(value: HelpFormatter.Value) =
        value.usages.sortedByDescending { it.length }.joinToString(", ") { it.replace(' ', '\u00a0') }

    private fun appendUsage(sb: StringBuilder, columns: Int, programName: String?, values: List<HelpFormatter.Value>) {
        var usageStart = usagePrefix + (if (programName != null) " $programName" else "")

        val valueSB = StringBuilder()
        for (value in values) value.run {
            if (usages.isNotEmpty()) {
                val theUsage = usages.associateWith { it.length }.maxBy { it.value }.key
                val usage = theUsage.replace(' ', NBSP_CODEPOINT.toChar())
                if (isRequired) {
                    valueSB.append(" $usage")
                } else {
                    valueSB.append(" [$usage]")
                }
                if (isRepeating) {
                    valueSB.append("...")
                }
            }
        }

        if (usageStart.length > columns / 2) {
            sb.append(usageStart)
            sb.append("\n")
            val valueIndent = ("$usagePrefix $indent").codePointWidth()
            val valueColumns = columns - valueIndent
            sb.append(valueSB.toString().wrapText(valueColumns).prependIndent(" ".repeat(valueIndent)))
        } else {
            usageStart += " "
            val valueColumns = columns - usageStart.length
            sb.append(columnize(usageStart, valueSB.toString().wrapText(valueColumns)))
        }
    }
}