package de.mii_termserv.babelfsh.api

enum class ResourceType {
    CodeSystem,
    ValueSet,
    RuleSet,
    ConceptMap
}