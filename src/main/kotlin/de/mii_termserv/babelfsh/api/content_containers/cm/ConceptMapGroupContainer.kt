package de.mii_termserv.babelfsh.api.content_containers.cm

import de.mii_termserv.babelfsh.api.content_containers.SerializableToFhir
import org.hl7.fhir.r4b.model.ConceptMap
import org.hl7.fhir.r5.model.CanonicalType
import org.hl7.fhir.r5.model.Enumerations
import org.hl7.fhir.r4b.model.ConceptMap as R4BCm
import org.hl7.fhir.r5.model.ConceptMap as R5Cm

data class ConceptMapGroupContainer(
    val sourceCanonical: Canonical,
    val targetCanonical: Canonical,
) : SerializableToFhir<R4BCm.ConceptMapGroupComponent, R5Cm.ConceptMapGroupComponent> {
    private val _elements: MutableList<ConceptMapGroupElementContainer> = mutableListOf()

    fun addElement(value: ConceptMapEntryContainer) {
        val matchingElement = _elements.find {
            it.sourceCode == value.sourceCode && it.sourceDisplay == value.sourceDisplay
        } ?: run {
            val newElement = ConceptMapGroupElementContainer(value.sourceCode, value.sourceDisplay)
            _elements.add(newElement)
            newElement
        }
        matchingElement.addTarget(
            ConceptMapGroupElementTargetContainer(
                value.targetCode,
                value.targetDisplay,
                value.equivalenceR4,
                value.relationshipR5,
                value.targetComment
            )
        )

    }

    override fun toR4B(): R4BCm.ConceptMapGroupComponent {
        return R4BCm.ConceptMapGroupComponent().apply {
            this.source = sourceCanonical.url
            this.sourceVersion = sourceCanonical.version
            this.target = targetCanonical.url
            this.targetVersion = targetCanonical.version

            this@ConceptMapGroupContainer._elements.forEach {
                this.addElement(it.toR4B())
            }
        }

    }

    override fun toR5(): R5Cm.ConceptMapGroupComponent {
        return R5Cm.ConceptMapGroupComponent().apply {
            this.sourceElement = CanonicalType(buildString {
                append(sourceCanonical.url)
                sourceCanonical.version?.let { append("|$it") }
            })
            this.targetElement = CanonicalType(buildString {
                append(targetCanonical.url)
                targetCanonical.version?.let { append("|$it") }
            })

            this@ConceptMapGroupContainer._elements.forEach {
                this.addElement(it.toR5())
            }
        }

    }
}

data class ConceptMapGroupElementContainer(
    val sourceCode: String?,
    val sourceDisplay: String?,
) : SerializableToFhir<R4BCm.SourceElementComponent, R5Cm.SourceElementComponent> {

    fun addTarget(conceptMapGroupElementTargetContainer: ConceptMapGroupElementTargetContainer) {
        this._target.add(conceptMapGroupElementTargetContainer)
    }

    private val _target: MutableList<ConceptMapGroupElementTargetContainer> = mutableListOf()
    val target get() = _target as List<ConceptMapGroupElementTargetContainer>
    override fun toR4B(): ConceptMap.SourceElementComponent = R4BCm.SourceElementComponent().apply {
        this.code = sourceCode
        this.display = sourceDisplay
        this@ConceptMapGroupElementContainer.target.forEach {
            this.addTarget(it.toR4B())
        }
    }

    override fun toR5(): org.hl7.fhir.r5.model.ConceptMap.SourceElementComponent {
        return R5Cm.SourceElementComponent().apply {
            this.code = sourceCode
            this.display = sourceDisplay
            this.noMap = _target.isEmpty()
            this@ConceptMapGroupElementContainer.target.forEach {
                this.addTarget(it.toR5())
            }
        }
    }
}

data class ConceptMapGroupElementTargetContainer(
    val targetCode: String?,
    val targetDisplay: String?,
    val equivalenceR4: R4BCm.ConceptMapEquivalence?,
    val relationshipR5: Enumerations.ConceptMapRelationship?,
    val targetComment: String?,
) : SerializableToFhir<R4BCm.TargetElementComponent, R5Cm.TargetElementComponent> {

    override fun toR4B(): R4BCm.TargetElementComponent = R4BCm.TargetElementComponent().apply {
        this.code = targetCode
        this.display = targetDisplay
        this.equivalence = equivalenceR4
        this.comment = comment
    }

    override fun toR5(): R5Cm.TargetElementComponent = R5Cm.TargetElementComponent().apply {
        this.code = targetCode
        this.display = targetDisplay
        this.relationship = relationshipR5
        this.comment = targetComment
    }
}