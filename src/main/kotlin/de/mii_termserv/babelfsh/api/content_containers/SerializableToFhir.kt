package de.mii_termserv.babelfsh.api.content_containers


interface SerializableToFhir<R4B, R5> {
    fun toR4B(): R4B
    fun toR5(): R5
}