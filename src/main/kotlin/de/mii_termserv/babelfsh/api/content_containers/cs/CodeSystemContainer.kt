package de.mii_termserv.babelfsh.api.content_containers.cs

import ca.uhn.fhir.parser.IParser
import de.mii_termserv.babelfsh.api.BabelfshContext
import de.mii_termserv.babelfsh.api.ResourceType
import de.mii_termserv.babelfsh.api.content_containers.PropertyContainer
import de.mii_termserv.babelfsh.api.content_containers.ResourceContainer
import kotlinx.serialization.json.*
import org.hl7.fhir.r4b.model.CodeSystem


class CodeSystemContainer(
    json: JsonObject
) : ResourceContainer<CodeSystem, org.hl7.fhir.r5.model.CodeSystem>(json, ResourceType.CodeSystem) {

    private val _concepts = mutableListOf<CodeSystemConceptContainer>()
    private val codeSet = mutableSetOf<String>()
    val concepts get() = _concepts as List<CodeSystemConceptContainer>

    override val identification get() = "CodeSystem ${json["name"]?.toString()}"

    override fun toR4B(babelfshContext: BabelfshContext, parser: IParser): CodeSystem =
        parser.parseResource(CodeSystem::class.java, jsonString).apply {
            concept.addAll(concepts.map { it.toR4B() })
            count = concepts.size
        }

    override fun toR5(babelfshContext: BabelfshContext, parser: IParser): org.hl7.fhir.r5.model.CodeSystem =
        parser.parseResource(org.hl7.fhir.r5.model.CodeSystem::class.java, jsonString).apply {
            concept.addAll(concepts.map { it.toR5() })
            count = concepts.size
        }

    fun getProperties(): List<PropertyContainer> {
        val props = json.getOrDefault("property", buildJsonArray {  }) as JsonArray
        return props.map {
            Json.decodeFromJsonElement(it)
        }
    }

    fun addConcept(value: CodeSystemConceptContainer) {
        if (codeSet.contains(value.code)) {
            throw IllegalStateException("Code ${value.code} already exists in CodeSystem ${json["name"].toString()}")
        }
        _concepts.add(value)
        codeSet.add(value.code)
    }
}
