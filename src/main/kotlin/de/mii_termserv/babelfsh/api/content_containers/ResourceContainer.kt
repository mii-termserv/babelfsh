package de.mii_termserv.babelfsh.api.content_containers


import ca.uhn.fhir.parser.DataFormatException
import ca.uhn.fhir.parser.IParser
import de.mii_termserv.babelfsh.api.BabelfshContext
import de.mii_termserv.babelfsh.api.ResourceType
import kotlinx.serialization.Serializable
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.*
import org.hl7.fhir.r4b.model.CodeSystem.PropertyComponent as R4BPropertyComponent
import org.hl7.fhir.r4b.model.CodeSystem.PropertyType as R4BPropertyType
import org.hl7.fhir.r5.model.CodeSystem.PropertyComponent as R5PropertyComponent
import org.hl7.fhir.r5.model.CodeSystem.PropertyType as R5PropertyType

abstract class ResourceContainer<R4BClass, R5Class>(
    protected val json: JsonObject,
    val resourceType: ResourceType
) {

    @Throws(DataFormatException::class)
    abstract fun toR4B(babelfshContext: BabelfshContext, parser: IParser): R4BClass

    @Throws(DataFormatException::class)
    abstract fun toR5(babelfshContext: BabelfshContext, parser: IParser): R5Class

    protected val jsonString = Json.encodeToString(json)

    abstract val identification: String
}

@Serializable
data class PropertyContainer(
    val code: String,
    val uri: String? = null,
    val description: String? = null,
    val type: PropertyTypeEnum
) : SerializableToFhir<R4BPropertyComponent, R5PropertyComponent> {
    override fun toR4B(): R4BPropertyComponent {
        return R4BPropertyComponent().apply {
            code = this@PropertyContainer.code
            uri = this@PropertyContainer.uri
            description = this@PropertyContainer.description
            type = R4BPropertyType.fromCode(this@PropertyContainer.type.name)
        }
    }

    override fun toR5(): R5PropertyComponent {
        return R5PropertyComponent().apply {
            code = this@PropertyContainer.code
            uri = this@PropertyContainer.uri
            description = this@PropertyContainer.description
            type = R5PropertyType.fromCode(this@PropertyContainer.type.name)
        }
    }

    @Suppress("EnumEntryName")
    enum class PropertyTypeEnum {
        code,
        Coding,
        string,
        integer,
        boolean,
        dateTime,
        decimal
    }
}