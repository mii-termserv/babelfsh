package de.mii_termserv.babelfsh.api.content_containers.cm

import ca.uhn.fhir.parser.IParser
import de.mii_termserv.babelfsh.api.BabelfshContext
import de.mii_termserv.babelfsh.api.ResourceType
import de.mii_termserv.babelfsh.api.content_containers.ResourceContainer
import kotlinx.serialization.json.JsonObject
import org.hl7.fhir.r4b.model.ConceptMap as R4BCm
import org.hl7.fhir.r5.model.ConceptMap as R5Cm


class ConceptMapContainer(
    json: JsonObject
) : ResourceContainer<R4BCm, R5Cm>(json, ResourceType.ConceptMap) {

    private val _groups = mutableListOf<ConceptMapGroupContainer>()
    val groups get() = _groups as List<ConceptMapGroupContainer>

    override fun toR4B(babelfshContext: BabelfshContext, parser: IParser): R4BCm {
        return parser.parseResource(R4BCm::class.java, jsonString).apply {
            group.addAll(groups.map { it.toR4B() })
        }
    }

    override fun toR5(babelfshContext: BabelfshContext, parser: IParser): R5Cm {
        return parser.parseResource(R5Cm::class.java, jsonString).apply {
            group.addAll(groups.map { it.toR5() })
        }
    }

    override val identification: String
        get() = "ConceptMap ${json["name"]?.toString()}"

    fun addElement(value: ConceptMapEntryContainer) {
        val matchingGroup = _groups.find {
            it.sourceCanonical == value.sourceCanonical && it.targetCanonical == value.targetCanonical
        } ?: run {
            val newGroup = ConceptMapGroupContainer(value.sourceCanonical, value.targetCanonical)
            _groups.add(newGroup)
            newGroup
        }
        matchingGroup.addElement(value)
    }
}
