package de.mii_termserv.babelfsh.api.content_containers.cm

import de.mii_termserv.babelfsh.api.plugins.BabelfshConversionResult
import org.hl7.fhir.r5.model.Enumerations.ConceptMapRelationship
import org.hl7.fhir.r4b.model.ConceptMap as R4BCm

/**
 * The elements are represented by groups internally.
 * Combining the groups is done during serialization of the entire CM to JSON
 */
data class ConceptMapEntryContainer(
    val sourceCanonical: Canonical,
    val targetCanonical: Canonical,
    val sourceCode: String? = null,
    val sourceDisplay: String? = null,
    val targetCode: String? = null,
    val targetDisplay: String? = null,
    val equivalenceR4: R4BCm.ConceptMapEquivalence? = null,
    val relationshipR5: ConceptMapRelationship? = null,
    val targetComment: String? = null
    // group.unmapped SHALL be provided in FSH, not in the source
) : BabelfshConversionResult


data class Canonical(
    val url: String,
    val version: String? = null
)