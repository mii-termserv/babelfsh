package de.mii_termserv.babelfsh.api.content_containers.vs

import ca.uhn.fhir.parser.IParser
import de.mii_termserv.babelfsh.api.BabelfshContext
import de.mii_termserv.babelfsh.api.ResourceType
import de.mii_termserv.babelfsh.api.content_containers.ResourceContainer
import kotlinx.serialization.json.JsonObject
import org.hl7.fhir.r4b.model.ValueSet


class ValueSetContainer(
    json: JsonObject
) : ResourceContainer<ValueSet, org.hl7.fhir.r5.model.ValueSet>(json, ResourceType.ValueSet) {

    override val identification get() = "ValueSet ${json["name"]?.toString()}"

    override fun toR4B(babelfshContext: BabelfshContext, parser: IParser): ValueSet = parser.parseResource(ValueSet::class.java, jsonString)

    override fun toR5(babelfshContext: BabelfshContext, parser: IParser): org.hl7.fhir.r5.model.ValueSet =
        parser.parseResource(org.hl7.fhir.r5.model.ValueSet::class.java, jsonString)

}