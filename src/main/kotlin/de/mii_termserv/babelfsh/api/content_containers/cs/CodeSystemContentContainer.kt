package de.mii_termserv.babelfsh.api.content_containers.cs

import de.mii_termserv.babelfsh.api.BabelfshContext
import de.mii_termserv.babelfsh.api.FhirVersion
import de.mii_termserv.babelfsh.api.content_containers.PropertyContainer
import de.mii_termserv.babelfsh.api.content_containers.SerializableToFhir
import de.mii_termserv.babelfsh.api.plugins.BabelfshConversionResult
import java.time.format.DateTimeFormatter
import org.hl7.fhir.r4b.model.BaseDateTimeType as R4BBaseDateTimeType
import org.hl7.fhir.r4b.model.BooleanType as R4BBooleanType
import org.hl7.fhir.r4b.model.CodeSystem.ConceptDefinitionComponent as R4BCSConcept
import org.hl7.fhir.r4b.model.CodeSystem.ConceptDefinitionDesignationComponent as R4BDesignation
import org.hl7.fhir.r4b.model.CodeSystem.ConceptPropertyComponent as R4BPropertyComponent
import org.hl7.fhir.r4b.model.CodeType as R4BCodeType
import org.hl7.fhir.r4b.model.Coding as R4BCoding
import org.hl7.fhir.r4b.model.DataType as R4BDataType
import org.hl7.fhir.r4b.model.DateTimeType as R4BDateTimeType
import org.hl7.fhir.r4b.model.DateType as R4BDateType
import org.hl7.fhir.r4b.model.DecimalType as R4bDecimalType
import org.hl7.fhir.r4b.model.IntegerType as R4BIntegerType
import org.hl7.fhir.r4b.model.StringType as R4BStringType
import org.hl7.fhir.r5.model.BaseDateTimeType as R5BaseDateTimeType
import org.hl7.fhir.r5.model.BooleanType as R5BooleanType
import org.hl7.fhir.r5.model.CodeSystem.ConceptDefinitionComponent as R5CSConcept
import org.hl7.fhir.r5.model.CodeSystem.ConceptDefinitionDesignationComponent as R5Designation
import org.hl7.fhir.r5.model.CodeSystem.ConceptPropertyComponent as R5PropertyComponent
import org.hl7.fhir.r5.model.CodeType as R5CodeType
import org.hl7.fhir.r5.model.Coding as R5Coding
import org.hl7.fhir.r5.model.DataType as R5DataType
import org.hl7.fhir.r5.model.DateTimeType as R5DateTimeType
import org.hl7.fhir.r5.model.DecimalType as R5DecimalType
import org.hl7.fhir.r5.model.IntegerType as R5IntegerType
import org.hl7.fhir.r5.model.StringType as R5StringType


data class CodeSystemConceptContainer(
    val code: String,
    val display: String?,
    val definition: String?,
    val designation: List<DesignationContainer> = listOf(),
    val property: List<ConceptPropertyContainer> = listOf(),
) : BabelfshConversionResult, SerializableToFhir<R4BCSConcept, R5CSConcept> {

    override fun toR5(): R5CSConcept = R5CSConcept(code).apply {
        display = this@CodeSystemConceptContainer.display
        definition = this@CodeSystemConceptContainer.definition
        this@CodeSystemConceptContainer.designation.forEach {
            addDesignation(it.toR5())
        }
        this@CodeSystemConceptContainer.property.forEach {
            addProperty(it.toR5())
        }
    }

    override fun toR4B(): R4BCSConcept = R4BCSConcept(code).apply {
        display = this@CodeSystemConceptContainer.display
        definition = this@CodeSystemConceptContainer.definition
        this@CodeSystemConceptContainer.designation.forEach { designation ->
            addDesignation(designation.toR4B())
        }
        this@CodeSystemConceptContainer.property.forEach { property ->
            addProperty(property.toR4B())
        }
    }

}

data class DesignationContainer(
    val language: String?,
    val use: PropertyValueType.CodingContainer?,
    val value: String,
    @FhirVersion(BabelfshContext.ReleaseVersion.R5)
    val additionalUse: List<PropertyValueType.CodingContainer> = listOf(),
) : SerializableToFhir<R4BDesignation, R5Designation> {
    override fun toR4B(): R4BDesignation {
        return R4BDesignation().apply {
            this.language = this@DesignationContainer.language
            this.use = this@DesignationContainer.use?.toR4B()
            this.value = this@DesignationContainer.value
        }
    }

    override fun toR5(): R5Designation {
        return R5Designation().apply {
            this.language = this@DesignationContainer.language
            this.use = this@DesignationContainer.use?.toR5()
            this.value = this@DesignationContainer.value
            this@DesignationContainer.additionalUse.forEach {
                addAdditionalUse(it.toR5())
            }
        }

    }
}

data class ConceptPropertyContainer(
    val code: String,
    val valueType: PropertyValueType<*, *>
) : SerializableToFhir<R4BPropertyComponent, R5PropertyComponent> {
    override fun toR4B(): R4BPropertyComponent {
        return R4BPropertyComponent().apply {
            this.code = this@ConceptPropertyContainer.code
            this.value = this@ConceptPropertyContainer.valueType.toR4B()
        }
    }

    override fun toR5(): R5PropertyComponent {
        return R5PropertyComponent().apply {
            this.code = this@ConceptPropertyContainer.code
            this.value = this@ConceptPropertyContainer.valueType.toR5()
        }
    }
}

sealed class PropertyValueType<R4 : R4BDataType, R5 : R5DataType> : SerializableToFhir<R4, R5> {

    abstract val propertyType: PropertyContainer.PropertyTypeEnum

    data class CodeContainer(val value: String) : PropertyValueType<R4BCodeType, R5CodeType>() {
        override fun toR4B(): R4BCodeType {
            return R4BCodeType(value)
        }

        override fun toR5(): R5CodeType {
            return R5CodeType(value)
        }

        override val propertyType: PropertyContainer.PropertyTypeEnum
            get() = PropertyContainer.PropertyTypeEnum.code
    }

    data class CodingContainer(
        val system: String?,
        val version: String?,
        val code: String?,
        val display: String?,
        val userSelected: Boolean?
    ) : PropertyValueType<R4BCoding, R5Coding>() {
        override fun toR5() = R5Coding(system, code, display).apply {
            this.version = this@CodingContainer.version
            this@CodingContainer.userSelected?.let { this.userSelected = it }
        }

        override fun toR4B(): org.hl7.fhir.r4b.model.Coding {
            return R4BCoding(system, code, display).apply {
                this.version = this@CodingContainer.version
                this@CodingContainer.userSelected?.let { this.userSelected = it }
            }
        }

        override val propertyType: PropertyContainer.PropertyTypeEnum
            get() = PropertyContainer.PropertyTypeEnum.Coding
    }

    data class StringContainer(val value: String) : PropertyValueType<R4BStringType, R5StringType>() {
        override fun toR4B(): R4BStringType {
            return R4BStringType(value)
        }

        override fun toR5(): R5StringType {
            return R5StringType(value)
        }

        override val propertyType: PropertyContainer.PropertyTypeEnum
            get() = PropertyContainer.PropertyTypeEnum.string
    }

    data class IntegerContainer(val value: Int) : PropertyValueType<R4BIntegerType, R5IntegerType>() {
        override fun toR4B(): R4BIntegerType = R4BIntegerType(value)

        override fun toR5(): R5IntegerType = R5IntegerType(value)

        override val propertyType: PropertyContainer.PropertyTypeEnum
            get() = PropertyContainer.PropertyTypeEnum.integer
    }

    data class BooleanContainer(val value: Boolean) : PropertyValueType<R4BBooleanType, R5BooleanType>() {
        override fun toR4B(): R4BBooleanType = R4BBooleanType(value)

        override fun toR5(): R5BooleanType = R5BooleanType(value)

        override val propertyType: PropertyContainer.PropertyTypeEnum
            get() = PropertyContainer.PropertyTypeEnum.boolean
    }

    data class DateTimeContainer(val value: String) : PropertyValueType<R4BBaseDateTimeType, R5BaseDateTimeType>() {
        private val fhirDateRegex = Regex("([0-9]([0-9]([0-9][1-9]|[1-9]0)|[1-9]00)|[1-9]000)(-(0[1-9]|1[0-2])(-(0[1-9]|[1-2][0-9]|3[0-1]))?)?")
        @Suppress("RegExpSingleCharAlternation")
        private val fhirDateTimeRegex = Regex("([0-9]([0-9]([0-9][1-9]|[1-9]0)|[1-9]00)|[1-9]000)(-(0[1-9]|1[0-2])(-(0[1-9]|[1-2][0-9]|3[0-1])" +
                "(T([01][0-9]|2[0-3]):[0-5][0-9]:([0-5][0-9]|60)(\\.[0-9]{1,9})?)?)?(Z|(\\+|-)((0[0-9]|1[0-3]):[0-5][0-9]|14:00)?)?)?")

        override fun toR4B(): R4BBaseDateTimeType = when {
            value.matches(fhirDateRegex) -> R4BDateType(value)
            value.matches(fhirDateTimeRegex) -> R4BDateTimeType(value)
            else -> throw IllegalArgumentException("Invalid date format: $value")
        }

        override fun toR5(): R5DateTimeType = when {
            value.matches(fhirDateRegex) -> R5DateTimeType(value)
            value.matches(fhirDateTimeRegex) -> R5DateTimeType(value)
            else -> throw IllegalArgumentException("Invalid date format: $value")
        }

        companion object {
            val formatterWithSecondsAtZulu: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'")
            @Suppress("unused")
            val formatterOnlyDate: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd")
        }

        override val propertyType: PropertyContainer.PropertyTypeEnum
            get() = PropertyContainer.PropertyTypeEnum.dateTime
    }

    data class DecimalContainer(val value: Double) : PropertyValueType<R4bDecimalType, R5DecimalType>() {
        override fun toR4B(): R4bDecimalType = R4bDecimalType(value)

        override fun toR5(): R5DecimalType = R5DecimalType(value)

        override val propertyType: PropertyContainer.PropertyTypeEnum
            get() = PropertyContainer.PropertyTypeEnum.decimal
    }

}