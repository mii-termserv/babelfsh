package de.mii_termserv.babelfsh.api

import ca.uhn.fhir.context.FhirContext
import com.sksamuel.hoplite.ConfigLoaderBuilder
import com.sksamuel.hoplite.addResourceOrFileSource
import de.mii_termserv.babelfsh.configuration_settings.ConfigurationSettings
import kotlinx.serialization.json.Json
import org.slf4j.LoggerFactory
import java.io.File

class BabelfshContext(
    val r4Context: FhirContext,
    val r5Context: FhirContext,
    val releaseVersion: ReleaseVersion = ReleaseVersion.R4B,
    val jsonPrettyPrint: Boolean,
    configurationSettingsSource: File? = null
) {

    private val logger = LoggerFactory.getLogger(javaClass)

    enum class ReleaseVersion {
        R4B, R5
    }

    val configurationSettings = configurationSettingsSource?.let {
        logger.debug("Loading configuration settings from file: ${it.absolutePath}")
        ConfigLoaderBuilder.default()
            .addResourceOrFileSource(it.absolutePath)
            .build()
            .loadConfigOrThrow<ConfigurationSettings>()
    } ?: ConfigurationSettings()

    val json = Json {
        prettyPrint = jsonPrettyPrint
    }
}