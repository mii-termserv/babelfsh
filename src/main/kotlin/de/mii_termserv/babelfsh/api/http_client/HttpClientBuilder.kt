package de.mii_termserv.babelfsh.api.http_client

import de.mii_termserv.babelfsh.configuration_settings.HttpSettings
import de.mii_termserv.babelfsh.utils.logging.debug
import de.mii_termserv.babelfsh.utils.logging.info
import io.ktor.client.*
import io.ktor.client.engine.*
import io.ktor.client.engine.cio.*
import io.ktor.client.plugins.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.utils.io.jvm.javaio.*
import org.slf4j.Logger
import java.net.URL
import java.nio.file.Path
import java.security.cert.X509Certificate
import javax.net.ssl.X509TrustManager
import kotlin.io.path.createTempFile
import kotlin.io.path.outputStream
import kotlin.time.Duration.Companion.seconds

class FileDownloadHelper {
    companion object {

        private val downloadMap = mutableMapOf<String,Path>()

        private suspend fun HttpClient.downloadToTempFile(url: URL, prefix: String? = null, suffix: String? = null, builder: (HttpRequestBuilder.() -> Unit)? = null): Path {
            val outputFile = createTempFile(prefix, suffix)
            this.prepareGet(url = url) {
                builder?.invoke(this)
            }.execute {
                it.bodyAsChannel().toInputStream().copyTo(outputFile.outputStream())
            }
            return outputFile
        }

        suspend fun downloadFileIfNecessary(pathToReadFrom: Path?, url: URL?, log: Logger): Path {
            return when {
                pathToReadFrom != null && url != null -> throw IllegalStateException("Both pathToReadFrom and url are set")
                pathToReadFrom != null -> pathToReadFrom
                url != null -> {
                    if (downloadMap.containsKey(url.toString())) {
                        log.debug { "File already downloaded, using cached version" }
                        return downloadMap[url.toString()]!!
                    }
                    log.info { "Downloading file from URL: $url" }
                    HttpClient(CIO).use { it.downloadToTempFile(url) }.also {
                        log.debug { "Downloaded file to: $it" }
                        downloadMap[url.toString()] = it
                    }
                }
                else -> throw IllegalStateException("Neither pathToReadFrom nor url are set")
            }
        }
    }
}

class HttpClientBuilder(private val httpSettings: HttpSettings) {

    fun buildClient(
        httpSettings: HttpSettings, configure: (HttpClientConfig<CIOEngineConfig>.() -> Unit)? = null
    ) = HttpClient(CIO) {
        httpSettings.proxyUrl?.let { configureProxy(it) }
        install(HttpTimeout) {
            requestTimeoutMillis = httpSettings.requestTimeoutSeconds.seconds.inWholeMilliseconds
        }
        configure?.invoke(this)
    }

    private fun HttpClientConfig<CIOEngineConfig>.configureProxy(proxyUrl: String) {
        engine {
            proxy = ProxyBuilder.http(proxyUrl)
            if (httpSettings.proxyDisableSslVerify == true) {
                configureTrustManager()
            }
        }
    }

    private fun CIOEngineConfig.configureTrustManager() {
        https {
            trustManager = object : X509TrustManager {
                override fun getAcceptedIssuers(): Array<X509Certificate?> = arrayOf()
                override fun checkClientTrusted(certs: Array<X509Certificate?>?, authType: String?) {}
                override fun checkServerTrusted(certs: Array<X509Certificate?>?, authType: String?) {}
            }
        }
    }
}

