package de.mii_termserv.babelfsh

import de.mii_termserv.babelfsh.antlr.service.babelfsh.comments.PluginCommentPreprocessor
import de.mii_termserv.babelfsh.antlr.service.fsh.file.*
import de.mii_termserv.babelfsh.api.BabelfshContext
import de.mii_termserv.babelfsh.api.ResourceType
import de.mii_termserv.babelfsh.api.content_containers.ResourceContainer
import de.mii_termserv.babelfsh.api.plugins.BabelfshConversionResult
import de.mii_termserv.babelfsh.api.plugins.BabelfshTerminologyPlugin
import de.mii_termserv.babelfsh.api.plugins.PluginArguments
import de.mii_termserv.babelfsh.api.plugins.cm.ConceptMapPlugin
import de.mii_termserv.babelfsh.resourcefactories.CodeSystemFactory
import de.mii_termserv.babelfsh.resourcefactories.ConceptMapFactory
import de.mii_termserv.babelfsh.utils.CriticalError
import de.mii_termserv.babelfsh.utils.exitBabelfsh
import de.mii_termserv.babelfsh.utils.getStrictParser
import de.mii_termserv.babelfsh.utils.logging.debug
import de.mii_termserv.babelfsh.utils.logging.error
import de.mii_termserv.babelfsh.utils.logging.info
import de.mii_termserv.babelfsh.utils.logging.warn
import org.antlr.v4.runtime.CharStreams
import org.hl7.fhir.r4b.model.CodeSystem
import org.hl7.fhir.r4b.model.ConceptMap
import org.hl7.fhir.r4b.model.ValueSet
import org.slf4j.LoggerFactory
import java.io.File
import java.nio.file.PathMatcher

class BabelFshApp(private val babelfshContext: BabelfshContext) {

    private val logger = LoggerFactory.getLogger(javaClass)

    fun runFromFsh(inputFolder: File, outputFolder: File, excludeFiles: List<PathMatcher>) {
        try {
            logger.info { "Reading $inputFolder" }

            val inputFiles = enumerateInputFiles(inputFolder).also {
                logger.debug { "Found: ${it.joinToString()}" }
            }.filter { file ->
                excludeFiles.none { it.matches(file.toPath().fileName) }
            }.also {
                logger.debug { "Excluded: ${it.joinToString()}" }
            }

            val parsedItems = inputFiles.fold(FshParseResult()) { acc, file ->
                parseFshFile(acc, file.absoluteFile)
            }
            val terminologyContent = parsedItems.resolveAliases().resolveInsertsAndParameters()
            validateAllElementsHaveIds(terminologyContent)

            logger.info(
                buildString {
                    append("Processed ${inputFiles.size} files, and found: ")
                    append("${terminologyContent.ruleSets.size} RuleSets, ")
                    append("${terminologyContent.codeSystems.size} CodeSystems, ")
                    append("${terminologyContent.valueSets.size} ValueSets, ")
                    append("${terminologyContent.conceptMaps.size} ConceptMaps.")
                }
            )

            val convertedData = convertResourceFromFileContent(
                terminologyContent = terminologyContent
            )
            convertedData.codeSystems.filterNotNull().forEach { cs ->
                convertedData.writeContent(outputFolder, cs, babelfshContext).also {
                    logger.info { "Wrote CodeSystem to: $it" }
                }
            }
            convertedData.valueSets.filterNotNull().forEach { vs ->
                convertedData.writeContent(outputFolder, vs, babelfshContext).also {
                    logger.info { "Wrote ValueSet to: $it" }
                }
            }
            convertedData.conceptMaps.filterNotNull().forEach { cm ->
                convertedData.writeContent(outputFolder, cm, babelfshContext).also {
                    logger.info { "Wrote ConceptMap to: $it" }
                }
            }
        } catch (e: FshSemanticException) {
            handleFshSemanticException(e)
        }
    }

    private fun validateAllElementsHaveIds(terminologyContent: FshParseResult) {
        val codeSystems = terminologyContent.codeSystems.filter { it.idFromFshItem == null }
        val valueSets = terminologyContent.valueSets.filter { it.idFromFshItem == null }
        val conceptMaps = terminologyContent.codeSystems.filter { it.idFromFshItem == null }
        val resourcesWithoutId = codeSystems + valueSets + conceptMaps
        resourcesWithoutId.forEach {
            logger.error { "The resource ${it.resourceType} defined in ${it.file} with name '${it.nameFromFshItem}' does not have an ID." }
        }
        if (resourcesWithoutId.isNotEmpty()) {
            throw MissingIdException()
        }
    }


    private fun parseFshFile(previousParseResult: FshParseResult, absoluteFile: File): FshParseResult {
        logger.info { "Processing file: $absoluteFile" }
        val charStream = CharStreams.fromPath(absoluteFile.toPath())
        val fshGrammarService = FshGrammarService(absoluteFile, charStream, previousParseResult)
        try {
            return fshGrammarService.parseForTerminologyContent()
        } catch (e: ParserException) {
            val offendingText = when (e.offendingText.contains("\n")) {
                true -> ""
                else -> ", text: '${e.offendingText}'"
            }
            logger.error { "Unable to parse FSH. ${e.message} (at line: ${e.line}$offendingText). The error occurred in the ANTLR context ${e.inRule}" }
            exitBabelfsh(CriticalError.FSH_PARSE_ERROR)
            throw e
        } catch (e: FshSemanticException) {
            handleFshSemanticException(e)
            exitBabelfsh(CriticalError.FSH_SEMANTIC_ERROR)
            throw e
        }
    }

    private fun handleFshSemanticException(e: FshSemanticException) {
        logger.error(
            buildString {
                append("The provided FSH is not semantically correct. ${e.message} ")
                append("(")
                e.file?.let {
                    append("at file: ${it.path}, ")
                }
                e.line?.let {
                    append("at line: ${e.line}, ")
                }
                e.resourceType?.let {
                    append("for $it: ${e.resourceName}")
                }
                append(")")
            }
        )
    }

    private fun enumerateInputFiles(inputFolder: File) = inputFolder.walkTopDown().filter { file ->
        file.extension == "fsh" && (file.nameWithoutExtension.lowercase().let { name ->
            when {
                name.endsWith("babelfsh") -> true
                name.endsWith("babel") -> true
                else -> false
            }
        })
    }.toList()


    class MissingPluginCommentException(resourceType: ResourceType, resourceName: String?) :
        IllegalStateException("No plugin comment found for $resourceType with name $resourceName")

    class MissingIdException : IllegalStateException("Resources without IDs were defined in the input FSH files.")

    companion object {
        private val logger = LoggerFactory.getLogger("BabelFshApp")
        fun parsePluginComments(resources: List<TsResourceData>): List<BabelfshTask> = resources.map { resource ->
            try {
                val pluginCommentData = PluginCommentPreprocessor(
                    resource.resourceType,
                    inputString = resource.terminologyPluginComment ?: throw MissingPluginCommentException(
                        resource.resourceType,
                        resource.identification
                    ),
                    resource.pluginDefinedIn,
                ).parseComment()
                val plugin = pluginCommentData.pluginConstructor()
                val args = plugin.parseArgs(pluginCommentData, resource)
                BabelfshTask(resource, plugin, args)
            } catch (e: MissingPluginCommentException) {
                logger.error { "No plugin comment found for ${resource.resourceType} '${resource.identification}'  { id '${resource.idFromFshItem}' }" }
                exitBabelfsh(CriticalError.MISSING_PLUGIN_COMMENT)
                throw e
            } catch (e: Exception) {
                logger.error { "Error parsing plugin comment for ${resource.resourceType} '${resource.identification}'  { id '${resource.idFromFshItem}' }: ${e.message}" }
                exitBabelfsh(CriticalError.PLUGIN_PARSE_ERROR)
                throw e
            }
        }
    }

    data class BabelfshTask(
        val resource: TsResourceData,
        val plugin: BabelfshTerminologyPlugin<ResourceContainer<*, *>, BabelfshConversionResult, *>,
        val pluginArguments: PluginArguments
    )

    private fun convertResourceFromFileContent(terminologyContent: FshParseResult): BabelfshResultSet<*, *, *> {
        val csFactory = CodeSystemFactory(babelfshContext)
        val cmFactory = ConceptMapFactory(babelfshContext)
        val codeSystems = csFactory.generateCodeSystems(terminologyContent.codeSystems)
        parsePluginComments(terminologyContent.valueSets).let {
            if (it.isNotEmpty()) {
                logger.warn { "ValueSets are not yet supported" }
            }
        }
        val conceptMaps = parsePluginComments(terminologyContent.conceptMaps).asSequence().map {
            it.plugin as ConceptMapPlugin<*>
            cmFactory.createResourceFromFshWithComment(it.resource, it.plugin, it.pluginArguments)
        }
        // TODO valuesets
        val r4Parser = babelfshContext.r4Context.getStrictParser()
        val r5Parser = babelfshContext.r5Context.getStrictParser()
        return when (babelfshContext.releaseVersion) {
            BabelfshContext.ReleaseVersion.R4B -> R4ResultSet(
                codeSystems = codeSystems.map {
                    it.toR4B(babelfshContext, r4Parser)
                },
                valueSets = emptySequence(),
                conceptMaps = conceptMaps.map {
                    it.toR4B(babelfshContext, r4Parser)
                }
            )

            BabelfshContext.ReleaseVersion.R5 -> R5ResultSet(
                codeSystems = codeSystems.map {
                    it.toR5(babelfshContext, r5Parser)
                },
                valueSets = emptySequence(),
                conceptMaps = conceptMaps.map {
                    it.toR5(babelfshContext, r4Parser)
                }
            )
        }
    }
}


class R4ResultSet(
    override val codeSystems: Sequence<CodeSystem>,
    override val valueSets: Sequence<ValueSet>,
    override val conceptMaps: Sequence<ConceptMap>
) : BabelfshResultSet<CodeSystem, ValueSet, ConceptMap>() {
    override fun getOutputFilenameCs(cs: CodeSystem): String {
        return "cs-${cs.idElement.idPart}.json"
    }

    override fun getOutputFilenameVs(vs: ValueSet): String {
        return "vs-${vs.idElement.idPart}.json"
    }

    override fun getOutputFilenameCm(cm: ConceptMap): String {
        return "cm-${cm.idElement.idPart}.json"
    }

    override fun writeContent(outputFolder: File, resource: Any, babelfshContext: BabelfshContext): File {
        val jsonParser = babelfshContext.r4Context.newJsonParser().setPrettyPrint(babelfshContext.jsonPrettyPrint)
        return when (resource) {
            is CodeSystem -> outputFolder.resolve(getOutputFilenameCs(resource)).also {
                it.writeText(jsonParser.encodeResourceToString(resource))
            }

            is ValueSet -> outputFolder.resolve(getOutputFilenameVs(resource)).also {
                it.writeText(jsonParser.encodeResourceToString(resource))
            }

            is ConceptMap -> outputFolder.resolve(getOutputFilenameCm(resource)).also {
                it.writeText(jsonParser.encodeResourceToString(resource))
            }

            else -> throw IllegalArgumentException("Unknown resource type: ${resource::class.simpleName}")
        }
    }
}


sealed class BabelfshResultSet<CSResourceType, VSResourceType, CMResourceType> {
    abstract val codeSystems: Sequence<CSResourceType>
    abstract val valueSets: Sequence<VSResourceType>
    abstract val conceptMaps: Sequence<CMResourceType>

    abstract fun getOutputFilenameCs(cs: CSResourceType): String
    abstract fun getOutputFilenameVs(vs: VSResourceType): String
    abstract fun getOutputFilenameCm(cm: CMResourceType): String

    abstract fun writeContent(outputFolder: File, resource: Any, babelfshContext: BabelfshContext): File
}


@Suppress("unused")
class R5ResultSet(
    override val codeSystems: Sequence<org.hl7.fhir.r5.model.CodeSystem>,
    override val valueSets: Sequence<org.hl7.fhir.r5.model.ValueSet>,
    override val conceptMaps: Sequence<org.hl7.fhir.r5.model.ConceptMap>
) : BabelfshResultSet<org.hl7.fhir.r5.model.CodeSystem, org.hl7.fhir.r5.model.ValueSet, org.hl7.fhir.r5.model.ConceptMap>() {
    override fun getOutputFilenameCs(cs: org.hl7.fhir.r5.model.CodeSystem): String {
        return "cs-${cs.idElement.idPart}.json"
    }

    override fun getOutputFilenameVs(vs: org.hl7.fhir.r5.model.ValueSet): String {
        return "vs-${vs.idElement.idPart}.json"
    }

    override fun getOutputFilenameCm(cm: org.hl7.fhir.r5.model.ConceptMap): String {
        return "cm-${cm.idElement.idPart}.json"
    }

    override fun writeContent(outputFolder: File, resource: Any, babelfshContext: BabelfshContext): File {
        val jsonParser = babelfshContext.r5Context.newJsonParser().setPrettyPrint(babelfshContext.jsonPrettyPrint)
        return when (resource) {
            is org.hl7.fhir.r5.model.CodeSystem -> outputFolder.resolve(getOutputFilenameCs(resource)).also {
                it.writeText(jsonParser.encodeResourceToString(resource))
            }

            is org.hl7.fhir.r5.model.ValueSet -> outputFolder.resolve(getOutputFilenameVs(resource)).also {
                it.writeText(jsonParser.encodeResourceToString(resource))
            }

            is org.hl7.fhir.r5.model.ConceptMap -> outputFolder.resolve(getOutputFilenameCm(resource)).also {
                it.writeText(jsonParser.encodeResourceToString(resource))
            }

            else -> throw IllegalArgumentException("Unknown resource type: ${resource::class.simpleName}")
        }
    }
}