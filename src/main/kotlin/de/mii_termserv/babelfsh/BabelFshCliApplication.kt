package de.mii_termserv.babelfsh

import ca.uhn.fhir.context.FhirContext
import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.core.subcommands
import com.github.ajalt.clikt.parameters.arguments.argument
import com.github.ajalt.clikt.parameters.arguments.default
import com.github.ajalt.clikt.parameters.options.*
import com.github.ajalt.clikt.parameters.types.choice
import com.github.ajalt.clikt.parameters.types.file
import de.mii_termserv.babelfsh.api.*
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import java.nio.file.FileSystems
import java.nio.file.PathMatcher
import kotlin.system.exitProcess

class RootCommand : CliktCommand(
    name = "babelfsh",
    printHelpOnEmptyArgs = true,
    help = "Convert FSH files to terminological FHIR resources with a robust plugin architecture"
) {

    override fun run() {
        // This is the root command, so it doesn't do anything
    }
}

class ConvertCommand(private val logger: Logger) : CliktCommand(
    name = "convert", printHelpOnEmptyArgs = true,
    help = "The main conversion part of the application"
) {
    private val inputFolder by option(
        "-i", "--input",
        help = "The input folder to read BabelFSH files from. Paths will be evaluated relative to this folder."
    ).file(canBeFile = false, canBeDir = true, mustExist = true, mustBeReadable = true).required()

    private val outputFolder by option(
        "-o", "--output",
        help = "The output folder for generated JSON files"
    ).file(canBeFile = false, canBeDir = true, mustExist = false).required().validate { value ->
        if (!value.exists()) {
            value.mkdirs()
            logger.info("Created output folder: $value")

        }
        require(value.isDirectory) {
            "Output folder is not a directory: $value"
        }
        require(value.canWrite()) {
            "Output folder is not writable: $value"
        }
    }

    private val excludeFiles: List<PathMatcher> by option("-e", "--exclude", help = "Exclude files matching this glob")
        .convert {
            FileSystems.getDefault().getPathMatcher("glob:$it")
        }
        .multiple()

    private val fhirVersion by option(
        "--r4b", "--r5",
        help = "FHIR version, default: R4B"
    ).switch(
        "--r4b" to BabelfshContext.ReleaseVersion.R4B,
        "--r5" to BabelfshContext.ReleaseVersion.R5
    ).default(BabelfshContext.ReleaseVersion.R4B)

    private val jsonPrettyPrint by option(
        "--pretty-print", "--no-pretty-print",
        help = "Pretty print JSON output (default: true)"
    ).switch(
        "--pretty-print" to true,
        "--no-pretty-print" to false
    ).default(true)

    private val configurationSettingsPath by option(
        "--config",
        help = "Path to a configuration file"
    ).file(
        canBeFile = true, canBeDir = false, mustExist = true, mustBeReadable = true
    )

    override fun run() {
        val babelfshContext = BabelfshContext(
            r4Context = FhirContext.forR4B(),
            r5Context = FhirContext.forR5(),
            releaseVersion = fhirVersion,
            jsonPrettyPrint = jsonPrettyPrint,
            configurationSettingsSource = configurationSettingsPath
        )
        CodeSystemPluginRegistry.registerAll(babelfshContext = babelfshContext)
        ConceptMapPluginRegistry.registerAll(babelfshContext = babelfshContext)
        BabelFshApp(babelfshContext).runFromFsh(inputFolder, outputFolder, excludeFiles)
    }
}

class PluginHelpCommand : CliktCommand(
    name = "plugin-help",
    help = "View documentation for the supported plugins and their command line switches",
    printHelpOnEmptyArgs = true
) {

    val resourceType by argument("RESOURCE_TYPE", help = "The resource type to get help for").choice(
        "CodeSystem" to ResourceType.CodeSystem,
        "ValueSet" to ResourceType.ValueSet,
        "ConceptMap" to ResourceType.ConceptMap
    )

    private val pluginName by argument(
        "PLUGIN_NAME",
        help = "The name of the plugin to get help for. " +
                "If you provide 'list' or leave this out, the names of the supported plugin for the resource type will be listed."
    ).default("list")

    override fun run() {
        val babelfshContext = BabelfshContext(
            r4Context = FhirContext.forR4B(),
            r5Context = FhirContext.forR5(),
            releaseVersion = BabelfshContext.ReleaseVersion.R4B, // doesn't matter in the help command
            jsonPrettyPrint = true // matters even less ;)
        )
        CodeSystemPluginRegistry.registerAll(babelfshContext)
        ValueSetPluginRegistry.registerAll(babelfshContext)
        ConceptMapPluginRegistry.registerAll(babelfshContext)
        val registry = when (resourceType) {
            ResourceType.CodeSystem -> CodeSystemPluginRegistry
            ResourceType.ValueSet -> ValueSetPluginRegistry
            ResourceType.ConceptMap -> ConceptMapPluginRegistry
            else -> throw UnsupportedOperationException("Resource type $resourceType is not supported")
        }

        if (pluginName == "list") {
            println("Supported plugins for $resourceType:")
            println(registry.generateHelpTable())
        } else {
            val plugin = registry.getPluginConstructor(pluginName)
                ?: run {
                    System.err.println("No plugin found with name $pluginName. Supported plugin IDs are: ${registry.printAllPluginIds()}")
                    exitProcess(1)
                }
            val pluginInstance = plugin()
            println(pluginInstance.getHelp())
        }
    }
}

fun main(args: Array<String>) {
    runApplication<BabelFshSpringApplication>(*args)
}

@SpringBootApplication
class BabelFshSpringApplication : CommandLineRunner {

    private val logger = LoggerFactory.getLogger(javaClass)

    override fun run(vararg args: String?) {
        RootCommand().subcommands(ConvertCommand(logger), PluginHelpCommand()).main(args.toList().filterNotNull())
    }


}
