package de.mii_termserv.babelfsh.utils

import kotlin.system.exitProcess

enum class CriticalError {
    FSH_PARSE_ERROR,
    FSH_SEMANTIC_ERROR,
    PLUGIN_PARSE_ERROR,
    MISSING_PLUGIN_COMMENT,
}

fun exitBabelfsh(error: CriticalError) {
    println("Critical error: $error")
    exitProcess(error.ordinal + 1) // exit code 0 is success, so add 1
}