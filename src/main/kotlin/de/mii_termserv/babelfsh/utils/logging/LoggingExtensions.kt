package de.mii_termserv.babelfsh.utils.logging

import org.slf4j.Logger
import org.slf4j.event.Level

fun Logger.log(level: Level, lambda: () -> Any?) {
    if (this.isEnabledForLevel(level)) {
        val stringValue = lambda().toString()
        this.atLevel(level).log(stringValue)
    }
}

fun Logger.trace(lambda: () -> Any?) = log(Level.TRACE, lambda)

fun Logger.debug(lambda: () -> Any?) = log(Level.DEBUG, lambda)

fun Logger.info(lambda: () -> Any?) = log(Level.INFO, lambda)

@Suppress("unused")
fun Logger.warn(lambda: () -> Any?) = log(Level.WARN, lambda)

fun Logger.error(lambda: () -> Any?) = log(Level.ERROR, lambda)