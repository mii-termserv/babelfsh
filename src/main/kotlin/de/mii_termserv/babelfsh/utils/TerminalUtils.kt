package de.mii_termserv.babelfsh.utils

import org.slf4j.LoggerFactory


private val logger = LoggerFactory.getLogger("TerminalUtils")
fun getTerminalWidth() = try {
    jline.TerminalFactory.get().width
} catch (e: Exception) {
    logger.debug("Could not get terminal width")
    120
}