package de.mii_termserv.babelfsh.utils

import ca.uhn.fhir.context.FhirContext
import ca.uhn.fhir.parser.IParser
import ca.uhn.fhir.parser.StrictErrorHandler

fun String.stripQuotes() = this.trim('\"', '\'')
fun String.stripCode() = this.stripQuotes().trim('#')
fun FhirContext.getStrictParser(): IParser = this.newJsonParser().setParserErrorHandler(StrictErrorHandler())