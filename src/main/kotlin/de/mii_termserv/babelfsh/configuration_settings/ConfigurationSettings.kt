package de.mii_termserv.babelfsh.configuration_settings


data class ConfigurationSettings(
    val http: HttpSettings = HttpSettings()
)

data class HttpSettings(
    val proxyUrl: String? = null,
    val proxyDisableSslVerify: Boolean? = false,
    val requestTimeoutSeconds: Int = 30
)