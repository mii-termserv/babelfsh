@file:Suppress("DuplicatedCode")

package de.mii_termserv.babelfsh.resourcefactories

import ca.uhn.fhir.parser.DataFormatException
import de.mii_termserv.babelfsh.antlr.service.babelfsh.comments.CommandLineParseException
import de.mii_termserv.babelfsh.antlr.service.fsh.file.GraphToJsonBuilder
import de.mii_termserv.babelfsh.antlr.service.fsh.file.TsResourceData
import de.mii_termserv.babelfsh.antlr.service.fsh.rules.FshPathParserService
import de.mii_termserv.babelfsh.antlr.service.fsh.rules.RuleParseTree
import de.mii_termserv.babelfsh.api.BabelfshContext
import de.mii_termserv.babelfsh.api.ResourceType
import de.mii_termserv.babelfsh.api.content_containers.ResourceContainer
import de.mii_termserv.babelfsh.api.plugins.BabelfshConversionResult
import de.mii_termserv.babelfsh.api.plugins.BabelfshTerminologyPlugin
import de.mii_termserv.babelfsh.api.plugins.PluginArguments
import de.mii_termserv.babelfsh.utils.getStrictParser
import de.mii_termserv.babelfsh.utils.logging.error
import de.mii_termserv.babelfsh.utils.logging.info
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonElement
import kotlinx.serialization.json.JsonObject
import org.slf4j.LoggerFactory
import kotlin.system.exitProcess

abstract class ResourceFactory<ResourceContainerClass : ResourceContainer<*, *>, ContentContainerClass : BabelfshConversionResult>(
    val babelfshContext: BabelfshContext, val resourceType: ResourceType
) {
    private val jsonFormatter = Json { prettyPrint = true }

    private val logger = LoggerFactory.getLogger(javaClass)

    private fun createResourceWithComment(
        resourceData: TsResourceData,
        plugin: BabelfshTerminologyPlugin<ResourceContainerClass, ContentContainerClass, *>,
        args: PluginArguments,
    ): ResourceContainerClass {
        logger.info {
            buildString {
                append("Running ${plugin.resourceType.name} plugin '${plugin.pluginId}' for resource '${resourceData.identification}'. ")
                plugin.attribution?.let {
                    append(it)
                }

            }.trim()
        }
        // TODO: this is currently where validation happens. This should be refactored to run immediately after
        // parsing the entire FSH file set, and validate all resources before running the plugins
        val contentJson = resourceData.parseFshItemToJson()
        try {
            val resource = createResourceContainer(contentJson)
            addContentToResource(resource, args, resourceData, plugin)
            return resource
        } catch (e: Exception) {
            logger.error { "Error converting resource ${resourceData.resourceType} ${resourceData.identification}: ${e.message}" }
            throw e
        }
    }

    abstract fun createResourceContainer(json: JsonObject): ResourceContainerClass

    protected abstract fun addContentToResource(
        resource: ResourceContainerClass,
        args: PluginArguments,
        resourceData: TsResourceData,
        plugin: BabelfshTerminologyPlugin<ResourceContainerClass, ContentContainerClass, *>
    )

    fun createResourceFromFshWithComment(
        resourceData: TsResourceData,
        pluginInstance: BabelfshTerminologyPlugin<ResourceContainerClass, ContentContainerClass, *>,
        pluginArguments: PluginArguments
    ): ResourceContainerClass {
        logger.info { "Generating $resourceType with name ${resourceData.nameFromFshItem} from FSH" }
        try {
            val resource = createResourceWithComment(resourceData, pluginInstance, pluginArguments)
            validateResourceSemantics(resource)
            return resource
        } catch (e: CommandLineParseException) {
            logger.error {
                when (e.tokenContext) {
                    null -> e.message
                    else -> "${e.message} (at column: ${e.tokenContext.start?.charPositionInLine}, text: '${e.tokenContext.text}')"
                }
            }
            throw e
        }
    }

    abstract fun validateResourceSemantics(resource: ResourceContainerClass)

    protected fun TsResourceData.parseFshItemToJson(): JsonObject {
        val parseTree = FshPathParserService(rules = this.rules).resolvePaths()

        validateRules(parseTree, this)

        val jsonBuilder = GraphToJsonBuilder(parseTree, this)
        val json = jsonBuilder.buildJson()
        return json
    }

    private fun validateRules(parseTree: RuleParseTree, tsResourceData: TsResourceData) {
        val rootNodes = parseTree.nodeGraph.vertexSet().filter { it.parent == parseTree.resourceRoot }
        val jsonBuilder = GraphToJsonBuilder(parseTree, tsResourceData)
        val r4Parser = babelfshContext.r4Context.getStrictParser()
        val r5Parser = babelfshContext.r5Context.getStrictParser()
        val validationErrors = rootNodes.mapNotNull { node ->
            val skeleton = buildResourceSkeleton(node.pathPart!!, jsonBuilder.buildJson(node))
            try {
                val resourceContainer = createResourceContainer(skeleton)
                when (babelfshContext.releaseVersion) {
                    BabelfshContext.ReleaseVersion.R4B -> resourceContainer.toR4B(babelfshContext, r4Parser)
                    BabelfshContext.ReleaseVersion.R5 -> resourceContainer.toR5(babelfshContext, r5Parser)
                }
                return@mapNotNull null
            } catch (e: DataFormatException) {
                val contexts = node.definitionContext.groupBy { it.key }
                val definedIn = contexts.map { (key, rules) ->
                    val lineNumbers = when {
                        rules.size == 1 -> "line number ${rules.first().lineNumber}"
                        else -> "line numbers: ${rules.joinToString { it.lineNumber.toString() }}"
                    }
                    return@map "${key.resourceType} ${key.resourceName} (file ${key.file?.absolutePath}, $lineNumbers)"
                }
                val definedInString = when (definedIn.size) {
                    1 -> definedIn.first()
                    else -> definedIn.joinToString("\n") { "  - $it" }
                }

                return@mapNotNull when (parseTree.nodeGraph.outDegreeOf(node)) {
                    0 -> buildString {
                        appendLine("HAPI FHIR was unable to validate the terminal rule '${node.fullPath}' due to: ${e.message}")
                        appendLine("  The rule is defined in:")
                        appendLine(definedInString.prependIndent("  "))
                    }

                    else -> buildString {
                        appendLine("Error validating the rules defined below '${node.fullPath}' (used in file ${tsResourceData.file?.absolutePath}) due to: ${e.message}")
                        appendLine("  It is not possible to give a precise location of the offending rules, but the error is present in one of the following locations:")
                        appendLine(definedInString.prependIndent("  "))
                    }
                }
            }
        }
        if (validationErrors.isNotEmpty()) {
            logger.error { "BabelFSH detected ${validationErrors.size} errors in your FSH code!" }
            validationErrors.forEach(logger::error)
            exitProcess(1)
        }
    }

    abstract fun buildResourceSkeleton(jsonKey: String, jsonElement: JsonElement): JsonObject

}

