package de.mii_termserv.babelfsh.resourcefactories

import de.mii_termserv.babelfsh.antlr.service.fsh.file.TsResourceData
import de.mii_termserv.babelfsh.api.BabelfshContext
import de.mii_termserv.babelfsh.api.ResourceType
import de.mii_termserv.babelfsh.api.content_containers.cm.ConceptMapContainer
import de.mii_termserv.babelfsh.api.content_containers.cm.ConceptMapEntryContainer
import de.mii_termserv.babelfsh.api.plugins.BabelfshTerminologyPlugin
import de.mii_termserv.babelfsh.api.plugins.PluginArguments
import kotlinx.serialization.json.JsonElement
import kotlinx.serialization.json.JsonObject
import kotlinx.serialization.json.buildJsonObject
import kotlinx.serialization.json.put
import org.slf4j.LoggerFactory

class ConceptMapFactory(babelfshContext: BabelfshContext) : ResourceFactory<ConceptMapContainer, ConceptMapEntryContainer>(babelfshContext, ResourceType.ConceptMap) {

    private val logger = LoggerFactory.getLogger(javaClass)

    override fun createResourceContainer(json: JsonObject): ConceptMapContainer {
        return ConceptMapContainer(json)
    }

    override fun validateResourceSemantics(resource: ConceptMapContainer) {

    }

    override fun addContentToResource(
        resource: ConceptMapContainer,
        args: PluginArguments,
        resourceData: TsResourceData,
        plugin: BabelfshTerminologyPlugin<ConceptMapContainer, ConceptMapEntryContainer, *>
    ) {
        val content = plugin.produceContent(args, resource)
        content.forEach {
            resource.addElement(it)
        }
        logger.info("Generated ${resource.groups.size} groups from ${content.size} mappings")
    }

    override fun buildResourceSkeleton(jsonKey: String, jsonElement: JsonElement) = buildJsonObject {
        put("resourceType", "ConceptMap")
        put(jsonKey, jsonElement)
    }
}