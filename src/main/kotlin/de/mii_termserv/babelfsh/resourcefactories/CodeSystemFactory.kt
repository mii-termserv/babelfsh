package de.mii_termserv.babelfsh.resourcefactories

import ca.uhn.fhir.parser.DataFormatException
import de.mii_termserv.babelfsh.BabelFshApp
import de.mii_termserv.babelfsh.antlr.service.fsh.file.TsResourceData
import de.mii_termserv.babelfsh.api.BabelfshContext
import de.mii_termserv.babelfsh.api.ResourceType
import de.mii_termserv.babelfsh.api.content_containers.PropertyContainer
import de.mii_termserv.babelfsh.api.content_containers.cs.CodeSystemConceptContainer
import de.mii_termserv.babelfsh.api.content_containers.cs.CodeSystemContainer
import de.mii_termserv.babelfsh.api.content_containers.cs.ConceptPropertyContainer
import de.mii_termserv.babelfsh.api.content_containers.cs.PropertyValueType
import de.mii_termserv.babelfsh.api.plugins.BabelfshTerminologyPlugin
import de.mii_termserv.babelfsh.api.plugins.PluginArguments
import de.mii_termserv.babelfsh.api.plugins.cs.CodeSystemPlugin
import de.mii_termserv.babelfsh.utils.logging.info
import de.mii_termserv.babelfsh.utils.logging.warn
import kotlinx.serialization.json.JsonElement
import kotlinx.serialization.json.JsonObject
import kotlinx.serialization.json.buildJsonObject
import kotlinx.serialization.json.put
import org.slf4j.LoggerFactory

class CodeSystemFactory(babelfshContext: BabelfshContext) : ResourceFactory<CodeSystemContainer, CodeSystemConceptContainer>(
    babelfshContext = babelfshContext,
    resourceType = ResourceType.CodeSystem
) {

    private val logger = LoggerFactory.getLogger(javaClass)

    private val implicitProperties: List<String> = when (babelfshContext.releaseVersion) {
        BabelfshContext.ReleaseVersion.R4B -> listOf(
            "status",
            "retirementDate",
            "deprecationDate",
            "parent",
            "child",
            "notSelectable"
        )

        BabelfshContext.ReleaseVersion.R5 -> listOf(
            "status",
            "inactive",
            "effectiveData",
            "deprecationDate",
            "retirementDate",
            "notSelectable",
            "parent",
            "child",
            "partOf",
            "synonym",
            "comment",
            "itemWeight"
        )
    }

    override fun createResourceContainer(json: JsonObject): CodeSystemContainer {
        return CodeSystemContainer(json)
    }

    fun generateCodeSystems(codeSystems: List<TsResourceData>): Sequence<CodeSystemContainer> {
        val thoseWithComment = codeSystems.filter { it.terminologyPluginComment != null }
        val thoseWithDirectConcepts = codeSystems.filter { it.directConcepts.isNotEmpty() }
        val csWithComments = BabelFshApp.parsePluginComments(thoseWithComment).asSequence().map {
            it.plugin as CodeSystemPlugin<*>
            createResourceFromFshWithComment(it.resource, it.plugin, it.pluginArguments)
        }
        val csWithDirect = thoseWithDirectConcepts.asSequence().map {
            createDirectConceptCodeSystem(it)
        }
        return csWithComments + csWithDirect
    }

    private fun createDirectConceptCodeSystem(cs: TsResourceData): CodeSystemContainer {
        logger.info { "Creating direct CodeSystem ${cs.identification} with ${cs.directConcepts.size} concepts" }
        val json = cs.parseFshItemToJson()
        val resource = createResourceContainer(json)
        cs.directConcepts.forEach { directConcept ->
            val concept = CodeSystemConceptContainer(
                code = directConcept.code,
                display = directConcept.display,
                definition = directConcept.definitionString,
                property = directConcept.childCodes.map {
                    ConceptPropertyContainer(
                        code = "child",
                        valueType = PropertyValueType.CodeContainer(it)
                    )
                }
            )
            resource.addConcept(concept)
        }
        return resource
    }

    override fun addContentToResource(
        resource: CodeSystemContainer,
        args: PluginArguments,
        resourceData: TsResourceData,
        plugin: BabelfshTerminologyPlugin<CodeSystemContainer, CodeSystemConceptContainer, *>
    ) {
        val conceptsAdded = plugin.produceContent(args, resource).fold(0L) { acc, concept ->
            resource.addConcept(concept)
            acc + 1
        }
        logger.info { "Added $conceptsAdded concepts to CodeSystem ${resourceType.name} (${babelfshContext.releaseVersion.name})" }

    }

    override fun buildResourceSkeleton(jsonKey: String, jsonElement: JsonElement) = buildJsonObject {
        put("resourceType", "CodeSystem")
        put(jsonKey, jsonElement)
    }

    /**
     * list all property keys and a list of examples that are used in the concepts
     */
    @Suppress("SameParameterValue")
    private fun listUsedPropertyKeysInConcepts(resource: CodeSystemContainer, numExamples: Int = 3): List<PropertyUsed> {
        val flatPropertyStream = resource.concepts.flatMap { c -> c.property }
        val usedKeys = flatPropertyStream.map { it.code }.distinct().toList()
        return usedKeys.map { usedKey ->
            val examples = flatPropertyStream.filter { it.code == usedKey }.stream()
                .map { it.valueType.toString() }.distinct()
                .limit(numExamples.toLong()).toList()
            val types = flatPropertyStream.filter { it.code == usedKey }.map { it.valueType.propertyType }.distinct()
            if (types.size > 1) {
                throw DataFormatException("The property '$usedKey' is used with different types: ${types.joinToString { it.name }}")
            }
            PropertyUsed(usedKey, flatPropertyStream.first { it.code == usedKey }.valueType.propertyType, examples)
        }
    }

    /**
     * list all codes for properties that are defined in the CodeSystem
     */
    private fun listDefinedPropertiesInResource(resource: CodeSystemContainer): List<PropertyDeclared> {
        return resource.getProperties().map { prop ->
            PropertyDeclared(prop.code, prop.type)
        }
    }

    override fun validateResourceSemantics(resource: CodeSystemContainer) {
        val definedProps = listDefinedPropertiesInResource(resource)
        val usedProps = listUsedPropertyKeysInConcepts(resource, 3)
        val undefinedProps = usedProps.filter { used ->
            definedProps.none { it.code == used.code } && !implicitProperties.contains(used.code)
        }
        if (undefinedProps.any()) {
            throw DataFormatException(
                "${resource.identification} uses properties that are not defined in the CodeSystem, and are not implicitly defined: ${
                    undefinedProps.joinToString { "**${it.code}** (e.g. ${it.examples})" }
                }"
            )
        }
        definedProps.filter { defined -> usedProps.none { defined.code == it.code } }.onEach { unused ->
            logger.warn { "The property '$unused' is declared in the FSH item, but isn't used in the resource" }
        }
        definedProps.associateWith { defined -> usedProps.firstOrNull { it.code == defined.code } }.forEach { (defined, used) ->
            if (used != null && defined.type != used.type) {
                throw DataFormatException(
                    "The property '${defined.code}' is defined as '${defined.type}', but is used as '${used.type}' in the resource"
                )
            }
        }
    }
}

data class PropertyDeclared(
    val code: String,
    val type: PropertyContainer.PropertyTypeEnum
)

data class PropertyUsed(
    val code: String,
    val type: PropertyContainer.PropertyTypeEnum,
    val examples: List<String>
)