RuleSet: ops-metadata(version)
* insert bfarm-sutermserv-tags
* ^meta.profile[+] = "http://hl7.org/fhir/StructureDefinition/shareablecodesystem"
* ^url = "http://fhir.de/CodeSystem/bfarm/ops"
* ^valueSet = "http://fhir.de/ValueSet/bfarm/ops"
* ^version = "{version}"
* ^copyright = "Die Erstellung erfolgt unter Verwendung der maschinenlesbaren Fassung des Bundesinstituts für Arzneimittel und Medizinprodukte (BfArM)."
* ^status = #active
* ^experimental = false
* ^content = #complete
* ^caseSensitive = false
* ^hierarchyMeaning = #classified-with
* ^property[+].code = #kind
* ^property[=].description = "Art der Schlüsselnummer, eines von: [chapter, block, category]"
* ^property[=].type = #string
* ^property[+].code = #inclusion
* ^property[=].description = "Ein Inklusisionskriterium"
* ^property[=].type = #string
* ^property[+].code = #exclusion
* ^property[=].description = "Ein Exklusionskriterium"
* ^property[=].type = #string
* ^property[+].code = #note
* ^property[=].description = "Eine Anmerkung zur Verwendung der Gruppe"
* ^property[=].type = #string
* ^property[+].code = #usage
* ^property[=].type = #string
* ^property[=].description = "Die Verwendung des Codes, enthält Angaben zur Seitigkeit"

RuleSet: ops-babelfsh(input, oid)
* ^identifier[+].system = "urn:ietf:rfc:3986"
* ^identifier[=].value = "urn:oid:{oid}"
/*^babelfsh
claml-bfarm
  --path="{input}"
  --mode=ops
^babelfsh*/

CodeSystem: OPS
Id: ops-2010
Description: "The German procedure classification (Operationen- und Prozedurenschlüssel - OPS) is the official classification for the encoding of operations, procedures and general medical measures."
Title: "OPS"
* insert ops-metadata("2010")
* insert ops-babelfsh("input-files/OPS/ops2010/p1sec2010/Klassifikationsdateien/ops2010syst_claml_20091028.xml", "1.2.276.0.76.5.385")

CodeSystem: OPS
Id: ops-2011
Description: "The German procedure classification (Operationen- und Prozedurenschlüssel - OPS) is the official classification for the encoding of operations, procedures and general medical measures."
Title: "OPS"
* insert ops-metadata("2011")
* insert ops-babelfsh("input-files/OPS/ops2011/p1sec2011/Klassifikationsdateien/ops2011syst_claml_20101021.xml", "1.2.276.0.76.5.389")

CodeSystem: OPS
Id: ops-2012
Description: "The German procedure classification (Operationen- und Prozedurenschlüssel - OPS) is the official classification for the encoding of operations, procedures and general medical measures."
Title: "OPS"
* insert ops-metadata("2012")
* insert ops-babelfsh("input-files/OPS/ops2012/p1sec2012/Klassifikationsdateien/ops2012syst_claml_20111103.xml", "1.2.276.0.76.5.410")

CodeSystem: OPS
Id: ops-2013
Description: "The German procedure classification (Operationen- und Prozedurenschlüssel - OPS) is the official classification for the encoding of operations, procedures and general medical measures."
Title: "OPS"
* insert ops-metadata("2013")
* insert ops-babelfsh("input-files/OPS/ops2013/p1sec2013/Klassifikationsdateien/ops2013syst_claml_20121012.xml", "1.2.276.0.76.5.414")

CodeSystem: OPS
Id: ops-2014
Description: "The German procedure classification (Operationen- und Prozedurenschlüssel - OPS) is the official classification for the encoding of operations, procedures and general medical measures."
Title: "OPS"
* insert ops-metadata("2014")
* insert ops-babelfsh("input-files/OPS/ops2014/p1sec2014-20131104/Klassifikationsdateien/ops2014syst_claml_20131104.xml", "1.2.276.0.76.5.418")

CodeSystem: OPS
Id: ops-2015
Description: "The German procedure classification (Operationen- und Prozedurenschlüssel - OPS) is the official classification for the encoding of operations, procedures and general medical measures."
Title: "OPS"
* insert ops-metadata("2015")
* insert ops-babelfsh("input-files/OPS/ops2015/p1sec-2015/Klassifikationsdateien/ops2015syst_claml_20141017.xml", "1.2.276.0.76.5.425")

CodeSystem: OPS
Id: ops-2016
Description: "The German procedure classification (Operationen- und Prozedurenschlüssel - OPS) is the official classification for the encoding of operations, procedures and general medical measures."
Title: "OPS"
* insert ops-metadata("2016")
* insert ops-babelfsh("input-files/OPS/ops2016/p1sec2016/Klassifikationsdateien/ops2016syst_claml_20151016.xml", "1.2.276.0.76.5.431")

CodeSystem: OPS
Id: ops-2017
Description: "The German procedure classification (Operationen- und Prozedurenschlüssel - OPS) is the official classification for the encoding of operations, procedures and general medical measures."
Title: "OPS"
* insert ops-metadata("2017")
* insert ops-babelfsh("input-files/OPS/ops2017/p1sec2017/Klassifikationsdateien/ops2017syst_claml_20161019.xml", "1.2.276.0.76.5.464")

CodeSystem: OPS
Id: ops-2018
Description: "The German procedure classification (Operationen- und Prozedurenschlüssel - OPS) is the official classification for the encoding of operations, procedures and general medical measures."
Title: "OPS"
* insert ops-metadata("2018")
* insert ops-babelfsh("input-files/OPS/ops2018/p1sec2018/Klassifikationsdateien/ops2018syst_claml_20171018.xml", "1.2.276.0.76.5.472")

CodeSystem: OPS
Id: ops-2019
Description: "The German procedure classification (Operationen- und Prozedurenschlüssel - OPS) is the official classification for the encoding of operations, procedures and general medical measures."
Title: "OPS"
* insert ops-metadata("2019")
* insert ops-babelfsh("input-files/OPS/ops2019/ops2019syst-claml/Klassifikationsdateien/ops2019syst_claml_20181019.xml", "1.2.276.0.76.5.478")

CodeSystem: OPS
Id: ops-2020
Description: "The German procedure classification (Operationen- und Prozedurenschlüssel - OPS) is the official classification for the encoding of operations, procedures and general medical measures."
Title: "OPS"
* insert ops-metadata("2020")
* insert ops-babelfsh("input-files/OPS/ops2020/ops2020syst-claml/Klassifikationsdateien/ops2020syst_claml_20191018.xml", "1.2.276.0.76.5.487")

CodeSystem: OPS
Id: ops-2021
Description: "The German procedure classification (Operationen- und Prozedurenschlüssel - OPS) is the official classification for the encoding of operations, procedures and general medical measures."
Title: "OPS"
* insert ops-metadata("2021")
* insert ops-babelfsh("input-files/OPS/ops2021/ops2021syst-claml/Klassifikationsdateien/ops2021syst_claml_20201016.xml", "1.2.276.0.76.5.503")

CodeSystem: OPS
Id: ops-2022
Description: "The German procedure classification (Operationen- und Prozedurenschlüssel - OPS) is the official classification for the encoding of operations, procedures and general medical measures."
Title: "OPS"
* insert ops-metadata("2022")
* insert ops-babelfsh("input-files/OPS/ops2022/ops2022syst-claml/Klassifikationsdateien/ops2022syst_claml_20211022.xml", "1.2.276.0.76.5.519")

CodeSystem: OPS
Id: ops-2023
Description: "The German procedure classification (Operationen- und Prozedurenschlüssel - OPS) is the official classification for the encoding of operations, procedures and general medical measures."
Title: "OPS"
* insert ops-metadata("2023")
* insert ops-babelfsh("input-files/OPS/ops2023syst-claml/Klassifikationsdateien/ops2023syst_claml_20221021.xml", "1.2.276.0.76.5.529")

CodeSystem: OPS
Id: ops-2024
Description: "The German procedure classification (Operationen- und Prozedurenschlüssel - OPS) is the official classification for the encoding of operations, procedures and general medical measures."
Title: "OPS"
* insert ops-metadata("2024")
* insert ops-babelfsh("input-files/OPS/ops2024syst-claml/Klassifikationsdateien/ops2024syst_claml_20231020.xml", "1.2.276.0.76.5.537")