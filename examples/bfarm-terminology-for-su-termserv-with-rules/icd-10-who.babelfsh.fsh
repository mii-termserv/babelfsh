RuleSet: icd10who-metadata(version)
* insert bfarm-sutermserv-tags
* ^meta.tag[+].system = "$sutermserv_project"
* ^meta.tag[=].code = "who"
* ^meta.tag[=].display = "World Health Organization"
* ^meta.tag[+].system = "$sutermserv_dataset"
* ^meta.tag[=].code = "who"
* ^meta.tag[=].display = "World Health Organization"
* ^meta.profile = "http://hl7.org/fhir/StructureDefinition/shareablecodesystem"
* ^url = "http://hl7.org/fhir/sid/icd-10"
* ^valueSet = "http://hl7.org/fhir/sid/icd-10/vs"
* ^version = "{version}"
* ^identifier[+].system = "urn:ietf:rfc:3986"
* ^identifier[=].value = "urn:oid:2.16.840.1.113883.6.3"
* ^copyright = "Die Erstellung erfolgt unter Verwendung der maschinenlesbaren Fassung des Bundesinstituts für Arzneimittel und Medizinprodukte (BfArM)."
* ^status = #active
* ^experimental = false
* ^content = #complete
* ^caseSensitive = false
* ^hierarchyMeaning = #classified-with
* insert icd10who-property_rubrics

// -----------------------------------

RuleSet: icd10who-property_rubrics
* ^property[+].code = #kind
  * ^property[=].description = "Art der Schlüsselnummer, eines von: [chapter, block, category]"
  * ^property[=].type = #string
* ^property[+].code = #inclusion
  * ^property[=].description = "Ein Inklusisionskriterium"
  * ^property[=].type = #string
* ^property[+].code = #exclusion
  * ^property[=].description = "Ein Exklusionskriterium"
  * ^property[=].type = #string
* ^property[+].code = #introduction
  * ^property[=].description = "Die Einführung eines Kapitels"
  * ^property[=].type = #string
* ^property[+].code = #note
  * ^property[=].description = "Eine Anmerkung zur Verwendung der Gruppe"
  * ^property[=].type = #string
* ^property[+].code = #text
  * ^property[=].description = "Ein freier Text zur Kategorie"
  * ^property[=].type = #string
* ^property[+].code = #coding-hint
  * ^property[=].description = "Kodierhinweise, die die Verwendung des Kodes erklären bzw. einen Hinweis zur Kodierung geben"
  * ^property[=].type = #string
* ^property[+].code = #usage
  * ^property[=].description = "Verwendung des Codes im Kreuz-Stern-System, eines von ['+', '!', '*']"
  * ^property[=].type = #string

// -----------------------------------

RuleSet: icd10who-babelfsh(input)
/*^babelfsh
claml-bfarm
  --mode=ICD_10_WHO
  --path="{input}"
^babelfsh*/

CodeSystem: Icd10Who
Id: icd-10-who-2011
Description: "Internationale statistische Klassifikation der Krankheiten und verwandter Gesundheitsprobleme, 10. Revision der WHO, Version 2011"
Title: "ICD-10-WHO"
* insert icd10who-metadata("2011")
* insert icd10who-babelfsh("./input-files/ICD-10-WHO/2011/icd10who2011/x1wec2011/Klassifikationsdateien/icd10who2011syst_claml_20101125.xml")

CodeSystem: Icd10Who
Id: icd-10-who-2013
Description: "Internationale statistische Klassifikation der Krankheiten und verwandter Gesundheitsprobleme, 10. Revision der WHO, Version 2013"
Title: "ICD-10-WHO"
* insert icd10who-metadata("2013")
* insert icd10who-babelfsh("./input-files/ICD-10-WHO/2013/icd10who2013/x1wec2013/Klassifikationsdateien/icd10who2013syst_claml_20121109.xml")

CodeSystem: Icd10Who
Id: icd-10-who-2016
Description: "Internationale statistische Klassifikation der Krankheiten und verwandter Gesundheitsprobleme, 10. Revision der WHO, Version 2016"
Title: "ICD-10-WHO"
* insert icd10who-metadata("2016")
* insert icd10who-babelfsh("./input-files/ICD-10-WHO/2016/x1wec2016/Klassifikationsdateien/icd10who2016syst_claml_20150717.xml")

CodeSystem: Icd10Who
Id: icd-10-who-2019
Description: "Internationale statistische Klassifikation der Krankheiten und verwandter Gesundheitsprobleme, 10. Revision der WHO, Version 2019"
Title: "ICD-10-WHO"
* insert icd10who-metadata("2019")
* insert icd10who-babelfsh("./input-files/ICD-10-WHO/2019/icd10who2019syst-claml/Klassifikationsdateien/icd10who2019syst_claml_20180824.xml")