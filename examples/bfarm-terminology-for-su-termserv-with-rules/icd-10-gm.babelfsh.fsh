RuleSet: icd10gm-metadata(version)
* insert bfarm-sutermserv-tags
* ^meta.profile = "http://hl7.org/fhir/StructureDefinition/shareablecodesystem"
* ^url = "http://fhir.de/CodeSystem/bfarm/icd-10-gm"
* ^valueSet = "http://fhir.de/ValueSet/bfarm/icd-10-gm"
* ^version = "{version}"
* ^identifier[+].system = "urn:ietf:rfc:3986"
* ^identifier[=].value = "urn:oid:{oid}"
* ^copyright = "Die Erstellung erfolgt unter Verwendung der maschinenlesbaren Fassung des Bundesinstituts für Arzneimittel und Medizinprodukte (BfArM)."
* ^status = #active
* ^experimental = false
* ^content = #complete
* ^caseSensitive = false
* ^hierarchyMeaning = #classified-with
* insert icd10gm-property_rubrics


RuleSet: icd10gm-property_rubrics
* ^property[+].code = #kind
* ^property[=].description = "Art der Schlüsselnummer, eines von: [chapter, block, category]"
* ^property[=].type = #string
* ^property[+].code = #inclusion
* ^property[=].description = "Ein Inklusisionskriterium"
* ^property[=].type = #string
* ^property[+].code = #exclusion
* ^property[=].description = "Ein Exklusionskriterium"
* ^property[=].type = #string
* ^property[+].code = #introduction
* ^property[=].description = "Die Einführung eines Kapitels"
* ^property[=].type = #string
* ^property[+].code = #note
* ^property[=].description = "Eine Anmerkung zur Verwendung der Gruppe"
* ^property[=].type = #string
* ^property[+].code = #text
* ^property[=].description = "Ein freier Text zur Kategorie"
* ^property[=].type = #string
* ^property[+].code = #coding-hint
* ^property[=].description = "Kodierhinweise, die die Verwendung des Kodes erklären bzw. einen Hinweis zur Kodierung geben"
* ^property[=].type = #string
* ^property[+].code = #usage
* ^property[=].description = "Verwendung des Codes im Kreuz-Stern-System, eines von ['+', '!', '*']"
* ^property[=].type = #string


RuleSet: icd10gm_meta_property_AgeHigh
* ^property[+].code = #AgeHigh
* ^property[=].type = #string
* ^property[=].description = "obere Altersgrenze für eine Schlüsselnummer (eine Krankheit kann auftreten ab einem Alter von mindestens n vollendeten Lebenstagen/-jahren). 9999 = irrelevant; t000 - t364 = 0 Tage - bis zu 364 Tagen; j001 - j124 = bis zu 1 Jahr – bis zu 124 Jahre"

RuleSet: icd10gm_meta_property_AgeHigh_with_alternative_format
* ^property[+].code = #AgeHigh
* ^property[=].type = #string
* ^property[=].description = "obere Altersgrenze für eine Schlüsselnummer. 999 = irrelevant, zur Interpretation sei auf die Metadaten in den ICD-10-Versionen 2009 bis 2017 verwiesen."
* ^property[=].uri = "https://www.bfarm.de/SharedDocs/Downloads/DE/Kodiersysteme/klassifikationen/icd-10-gm/vorgaenger-bis-2020/icd10gm2013_zip.html"

RuleSet: icd10gm_meta_property_AgeHighDiff
* ^property[+].code = #AgeHighDiff
* ^property[=].type = #string
* ^property[=].description = "obere Altersgrenze für eine Schlüsselnummer, alternatives Format. 9999 = irrelevant; t000 - t365 = 0 Tage bis unter 1 Jahr; j001 - j124 = 1 Jahr bis unter 124 Jahre"

RuleSet: icd10gm_meta_property_AgeLow
* ^property[+].code = #AgeLow
* ^property[=].type = #string
* ^property[=].description = "untere Altersgrenze für eine Schlüsselnummer (eine Krankheit kann auftreten bis zu einem Alter von höchstens m vollendeten Lebenstagen/-jahren). 9999 = irrelevant; t000 - t364 = ab 0 Tage einschließlich Fetalzeit - ab 364 Lebenstagen; j001 - j124 = ab 1 Lebensjahr - ab 124 Lebensjahren"

RuleSet: icd10gm_meta_property_AgeLow_with_alternative_format
* ^property[+].code = #AgeLow
* ^property[=].type = #string
* ^property[=].description = "untere Altersgrenze für eine Schlüsselnummer. 999 = irrelevant, zur Interpretation sei auf die Metadaten in den ICD-10-Versionen 2009 bis 2017 verwiesen."
* ^property[=].uri = "https://www.bfarm.de/SharedDocs/Downloads/DE/Kodiersysteme/klassifikationen/icd-10-gm/vorgaenger-bis-2020/icd10gm2013_zip.html"

RuleSet: icd10gm_meta_property_AgeLowDiff
* ^property[+].code = #AgeLowDiff
* ^property[=].type = #string
* ^property[=].description = "untere Altersgrenze für eine Schlüsselnummer, alternatives Format. 9999 = irrelevant; t000 - t365 = 0 Tage bis unter 1 Jahr; j001 - j124 = 1 Jahr bis unter 124 Jahre"

RuleSet: icd10gm_meta_property_AgeReject
* ^property[+].code = #AgeReject
* ^property[=].type = #string
* ^property[=].description = "Art des Fehlers bei Altersbezug; 9 = irrelevant; M = Muss-Fehler; K = Kann-Fehler"

RuleSet: icd10gm_meta_property_Content
* ^property[+].code = #Content
* ^property[=].type = #boolean
* ^property[=].description = "Schlüsselnummer mit Inhalt belegt?"

RuleSet: icd10gm_meta_property_EBMLabor
* ^property[+].code = #EBMLabor
* ^property[=].type = #boolean
* ^property[=].description = "IfSG-Labor, kennzeichnet, dass bei Laboruntersuchungen zu diesen Diagnosen die Laborausschlussziffer des EBM (32006) gewählt werden kann."

RuleSet: icd10gm_meta_property_Exotic
* ^property[+].code = #Exotic
* ^property[=].type = #string
* ^property[=].description = "Krankheit in Mitteleuropa sehr selten?"

RuleSet: icd10gm_meta_property_Infectious
* ^property[+].code = #Infectious
* ^property[=].type = #boolean
* ^property[=].description = "IfSG-Meldung, kennzeichnet, dass bei Diagnosen, die mit dieser Schlüsselnummer kodiert sind, besonders auf die Arzt-Meldepflicht nach dem Infektionsschutzgesetz (IfSG) hinzuweisen ist."

RuleSet: icd10gm_meta_property_MorbLCode
* ^property[+].code = #MorbLCode
* ^property[=].description = "Bezug zur Morbiditätsliste"
* ^property[=].type = #string

RuleSet: icd10gm_meta_property_MortBCode
* ^property[+].code = #MortBCode
* ^property[=].description = "Bezug zur Morbiditätsliste"
* ^property[=].type = #string

RuleSet: icd10gm_meta_property_MortL1Code
* ^property[+].code = #MortL1Code
* ^property[=].type = #string
* ^property[=].description = "Bezug zur Mortalitätsliste 1"

RuleSet: icd10gm_meta_property_MortL2Code
* ^property[+].code = #MortL2Code
* ^property[=].type = #string
* ^property[=].description = "Bezug zur Mortalitätsliste 2"

RuleSet: icd10gm_meta_property_MortL3Code
* ^property[+].code = #MortL3Code
* ^property[=].type = #string
* ^property[=].description = "Bezug zur Mortalitätsliste 3"

RuleSet: icd10gm_meta_property_MortL4Code
* ^property[+].code = #MortL4Code
* ^property[=].type = #string
* ^property[=].description = "Bezug zur Mortalitätsliste 4"

RuleSet: icd10gm_meta_property_Para295
* ^property[+].code = #Para295
* ^property[=].type = #string
* ^property[=].description = "Verwendung der Schlüsselnummer nach Paragraph 295 SGB V. P = zur Primärverschlüsselung zugelassene Schlüsselnummer; O = nur als Sternschlüsselnummer zugelassen; Z = nur als Ausrufezeichenschlüsselnummer zugelassen; V = nicht zur Verschlüsselung zugelassen"

RuleSet: icd10gm_meta_property_Para301
* ^property[+].code = #Para301
* ^property[=].type = #string
* ^property[=].description = "Verwendung der Schlüsselnummer nach Paragraph 301 SGB V. P = zur Primärverschlüsselung zugelassene Schlüsselnummer; O = nur als Sternschlüsselnummer zugelassen; Z = nur als Ausrufezeichenschlüsselnummer zugelassen; V = nicht zur Verschlüsselung zugelassen"

RuleSet: icd10gm_meta_property_RareDisease
* ^property[+].code = #RareDisease
* ^property[=].type = #string
* ^property[=].description = "Krankheit in Mitteleuropa sehr selten?"

RuleSet: icd10gm_meta_property_SexCode
* ^property[+].code = #SexCode
* ^property[=].type = #string
* ^property[=].description = "Geschlechtsbezug der Schlüsselnummer; 9 = kein Geschlechtsbezug; M = männlich; W = weiblich"

RuleSet: icd10gm_meta_property_SexReject
* ^property[+].code = #SexReject
* ^property[=].type = #string
* ^property[=].description = "Art des Fehlers bei Geschlechtsbezug; 9 = irrelevant; K = Kann-Fehler"

RuleSet: icd10gm_propgroup_agediff
* insert icd10gm_meta_property_AgeHigh_with_alternative_format
* insert icd10gm_meta_property_AgeHighDiff
* insert icd10gm_meta_property_AgeLow_with_alternative_format
* insert icd10gm_meta_property_AgeLowDiff
* insert icd10gm_meta_property_AgeReject

RuleSet: icd10gm_propgroup_age_without_diff
* insert icd10gm_meta_property_AgeHigh
* insert icd10gm_meta_property_AgeLow
* insert icd10gm_meta_property_AgeReject

RuleSet: icd10gm_propgroup_MortB_MortL
* insert icd10gm_meta_property_MortBCode
* insert icd10gm_meta_property_MortL1Code
* insert icd10gm_meta_property_MortL2Code
* insert icd10gm_meta_property_MortL3Code
* insert icd10gm_meta_property_MortL4Code

RuleSet: icd10gm_propgroup_MorbL_MortL
* insert icd10gm_meta_property_MorbLCode
* insert icd10gm_meta_property_MortL1Code
* insert icd10gm_meta_property_MortL2Code
* insert icd10gm_meta_property_MortL3Code
* insert icd10gm_meta_property_MortL4Code

RuleSet: icd10gm-metadata_2009-2017
* insert icd10gm_propgroup_agediff
* insert icd10gm_meta_property_Content
* insert icd10gm_meta_property_EBMLabor
* insert icd10gm_meta_property_Infectious
* insert icd10gm_propgroup_MortB_MortL
* insert icd10gm_meta_property_Para295
* insert icd10gm_meta_property_Para301
* insert icd10gm_meta_property_RareDisease
* insert icd10gm_meta_property_SexCode
* insert icd10gm_meta_property_SexReject

RuleSet: icd10gm-metadata_2018-2019
* insert icd10gm_propgroup_age_without_diff
* insert icd10gm_meta_property_Content
* insert icd10gm_meta_property_EBMLabor
* insert icd10gm_meta_property_Infectious
* insert icd10gm_propgroup_MortB_MortL
* insert icd10gm_meta_property_Para295
* insert icd10gm_meta_property_Para301
* insert icd10gm_meta_property_RareDisease
* insert icd10gm_meta_property_SexCode
* insert icd10gm_meta_property_SexReject

RuleSet: icd10gm-metadata_2020
* insert icd10gm_propgroup_age_without_diff
* insert icd10gm_meta_property_Content
* insert icd10gm_meta_property_EBMLabor
* insert icd10gm_meta_property_Exotic
* insert icd10gm_meta_property_Infectious
* insert icd10gm_propgroup_MortB_MortL
* insert icd10gm_meta_property_Para295
* insert icd10gm_meta_property_Para301
* insert icd10gm_meta_property_SexCode
* insert icd10gm_meta_property_SexReject

RuleSet: icd10gm-metadata_2021-2024
* insert icd10gm_propgroup_age_without_diff
* insert icd10gm_meta_property_Content
* insert icd10gm_meta_property_EBMLabor
* insert icd10gm_meta_property_Exotic
* insert icd10gm_meta_property_Infectious
* insert icd10gm_propgroup_MorbL_MortL
* insert icd10gm_meta_property_Para295
* insert icd10gm_meta_property_Para301
* insert icd10gm_meta_property_SexCode
* insert icd10gm_meta_property_SexReject

RuleSet: icd10gm-babelfsh(input, oid)
* ^identifier[+].system = "urn:ietf:rfc:3986"
* ^identifier[=].value = "urn:oid:{oid}"
/*^babelfsh
claml-bfarm
  --mode=ICD_10_GM
  --path="{input}"
  --map-all-meta-to-properties
^babelfsh*/

// Note: The ClaML for version 2009 contains a 'Byte Order Mark' as the first three bytes of the file (0xEF 0xBB 0xBF).
// This results in a warning, but the file is still processed correctly.
CodeSystem: Icd10Gm
Id: icd-10-gm-2009
Description: "The International Statistical Classification Of Diseases And Related Health Problems, 10th revision, German Modification (ICD-10-GM) is the official classification for the encoding of diagnoses in inpatient and outpatient medical care in Germany."
Title: "ICD-10-GM"
* insert icd10gm-metadata("2009")
* insert icd10gm-metadata_2009-2017
* insert icd10gm-babelfsh("./input-files/ICD-10-GM/icd10gm2009/x1gex2009/Klassifikationsdateien/icd10gm2009syst_claml_20080929.xml", "1.2.276.0.76.5.356")

CodeSystem: Icd10Gm
Id: icd-10-gm-2010
Description: "The International Statistical Classification Of Diseases And Related Health Problems, 10th revision, German Modification (ICD-10-GM) is the official classification for the encoding of diagnoses in inpatient and outpatient medical care in Germany."
Title: "ICD-10-GM"
* insert icd10gm-metadata("2010")
* insert icd10gm-metadata_2009-2017
* insert icd10gm-babelfsh("./input-files/ICD-10-GM/icd10gm2010/x1gec2010/Klassifikationsdateien/icd10gm2010syst_claml_20091019.xml", "1.2.276.0.76.5.384")

CodeSystem: Icd10Gm
Id: icd-10-gm-2011
Description: "The International Statistical Classification Of Diseases And Related Health Problems, 10th revision, German Modification (ICD-10-GM) is the official classification for the encoding of diagnoses in inpatient and outpatient medical care in Germany."
Title: "ICD-10-GM"
* insert icd10gm-metadata("2011")
* insert icd10gm-metadata_2009-2017
* insert icd10gm-babelfsh("./input-files/ICD-10-GM/icd10gm2011/x1gec2011/Klassifikationsdateien/icd10gm2011syst_claml_20100924.xml", "1.2.276.0.76.5.388")

CodeSystem: Icd10Gm
Id: icd-10-gm-2012
Description: "The International Statistical Classification Of Diseases And Related Health Problems, 10th revision, German Modification (ICD-10-GM) is the official classification for the encoding of diagnoses in inpatient and outpatient medical care in Germany."
Title: "ICD-10-GM"
* insert icd10gm-metadata("2012")
* insert icd10gm-metadata_2009-2017
* insert icd10gm-babelfsh("./input-files/ICD-10-GM/icd10gm2012/x1gec2012/Klassifikationsdateien/icd10gm2012syst_claml_20110923.xml", "1.2.276.0.76.5.409")

CodeSystem: Icd10Gm
Id: icd-10-gm-2013
Description: "The International Statistical Classification Of Diseases And Related Health Problems, 10th revision, German Modification (ICD-10-GM) is the official classification for the encoding of diagnoses in inpatient and outpatient medical care in Germany."
Title: "ICD-10-GM"
* insert icd10gm-metadata("2013")
* insert icd10gm-metadata_2009-2017
* insert icd10gm-babelfsh("./input-files/ICD-10-GM/icd10gm2013/x1gec2013/Klassifikationsdateien/icd10gm2013syst_claml_20120921.xml", "1.2.276.0.76.5.413")

CodeSystem: Icd10Gm
Id: icd-10-gm-2014
Description: "The International Statistical Classification Of Diseases And Related Health Problems, 10th revision, German Modification (ICD-10-GM) is the official classification for the encoding of diagnoses in inpatient and outpatient medical care in Germany."
Title: "ICD-10-GM"
* insert icd10gm-metadata("2014")
* insert icd10gm-metadata_2009-2017
* insert icd10gm-babelfsh("./input-files/ICD-10-GM/icd10gm2014/x1gec2014/Klassifikationsdateien/icd10gm2014syst_claml_20130920.xml", "1.2.276.0.76.5.417")

CodeSystem: Icd10Gm
Id: icd-10-gm-2015
Description: "The International Statistical Classification Of Diseases And Related Health Problems, 10th revision, German Modification (ICD-10-GM) is the official classification for the encoding of diagnoses in inpatient and outpatient medical care in Germany."
Title: "ICD-10-GM"
* insert icd10gm-metadata("2015")
* insert icd10gm-metadata_2009-2017
* insert icd10gm-babelfsh("./input-files/ICD-10-GM/icd10gm2015/x1gec2015/Klassifikationsdateien/icd10gm2015syst_claml_20140919.xml", "1.2.276.0.76.5.424")

CodeSystem: Icd10Gm
Id: icd-10-gm-2016
Description: "The International Statistical Classification Of Diseases And Related Health Problems, 10th revision, German Modification (ICD-10-GM) is the official classification for the encoding of diagnoses in inpatient and outpatient medical care in Germany."
Title: "ICD-10-GM"
* insert icd10gm-metadata("2016")
* insert icd10gm-metadata_2009-2017
* insert icd10gm-babelfsh("./input-files/ICD-10-GM/icd10gm2016/x1gec2016/Klassifikationsdateien/icd10gm2016syst_claml_20150925.xml", "1.2.276.0.76.5.430")

CodeSystem: Icd10Gm
Id: icd-10-gm-2017
Description: "The International Statistical Classification Of Diseases And Related Health Problems, 10th revision, German Modification (ICD-10-GM) is the official classification for the encoding of diagnoses in inpatient and outpatient medical care in Germany."
Title: "ICD-10-GM"
* insert icd10gm-metadata("2017")
* insert icd10gm-metadata_2009-2017
* insert icd10gm-babelfsh("./input-files/ICD-10-GM/icd10gm2017/x1gec2017/x1gec2017/Klassifikationsdateien/icd10gm2017syst_claml_20160923.xml", "1.2.276.0.76.5.463")

CodeSystem: Icd10Gm
Id: icd-10-gm-2018
Description: "The International Statistical Classification Of Diseases And Related Health Problems, 10th revision, German Modification (ICD-10-GM) is the official classification for the encoding of diagnoses in inpatient and outpatient medical care in Germany."
Title: "ICD-10-GM"
* insert icd10gm-metadata("2018")
* insert icd10gm-metadata_2018-2019
* insert icd10gm-babelfsh("./input-files/ICD-10-GM/icd10gm2018/x1gec2018/Klassifikationsdateien/icd10gm2018syst_claml_20170922.xml", "1.2.276.0.76.5.471")

CodeSystem: Icd10Gm
Id: icd-10-gm-2019
Description: "The International Statistical Classification Of Diseases And Related Health Problems, 10th revision, German Modification (ICD-10-GM) is the official classification for the encoding of diagnoses in inpatient and outpatient medical care in Germany."
Title: "ICD-10-GM"
* insert icd10gm-metadata("2019")
* insert icd10gm-metadata_2018-2019
* insert icd10gm-babelfsh("./input-files/ICD-10-GM/icd10gm2019/icd10gm2019syst-claml/Klassifikationsdateien/icd10gm2019syst_claml_20180921.xml", "1.2.276.0.76.5.477")

CodeSystem: Icd10Gm
Id: icd-10-gm-2020
Description: "The International Statistical Classification Of Diseases And Related Health Problems, 10th revision, German Modification (ICD-10-GM) is the official classification for the encoding of diagnoses in inpatient and outpatient medical care in Germany."
Title: "ICD-10-GM"
* insert icd10gm-metadata("2020")
* insert icd10gm-metadata_2020
* insert icd10gm-babelfsh("./input-files/ICD-10-GM/icd10gm2020/icd10gm2020syst-claml/Klassifikationsdateien/icd10gm2020syst_claml_20190920.xml", "1.2.276.0.76.5.486")

CodeSystem: Icd10Gm
Id: icd-10-gm-2021
Description: "The International Statistical Classification Of Diseases And Related Health Problems, 10th revision, German Modification (ICD-10-GM) is the official classification for the encoding of diagnoses in inpatient and outpatient medical care in Germany."
Title: "ICD-10-GM"
* insert icd10gm-metadata("2021")
* insert icd10gm-metadata_2021-2024
* insert icd10gm-babelfsh("./input-files/ICD-10-GM/icd10gm2021/icd10gm2021syst-claml-20201111/Klassifikationsdateien/icd10gm2021syst_claml_20200918_20201111.xml", "1.2.276.0.76.5.502")

CodeSystem: Icd10Gm
Id: icd-10-gm-2022
Description: "The International Statistical Classification Of Diseases And Related Health Problems, 10th revision, German Modification (ICD-10-GM) is the official classification for the encoding of diagnoses in inpatient and outpatient medical care in Germany."
Title: "ICD-10-GM"
* insert icd10gm-metadata("2022")
* insert icd10gm-metadata_2021-2024
* insert icd10gm-babelfsh("./input-files/ICD-10-GM/icd10gm2022/icd10gm2022syst-claml/Klassifikationsdateien/icd10gm2022syst_claml_20210917.xml", "1.2.276.0.76.5.518")

CodeSystem: Icd10Gm
Id: icd-10-gm-2023
Description: "The International Statistical Classification Of Diseases And Related Health Problems, 10th revision, German Modification (ICD-10-GM) is the official classification for the encoding of diagnoses in inpatient and outpatient medical care in Germany."
Title: "ICD-10-GM"
* insert icd10gm-metadata("2023")
* insert icd10gm-metadata_2021-2024
* insert icd10gm-babelfsh("./input-files/ICD-10-GM/icd10gm2023/icd10gm2023syst-claml/Klassifikationsdateien/icd10gm2023syst_claml_20220916.xml", "1.2.276.0.76.5.530")

CodeSystem: Icd10Gm
Id: icd-10-gm-2024
Description: "The International Statistical Classification Of Diseases And Related Health Problems, 10th revision, German Modification (ICD-10-GM) is the official classification for the encoding of diagnoses in inpatient and outpatient medical care in Germany."
Title: "ICD-10-GM"
* insert icd10gm-metadata("2024")
* insert icd10gm-metadata_2021-2024
* insert icd10gm-babelfsh("./input-files/ICD-10-GM/icd10gm2024/icd10gm2024syst-claml/Klassifikationsdateien/icd10gm2024syst_claml_20230915.xml", "1.2.276.0.76.5.537")
