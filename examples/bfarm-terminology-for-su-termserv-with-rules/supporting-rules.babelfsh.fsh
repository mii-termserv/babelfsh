Alias: $sutermserv_project = https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-project
Alias: $sutermserv_dataset = https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-dataset
Alias: $sutermserv_license = https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-license

RuleSet: bfarm-sutermserv-tags
* ^meta.tag[+].system = "$sutermserv_project"
* ^meta.tag[=].code = "bfarm"
* ^meta.tag[=].display = "Bundesinstitut für Arzneimittel und Medizinprodukte"
* ^meta.tag[+].system = "$sutermserv_dataset"
* ^meta.tag[=].code = "bfarm"
* ^meta.tag[=].display = "Bundesinstitut für Arzneimittel und Medizinprodukte"
* ^meta.tag[+].system = "$sutermserv_license"
* ^meta.tag[=].code = "bfarm"
* ^meta.tag[=].display = "BfArM license terms"