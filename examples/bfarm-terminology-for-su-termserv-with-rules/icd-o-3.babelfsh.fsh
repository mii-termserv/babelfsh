RuleSet: icd-o-3-metadata(url, version, input)
* ^meta.tag[+].system = "$sutermserv_project"
* ^meta.tag[=].code = "who"
* ^meta.tag[=].display = "World Health Organization"
* ^meta.tag[+].system = "$sutermserv_dataset"
* ^meta.tag[=].code = "who"
* ^meta.tag[=].display = "World Health Organization"
* insert bfarm-sutermserv-tags
* ^url = "{url}"
* ^version = "{version}"
* ^copyright = "Die Erstellung erfolgt unter Verwendung der maschinenlesbaren Fassung des Bundesinstituts für Arzneimittel und Medizinprodukte (BfArM). ICD-O-3-Kodes, -Begriffe und -Texte © Bundesinstitut für Arzneimittel und Medizinprodukte (BfArM), übersetzt von der International classification of diseases for oncology, 3rd edition – ICD-O-3, herausgegeben durch die Weltgesundheitsorganisation. Die ICD-O-3 muss so genutzt werden, wie in der Klassifikation und in den einführenden Kapiteln und in den Anhängen zur ICD-O-3 beschrieben."
* ^status = #active
* ^experimental = false
* ^caseSensitive = false
* ^content = #complete
* ^hierarchyMeaning = #classified-with
* ^identifier[+].system = "urn:ietf:rfc:3986"
* ^identifier[=].value = "urn:oid:2.16.840.1.113883.6.43.1"
* ^property[+].code = #kind
* ^property[=].type = #string
* ^property[=].description = "Art der Schlüsselnummer, eines von: [chapter, block, category]"
* ^property[+].code = #inclusion
* ^property[=].type = #string
* ^property[=].description = "Ein Inklusisionskriterium"
* ^property[+].code = #exclusion
* ^property[=].type = #string
* ^property[=].description = "Ein Exklusionskriterium"
* ^property[+].code = #note
* ^property[=].type = #string
* ^property[=].description = "Eine Anmerkung zur Verwendung der Gruppe"
/*^babelfsh
claml-bfarm
  --mode=ICD_O_3
  --path="{input}"
^babelfsh*/


CodeSystem: IcdO3ErsteRevision
Id: icd-o-3-erste-revision
Title: "ICD-O-3 Erste Revision 2014"
Description: "For the encoding of neoplasms in cancer Germany uses the German-language edition of the International Classification of Diseases for Oncology, third edition (ICD-O-3), first published by the World Health Organisation (WHO) in 2000."
* insert icd-o-3-metadata("http://terminology.hl7.org/CodeSystem/icd-o-3", "2014", "./input-files/ICD-O/icdo3rev1ec2014/Klassifikationsdateien/icdo3rev1_2014syst_claml_20140227.xml")

CodeSystem: IcdO3ZweiteRevision
Id: icd-o-3-zweite-revision
Title: "ICD-O-3 Zweite Revision 2019"
Description: "For the encoding of neoplasms in cancer Germany uses the German-language edition of the International Classification of Diseases for Oncology, third edition (ICD-O-3), first published by the World Health Organisation (WHO) in 2000."
* insert icd-o-3-metadata("http://terminology.hl7.org/CodeSystem/icd-o-3", "2019", "./input-files/ICD-O/icdo3rev2-2019syst-claml-20210129/Klassifikationsdateien/icdo3rev2_2019syst_claml_20210129.xml")

CodeSystem: IcdO3ZweiteRevisionOid
Id: icd-o-3-zweite-revision-oid
Title: "ICD-O-3 Zweite Revision 2019"
Description: "For the encoding of neoplasms in cancer Germany uses the German-language edition of the International Classification of Diseases for Oncology, third edition (ICD-O-3), first published by the World Health Organisation (WHO) in 2000."
* insert icd-o-3-metadata("urn:oid:2.16.840.1.113883.6.43.1", "2019", "./input-files/ICD-O/icdo3rev2-2019syst-claml-20210129/Klassifikationsdateien/icdo3rev2_2019syst_claml_20210129.xml")
