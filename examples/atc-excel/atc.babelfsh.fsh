Alias: $sutermserv_project = https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-project
Alias: $sutermserv_dataset = https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-dataset
Alias: $sutermserv_license = https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-license

RuleSet: bfarm-sutermserv-tags
* ^meta.tag[+].system = "$sutermserv_project"
* ^meta.tag[=].code = "bfarm"
* ^meta.tag[=].display = "Bundesinstitut für Arzneimittel und Medizinprodukte"
* ^meta.tag[+].system = "$sutermserv_dataset"
* ^meta.tag[=].code = "bfarm"
* ^meta.tag[=].display = "Bundesinstitut für Arzneimittel und Medizinprodukte"
* ^meta.tag[+].system = "$sutermserv_license"
* ^meta.tag[=].code = "bfarm"
* ^meta.tag[=].display = "BfArM license terms"

RuleSet: atc-babelfsh(version, input_file, sheet)
* ^url = "http://fhir.de/CodeSystem/bfarm/atc"
* insert bfarm-sutermserv-tags
* ^meta.profile = "http://hl7.org/fhir/StructureDefinition/shareablecodesystem"
* ^version = "{version}"
* ^status = #active
* ^experimental = false
* ^caseSensitive = false
* ^content = #complete
* ^publisher = "Wissenschaftliches Institut der AOK (WIdO)"
* ^copyright = "Die Erstellung erfolgt unter Verwendung der maschinenlesbaren Fassung des Wissenschaftlichen Institutes der AOK (WiDO)."
* ^property[+].code = "ddd"
* ^property[=].description = "Die definierte Tagesdosis (defined daily dose, DDD) ist ein Maß für die verordnete Arzneimittelmenge. Die DDD basiert auf der Menge eines Wirkstoffs, die typischerweise in der Hauptindikation bei Erwachsenen pro Tag angewendet wird."
* ^property[=].type = "string"
* ^property[=].uri = "https://www.wido.de/publikationen-produkte/analytik/arzneimittel-klassifikation"
/*^babelfsh
atc-ddd --file={input_file} --sheet="{sheet}" --ddd-property-column=4 --ddd-property-code="ddd"
^babelfsh*/

CodeSystem: AtcAmtlich
Id: atc-amtlich-2024
Title: "Anatomisch-therapeutisch chemische Klassifikation (ATC) Amtliche deutsche Fassung"
Description: "Anatomisch-therapeutisch-chemische-Klassifikation mit Tagesdosen Amtliche Fassung des ATC-Index mit DDD-Angaben für Deutschland"
* insert atc-babelfsh("2024", "./input-files/atc-2024-amtlich.xlsm", "amtl.Index 2024 ATC-sortiert")