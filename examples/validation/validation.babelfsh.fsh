CodeSystem: VALIDATION_ERROR
Title: "Validation Error Demonstration"
Id: validation-error
Description: "This CS demonstrates the validation of FSH paths in the BabelFSH app"
* ^canonical = "http://purl.obolibrary.org/obo/hp.fhir"
// note: not url
* ^valueset = "http://purl.obolibrary.org/obo/hp.fhir?vs"
// note: not valueSet
* ^businessVersion = "2024017"
// note: not version
* ^property[+].code = #imported
* ^property[=].description = "Whether the concept is from the focus ontology or imported from another ontology"
* ^property[=].type = #not-a-type
// note type
* ^property[+].code = #root
* ^property[=].description = "Whether the concept is a root concept"
* ^property[=].type = #boolean
* ^property[+].code = deprecated
* ^property[=].description = "Whether the concept is deprecated"
* ^property[=].type = #boolean
/*^babelfsh
owl
  --file=./input-files/hp.owl
  --main-ns="http://purl.obolibrary.org/obo/HP_"
  --synonyms-property="http://www.geneontology.org/formats/oboInOwl#hasExactSynonym"
  --iri-mappings="./iri_mappings.csv"
  --code-replace="_,:"
^babelfsh*/