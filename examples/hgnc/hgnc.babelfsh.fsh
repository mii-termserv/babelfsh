CodeSystem: HGNC_Gene_Names
Id: hgnc-gene-names
Title: "HGNC Complete Set Gene Names"
Description: "The HGNC is responsible for approving unique symbols and names for human loci, including protein coding genes, ncRNA genes and pseudogenes, to allow unambiguous scientific communication."
* ^url = "http://www.genenames.org/geneId"
* ^valueSet = "http://www.genenames.org/geneId?vs"
* ^date = "2024-07-02"
* ^version = "20240702"
/*^babelfsh
csv
  --url="https://ftp.ebi.ac.uk/pub/databases/genenames/hgnc/archive/quarterly/tsv/hgnc_complete_set_2024-07-02.txt"
  --code-column="hgnc_id"
  --display-column="symbol"
  --definition-column="name"
  --delimiter='\t'
^babelfsh*/

CodeSystem: HGNC_Gene_Groups
Id: hgnc-gene-groups
Title: "HGNC Complete Set Gene Groups"
Description: "The HGNC is responsible for approving unique symbols and names for human loci, including protein coding genes, ncRNA genes and pseudogenes, to allow unambiguous scientific communication. The HGNC strongly encourages naming families and groups of genes related by sequence and/or function using a “root” symbol. This is an efficient and informative way to name related genes, and already works well for a number of established gene groups."
* ^url = "http://www.genenames.org/genegroup"
* ^valueSet = "http://www.genenames.org/genegroup?vs"
* ^date = "2024-07-02"
* ^version = "20240702"
/*^babelfsh
csv
  --url="https://ftp.ebi.ac.uk/pub/databases/genenames/hgnc/archive/quarterly/tsv/hgnc_complete_set_2024-07-02.txt"
  --code-column="gene_group_id"
  --display-column="gene_group"
  --delimiter='\t'
  --allow-missing-code-column
  --filter-duplicates
  --split-code-and-display-using="|"
^babelfsh*/