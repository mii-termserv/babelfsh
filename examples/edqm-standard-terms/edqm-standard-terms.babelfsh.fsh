RuleSet: EDQM
* ^version = "20240708"
* ^date = "2024-07-08"
* ^content = #complete
* ^experimental = false
* ^publisher = "European Directorate for the Quality of Medicines & HealthCare, Council of Europe (EDQM)"
* ^copyright = "This FHIR resource reproduces the content of the EDQM Standard Terms database, available at http://standardterms.edqm.eu, and is reproduced with the permission of the European Directorate for the Quality of Medicines & HealthCare, Council of Europe (EDQM)."
* ^status = #active

CodeSystem: EDQM_Standard_Terms_Classes
Id: edqm-standard-terms-classes
Title: "EDQM Standard Terms Classes"
Description: "This CodeSystem represents the set of classes that are available in the EDQM Standard Terms database. Each concept is assigned a class from this CodeSystem."
* ^url = "http://standardterms.edqm.eu/classes"
* insert EDQM
/*^babelfsh
edqm
  --username-env="EDQM_USERNAME"
  --api-key-env="EDQM_API_KEY"
  --mode-class-codes
^babelfsh*/

CodeSystem: EDQM_Standard_Terms
Id: edqm-standard-terms
Title: "EDQM Standard Terms"
Description: "EDQM Standard Terms"
* ^url = "http://standardterms.edqm.eu"
* ^valueSet = "http://standardterms.edqm.eu?vs"
* insert EDQM
* ^property[+].code = #class
* ^property[=].description = "The class of the term"
* ^property[=].type = #string
* ^property[=].uri = "http://standardterms.edqm.eu/class"
* ^property[+].code = #domain
* ^property[=].description = "The domain of the term"
* ^property[=].type = #string
* ^property[+].code = #modificationDate
* ^property[=].description = "The date when the term was last modified"
* ^property[=].type = #dateTime
* ^property[+].code = #creationDate
* ^property[=].description = "The date when the term was created"
* ^property[=].type = #dateTime
* ^property[+].code = #child
* ^property[=].description = "Links this concept to another concept in the EDQM Standard Terms database"
* ^property[=].type = #code
* ^property[+].code = #version
* ^property[=].description = "The version of the term"
* ^property[=].type = #integer
* ^property[+].code = #status
* ^property[=].description = "The status of the term"
* ^property[=].type = #string
/*^babelfsh
edqm
  --username-env="EDQM_USERNAME"
  --api-key-env="EDQM_API_KEY"
  --language="de"
  --mode-standard-terms
  --version-property="version"
  --modification-date-property="modificationDate"
  --creation-date-property="creationDate"
  --link-property="child"
^babelfsh*/
