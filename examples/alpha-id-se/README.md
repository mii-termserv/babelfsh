# Example files for converting Alpha-ID-SE to FHIR

## Download source

https://www.bfarm.de/DE/Kodiersysteme/Services/Downloads/_node.html

Download the required files, and put the extracted CSV files in `input-files`.
