CodeSystem: AlphaId
Id: alphaid-2023
Title: "Alpha-ID-SE"
Description: "The Alpha-ID is a sequential and stable identification number, which is allocated to each entry in the alphabetical index. It permits the encoding of medical and natural language diagnostic terms."
* ^url = "http://fhir.de/CodeSystem/bfarm/alpha-id"
* ^identifier.system = "urn:ietf:rfc:3986"
* ^identifier.value = "urn:oid:1.2.276.0.76.5.538"
* ^meta.profile = "http://hl7.org/fhir/StructureDefinition/shareablecodesystem"
* ^copyright = "Die Erstellung erfolgt unter Verwendung der maschinenlesbaren Fassung des Bundesinstituts für Arzneimittel und Medizinprodukte (BfArM)."
* ^version = "2023"
* ^status = #active
* ^experimental = false
* ^publisher = "Bundesinstitut für Arzneimittel und Medizinprodukte (BfArM)"
* ^property[+].code = #icd_10_primaer
* ^property[=].type = #string
* ^property[=].description = "Der ICD-Code aus der Alpha-ID"
* ^property[+].code = #icd_10_stern
* ^property[=].type  = #string
* ^property[=].description = "Der Sterncode aus der Alpha-ID"
* ^property[+].code = #icd_10_zusatz
* ^property[=].type = #string
* ^property[=].description = "Der Sekundär-Code (mit Ausrufezeichen) aus der Alpha-ID"
* ^property[+].code = #icd_10_primaer2
* ^property[=].type = #string
* ^property[=].description = "Der zweite Primär-Code aus der Alpha-ID"
* ^property[+].code = #orpha
* ^property[=].type = #string
* ^property[=].description = "Der mit dem Konzept verknüpfte Orphanet-Code"
* ^property[=].uri = "http://www.orpha.net"
* ^property[+].code = #inactive
* ^property[=].type = #boolean
* ^property[=].description = "Gibt an, ob der Eintrag gültig ist"
* ^property[=].uri = "http://hl7.org/fhir/concept-properties#inactive"
/*^babelfsh 
csv --path='./input-files/icd10gm2023_alphaidse_edvtxt_20220930.txt'
--headers=["gueltig", "code", "icd_10", "stern", "ausrufezeichen", "icd_10_2", "orpha", "display"]
--delimiter='|' 
--charset=UTF-8 
--code-column=code 
--display-column='display'
--property-mapping=[{"column":"icd_10","property":"icd_10_primaer"},{"column":"stern","property":"icd_10_stern"},{"column":"ausrufezeichen","property":"icd_10_zusatz"},{"column":"icd_10_2","property":"icd_10_primaer2"},{"column":"orpha","property":"orpha"},{"column":"gueltig","property":"inactive","mapper":{"id":"boolean","arguments":{"true":"0","false":"1"}}}]
^babelfsh*/