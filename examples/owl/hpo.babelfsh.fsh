CodeSystem: HPO
Title: "Human Phenotype Ontology"
Id: hpo
Description: "The Human Phenotype Ontology (HPO) provides a standardized vocabulary of phenotypic abnormalities encountered in human disease. Each term in the HPO describes a phenotypic abnormality, such as Atrial septal defect. The HPO is currently being developed using the medical literature, Orphanet, DECIPHER, and OMIM. HPO currently contains over 13,000 terms and over 156,000 annotations to hereditary diseases."
* ^url = "http://purl.obolibrary.org/obo/hp.fhir"
* ^valueSet = "http://purl.obolibrary.org/obo/hp.fhir?vs"
* ^version = "2024017"
* ^property[+].code = #imported
* ^property[=].description = "Whether the concept is from the focus ontology or imported from another ontology"
* ^property[=].type = #boolean
* ^property[+].code = #root
* ^property[=].description = "Whether the concept is a root concept"
* ^property[=].type = #boolean
* ^property[+].code = #deprecated
* ^property[=].description = "Whether the concept is deprecated"
* ^property[=].type = #boolean
/*^babelfsh
owl
  --file=./input-files/hp.owl
  --main-ns="http://purl.obolibrary.org/obo/HP_"
  --synonyms-property="http://www.geneontology.org/formats/oboInOwl#hasExactSynonym"
  --iri-mappings="./iri_mappings.csv"
  --code-replace="_,:"
^babelfsh*/