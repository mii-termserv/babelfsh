#!/bin/bash

if [ -z "$1" ]; then
  echo "Usage: $0 <example-path>"
  exit 1
fi

EXAMPLE_PATH=$1
mkdir -pv "$EXAMPLE_PATH"
INPUT_FILES_PATH="$EXAMPLE_PATH/input_files"
OUTPUT_FILES_PATH="$EXAMPLE_PATH/output_files"
mkdir -pv "$INPUT_FILES_PATH"
cat << EOF > "$INPUT_FILES_PATH/.gitignore"
**
!.gitignore
EOF

mkdir -pv "$OUTPUT_FILES_PATH"
cat << EOF > "$OUTPUT_FILES_PATH/.gitignore"
**
!.gitignore
EOF

cat << EOF > "$EXAMPLE_PATH/README.md"
# Example: $(basename "$EXAMPLE_PATH")
EOF

echo "Created example at $EXAMPLE_PATH"
