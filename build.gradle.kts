@file:Suppress("LocalVariableName")

import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import org.springframework.boot.gradle.tasks.bundling.BootJar


plugins {

    val kotlinVersion = "2.0.21"

    kotlin("jvm") version kotlinVersion
    antlr
    kotlin("plugin.serialization") version kotlinVersion
    id("org.springframework.boot") version "3.3.5"
    id("io.spring.dependency-management") version "1.1.5"
    kotlin("plugin.spring") version kotlinVersion
    id("org.cyclonedx.bom") version "1.10.0"
}

group = "de.mii-termserv"
version = "1.1.2-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    testImplementation("org.jetbrains.kotlin:kotlin-test")
}

tasks.test {
    useJUnitPlatform()
}
kotlin {
    jvmToolchain(21)
}

dependencies {

    val antlr_version: String by project
    val hapi_fhir_version: String by project
    val apache_poi_version: String by project
    val kotest_version: String by project
    val ktor_version: String by project
    val owlapi_version: String by project
    val jfact_version: String by project
    val elk_version: String by project
    val hoplite_version: String by project

    implementation("org.jetbrains.kotlin:kotlin-reflect")

    implementation("org.springframework.boot:spring-boot-starter")
    // implementation("org.springframework.boot:spring-boot-starter-web")
    testImplementation("org.springframework.boot:spring-boot-starter-test")
    testImplementation("org.jetbrains.kotlin:kotlin-test-junit5")
    testRuntimeOnly("org.junit.platform:junit-platform-launcher")

    antlr("org.antlr:antlr4:$antlr_version")
    implementation("org.antlr:antlr4-runtime:$antlr_version")

    implementation("com.github.ajalt.clikt:clikt:4.4.0")
    implementation("com.xenomachina:kotlin-argparser:2.0.7")
    // two command line parsers, because the app command line requires subcommands, which argparser doesn't have;
    // but Clikt is not very suitable for the plugin command line in the comments in the FSH files

    implementation("ca.uhn.hapi.fhir:hapi-fhir-base:$hapi_fhir_version") {
        exclude(group = "org.slf4j")
    }
    // region HAPI FHIR not vulnerable - GHSA-gr3c-q7xf-47vh does not apply since no XML parsing is done in this application using HAPI FHIR
    @Suppress("VulnerableLibrariesLocal", "RedundantSuppression")
    implementation("ca.uhn.hapi.fhir:hapi-fhir-structures-r4b:$hapi_fhir_version")
    @Suppress("VulnerableLibrariesLocal", "RedundantSuppression")
    implementation("ca.uhn.hapi.fhir:hapi-fhir-structures-r5:$hapi_fhir_version")
    // endregion


    implementation("com.github.doyaaaaaken:kotlin-csv-jvm:1.9.3")
    implementation("se.sawano.java:alphanumeric-comparator:1.4.1")
    implementation("org.slf4j:slf4j-api:2.0.13")
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.7.1")
    implementation("jakarta.xml.bind:jakarta.xml.bind-api:4.0.2")
    implementation("com.sun.xml.bind:jaxb-impl:4.0.5")
    implementation("org.jgrapht:jgrapht-core:1.5.2") {
        exclude("org.apfloat", "apfloat")
    }
    implementation("org.apfloat:apfloat:1.14.0")
    implementation("org.jgrapht:jgrapht-io:1.5.2")
    implementation("net.sf.saxon:Saxon-HE:12.5")
    implementation("org.xmlunit:xmlunit-core:2.10.0") //fixes CVE-2024-31573
    implementation("org.apache.poi:poi:$apache_poi_version")
    implementation("org.apache.poi:poi-ooxml:$apache_poi_version")
    implementation("org.apache.commons:commons-text:1.12.0")
    implementation("io.github.furstenheim:copy_down:1.1")
    implementation("org.apache.commons:commons-compress:1.26.2") //fixes CVE-2024-26308 and CVE-2024-25710
    implementation("commons-validator:commons-validator:1.9.0") {
        exclude("commons-collections", "commons-collections")
        // commons-collections 3.x is vulnerable to Cx78f40514-81ff, and as we don't really use it for the dependency, we exclude it
        // if needed, it might be 1:1 replaceable with commons-collections4
    }
    implementation("io.ktor:ktor-client-core:$ktor_version")
    implementation("io.ktor:ktor-client-cio:$ktor_version")
    implementation("io.ktor:ktor-client-content-negotiation:$ktor_version")
    implementation("io.ktor:ktor-serialization-kotlinx-json:$ktor_version")

    implementation("com.sksamuel.hoplite:hoplite-core:$hoplite_version")
    implementation("com.sksamuel.hoplite:hoplite-yaml:$hoplite_version")

    implementation("net.sourceforge.owlapi:owlapi-api:$owlapi_version")
    implementation("net.sourceforge.owlapi:owlapi-apibinding:$owlapi_version")
    implementation("net.sourceforge.owlapi:owlapi-distribution:$owlapi_version")
    implementation("net.sourceforge.owlapi:jfact:$jfact_version")
    implementation("com.google.guava:guava:33.2.1-jre")
    implementation("org.eclipse.rdf4j:rdf4j-util:3.7.7") // fixes CVE-2018-1000644 and CVE-2018-20227
    implementation("io.github.liveontologies:elk-owlapi:$elk_version")
    implementation("org.slf4j:log4j-over-slf4j:2.0.13")
    implementation("jline:jline:2.14.2")
    implementation("de.vandermeer:asciitable:0.3.2")

    testImplementation("io.kotest:kotest-runner-junit5:$kotest_version")
    testImplementation("io.kotest:kotest-assertions-core:$kotest_version")
    testImplementation("io.kotest:kotest-property:$kotest_version")
}

kotlin {
    compilerOptions {
        freeCompilerArgs.addAll("-Xjsr305=strict")
    }
}

tasks.withType<KotlinCompile>().configureEach {
    dependsOn(tasks.withType<AntlrTask>())
}

tasks.withType<Test>().configureEach {
    useJUnitPlatform()
}

tasks.withType<BootJar> {
    manifest {
        mainClass = "de.mii_termserv.babelfsh.BabelFshCliApplicationKt"
        attributes("Implementation-Title" to "BabelFSH", "Implementation-Version" to project.version)
    }
}

tasks.bootRun {
    mainClass = "de.mii_termserv.babelfsh.BabelFshCliApplicationKt"
}

sourceSets {
    main {
        antlr {
            srcDir("src/main/antlr")
        }
    }
}

tasks.generateGrammarSource {
    arguments = arguments + listOf("-visitor", "-package", "de.mii_termserv.babelfsh.antlr")
    outputDirectory = file("${projectDir}/src/main/java/de/mii_termserv/babelfsh/antlr")
}

tasks.cyclonedxBom {
    skipConfigs.add("kotlinNativeBundleConfiguration")
}