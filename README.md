# BabelFSH

BabelFSH is a tool to convert terminological artefacts to HL7 FHIR CodeSystem resources.

What sets it apart from purpose-built converters is the use of FSH (FHIR Shorthand) as the input format. This allows for
a more expressive and concise representation of the artefacts using a language that implementers are already familiar
with. We have found that converting any terminological artefact into FHIR basically always looks the same: first, you
define the metadata of the output resource. This may be done in an ugly hard-coded fashion, or sometimes in a more
organized way using configuration files etc. Then, when the metadata is defined, the actual content of the resource is
generated from the reference source that you are converting (most often, a file or set of files, sometimes a web API or
a database). This is where BabelFSH comes in: it allows you to define the metadata of the output resource in a concise
manner, and then generate the content of the resource from the source data in a way that is easy to understand and
implement using a specific plugin.

This tool is currently under very active development, and may have bugs. If you are interested in
contributing, please reach out to the project maintainer: [Joshua Wiedekopf](mailto:j.wiedekopf@uni-luebeck.de)

## Architecture

The FSH dialect used by BabelFSH is a compatible subset of the full FSH specification. Defining resources other than
CodeSystems (and eventually, ValueSets and ConceptMaps) is not supported by BabelFSH, since SUSHI very nicely fills that
gap.

> Note as of 2025-01-07: Currently, generating CodeSystems is supported. Preliminary support for ConceptMap generation
> using a CSV file
> is also now available, but due to the changes to ConceptMap between R4 and R5, this is not yet fully functional.
> ValueSet generation is not yet supported (the implementation for ConceptMap didn't require VS generation yet).

The BabelFSH architecture is based on a plugin system. Each plugin is responsible for converting a specific type of
input file to a set of concepts in a CodeSystem. You manually define the metadata using FSH files (with the
filename `.babel.fsh` for the time being). In the FSH files, you include a specifically-formatted comment as the last
name of the file, which tells BabelFSH which plugin to use. The plugin is then responsible for parsing the input file
and generating a list of concepts. The fully-converted resource is then output as a FHIR CodeSystem resource in JSON.

Here's a fully functional example for converting a plain-text (roughly CSV-like) file to a FHIR CodeSystem (this example
is taken from
the [alpha-id-se example in this repo](examples/bfarm-terminology-for-su-termserv-with-rules/alphaid-se.babelfsh.fsh):

```fsh
RuleSet: alphaid-se-metadata
* ^url = "http://fhir.de/CodeSystem/bfarm/alpha-id"
* ^status = #active
* ^experimental = false
* ^copyright = "Die Erstellung erfolgt unter Verwendung der maschinenlesbaren Fassung des Bundesinstituts für Arzneimittel und Medizinprodukte (BfArM)."
* ^property[+].code = #orpha
* ^property[=].type = #string
* ^property[=].description = "Der mit dem Konzept verknüpfte Orphanet-Code"
// further properties omitted for brevity

RuleSet: alphaid-se-babelfsh-2018ff(version, oid, path)
* ^version = "{version}"
* ^identifier[+].system = "urn:ietf:rfc:3986"
* ^identifier[=].value = "urn:oid:{oid}"
* insert alphaid-se-metadata
* ^property[+].code = #icd_10_primaer2
* ^property[=].type = #string
* ^property[=].description = "Der zweite Primär-Code aus der Alpha-ID"
/*^babelfsh
csv
  --path='{path}'
  --headers=["gueltig", "code", "icd_10", "stern", "ausrufezeichen", "icd_10_2", "orpha", "display"]
  --delimiter='|'
  --charset=UTF-8
  --code-column=code
  --display-column='display'
  --property-mapping=[{"column":"icd_10","property":"icd_10_primaer"},{"column":"stern","property":"icd_10_stern"},{"column":"ausrufezeichen","property":"icd_10_zusatz"},{"column":"icd_10_2","property":"icd_10_primaer2"},{"column":"orpha","property":"orpha"},{"column":"gueltig","property":"inactive","mapper":{"id":"boolean","arguments":{"true":"0","false":"1"}}}]
^babelfsh*/

CodeSystem: AlphaIdSe
Id: alphaid-se-2024
Title: "Alpha-ID-SE"
Description: "The Alpha-ID is a sequential and stable identification number, which is allocated to each entry in the alphabetical index. It permits the encoding of medical and natural language diagnostic terms."
* insert alphaid-se-babelfsh-2018ff("2024", "1.2.276.0.76.5.538", "./input-files/Alpha-ID/alphaidse2024/icd10gm2024_alphaidse_edvtxt_20230929.txt")
```

The CodeSystem converted here is a specific German terminology that serves as a non-semantic index for ICD-10-GM codes (
each code in Alpha-ID-SE is referenced to at least one ICD-10-GM code, which are quasi-synonyms in terms of the
ICD-10-GM classification). The input file is a CSV-like file with a specific delimiter (`|`) and character encoding. The
plugin used here is, however, the general-purpose CSV plugin, which is capable of parsing any CSV-like file. The plugin
defines a number of "command-line parameters" (literally! The plugin uses a command line parser to parse the
parameters), which define its operations. The plugin is then invoked by the `^babelfsh` directive in the FSH file,
which, to be compatible with FSH, is written as a comment.

Also consider the use of RuleSets, which dramatically simplify the re-use of metadata that doesn't change. In the
example linked [here](examples/bfarm-terminology-for-su-termserv-with-rules/alphaid-se.babelfsh.fsh), the metadata is
reused to define ten CodeSystems using slight variations due to changes in the input file formats, but each CodeSystem
FSH item only requires 5 lines of code.

This process is illustrated by this diagram:

![BabelFSH infrastructure diagram](docs/babelfsh-codesystem.png)